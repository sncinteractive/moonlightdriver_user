<?php 
	header("Content-Type:text/html; charset=utf-8");
	
	require_once ("inc/config.php");
	
	if(!isset($_SNC_ERROR)) {
		moveToSpecificPage(CONF_URL_ROOT);
		exit;
	}
	
	debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
?>

<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CONF_SITE_TITLE . " > ERROR"; ?></title>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/metisMenu.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/sb-admin-2.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/font-awesome.min.css">
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/main.css?<?php echo time();?>">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/metisMenu.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/sb-admin-2.js"></script>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<div class="login-panel panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">ERROR!</h3></div>
						<div class="panel-body">
							<form role="form">
								<fieldset>
									<h3><?php echo $_SNC_ERROR["message"]; ?></h3>
								</fieldset>
								<fieldset>
									<a href="<?php echo CONF_URL_ROOT; ?>" class="btn btn-lg btn-success btn-block" id="btnLogin">홈으로 돌아가기.</a>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

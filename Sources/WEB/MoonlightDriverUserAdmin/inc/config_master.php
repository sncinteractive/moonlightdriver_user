<?php
	/**
	 * URL
	 */
	define('CONF_URL_INDEX',			CONF_URL_ROOT . "index.php");
	define('CONF_URL_ERROR',			CONF_URL_ROOT . "error.php");
	define('CONF_URL_AJAX',				CONF_URL_ROOT . "ajax/ajax.php");
	
	define('CONF_URL_MENU',				CONF_PATH_ROOT . "menu.php");
	define('CONF_URL_FOOTER',			CONF_PATH_ROOT . "footer.php");
	
	define('CONF_URL_LOGIN_ROOT',		CONF_URL_ROOT . "login/");
	define('CONF_URL_LOGIN',			CONF_URL_LOGIN_ROOT . "login.php");
	define('CONF_URL_LOGOUT',			CONF_URL_LOGIN_ROOT . "logout.php");
	define('CONF_URL_LOGIN_ACTION',		CONF_URL_LOGIN_ROOT . "login_action.php");
	
	define('CONF_URL_PAGE_ROOT',			CONF_URL_ROOT . "pages/");
	define('CONF_URL_USER_MANAGEMENT',		CONF_URL_ROOT . "pages/user_management.php");
	define('CONF_URL_USER_POS_MANAGEMENT',		CONF_URL_ROOT . "pages/user_pos_management.php");
	define('CONF_URL_CALL_MANAGEMENT',		CONF_URL_ROOT . "pages/call_management.php");
	define('CONF_URL_CRACKDOWN_MANAGEMENT',	CONF_URL_ROOT . "pages/crackdown_management.php");
	define('CONF_URL_COMPANY_MANAGEMENT',	CONF_URL_ROOT . "pages/company_management.php");
	define('CONF_URL_ACCOUNT_MANAGEMENT',	CONF_URL_ROOT . "pages/account_management.php");
	define('CONF_URL_NOTICE_MANAGEMENT',	CONF_URL_ROOT . "pages/notice_management.php");
	define('CONF_URL_CRACKDOWN_RANK_MANAGEMENT',	CONF_URL_ROOT . "pages/crackdown_rank_management.php");
	define('CONF_URL_CRACKDOWN_BLACKLIST_MANAGEMENT',	CONF_URL_ROOT . "pages/crackdown_blacklist_management.php");
	define('CONF_URL_COUPON_MANAGEMENT',	CONF_URL_ROOT . "pages/coupon_management.php");
	define('CONF_URL_DRIVER_MANAGEMENT',		CONF_URL_ROOT . "pages/driver_management.php");
	
	define('CONF_URL_FILE_NOTICE',			CONF_URL_FILE_ROOT . "notice/");
	define('CONF_PATH_FILE_NOTICE',			CONF_PATH_FILE_ROOT . "notice/");
	define('CONF_URL_FILE_PUSH',			CONF_URL_FILE_ROOT . "push/");
	define('CONF_PATH_FILE_PUSH',			CONF_PATH_FILE_ROOT . "push/");
	define('CONF_URL_FILE_COUPON',			CONF_URL_FILE_ROOT . "coupon/");
	define('CONF_PATH_FILE_COUPON',			CONF_PATH_FILE_ROOT . "coupon/");
	
	define("GOOGLE_API_KEY", "AIzaSyDYQVSDIL8OgaS_n7X3QRgzo1f8I0ahVm0");
	define("GOOGLE_SERVER_KEY", "AIzaSyDb5DTLNl-uMS820Bozj5S1AVohg1g_uHY");
// 	define("TMAP_KEY", "013703a7-d144-36c8-a049-9dafe4e4417b");
	define("TMAP_KEY", "22bd40b6-125d-48ca-bdc1-3f26155f4e8b");
	
	define('EXCEL_EXPORT_PASSWORD',	"050727d729cd6b7e94d1f57de88fdaf6");
	define('EXCEL_EXPORT_FILENAME',	"User");
	
	define("INSURANCE_API_KEY", "C99F059C-2672-494C-8A33-BE8A32108F51");
	define("INSURANCE_CRYPT_KEY", "B48AC0F1AE1E4D86ACCC948E491057D6");
	
	define('MAX_PUS_MESSAGE_LENGTH',	4000);
	
	define("CONF_SITE_TITLE", "카대리 :: 관리자 페이지");
	
	$ROLE = array(
		1 => "admin",
		2 => "manager",
		3 => "employee"
	);
	
	$ROLE_LEVEL = array(
		"account" => array("inquiry" => 1, "create" => 1, "modify" => 1, "delete" => 1),
		"excel_export" => array("inquiry" => 1, "create" => 1, "modify" => 1, "delete" => 1),
		"user" => array("inquiry" => 2, "create" => 1, "modify" => 1, "delete" => 1),
		"user_pos" => array("inquiry" => 2, "create" => 1, "modify" => 1, "delete" => 1),
		"call" => array("inquiry" => 3, "create" => 3, "modify" => 3, "delete" => 3),
		"crackdown" => array("inquiry" => 3, "create" => 3, "modify" => 3, "delete" => 3),
		"company" => array("inquiry" => 3, "create" => 3, "modify" => 3, "delete" => 3),
		"notice" => array("inquiry" => 3, "create" => 3, "modify" => 3, "delete" => 3),
		"crackdown_rank" => array("inquiry" => 3, "create" => 3, "modify" => 3, "delete" => 3),
		"crackdown_blacklist" => array("inquiry" => 3, "create" => 3, "modify" => 3, "delete" => 3),
		"coupon" => array("inquiry" => 3, "create" => 3, "modify" => 3, "delete" => 3),
		"driver" => array("inquiry" => 2, "create" => 1, "modify" => 1, "delete" => 1)
	);
	
	$REGION = array("전국콜", "서울경기", "강원도", "충청도", "경상도", "전라도", "제주도", "대구");
	
	$CALL_SATATUS = array(
		"wait" => "배차 대기",
		"catch" => "배차 완료",
		"start" => "운행 시작",
		"end" => "운행 종료",
		"cancel" => "운행 취소",
		"done" => "운행 완료"
	);
	
	// 휴대폰 인증
	define("OK_NAME_END_POINT_URL", "http://safe.ok-name.co.kr/KcbWebService/OkNameService");
	define("OK_NAME_END_POINT_URL_TEST", "http://tsafe.ok-name.co.kr:29080/KcbWebService/OkNameService");
	define("OK_NAME_COMMON_SVL_URL", "https://safe.ok-name.co.kr/CommonSvl");
	define("OK_NAME_COMMON_SVL_URL_TEST", "https://tsafe.ok-name.co.kr:2443/CommonSvl");
	define("OK_NAME_ID", "V34850000000");
?>
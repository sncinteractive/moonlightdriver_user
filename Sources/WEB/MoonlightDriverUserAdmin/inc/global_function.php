<?php
	/**
	 * require_once_classes(Array("CUtil"));
	 */
	function require_once_classes($_classNameArr) {
		foreach ($_classNameArr AS $className) {
			require_once(CONF_PATH_CLASS . $className . ".php");
		}
	}

	/**
	 * debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, "can not connect to memcached");
	 */
	function debug_mesg($_type, $_class, $_function, $_line, $_mesg) {
		if (!CONF_DEBUG) {
			return;
		}
	
		if ((strcmp($_type, "E") == 0) && !CONF_DEBUG_ERROR) {
			return;
		} else if ((strcmp($_type, "I")) && !CONF_DEBUG_INFO) {
			return;
		}
	
		$_mesg .= "@1-". whereCalled(1);
		$_mesg .= "@2-". whereCalled(2);
		$_mesg .= "@3-". whereCalled(3);
		
		if (CONF_DEBUG_DISPLAY) {
			printf("%s|%s|%s|%s|%d|%s<br/>\r\n", date("Y-m-d H:i:s"), $_type, $_class, $_function, $_line, substr($_mesg, 0, 51200));
		}
	
		if (CONF_DEBUG_SAVE) {
			$save_file = CONF_DEBUG_SAVE_FILE_ETC;
			if ($_type == "E") {
				$save_file = CONF_DEBUG_SAVE_FILE_ERROR;
			}
			
			$f = sprintf("%s%d_%s", CONF_PATH_DEBUG_FILE, date("YmdH"), $save_file);
	
			$is_new = true;
			if (file_exists($f)) {
				$is_new = false;
			}
	
			$fp = fopen($f, "a");
	
			if ($is_new) {
				chmod($f, 0666);
			}
	
			if ($fp) {
				fprintf($fp, "%s|%s|%s|%s|%d|%s\r\n", date("Y-m-d H:i:s"), $_type, $_class, $_function, $_line, substr($_mesg, 0, 51200));
				fclose($fp);
			}
		}
	}

	function whereCalled($_level = 1) {
		$trace = debug_backtrace();
		
		if (!isset($trace[$_level])) {
			return "nothing";
		}
			
		$file = $trace[$_level]["file"];
		$line = $trace[$_level]["line"];
		$object = isset($trace[$_level]["object"]) ? $trace[$_level]["object"] : null;
		
		$args = "";
		if ($trace[$_level]["args"]) {
			unSet($trace[$_level]["args"]["_query"]);
			$args = serialize($trace[$_level]["args"]);
		}
	
		if (is_object($object)) {
			$object = get_class($object);
		}
	
		return "Where called [" . $_level . "]: line " . $line . " of " . $object . " with args[" . $args . "] (in " . $file . ")";
	}

	function snc_return($_result = "OK", $_message = "SUCCESS", $_data = null) {
	    return json_encode(Array("result" => $_result, "message" => $_message, "data" => $_data));
	}
	
	function snc_error($_message) {
		global $_SNC_ERROR;
	
		$_SNC_ERROR = Array('message' => $_message);
	}
	
	function moveToSpecificPage($_url, $_redirect_url = null) {
		if(isset($_redirect_url) && !empty($_redirect_url)) {
			$_url = "?redirect=" . $_redirect_url;
		}
		
		header('Location: ' . filter_var($_url, FILTER_SANITIZE_URL));
	}
	
	function calculatePage($_page, $_total_count, $_page_item_count) {
		$endpage = (int)($_total_count / $_page_item_count);
			
		if(($_total_count % $_page_item_count) != 0) {
			$endpage++;
		}
			
		$start = ($_page * $_page_item_count) - $_page_item_count;
		$end = $start + $_page_item_count;
			
		if($start == (int)($_total_count / $_page_item_count)) {
			$end = ($start + $_total_count) % $_page_item_count;
		}
		
		return array("start_offset" => $start, "end_page" => $endpage);
	}

	function getCalculatedTimesPerMeter($_pointsList, $_totalTime) {
		$time_per_miter = DEFAULT_TIME_PER_METER;
		if(!empty($_totalTime)) {
			$total_distance = getTotalDistance($_pointsList);
// 			echo "" . $shuttle_id . " : " . $total_dist . "m / " . ($row["totalTime"] * 60) . "sec => " . (($row["totalTime"] * 60) / $total_dist) . "sec/m\n";
			$time_per_miter = (($_totalTime * 60) / $total_distance);
		}
		
		return $time_per_miter;
	}
	
	function getTotalDistance($_lists) {
		$tot_dist = 0;
		$prev_lat = null;
		$prev_lng = null;
		
		foreach($_lists as $r) {
			if($prev_lat != null) {
				$tot_dist += getDistance($prev_lat, $prev_lng, $r["locations"]["coordinates"][1], $r["locations"]["coordinates"][0]);
// 				echo "dist[(" . $prev_lat . ", " . $prev_lng . ") to (" . $r["lat"] . ", " . $r["lng"] . ")] : " . $tot_dist . "<br/>";
			}
				
			$prev_lat = $r["locations"]["coordinates"][1];
			$prev_lng = $r["locations"]["coordinates"][0];
		}
	
		return $tot_dist;
	}
	
	function getDistance($lat1, $lon1, $lat2, $lon2) {
		/* WGS84 stuff */
		$a = 6378137;
		$b = 6356752.3142;
		$f = 1/298.257223563;
		/* end of WGS84 stuff */
	
		$L = deg2rad($lon2-$lon1);
		$U1 = atan((1-$f) * tan(deg2rad($lat1)));
		$U2 = atan((1-$f) * tan(deg2rad($lat2)));
		$sinU1 = sin($U1);
		$cosU1 = cos($U1);
		$sinU2 = sin($U2);
		$cosU2 = cos($U2);
	
		$lambda = $L;
		$lambdaP = 2*pi();
		$iterLimit = 20;
		while ((abs($lambda-$lambdaP) > pow(10, -12)) && ($iterLimit-- > 0)) {
			$sinLambda = sin($lambda);
			$cosLambda = cos($lambda);
			$sinSigma = sqrt(($cosU2*$sinLambda) * ($cosU2*$sinLambda) + ($cosU1*$sinU2-$sinU1*$cosU2*$cosLambda) * ($cosU1*$sinU2-$sinU1*$cosU2*$cosLambda));
	
			if ($sinSigma == 0) {
				return 0;
			}
	
			$cosSigma   = $sinU1*$sinU2 + $cosU1*$cosU2*$cosLambda;
			$sigma      = atan2($sinSigma, $cosSigma);
			$sinAlpha   = $cosU1 * $cosU2 * $sinLambda / $sinSigma;
			$cosSqAlpha = 1 - $sinAlpha*$sinAlpha;
			$cos2SigmaM = $cosSigma - 2*$sinU1*$sinU2/$cosSqAlpha;
	
			if (is_nan($cos2SigmaM)) {
				$cos2SigmaM = 0;
			}
	
			$C = $f/16*$cosSqAlpha*(4+$f*(4-3*$cosSqAlpha));
			$lambdaP = $lambda;
			$lambda = $L + (1-$C) * $f * $sinAlpha *($sigma + $C*$sinSigma*($cos2SigmaM+$C*$cosSigma*(-1+2*$cos2SigmaM*$cos2SigmaM)));
		}
	
		if ($iterLimit == 0) {
			// formula failed to converge
			return NaN;
		}
	
		$uSq = $cosSqAlpha * ($a*$a - $b*$b) / ($b*$b);
		$A = 1 + $uSq/16384*(4096+$uSq*(-768+$uSq*(320-175*$uSq)));
		$B = $uSq/1024 * (256+$uSq*(-128+$uSq*(74-47*$uSq)));
		$deltaSigma = $B*$sinSigma*($cos2SigmaM+$B/4*($cosSigma*(-1+2*$cos2SigmaM*$cos2SigmaM)- $B/6*$cos2SigmaM*(-3+4*$sinSigma*$sinSigma)*(-3+4*$cos2SigmaM*$cos2SigmaM)));
	
		// 		return round($b*$A*($sigma-$deltaSigma) / 1000);
		return round($b*$A*($sigma-$deltaSigma));	// m단위로 표시
	
	
		/* sphere way */
		$distance = rad2deg(acos(sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($lon1 - $lon2))));
	
		$distance *= 111.18957696; // Convert to km
	
		return $distance;
	}
	
	function convertShuttleTime($_orgTime) {
		if(!isset($_orgTime) || empty($_orgTime)) {
			return "";
		}
		
		$timeArray = explode(":", $_orgTime);
		$timeHours = intval($timeArray[0]);
		
		if($timeHours < 6) {
			$timeHours = $timeHours + 24;
			if($timeHours < 10) {
				$timeHours = "0" . $timeHours;
			}
		} else if($timeHours >= 24) {
			$timeHours = $timeHours - 24;
			if($timeHours < 10) {
				$timeHours = "0" . $timeHours;
			}
		}
		
		$convertedTime = $timeHours . ":" . $timeArray[1];
		
		return $convertedTime;
	}
	
	function startsWith($haystack, $needle) {
		// search backwards starting from haystack length characters from the end
		return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
	}
	
	function endsWith($haystack, $needle) {
		// search forward starting from end minus needle length characters
		return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
	}
	
	function convertPhoneNumber($_phone) {
		if(startsWith($_phone, "+82")) {
			return "0" . substr($_phone, 3);
		} else if(startsWith($_phone, "01")) {
			return "+82" . substr($_phone, 1);
		}
		
		return $_phone;
	}
	
	function generateScretRegistrant($_phone) {
		return substr_replace($_phone, "XX", 1, 2);
	}
	
	function getNextSequence($_counters, $_seq_name) {
		$retval = $_counters->findAndModify(
			array('_id' => $_seq_name),
			array('$inc' => array("seq" => 1)),
			null,
			array("new" => true)
		);
			
		return $retval['seq'];
	}
	
	function encrypt ($key, $value) {
		$padSize = 16 - (strlen ($value) % 16) ;
		$value = $value . str_repeat (chr ($padSize), $padSize) ;
		$output = mcrypt_encrypt (MCRYPT_RIJNDAEL_128, $key, $value, MCRYPT_MODE_CBC, str_repeat(chr(0),16)) ;
		return base64_encode ($output) ;
	}
	
	function decrypt ($key, $value) {
		$value = base64_decode ($value) ;
		$output = mcrypt_decrypt (MCRYPT_RIJNDAEL_128, $key, $value, MCRYPT_MODE_CBC, str_repeat(chr(0),16)) ;
	
		$valueLen = strlen ($output) ;
		if ( $valueLen % 16 > 0 )
			$output = "";
	
			$padSize = ord ($output{$valueLen - 1}) ;
			if ( ($padSize < 1) or ($padSize > 16) )
				$output = "";                // Check padding.
	
				for ($i = 0; $i < $padSize; $i++)
				{
					if ( ord ($output{$valueLen - $i - 1}) != $padSize )
						$output = "";
				}
				$output = substr ($output, 0, $valueLen - $padSize) ;
	
				return $output;
	}
?>

<?php 
	class CUserPointHistManager {
		var $useradditionals;
		var $userpointhists;
		
		var $UserManager;
		
		function CUserPointHistManager($_useradditionals, $_userpointhists) {
			$this->useradditionals = $_useradditionals;
			$this->userpointhists = $_userpointhists;
		}
		
		function getUserManagerClass($_users) {
			if ($this->UserManager) {
				return;
			}
		
			require_once_classes(Array('CUserManager'));
			$this->UserManager = new CUserManager($_users);
		}
		
		function getTotalUserPointHistCount() {
			return $this->userpointhists->count();
		}
		
		function getTotalFilteredUserPointHistCount($_find) {
			return $this->userpointhists->count($_find);
		}
		
		function getUserPointHistList($_find, $_start, $_length, $_sort) {
			$userPointHistList = $this->userpointhists->find($_find)->sort($_sort)->skip($_start)->limit($_length);
		
			$retUserPointHistList = array();
			foreach (userPointHistList as $row) {
				$rowArray = array();
		
				$userPointHistId = strval($row["_id"]);
				$rowArray["userPointHistId"] = $userPointHistId;
				$rowArray["userId"] = isset($row["userId"]) ? $row["userId"] : "";
				$rowArray["point"] = isset($row["point"]) ? $row["point"] : 0;
				$rowArray["beforePoint"] = isset($row["beforePoint"]) ? $row["beforePoint"] : 0;
				$rowArray["afterPoint"] = isset($row["afterPoint"]) ? $row["afterPoint"] : 0;
				$rowArray["pointMessage"] = isset($row["pointMessage"]) ? $row["pointMessage"] : "";
				$rowArray["createdDate"] = isset($row["createdDate"]) ? date("Y-m-d H:i:s", ($row["createdDate"] / 1000)) : "";
				
				$retUserPointHistList[] = $rowArray;
			}
				
			return $retUserPointHistList;
		}
		
		function getUserPointHistInfo($_userPointHistId) {
			$rerUserPointHistInfo = array();
			$userPointHistInfo = $this->userpointhists->findOne(array('_id' => new MongoId($_pointId)));
			
			if(isset($userPointHistInfo)) {
				$userPointHistId = strval($userPointHistInfo["_id"]);
			
				$rerUserPointHistInfo["userPointHistId"] = $userPointHistId;
				$rerUserPointHistInfo["userId"] = isset($userPointHistInfo["userId"]) ? $userPointHistInfo["userId"] : "";
				$rerUserPointHistInfo["point"] = isset($userPointHistInfo["point"]) ? $userPointHistInfo["point"] : 0;
				$rerUserPointHistInfo["beforePoint"] = isset($userPointHistInfo["beforePoint"]) ? $userPointHistInfo["beforePoint"] : 0;
				$rerUserPointHistInfo["afterPoint"] = isset($userPointHistInfo["afterPoint"]) ? $userPointHistInfo["afterPoint"] : 0;
				$rerUserPointHistInfo["pointMessage"] = isset($userPointHistInfo["pointMessage"]) ? $userPointHistInfo["pointMessage"] : "";
				$rerUserPointHistInfo["createdDate"] = isset($userPointHistInfo["createdDate"]) ? date("Y-m-d H:i:s", ($userPointHistInfo["createdDate"] / 1000)) : "";
			}
			
			return $rerUserPointHistInfo;
		}
		
		function addUserPoint($_users, $_point, $_pointMessage, $_recvUserType, $_recvUser) {
			$_point = intval($_point);
			
			$this->getUserManagerClass($_users);
				
			$userList = $this->UserManager->getUserListByRecvUserType($_recvUserType, $_recvUser, 0, 0);
				
			$updateData = array(
				'lastUpdateDate' => new MongoDate()
			);
			
			$incData = array(
				'point' => $_point
			);
			
			foreach ($userList as $row) {
				$userId = strval($row["_id"]);
				
				$userAdditionalInfo = $this->useradditionals->findOne(array('userId' => $userId));
				$userAdditionalPoint = isset($userAdditionalInfo["point"]) ? intval($userAdditionalInfo["point"]) : 0;
				
				$ret = $this->useradditionals->update(array('userId' => $userId), array('$set' => $updateData, '$inc' => $incData));
			
				$newData = array(
					'userId' => $userId,
					'point' => $_point,
					'beforePoint' => $userAdditionalPoint,
					'afterPoint' => $userAdditionalPoint + $_point,
					'pointMessage' => $_pointMessage,
					'createdDate' => floatval(round(microtime(true) * 1000))
				);
				
				$ret = $this->userpointhists->insert($newData);
			}
		}
	}
?>
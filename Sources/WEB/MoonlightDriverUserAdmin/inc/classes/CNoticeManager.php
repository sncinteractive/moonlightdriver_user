<?php 
	class CNoticeManager {
		var $usernotices;
		
		function CNoticeManager($_usernotices) {
			$this->usernotices = $_usernotices;
		}
		
		function getTotalNoticeCount() {
			return $this->usernotices->count();
		}
		
		function getTotalFilteredNoticeCount($_find) {
			return $this->usernotices->count($_find);
		}
		
		function getNoticeList($_find, $_start, $_length, $_sort) {
			$notice_list = $this->usernotices->find($_find)->sort($_sort)->skip($_start)->limit($_length);
				
			$ret_notice_list = array();
			foreach ($notice_list as $row) {
				$rowArray = array();
				
				$rowArray['notice_id'] = strval($row['_id']);
				$rowArray['seq'] = $row['seq'];
				$rowArray['title'] = $row['title'];
				$rowArray['contents'] = "<div class='line-ellipsis'>" . $row['contents'] . "</div>";
				$rowArray['imagePath'] = $row['imagePath'];
				$rowArray['orgImagePath'] = $row['orgImagePath'];				
				$rowArray['linkUrl'] = $row['linkUrl'];
				$rowArray['startDate'] = date("Y-m-d H:i:s", ($row["startDate"] / 1000));
				$rowArray['endDate'] = date("Y-m-d H:i:s", ($row["endDate"] / 1000));
				$rowArray['createdDate'] = date("Y-m-d H:i:s", ($row["createdDate"] / 1000));
				$rowArray['deletion'] = "<button type='button' class='btn btn-danger btn-sm btn_delete_notice'>삭제</button>";
		
				$ret_notice_list[] = $rowArray;
			}
			
			return $ret_notice_list;
		}
		
		function getNoticeInfo($_notice_id) {
			$ret_notice_info = array();
			$notice_info = $this->usernotices->findOne(array('_id' => new MongoId($_notice_id)));
				
			if(isset($notice_info)) {
				$ret_notice_info['notice_id'] = strval($notice_info['_id']);
				$ret_notice_info['seq'] = $notice_info['seq'];
				$ret_notice_info['title'] = $notice_info['title'];
				$ret_notice_info['contents'] = $notice_info['contents'];
				$ret_notice_info['imagePath'] = $notice_info['imagePath'];
				$ret_notice_info['orgImagePath'] = $notice_info['orgImagePath'];
				$ret_notice_info['linkUrl'] = $notice_info['linkUrl'];
				$ret_notice_info['startDate'] = date("Y-m-d H:i:s", ($notice_info["startDate"] / 1000));
				$ret_notice_info['endDate'] = date("Y-m-d H:i:s", ($notice_info["endDate"] / 1000));
				$ret_notice_info['createdDate'] = date("Y-m-d H:i:s", ($notice_info["createdDate"] / 1000));
			}
				
			return $ret_notice_info;
		}

		function addNotice($_title, $_contents, $_imagePath, $_orgImagePath, $_linkUrl, $_startDate, $_endDate) {
			$noticeSeqArray = $this->usernotices->find()->sort(array('seq' => -1))->limit(1);
			
			$noticeSeq = 1;
			foreach ($noticeSeqArray as $row) {
				$noticeSeq = intval($row['seq']) + 1;				
			}
			
			$startDate = strtotime($_startDate) * 1000;
			$endDate = strtotime($_endDate) * 1000;
			
			$newData = array(
				'seq' => $noticeSeq,
				'title' => $_title,
				'contents' => $_contents,
				'imagePath' => $_imagePath,
				'orgImagePath' => $_orgImagePath,
				'linkUrl' => $_linkUrl,
				'startDate' => $startDate,
				'endDate' => $endDate,
				'createdDate' => floatval(round(microtime(true) * 1000)),
				'updatedDate' => floatval(round(microtime(true) * 1000))
			);
			
			$ret = $this->usernotices->insert($newData);
				
			return $ret;
		}
		
		function updateNotice($_notice_id, $_title, $_contents, $_imagePath, $_orgImagePath, $_linkUrl, $_startDate, $_endDate) {
			$startDate = strtotime($_startDate) * 1000;
			$endDate = strtotime($_endDate) * 1000;
			
			$updateData = array(
				'title' => $_title,
				'contents' => $_contents,
				'linkUrl' => $_linkUrl,
				'startDate' => $startDate,
				'endDate' => $endDate,
				'updatedDate' => floatval(round(microtime(true) * 1000))
			);
			
			if(!empty($_imagePath)) {
				$updateData["imagePath"] = $_imagePath;
				$updateData["orgImagePath"] = $_orgImagePath;
			}
			
			$ret = $this->usernotices->update(array('_id' => new MongoId($_notice_id)), array('$set' => $updateData));
				
			return $ret;
		}
		
		function deleteNotice($_notice_id) {
			$this->usernotices->remove(array('_id' => new MongoId($_notice_id)));
		}
	}
?>
<?php 
	class CCompanyManager {
		function CCompanyManager() {}
		
		function getCompanyList($_chauffeurcompanies, $_find, $_start, $_length, $_sort) {
			$company_list = $_chauffeurcompanies->find($_find)->sort($_sort)->skip($_start)->limit($_length);
				
			$ret_company_list = array();
			foreach ($company_list as $row) {
				$rowArray = array();
				
				$company_id = strval($row['_id']);
				$rowArray['company_id'] = $company_id;
				$rowArray['companyName'] = $row['companyName'];
				$rowArray['telNumber'] = $row['telNumber'];
				$rowArray['region'] = $row['region'];
				$rowArray['price'] = $row['price'];
				$rowArray['stopByFee'] = $row['stopByFee'];
				$rowArray['deletion'] = "<button type='button' class='btn btn-danger btn-sm btn_delete_company'>삭제</button>";
		
				$ret_company_list[] = $rowArray;
			}
			
			return $ret_company_list;
		}
		
		function getCompanyInfo($_chauffeurcompanies, $_company_id) {
			$ret_company_info = array();
			$company_info = $_chauffeurcompanies->findOne(array('_id' => new MongoId($_company_id)));
				
			if(isset($company_info)) {
				$company_id = strval($company_info['_id']);
				
				$ret_company_info['company_id'] = $company_id;
				$ret_company_info['companyName'] = $company_info['companyName'];
				$ret_company_info['telNumber'] = $company_info['telNumber'];
				$ret_company_info['region'] = $company_info['region'];
				$ret_company_info['price'] = $company_info['price'];
				$ret_company_info['stopByFee'] = $company_info['stopByFee'];
			}
				
			return $ret_company_info;
		}

		function addCompany($_chauffeurcompanies, $_companyName, $_telNumber, $_region, $_price, $_stopByFee) {
			$companyPkArray = $_chauffeurcompanies->find()->sort(array('companyPK' => -1))->limit(1);
			
			$companyPk = 1;
			foreach ($companyPkArray as $row) {
				$companyPk = intval($row['companyPK']) + 1;				
			}
			
			$newData = array(
				'companyPK' => $companyPk,
				'companyName' => $_companyName,
				'telNumber' => $_telNumber,
				'region' => $_region,
				'price' => intval($_price),
				'stopByFee' => intval($_stopByFee),
				'useCount' => 0,
				'createdDate' => floatval(round(microtime(true) * 1000)),
				'updatedDate' => floatval(round(microtime(true) * 1000))
			);
			
			$ret = $_chauffeurcompanies->insert($newData);
				
			return $ret;
		}
		
		function updateCompany($_chauffeurcompanies, $_company_id, $_companyName, $_telNumber, $_region, $_price, $_stopByFee) {
			$updateData = array(
				'companyName' => $_companyName,
				'telNumber' => $_telNumber,
				'region' => $_region,
				'price' => intval($_price),
				'stopByFee' => intval($_stopByFee),
				'updatedDate' => floatval(round(microtime(true) * 1000))
			);
			
			$ret = $_chauffeurcompanies->update(array('_id' => new MongoId($_company_id)), array('$set' => $updateData));
				
			return $ret;
		}
		
		function deleteCompany($_chauffeurcompanies, $_company_id) {
			$_chauffeurcompanies->remove(array('_id' => new MongoId($_company_id)));
		}
	}
?>
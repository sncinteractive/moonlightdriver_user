<?php 
	class CCrackdownRankWeeklyManager {
		var $crackdownrankweeklies;
		
		function CCrackdownRankWeeklyManager($_crackdownrankweeklies) {
			$this->crackdownrankweeklies = $_crackdownrankweeklies;
		}
		
		function getTotalCrackdownRankWeeklyCount() {
			return $this->crackdownrankweeklies->count();
		}
		
		function getTotalFilteredCrackdownRankWeeklyCount($_find) {
			return $this->crackdownrankweeklies->count($_find);
		}
		
		function getCrackdownRankWeeklyList($_find, $_start, $_length, $_sort) {
			$crackdown_rank_weekly_list = $this->crackdownrankweeklies->find($_find)->sort($_sort)->skip($_start)->limit($_length);
				
			$ret_crackdown_rank_weekly_list = array();
			foreach ($crackdown_rank_weekly_list as $row) {
				$rowArray = array();
				
				$rowArray['crackdown_rank_id'] = strval($row['_id']);
				$rowArray['month'] = 0;
				$rowArray['weekOfYear'] = $row["weekOfYear"] . "주 (" . $row["weekStartDate"] . ")";
				$rowArray['phone'] = $row['phone'];
				$rowArray['normalCount'] = $row['normalCount'] . "회 / " . ($row['normalCount'] * 1) . "점";
				$rowArray['voiceCount'] = $row['voiceCount'] . "회 / " . ($row['voiceCount'] * 1) . "점";
				$rowArray['photoCount'] = $row['photoCount'] . "회 / " . ($row['photoCount'] * 2) . "점";
				$rowArray['totalCount'] = $row['totalCount'];
				$rowArray['point'] = $row['point'];
				$rowArray['deletion'] = "<button type='button' class='btn btn-danger btn-sm btn_delete_crackdown_rank_weekly'>삭제</button>";
		
				$ret_crackdown_rank_weekly_list[] = $rowArray;
			}
			
			return $ret_crackdown_rank_weekly_list;
		}
		
		function getCrackdownRankWeeklyInfo($_crackdown_rank_weekly_id) {
			$ret_info = array();
			$crackdown_rank_weekly_info = $this->crackdownrankweeklies->findOne(array('_id' => new MongoId($_crackdown_rank_weekly_id)));
				
			if(isset($crackdown_rank_weekly_info)) {
				$ret_info['crackdown_rank_weekly_id'] = strval($crackdown_rank_weekly_info['_id']);
				$rowArray['month'] = 0;
				$ret_info['userId'] = $crackdown_rank_weekly_info['userId'];
				$ret_info['registrant'] = $crackdown_rank_weekly_info['registrant'];
				$ret_info['weekStartDate'] = $crackdown_rank_weekly_info["weekStartDate"];
				$ret_info['weekOfYear'] = $crackdown_rank_weekly_info["weekOfYear"];
				$ret_info['phone'] = $crackdown_rank_weekly_info['phone'];
				$ret_info['normalCount'] = $crackdown_rank_weekly_info['normalCount'];
				$ret_info['voiceCount'] = $crackdown_rank_weekly_info['voiceCount'];
				$ret_info['photoCount'] = $crackdown_rank_weekly_info['photoCount'];
				$ret_info['totalCount'] = $crackdown_rank_weekly_info['totalCount'];
				$ret_info['point'] = $crackdown_rank_weekly_info['point'];
				$ret_info['createTime'] = date("Y-m-d H:i:s", ($crackdown_rank_weekly_info["createTime"] / 1000));
				$ret_info['updateTime'] = date("Y-m-d H:i:s", ($crackdown_rank_weekly_info["updateTime"] / 1000));
			}
				
			return $ret_info;
		}

		function addCrackdownRankWeekly($_weekStartDate, $_weekOfYear, $_userId, $_phone, $_photoCount, $_normalCount, $_voiceCount) {
			$point = ($_photoCount * 2) + ($_normalCount * 1) + ($_voiceCount * 1);
			$totalCount = $_photoCount + $_normalCount + $_voiceCount;
			
			$newData = array(
				'weekStartDate' => $_weekStartDate,
				'weekOfYear' => weekOfYear,
				'userId' => $_userId,
				'registrant' => generateScretRegistrant($_phone),
				'phone' => $_phone,
				'photoCount' => $_photoCount,
				'normalCount' => $_normalCount,
				'voiceCount' => $_voiceCount,
				'totalCount' => $totalCount,
				'point' => $point,
				'createTime' => floatval(round(microtime(true) * 1000)),
				'updateTime' => floatval(round(microtime(true) * 1000))
			);
			
			$ret = $this->crackdownrankweeklies->insert($newData);
				
			return $ret;
		}
		
		function updateCrackdownRankWeekly($_crackdown_rank_weekly_id, $_weekStartDate, $_weekOfYear, $_userId, $_phone, $_photoCount, $_normalCount, $_voiceCount) {
			$point = ($_photoCount * 2) + ($_normalCount * 1) + ($_voiceCount * 1);
			$totalCount = $_photoCount + $_normalCount + $_voiceCount;
			
			$newData = array(
				'weekStartDate' => $_weekStartDate,
				'weekOfYear' => weekOfYear,
				'userId' => $_userId,
				'registrant' => generateScretRegistrant($_phone),
				'phone' => $_phone,
				'photoCount' => $_photoCount,
				'normalCount' => $_normalCount,
				'voiceCount' => $_voiceCount,
				'totalCount' => $totalCount,
				'point' => $point,
				'updateTime' => floatval(round(microtime(true) * 1000))
			);
			
			$ret = $this->crackdownrankweeklies->update(array('_id' => new MongoId($_crackdown_rank_weekly_id)), array('$set' => $updateData));
				
			return $ret;
		}
		
		function deleteCrackdownRankWeekly($_crackdown_rank_weekly_id) {
			$this->crackdownrankweeklies->remove(array('_id' => new MongoId($_crackdown_rank_weekly_id)));
		}
	}
?>
<?php 
	class CCallManager {
		var $calls;
		var $UserManager;
		
		function CCallManager($_calls) {
			$this->calls = $_calls;
		}
		
		function getUserManagerClass($_users) {
			if ($this->UserManager) {
				return;
			}
		
			require_once_classes(Array('CUserManager'));
			$this->UserManager = new CUserManager($_users);
		}
		
		function getTotalCallCount() {
			return $this->calls->count();
		}
		
		function getTotalFilteredCallCount($_find) {
			return $this->calls->count($_find);
		}
		
		function getCallList($_find, $_start, $_length, $_sort) {
			$call_list = $this->calls->find($_find)->sort($_sort)->skip($_start)->limit($_length);
				
			global $CALL_SATATUS;
			
			$ret_call_list = array();
			foreach ($call_list as $row) {
				$rowArray = array();
				
				$rowArray['callId'] = strval($row['_id']);
				$rowArray['callType'] = $row['callType'];
				$rowArray['money'] = isset($row['money']) ? number_format($row['money']) : 0;
				$rowArray['paymentOption'] = $row['paymentOption'];
				$rowArray['paymentYn'] = $row['paymentYn'];
				$rowArray['departureTime'] = $row['departureTime'];
				$rowArray['estimateTime'] = $row['estimateTime'];
				$rowArray['etc'] = $row['etc'];
				$rowArray['useReliefMessage'] = $row['useReliefMessage'];
				$rowArray['evaluatedYN'] = $row['evaluatedYN'];
				$rowArray['reportedUnfairnessYN'] = $row['reportedUnfairnessYN'];
				$rowArray['apiDrivingId'] = $row['apiDrivingId'];
				$rowArray['premiums'] = isset($row['premiums']) && !empty($row['premiums']) ? number_format($row['premiums']) : "";
				$rowArray['status'] = $CALL_SATATUS[$row['status']];
				$rowArray['updatedTime'] = isset($row['updatedTime']) && !empty($row['updatedTime']) ? date("Y-m-d H:i:s", ($row["updatedTime"] / 1000)) : "";
				$rowArray['createdTime'] = isset($row['createdTime']) && !empty($row['createdTime']) ? date("Y-m-d H:i:s", ($row["createdTime"] / 1000)) : "";
// 				$rowArray['driver'] = $row['driver'];
// 				$rowArray['user'] = $row['user'];
				$rowArray['userPhone'] = $row['user']["phone"];
				$rowArray['throughPosition'] = "";
				
				if(!empty($row['through']['address'])) {
					$rowArray['throughPosition'] = $row['through']['address'];
					
					$count = 0;
					if(!empty($row['through1']['address'])) {
						$count++;
					}
					
					if(!empty($row['through2']['address'])) {
						$count++;
					}
					
					$rowArray['throughPosition'] = $rowArray['throughPosition'] . "외 " . $count . "곳";
				}
				
				$rowArray['startPosition'] = $row['start']['address'];
				$rowArray['endPosition'] = $row['end']['address'];
				$rowArray['mapLocation'] = "<button type='button' class='btn btn-info btn-sm btn_map_location' data-callid='" . $rowArray['callId'] . "'>보기</button>";
				$rowArray['deletion'] = "<button type='button' class='btn btn-danger btn-sm btn_delete_call'>삭제</button>";
		
				$ret_call_list[] = $rowArray;
			}
			
			return $ret_call_list;
		}
		
		function getCallInfo($_callId) {
			$retCallInfo = array();
			$callInfo = $this->calls->findOne(array('_id' => new MongoId($_callId)));
				
			if(isset($callInfo)) {
				$retCallInfo['callId'] = strval($callInfo['_id']);
				$retCallInfo['callType'] = $callInfo['callType'];
				$retCallInfo['money'] = $callInfo['money'];
				$retCallInfo['paymentOption'] = $callInfo['paymentOption'];
				$retCallInfo['paymentYn'] = $callInfo['paymentYn'];
				$retCallInfo['departureTime'] = $callInfo['departureTime'];
				$retCallInfo['estimateTime'] = $callInfo['estimateTime'];
				$retCallInfo['etc'] = $callInfo['etc'];
				$retCallInfo['useReliefMessage'] = $callInfo['useReliefMessage'];
				$retCallInfo['evaluatedYN'] = $callInfo['evaluatedYN'];
				$retCallInfo['reportedUnfairnessYN'] = $callInfo['reportedUnfairnessYN'];
				$retCallInfo['apiDrivingId'] = $callInfo['apiDrivingId'];
				$retCallInfo['premiums'] = isset($row['premiums']) && !empty($row['premiums']) ? $row['premiums'] : "";
				$retCallInfo['status'] = $callInfo['status'];
				$retCallInfo['updatedTime'] = isset($callInfo['updatedTime']) && !empty($callInfo['updatedTime']) ? date("Y-m-d H:i:s", ($callInfo["updatedTime"] / 1000)) : "";
				$retCallInfo['createdTime'] = isset($callInfo['createdTime']) && !empty($callInfo['createdTime']) ? date("Y-m-d H:i:s", ($callInfo["createdTime"] / 1000)) : "";
				$retCallInfo['driver'] = $callInfo['driver'];
				$retCallInfo['user'] = $callInfo['user'];
				$retCallInfo['end'] = $callInfo['end'];
				$retCallInfo['through'] = $callInfo['through'];
				$retCallInfo['through1'] = $callInfo['through1'];
				$retCallInfo['through2'] = $callInfo['through2'];
				$retCallInfo['start'] = $callInfo['start'];
			}
				
			return $retCallInfo;
		}

		function addCall($_callSeq, $_callType, $_money, $_payment_option, $_etc, $_status, $_driver, $_user, $_start, $_end, $_through, $_through1, $_through2) {
			$newData = array(
// 				'callSeq' => $_callSeq,
				'callType' => $_callType,
				'money' => $_money,
				'paymentOption' => $_payment_option,
				'etc' => $_etc,
				'status' => $_status,
				'driver' => $_driver,
				'user' => $_user,
				'start' => $_start,
				'end' => $_end,
				'through' => $_through,
				'through1' => $_through1,
				'through2' => $_through2,
				'paymentYn' => "N",
				'departureTime' => 0,
				'estimateTime' => 0,
				'useReliefMessage' => "",
				'evaluatedYN' => "N",
				'reportedUnfairnessYN' => "N",
				'apiDrivingId' => "",
				'premiums' => "",
				'createdTime' => floatval(round(microtime(true) * 1000)),
				'updatedTime' => floatval(round(microtime(true) * 1000))
			);
			
			$ret = $this->calls->insert($newData);
				
			return $ret;
		}
		
		function updateCall($_call_id, $_callType, $_money, $_payment_option, $_etc, $_status, $_user, $_start, $_end, $_through) {
			$updateData = array(
				'callType' => $_callType,
				'money' => $_money,
				'payment_option' => $_payment_option,
				'etc' => $_etc,
				'status' => $_status,
				'user' => $_user,
				'start' => $_start,
				'end' => $_end,
				'through' => $_through,
				'updatedTime' => floatval(round(microtime(true) * 1000))
			);
			
			$ret = $this->calls->update(array('_id' => new MongoId($_call_id)), array('$set' => $updateData));
				
			return $ret;
		}
		
		function updateCallStatus($_call_id, $_status) {
			$updateData = array(
				'status' => $_status,
				'updatedTime' => floatval(round(microtime(true) * 1000))
			);
				
			$ret = $this->calls->update(array('_id' => new MongoId($_call_id)), array('$set' => $updateData));
		
			return $ret;
		}
		
		function deleteCall($_call_id) {
			$this->calls->remove(array('_id' => new MongoId($_call_id)));
		}
		
		function getPeaktimeList($_peaktimes) {
			$peaktimeList = $_peaktimes->find();
		
			$retPeaktimeList = array();
			foreach ($peaktimeList as $row) {
				$rowArray = array();
		
				$peaktimeId = strval($row["_id"]);
				$rowArray["peaktimeId"] = $peaktimeId;
				$rowArray["startDate"] = $row["startDate"];
				$rowArray["endDate"] = $row["endDate"];
				$rowArray["btnDelete"] = "<button type='button' class='btn btn-danger btn-sm btnDeletePeaktime'>삭제</button>";
		
				$retPeaktimeList[] = $rowArray;
			}
		
			return $retPeaktimeList;
		}
		
		function addPeaktime($_peaktimes, $_startDate, $_endDate) {
			$newData = array(
				'startDate' => $_startDate,
				'endDate' => $_endDate
			);
				
			$ret = $_peaktimes->insert($newData);
			if(isset($newData["_id"])) {
				$newData["peaktimeId"] = strval($newData["_id"]);
			}
			
			return $newData;
		}
		
		function deletePeaktime($_peaktimes, $_peaktimeId) {
			$_peaktimes->remove(array('_id' => new MongoId($_peaktimeId)));
		}
	}
?>
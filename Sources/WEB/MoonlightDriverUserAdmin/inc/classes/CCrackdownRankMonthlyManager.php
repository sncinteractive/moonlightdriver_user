<?php 
	class CCrackdownRankMonthlyManager {
		var $crackdownrankmonthlies;
		
		function CCrackdownRankMonthlyManager($_crackdownrankmonthlies) {
			$this->crackdownrankmonthlies = $_crackdownrankmonthlies;
		}
		
		function getTotalCrackdownRankMonthlyCount() {
			return $this->crackdownrankmonthlies->count();
		}
		
		function getTotalFilteredCrackdownRankMonthlyCount($_find) {
			return $this->crackdownrankmonthlies->count($_find);
		}
		
		function getCrackdownRankMonthlyList($_find, $_start, $_length, $_sort) {
			$crackdown_rank_monthly_list = $this->crackdownrankmonthlies->find($_find)->sort($_sort)->skip($_start)->limit($_length);
				
			$ret_crackdown_rank_monthly_list = array();
			foreach ($crackdown_rank_monthly_list as $row) {
				$rowArray = array();
				
				$rowArray['crackdown_rank_id'] = strval($row['_id']);
				$rowArray['month'] = $row["month"];
				$rowArray['weekOfYear'] = 0;
				$rowArray['phone'] = $row['phone'];
				$rowArray['normalCount'] = $row['normalCount'] . "회 / " . ($row['normalCount'] * 1) . "점";
				$rowArray['voiceCount'] = $row['voiceCount'] . "회 / " . ($row['voiceCount'] * 1) . "점";
				$rowArray['photoCount'] = $row['photoCount'] . "회 / " . ($row['photoCount'] * 2) . "점";
				$rowArray['totalCount'] = $row['totalCount'];
				$rowArray['point'] = $row['point'];
				$rowArray['deletion'] = "<button type='button' class='btn btn-danger btn-sm btn_delete_crackdown_rank_monthly'>삭제</button>";
		
				$ret_crackdown_rank_monthly_list[] = $rowArray;
			}
			
			return $ret_crackdown_rank_monthly_list;
		}
		
		function getCrackdownRankMonthlyInfo($_crackdown_rank_monthly_id) {
			$ret_info = array();
			$crackdown_rank_monthly_info = $this->crackdownrankmonthlies->findOne(array('_id' => new MongoId($_crackdown_rank_monthly_id)));
				
			if(isset($crackdown_rank_monthly_info)) {
				$ret_info['crackdown_rank_monthly_id'] = strval($crackdown_rank_monthly_info['_id']);
				$ret_info['month'] = $crackdown_rank_monthly_info["month"];
				$ret_info['userId'] = $crackdown_rank_monthly_info['userId'];
				$ret_info['registrant'] = $crackdown_rank_monthly_info['registrant'];
				$ret_info['weekOfYear'] = 0;
				$ret_info['phone'] = $crackdown_rank_monthly_info['phone'];
				$ret_info['normalCount'] = $crackdown_rank_monthly_info['normalCount'];
				$ret_info['voiceCount'] = $crackdown_rank_monthly_info['voiceCount'];
				$ret_info['photoCount'] = $crackdown_rank_monthly_info['photoCount'];
				$ret_info['totalCount'] = $crackdown_rank_monthly_info['totalCount'];
				$ret_info['point'] = $crackdown_rank_monthly_info['point'];
				$ret_info['createTime'] = date("Y-m-d H:i:s", ($crackdown_rank_monthly_info["createTime"] / 1000));
			}
				
			return $ret_info;
		}

		function addCrackdownRankMonthly($_month, $_userId, $_phone, $_photoCount, $_normalCount, $_voiceCount) {
			$point = ($_photoCount * 2) + ($_normalCount * 1) + ($_voiceCount * 1);
			$totalCount = $_photoCount + $_normalCount + $_voiceCount;
			
			$newData = array(
				'month' => $_month,
				'userId' => $_userId,
				'registrant' => generateScretRegistrant($_phone),
				'phone' => $_phone,
				'photoCount' => $_photoCount,
				'normalCount' => $_normalCount,
				'voiceCount' => $_voiceCount,
				'totalCount' => $totalCount,
				'point' => $point,
				'createTime' => floatval(round(microtime(true) * 1000)),
				'updateTime' => floatval(round(microtime(true) * 1000))
			);
			
			$ret = $this->crackdownrankmonthlies->insert($newData);
				
			return $ret;
		}
		
		function updateCrackdownRankMonthly($_crackdown_rank_monthly_id, $_month, $_userId, $_phone, $_photoCount, $_normalCount, $_voiceCount) {
			$point = ($_photoCount * 2) + ($_normalCount * 1) + ($_voiceCount * 1);
			$totalCount = $_photoCount + $_normalCount + $_voiceCount;
			
			$newData = array(
				'weekStartDate' => $_weekStartDate,
				'month' => $_month,
				'userId' => $_userId,
				'registrant' => generateScretRegistrant($_phone),
				'phone' => $_phone,
				'photoCount' => $_photoCount,
				'normalCount' => $_normalCount,
				'voiceCount' => $_voiceCount,
				'totalCount' => $totalCount,
				'point' => $point,
				'updateTime' => floatval(round(microtime(true) * 1000))
			);
			
			$ret = $this->crackdownrankmonthlies->update(array('_id' => new MongoId($_crackdown_rank_monthly_id)), array('$set' => $updateData));
				
			return $ret;
		}
		
		function deleteCrackdownRankMonthly($_crackdown_rank_monthly_id) {
			$this->crackdownrankmonthlies->remove(array('_id' => new MongoId($_crackdown_rank_monthly_id)));
		}
	}
?>
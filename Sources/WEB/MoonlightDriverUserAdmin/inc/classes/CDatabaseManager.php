<?php
	class CDatabaseManager {
		var $ip;
		var $port;
		var $database_name;
		var $user;
		var $pass;
		var $database = null;
		
		function CDatabaseManager($_alias = "DEF") {
			$this->ip = constant($_alias. '_MONGO_IP');
			
			if(!isset($this->ip)) {
				throw new Exception('Not found database IP address!');
				return;
			}
			
			$this->port = constant($_alias. '_MONGO_PORT');
			$this->database_name = constant($_alias. '_MONGO_DB');
			$this->user = constant($_alias. '_MONGO_USER');
			$this->pass = constant($_alias. '_MONGO_PASS');
			
			$this->createDb();
		}
		
		function createDb() {
			if(PRODUCTION_CODE == "DEV") {
				/**
				 *  로컬 세팅
				 */
				if(isset($this->user) && !empty($this->user) && isset($this->pass) && !empty($this->pass)) {
					$database = new MongoClient("mongodb://" . $this->user . ":" . $this->pass . "@" . $this->ip);				
				} else {
					$database = new MongoClient("mongodb://" . $this->ip);
				}
				
				$this->database = $database->dev_moonlight;
			} else if(PRODUCTION_CODE == "REAL") {
				/**
				 * 실서버 세팅
				 * @var unknown
				 */
				if(isset($this->user) && !empty($this->user) && isset($this->pass) && !empty($this->pass)) {
					$db_client = new MongoClient($this->ip, array("username" => $this->user, "password" => $this->pass , "db" => $this->database_name));
				} else {
					$db_client = new MongoClient($this->ip, array("db" => $this->database_name));
				}
				
				$this->database = $db_client->selectDB($this->database_name);
			}
		}
		
		function getDb() {
			if(!isset($this->database)) {
				$this->createDb();
			}
			
			return $this->database;
		}
	}
?>
<?php 
	class CDriverManager {
		function CDriverManager() {}
		
		function getDriverList($_cardaridrivers, $_find, $_start, $_length, $_sort) {
			$driver_list = $_cardaridrivers->find($_find)->sort($_sort)->skip($_start)->limit($_length);
				
			global $ROLE;
			
			$ret_driver_list = array();
			foreach ($driver_list as $row) {
				$rowArray = array();
				
				$driverId = strval($row["_id"]);
				$rowArray["driverId"] = $driverId;
				$rowArray["name"] = $row["name"];
				$rowArray["phone"] = $row["phone"];
				$rowArray["socialNumber"] = $row["socialNumber"];
				$rowArray["licenseType"] = $row["licenseType"];
				$rowArray["licenseNumber"] = $row["licenseNumber"];
				$rowArray["driverStatus"] = $row["driverStatus"];
				$rowArray["insuranceStatus"] = $row["insuranceStatus"];
				$rowArray["apiDriverId"] = $row["apiDriverId"];
				$rowArray["insureNumber"] = $row["insureNumber"];
				$rowArray["email"] = $row["email"];
				$rowArray["balance"] = "<button type='button' class='btn btn-info btn-sm btnBalance'>" . number_format($row["balance"]) . "</button>";
				$rowArray["lastLoginDate"] = isset($row["lastLoginDate"]) && !empty($row["lastLoginDate"]) ? date('Y-m-d H:i:s', $row["lastLoginDate"]->sec) : "";
				$rowArray["availableDate"] = isset($row["availableDate"]) && !empty($row["availableDate"]) ? date('Y-m-d H:i:s', $row["availableDate"]->sec) : "";
				$rowArray["createdDate"] = isset($row["createdDate"]) && !empty($row["createdDate"]) ? date('Y-m-d H:i:s', $row["createdDate"]->sec) : "";
				$rowArray["btnBlock"] = "";
				if($rowArray["driverStatus"] == "success") {
					$rowArray["btnBlock"] = "<button type='button' class='btn btn-danger btn-sm btnBlock'>중지</button>";
				} else if($rowArray["driverStatus"] == "block" && $rowArray["insuranceStatus"] == "success") {
					$rowArray["btnBlock"] = "<button type='button' class='btn btn-primary btn-sm btnSuccess'>사용</button>";
				} else {
					$rowArray["btnBlock"] = "사용 대기";
				}
				$rowArray["btnHistory"] = "<button type='button' class='btn btn-primary btn-sm btnHistory'>운행내역</button>";
		
				$ret_driver_list[] = $rowArray;
			}
			
			return $ret_driver_list;
		}
		
		function getDriverInfo($_cardaridrivers, $_driverId) {
			$ret_driver_info = array();
			$driver_info = $_cardaridrivers->findOne(array('_id' => new MongoId($_driverId)));
				
			if(isset($driver_info)) {
				$driverId = strval($driver_info["_id"]);
				$ret_driver_info["driverId"] = $driverId;
				$ret_driver_info["name"] = $driver_info["name"];
				$ret_driver_info["phone"] = $driver_info["phone"];
				$ret_driver_info["socialNumber"] = $driver_info["socialNumber"];
				$ret_driver_info["licenseType"] = $driver_info["licenseType"];
				$ret_driver_info["licenseNumber"] = $driver_info["licenseNumber"];
				$ret_driver_info["driverStatus"] = $driver_info["driverStatus"];
				$ret_driver_info["insuranceStatus"] = $driver_info["insuranceStatus"];
				$ret_driver_info["apiDriverId"] = $driver_info["apiDriverId"];
				$ret_driver_info["insureNumber"] = $driver_info["insureNumber"];
				$ret_driver_info["email"] = $driver_info["email"];
				$ret_driver_info["balance"] = intval($driver_info["balance"]);
				$ret_driver_info["lastLoginDate"] = isset($driver_info["lastLoginDate"]) && !empty($driver_info["lastLoginDate"]) ? date('Y-m-d H:i:s', $driver_info["lastLoginDate"]->sec) : "";
				$ret_driver_info["availableDate"] = isset($driver_info["availableDate"]) && !empty($driver_info["availableDate"]) ? date('Y-m-d H:i:s', $driver_info["availableDate"]->sec) : "";
				$ret_driver_info["createdDate"] = isset($driver_info["createdDate"]) && !empty($driver_info["createdDate"]) ? date('Y-m-d H:i:s', $driver_info["createdDate"]->sec) : "";
			}
				
			return $ret_driver_info;
		}
		
		function addDriver($_cardaridrivers, $_name, $_phone, $_socialNumber, $_licenseType, $_licenseNumber, $_email) {
			$newData = array(
				"name" => $_name,
				"phone" => $_phone,
				"phone1" => "",
				"socialNumber" => $_socialNumber,
				"licenseType" => $_licenseType,
				"licenseNumber" => $_licenseNumber,
				"driverStatus" => "wait",
				"insuranceStatus" => "wait",
				"apiDriverId" => "",
				"insureNumber" => "",
				"email" => $_email,
				"balance" => 0,
				"gcmId" => "",
				"version" => "",
				"image" => "",
				"ynLogin" => "N",
				"macAddr" => "",
				"lastLoginDate" => "",
				"starnum" => 0,
				"star" => 0,
				"point" => 0,
				"locations" => array(
					"type" => "Point",
					"coordinates" => array(0, 0)
				),
				"availableDate" => "",
				"createdDate" => new MongoDate(),
				"updatedDate" => new MongoDate()
			);
			
			$ret = $_cardaridrivers->insert($newData);
				
			if(isset($newData["_id"])) {
				$newData["driverId"] = strval($newData["_id"]);
				$newData["createdDate"] = isset($driver_info["createdDate"]) ? date('Y-m-d H:i:s', $driver_info["createdDate"]->sec) : "";
				$newData["updatedDate"] = isset($driver_info["updatedDate"]) ? date('Y-m-d H:i:s', $driver_info["updatedDate"]->sec) : "";
			}
				
			return $newData;
		}
		
		function updateDriver($_cardaridrivers, $_driverId, $_name, $_phone, $_socialNumber, $_licenseType, $_licenseNumber, $_email) {
			$updateData = array(
				'name' => $_name,
				'phone' => $_phone,
				'socialNumber' => $_socialNumber,
				'licenseType' => $_licenseType,
				'licenseNumber' => $_licenseNumber,
				'email' => $_email,
				'updatedDate' => new MongoDate()
			);
			
			$ret = $_cardaridrivers->update(array('_id' => new MongoId($_driverId)), array('$set' => $updateData));
				
			return $this->getDriverInfo($_cardaridrivers, $_driverId);
		}
		
		function updateDriverStatus($_cardaridrivers, $_driverId, $driverStatus) {
			$updateData = array(
				'driverStatus' => $driverStatus,
				'updatedDate' => new MongoDate()
			);
				
			$ret = $_cardaridrivers->update(array('_id' => new MongoId($_driverId)), array('$set' => $updateData));
			
			return $this->getDriverInfo($_cardaridrivers, $_driverId);
		}
		
		function getDriverBalanceList($_logdriverbalances, $_driverId) {
			$driverBalancelist = $_logdriverbalances->find(array("driverId" => $_driverId));
				
			$ret_driver_list = array();
			foreach ($driverBalancelist as $row) {
				$rowArray = array();
		
				$driverId = strval($row["_id"]);
				$rowArray["driverId"] = $driverId;
				$rowArray["callId"] = $row["callId"];
				$rowArray["beforeBalance"] = isset($row["beforeBalance"]) ? number_format($row["beforeBalance"]) : 0;
				$rowArray["afterBalance"] = isset($row["afterBalance"]) ? number_format($row["afterBalance"]) : 0;
				$rowArray["balance"] = isset($row["balance"]) ? number_format($row["balance"]) : 0;
				$rowArray["orgBalance"] = $row["balance"];
				$rowArray["reason"] = $row["reason"];
				$rowArray["createdTime"] = date("Y-m-d H:i:s", ($row["createdTime"] / 1000));
		
				$ret_driver_list[] = $rowArray;
			}
				
			return $ret_driver_list;
		}
		
		function updateDriverBalance($_cardaridrivers, $_logdriverbalances, $_driverId, $balance) {
			$driverInfo = $this->getDriverInfo($_cardaridrivers, $_driverId);
			
			$balance = intval($balance);
			
			$updateData = array(
				'balance' => $balance
			);
		
			$ret = $_cardaridrivers->update(array('_id' => new MongoId($_driverId)), array('$inc' => $updateData, '$set' => array("updatedDate" => new MongoDate())));
			if($ret["ok"] == 1) {
				$insertData = array(
					'driverId' => $_driverId,
					'callId' => '',
					'beforeBalance' => $driverInfo["balance"],
					'afterBalance' => $driverInfo["balance"] + $balance,
					'balance' => $balance,
					'reason' => "관리자 추가 적립",
					'createdTime' => floatval(round(microtime(true) * 1000)),
					'logType' => 'charge',
					'logDate' => new MongoDate()
				);
				
				$ret = $_logdriverbalances->insert($insertData);
			}
				
			return $ret;
		}
		
		function getDriverHistoryList($_calls, $_driverId) {
			$driverHistorylist = $_calls->find(array("driver.driverId" => $_driverId, "status" => array('$in' => array("end", "done"))));
		
			$ret_driver_list = array();
			foreach ($driverHistorylist as $row) {
				$rowArray = array();
		
				$callId = strval($row["_id"]);
				$rowArray["callId"] = $callId;
				$rowArray["callType"] = $row["callType"];
				$rowArray["money"] = isset($row["money"]) ? number_format($row["money"]) : 0;
				$rowArray["paymentOption"] = $row["paymentOption"];
				$rowArray["paymentYn"] = $row["paymentYn"];
				$rowArray["departureTime"] = $row["departureTime"];
				$rowArray["estimateTime"] = $row["estimateTime"];
				$rowArray["etc"] = $row["etc"];
				$rowArray["useReliefMessage"] = $row["useReliefMessage"];
				$rowArray["evaluatedYN"] = $row["evaluatedYN"];
				$rowArray["reportedUnfairnessYN"] = $row["reportedUnfairnessYN"];
				$rowArray["apiDrivingId"] = $row["apiDrivingId"];
				$rowArray['premiums'] = isset($row['premiums']) && !empty($row['premiums']) ? number_format($row['premiums']) : "0";
				$rowArray["status"] = $row["status"];
				$rowArray["updatedTime"] = date("Y-m-d H:i:s", ($row["updatedTime"] / 1000));
				$rowArray["createdTime"] = date("Y-m-d H:i:s", ($row["createdTime"] / 1000));
				$rowArray["driver"] = $row["driver"];
				$rowArray["user"] = $row["user"];
				$rowArray["start"] = $row["start"];
				$rowArray["end"] = $row["end"];
				$rowArray["through"] = $row["through"];
				$rowArray["through1"] = $row["through1"];
				$rowArray["through2"] = $row["through2"];
		
				$ret_driver_list[] = $rowArray;
			}
		
			return $ret_driver_list;
		}
	}
?>
<?php 
	class CCouponManager {
		var $coupons;
		var $couponpushes;
		var $usercoupons;
		
		var $UserManager;
		
		function CCouponManager($_coupons, $_couponpushes, $_usercoupons) {
			$this->coupons = $_coupons;
			$this->couponpushes = $_couponpushes;
			$this->usercoupons = $_usercoupons;
		}
		
		function getUserManagerClass($_users) {
			if ($this->UserManager) {
				return;
			}
		
			require_once_classes(Array('CUserManager'));
			$this->UserManager = new CUserManager($_users);
		}
		
		function getTotalCouponCount() {
			return $this->coupons->count();
		}
		
		function getTotalFilteredCouponCount($_find) {
			return $this->coupons->count($_find);
		}
		
		function getCouponList($_find, $_start, $_length, $_sort) {
			$couponList = $this->coupons->find($_find)->sort($_sort)->skip($_start)->limit($_length);
		
			$retCouponList = array();
			foreach ($couponList as $row) {
				$rowArray = array();
		
				$couponId = strval($row["_id"]);
				$rowArray["couponId"] = $couponId;
				$rowArray["couponName"] = isset($row["couponName"]) ? $row["couponName"] : "";
				$rowArray["couponDesc"] = isset($row["couponDesc"]) ? $row["couponDesc"] : "";
				$rowArray["couponImgPath"] = isset($row["couponImgPath"]) ? $row["couponImgPath"] : "";
				$rowArray["couponOrgImgPath"] = isset($row["couponOrgImgPath"]) ? $row["couponOrgImgPath"] : "";
				$rowArray["couponStartDate"] = isset($row["couponStartDate"]) ? $row["couponStartDate"] : "";
				$rowArray["couponEndDate"] = isset($row["couponEndDate"]) ? $row["couponEndDate"] : "";
				$rowArray["delYn"] = isset($row["delYn"]) ? $row["delYn"] : "N";
				$rowArray["createdDate"] = isset($row["createdDate"]) ? date("Y-m-d H:i:s", ($row["createdDate"] / 1000)) : "";
				
				if($rowArray["delYn"] == "N") {
					$rowArray["deletion"] = "<button type='button' class='btn btn-danger btn-sm btnDeleteCoupon'>삭제</button>";
				} else {
					$rowArray["deletion"] = "삭제";
				}
				
				$retCouponList[] = $rowArray;
			}
				
			return $retCouponList;
		}
		
		function getSelectCouponList() {
			$couponList = $this->coupons->find(array("couponEndDate" => array('$gt' => date("Y-m-d H:i:s"))))->sort(array("createdDate" => -1));
		
			$retCouponList = array();
			foreach ($couponList as $row) {
				$rowArray = array();
		
				$couponId = strval($row["_id"]);
				$rowArray["couponId"] = $couponId;
				$rowArray["couponName"] = isset($row["couponName"]) ? $row["couponName"] : "";
				
				$retCouponList[] = $rowArray;
			}
		
			return $retCouponList;
		}
		
		function getCouponInfo($_couponId) {
			$rerCouponInfo = array();
			$couponInfo = $this->coupons->findOne(array('_id' => new MongoId($_couponId)));
			
			if(isset($couponInfo)) {
				$couponId = strval($couponInfo["_id"]);
			
				$rerCouponInfo["couponId"] = $couponId;
				$rerCouponInfo["couponName"] = isset($couponInfo["couponName"]) ? $couponInfo["couponName"] : "";
				$rerCouponInfo["couponDesc"] = isset($couponInfo["couponDesc"]) ? $couponInfo["couponDesc"] : "";
				$rerCouponInfo["couponImgPath"] = isset($couponInfo["couponImgPath"]) ? $couponInfo["couponImgPath"] : "";
				$rerCouponInfo["couponOrgImgPath"] = isset($couponInfo["couponOrgImgPath"]) ? $couponInfo["couponOrgImgPath"] : "";
				$rerCouponInfo["couponStartDate"] = isset($couponInfo["couponStartDate"]) ? $couponInfo["couponStartDate"] : "";
				$rerCouponInfo["couponEndDate"] = isset($couponInfo["couponEndDate"]) ? $couponInfo["couponEndDate"] : "";
				$rerCouponInfo["delYn"] = isset($couponInfo["delYn"]) ? $couponInfo["delYn"] : "N";
				$rerCouponInfo["createdDate"] = isset($couponInfo["createdDate"]) ? date("Y-m-d H:i:s", ($couponInfo["createdDate"] / 1000)) : "";
				$rerCouponInfo["updatedDate"] = isset($couponInfo["updatedDate"]) ? date("Y-m-d H:i:s", ($couponInfo["updatedDate"] / 1000)) : "";
			}
			
			return $rerCouponInfo;
		}
		
		function addCoupon($_couponName, $_couponDesc, $_couponImgPath, $_couponOrgImgPath, $_couponStartDate, $_couponEndDate, $_delYn) {
			$newData = array(
				'couponName' => $_couponName,
				'couponDesc' => $_couponDesc,
				'couponImgPath' => $_couponImgPath,
				'couponOrgImgPath' => $_couponOrgImgPath,
				'couponStartDate' => $_couponStartDate,
				'couponEndDate' => $_couponEndDate,
				'delYn' => $_delYn,
				'createdDate' => floatval(round(microtime(true) * 1000)),
				'updatedDate' => floatval(round(microtime(true) * 1000))
			);
			
			$ret = $this->coupons->insert($newData);
				
			if(isset($newData["_id"])) {
				$newData["couponId"] = strval($newData["_id"]);
				$newData["createdDate"] = date("Y-m-d H:i:s", ($newData["createdDate"] / 1000));
				$newData["updatedDate"] = date("Y-m-d H:i:s", ($newData["updatedDate"] / 1000));
			}
				
			return $newData;
		}
		
		function updateCoupon($_couponId, $_couponName, $_couponDesc, $_couponImgPath, $_couponOrgImgPath, $_couponStartDate, $_couponEndDate) {
			$updateData = array(
				'couponName' => $_couponName,
				'couponDesc' => $_couponDesc,
				'couponImgPath' => $_couponImgPath,
				'couponOrgImgPath' => $_couponOrgImgPath,
				'couponStartDate' => $_couponStartDate,
				'couponEndDate' => $_couponEndDate,
				'updatedDate' => floatval(round(microtime(true) * 1000))
			);
			
			if(!empty($_couponOrgImgPath)) {
				$updateData["couponImgPath"] = $_couponImgPath;
				$updateData["couponOrgImgPath"] = $_couponOrgImgPath;
			}
				
			$ret = $this->coupons->update(array('_id' => new MongoId($_couponId)), array('$set' => $updateData));
				
			return $this->getCouponInfo($_couponId);
		}

		function deleteCoupon($_couponId) {
			$updateData = array(
				'delYn' => 'Y',
				'updatedDate' => floatval(round(microtime(true) * 1000))
			);
				
			$ret = $this->coupons->update(array('_id' => new MongoId($_couponId)), array('$set' => $updateData));
				
			return true;
		}
		
		function addCouponPushes($_couponId, $_couponPushTitle, $_couponPushMessage, $_recvUserType, $_recvUser, $_useCouponImgYn, $_couponPushImgPath) {
			$newData = array(
				'couponId' => $_couponId,
				'couponPushTitle' => $_couponPushTitle,
				'couponPushMessage' => $_couponPushMessage,
				'recvUserType' => $_recvUserType,
				'recvUser' => $_recvUser,
				'useCouponImgYn' => $_useCouponImgYn,
				'couponPushImgPath' => $_couponPushImgPath,
				'createdDate' => floatval(round(microtime(true) * 1000))
			);
				
			$ret = $this->couponpushes->insert($newData);
		
			if(isset($newData["_id"])) {
				$newData["couponPushId"] = strval($newData["_id"]);
				$newData["createdDate"] = date("Y-m-d H:i:s", ($newData["createdDate"] / 1000));
			}
		
			return $newData;
		}
		
		function addUserCoupons($_users, $_couponId, $_recvUserType, $_recvUser, $_lat, $_lng) {
			$this->getUserManagerClass($_users);
			
			$userList = $this->UserManager->getUserListByRecvUserType($_recvUserType, $_recvUser, $_lat, $_lng);
			
			foreach ($userList as $row) {
				$newData = array(
					'userId' => strval($row["_id"]),
					'couponId' => $_couponId,
					'status' => "new",
					'delYn' => 'N',
					'createdDate' => floatval(round(microtime(true) * 1000)),
					'updatedDate' => floatval(round(microtime(true) * 1000))
				);
				
				$ret = $this->usercoupons->insert($newData);
			}	
		}
	}
?>
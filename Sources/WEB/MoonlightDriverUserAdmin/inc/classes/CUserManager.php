<?php 
	class CUserManager {
		var $users;
		
		function CUserManager($_users) {
			$this->users = $_users;
		}
		
		function getTotalUserCount() {
			return $this->users->count();
		}
		
		function getTotalFilteredUserCount($_find) {
			return $this->users->count($_find);
		}
		
		function getUserList($_find, $_start, $_length, $_sort) {
			$user_list = $this->users->find($_find)->sort($_sort)->skip($_start)->limit($_length);
				
			$ret_user_list = array();
			foreach ($user_list as $row) {
				$rowArray = array();
				
				$user_id = strval($row["_id"]);
				$rowArray["user_id"] = $user_id;
				$rowArray["phone"] = isset($row["phone"]) ? $row["phone"] : "";
				$rowArray["appVersion"] = isset($row["appVersion"]) ? $row["appVersion"] : "";
				$rowArray["createdDate"] = isset($row["createdDate"]) ? date('Y-m-d H:i:s', $row["createdDate"]->sec) : "";
				$rowArray["lastLoginDate"] = isset($row["lastLoginDate"]) ? date('Y-m-d H:i:s', $row["lastLoginDate"]->sec) : "";
				$rowArray["selectCheckbox"] = "<input type='checkbox' value='" . $row["phone"] . "'>";
		
				$ret_user_list[] = $rowArray;
			}
			
			return $ret_user_list;
		}
		
		function getAllUserList() {
			$user_list = $this->users->find(array(), array("phone" => 1));
			return $user_list;
		}
		
		function getUserInfo($_user_id) {
			$ret_user_info = array();
			$user_info = $this->users->findOne(array('_id' => new MongoId($_user_id)));
				
			if(isset($user_info)) {
				$user_id = strval($user_info["_id"]);
				
				$ret_user_info["user_id"] = $user_id;
				$ret_user_info["gcmId"] = isset($user_info["gcmId"]) ? $user_info["gcmId"] : "";
				$ret_user_info["email"] = isset($user_info["email"]) ? $user_info["email"] : "";
				$ret_user_info["phone"] = isset($user_info["phone"]) ? $user_info["phone"] : "";
				$ret_user_info["device_uuid"] = isset($user_info["device_uuid"]) ? $user_info["device_uuid"] : "";
				$ret_user_info["name"] = isset($user_info["name"]) ? $user_info["name"] : "";
				$ret_user_info["macAddr"] = isset($user_info["macAddr"]) ? $user_info["macAddr"] : "";
				$ret_user_info["locations"] = isset($user_info["locations"]) ? $user_info["locations"] : "";
				$ret_user_info["profileImage"] = isset($user_info["profileImage"]) ? $user_info["profileImage"] : "";
				$ret_user_info["createdDate"] = isset($user_info["createdDate"]) ? date('Y-m-d H:i:s', $user_info["createdDate"]->sec) : "";
				$ret_user_info["lastLoginDate"] = isset($user_info["lastLoginDate"]) ? date('Y-m-d H:i:s', $user_info["lastLoginDate"]->sec) : "";
			}
				
			return $ret_user_info;
		}
		
		function getUserInfoByPhoneNumber($_phone) {
			$convertedPhone = convertPhoneNumber($_phone);
			$find = array(
				'$or' => array(
					array("phone" => $_phone), 
					array("phone" => $convertedPhone)
				)
			);
			
			$user_info = $this->users->findOne($find);
			
			if(!isset($user_info)) {
				return null;
			}

			$user_id = strval($user_info["_id"]);
			
			$ret_user_info = array();
			$ret_user_info["user_id"] = $user_id;
			$ret_user_info["gcmId"] = isset($user_info["gcmId"]) ? $user_info["gcmId"] : "";
			$ret_user_info["email"] = isset($user_info["email"]) ? $user_info["email"] : "";
			$ret_user_info["phone"] = isset($user_info["phone"]) ? $user_info["phone"] : "";
			$ret_user_info["device_uuid"] = isset($user_info["device_uuid"]) ? $user_info["device_uuid"] : "";
			$ret_user_info["name"] = isset($user_info["name"]) ? $user_info["name"] : "";
			$ret_user_info["macAddr"] = isset($user_info["macAddr"]) ? $user_info["macAddr"] : "";
			$ret_user_info["locations"] = isset($user_info["locations"]) ? $user_info["locations"] : "";
			$ret_user_info["profileImage"] = isset($user_info["profileImage"]) ? $user_info["profileImage"] : "";
			$ret_user_info["createdDate"] = isset($user_info["createdDate"]) ? date('Y-m-d H:i:s', $user_info["createdDate"]->sec) : "";
			$ret_user_info["lastLoginDate"] = isset($user_info["lastLoginDate"]) ? date('Y-m-d H:i:s', $user_info["lastLoginDate"]->sec) : "";
			
			return $ret_user_info;
		}
		
		function getUserListByPhoneNumberReg($_phone) {
			$_phone = str_replace("+", "\+", $_phone);
			$regex = new MongoRegex("/" . $_phone . "/g");
			$user_list = $this->users->find(array('phone' => $regex));
				
			if(!isset($user_list)) {
				return null;
			}
			
			$ret_user_list = array();			
			foreach ($user_list as $user_info) {
				$ret_user_info = array();
				
				$ret_user_info["user_id"] = strval($user_info["_id"]);
				$ret_user_info["gcmId"] = isset($user_info["gcmId"]) ? $user_info["gcmId"] : "";
				$ret_user_info["email"] = isset($user_info["email"]) ? $user_info["email"] : "";
				$ret_user_info["phone"] = isset($user_info["phone"]) ? $user_info["phone"] : "";
				$ret_user_info["device_uuid"] = isset($user_info["device_uuid"]) ? $user_info["device_uuid"] : "";
				$ret_user_info["name"] = isset($user_info["name"]) ? $user_info["name"] : "";
				$ret_user_info["macAddr"] = isset($user_info["macAddr"]) ? $user_info["macAddr"] : "";
				$ret_user_info["locations"] = isset($user_info["locations"]) ? $user_info["locations"] : "";
				$ret_user_info["profileImage"] = isset($user_info["profileImage"]) ? $user_info["profileImage"] : "";
				$ret_user_info["createdDate"] = isset($user_info["createdDate"]) ? date('Y-m-d H:i:s', $user_info["createdDate"]->sec) : "";
				$ret_user_info["lastLoginDate"] = isset($user_info["lastLoginDate"]) ? date('Y-m-d H:i:s', $user_info["lastLoginDate"]->sec) : "";
				
				$ret_user_list[] = $ret_user_info;
			}

			return $ret_user_list;
		}
		
		function getRegIdList($_recv_user_type, $_phones, $_skip, $_limit) {
			if($_recv_user_type == "selected") {
				$user_gcm_list = $this->users->find(array('phone' => array('$in' => $_phones)), array("gcmId" => 1))->skip($_skip)->limit($_limit);
			} else if($_recv_user_type == "all") {
				$user_gcm_list = $this->users->find(array(), array("gcmId" => 1))->skip($_skip)->limit($_limit);
			} else if($_recv_user_type == "500") {
				$user_gcm_list = $this->users->find(array(), array("gcmId" => 1))->skip($_skip)->limit($_limit);
			} else if($_recv_user_type == "1000") {
				$user_gcm_list = $this->users->find(array(), array("gcmId" => 1))->skip($_skip)->limit($_limit);
			} else {
				return null;
			}
				
			if(!isset($user_gcm_list)) {
				return null;
			}
		
			$ret_gcm_list = array();
			foreach ($user_gcm_list as $row) {
				$ret_gcm_list[] = $row["gcmId"];
			}
		
			return $ret_gcm_list;
		}
		
		function getUserListByRecvUserType($_recvUserType, $_phones, $_lat, $_lng) {
			$userList = null;
			
			if($_recvUserType == "selected") {
				$userList = $this->users->find(array('phone' => array('$in' => $_phones)), array("_id" => 1));
			} else if($_recvUserType == "all") {
				$userList = $this->users->find(array(), array("_id" => 1));
			} else if($_recvUserType == "500" || $_recvUserType == "1000") {
				$opt = array(
					'$geoNear' => array(
						'near' => array(
							'type' => 'Point',
							'coordinates' => array($_lng, $_lat)
						)
					),
					"maxDistance" => intval($_recvUserType),
					"spherical" => true,
					"limit" => 100000
				);
				
				$userList = $this->users->aggregate($opt);
			} else {
				return null;
			}
		
			return $userList;
		}
	}
?>
<?php 
	class CAccountManager {
		function CAccountManager() {}
		
		function getAccountList($_useraccounts, $_find, $_start, $_length, $_sort) {
			$account_list = $_useraccounts->find($_find)->sort($_sort)->skip($_start)->limit($_length);
				
			global $ROLE;
			
			$ret_account_list = array();
			foreach ($account_list as $row) {
				$rowArray = array();
				
				$account_id = strval($row["_id"]);
				$rowArray["account_id"] = $account_id;
				$rowArray["id"] = $row["id"];
				$rowArray["name"] = $row["name"];
				$rowArray["phone"] = $row["phone"];
				$rowArray["email"] = $row["email"];
				$rowArray["role"] = $ROLE[$row["level"]];
				$rowArray["createdDate"] = date("Y-m-d H:i:s", ($row["createdDate"] / 1000));
				$rowArray["deletion"] = "<button type='button' class='btn btn-danger btn-sm btn_delete_account'>삭제</button>";
		
				$ret_account_list[] = $rowArray;
			}
			
			return $ret_account_list;
		}
		
		function getAccountInfo($_useraccounts, $_account_id) {
			$ret_account_info = array();
			$account_info = $_useraccounts->findOne(array('_id' => new MongoId($_account_id)));
				
			if(isset($account_info)) {
				$account_id = strval($account_info["_id"]);
				
				global $ROLE;
				
				$ret_account_info["account_id"] = $account_id;
				$ret_account_info["id"] = $account_info["id"];
				$ret_account_info["name"] = $account_info["name"];
				$ret_account_info["phone"] = $account_info["phone"];
				$ret_account_info["email"] = $account_info["email"];
				$ret_account_info["level"] = $account_info["level"];
				$ret_account_info["role"] = $ROLE[$account_info["level"]];
				$ret_account_info["createdDate"] = date("Y-m-d H:i:s", ($account_info["createdDate"] / 1000));
			}
				
			return $ret_account_info;
		}
		
		function isDuplicatedId($_useraccounts, $_id, $_account_id) {
			$account_list = $_useraccounts->find(array("id" => $_id));
			
			$count = 0;
			foreach ($account_list as $row) {
				$account_id = strval($row["_id"]);
				if(isset($_account_id)) {
					if($account_id != $_account_id) {
						$count++;
					}
				} else {
					$count++;
				}
			}
			
			if($count > 0) {
				return true;
			} else {
				return false;
			}
		}
		
		function addAccount($_useraccounts, $_id, $_pw, $_name, $_phone, $_email, $_level) {
			$newData = array(
				'id' => $_id,
				'pw' => md5($_pw),
				'name' => $_name,
				'phone' => $_phone,
				'email' => $_email,
				'level' => $_level,
				'createdDate' => floatval(round(microtime(true) * 1000)),
				'updatedDate' => floatval(round(microtime(true) * 1000))
			);
			
			$ret = $_useraccounts->insert($newData);
				
			if(isset($newData["_id"])) {
				global $ROLE;
				
				$newData["account_id"] = strval($newData["_id"]);
				$newData["role"] = $ROLE[$newData["level"]];
				$newData["createdDate"] = date("Y-m-d H:i:s", ($newData["createdDate"] / 1000));
				$newData["updatedDate"] = date("Y-m-d H:i:s", ($newData["updatedDate"] / 1000));
			}
				
			return $newData;
		}
		
		function updateAccount($_useraccounts, $_account_id, $_id, $_pw, $_name, $_phone, $_email, $_level) {
			$updateData = array(
				'id' => $_id,
				'name' => $_name,
				'phone' => $_phone,
				'email' => $_email,
				'level' => $_level,
				'updatedDate' => floatval(round(microtime(true) * 1000))
			);
			
			if(isset($_pw) && !empty($_pw)) {
				$updateData["pw"] = md5($_pw);
			}
				
			$ret = $_useraccounts->update(array('_id' => new MongoId($_account_id)), array('$set' => $updateData));
				
			return $this->getAccountInfo($_useraccounts, $_account_id);
		}
		
		function deleteAccount($_useraccounts, $_account_id) {
			$_useraccounts->remove(array("_id" => new MongoId($_account_id)));
		}
		
		function insertUserAuthResult($_userauthresult, $_rqst_caus_cd, $_result_cd, $_result_msg, $_svc_tx_seqno, $_cert_dt_tm, $_di, $_ci, $_name, $_birthday, $_sex, $_nation, $_tel_com_cd, $_tel_no, $_return_msg) {
			$insertData = array(
				'rqst_caus_cd' => $_rqst_caus_cd,
				'result_cd' => $_result_cd,
				'result_msg' => $_result_msg,
				'svc_tx_seqno' => $_svc_tx_seqno,
				'cert_dt_tm' => $_cert_dt_tm,
				'di' => $_di,
				'ci' => $_ci,
				'name' =>$_name,
				'birthday' => $_birthday,
				'sex' => $_sex,
				'nation' => $_nation,
				'tel_com_cd' => $_tel_com_cd,
				'tel_no' => $_tel_no,
				'return_msg' => $_return_msg
			);
			
			$ret = $_userauthresult->insert($insertData);
			
			return isset($newData["_id"]);
		}
	}
?>
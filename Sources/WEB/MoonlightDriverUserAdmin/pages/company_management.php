<?php
	header("Content-Type:text/html; charset=utf-8");

	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession"));
		
		$session = new CSession();
		
		if(!$session->isLogin()) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		$permissions = array("company" => array("inquiry"));
		if(!$session->checkPermission($permissions)) {
			echo "<script>";
			echo "alert('접근권한이 없습니다.');";
			echo "location.href = '" . CONF_URL_ROOT . "';";
			echo "</script>";
			exit;
		}
		
		global $REGION;
		$company_region = json_encode($REGION, JSON_UNESCAPED_UNICODE);
	} catch (Exception $e) {
 		snc_error($e->getMessage());
 		moveToSpecificPage(CONF_URL_ERROR);
		exit;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		
		<title><?php echo CONF_SITE_TITLE . " > 대리운전 회사 관리"; ?></title>
		
		<!-- Bootstrap 3.3.6 -->
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap.min.css">
		<!-- Font Awesome -->
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/font-awesome.min.css">
		<!-- Ionicons -->
  		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/ionicons.min.css">
  		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.bootstrap.css">
		<!-- Theme style -->
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/AdminLTE.min.css">
		<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/skins/_all-skins.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/loading.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/main.css">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/fastclick.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/adminLTE.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/md5.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery.form.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/dataTables.bootstrap.min.js"></script>
		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
			var company_region = <?php echo $company_region; ?>;
		</script>
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js/company_manager.js?<?php echo time(); ?>"></script>
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="loading">Loading</div>
		<div class="wrapper">
			<?php include_once CONF_URL_MENU; ?>
			<div class="content-wrapper">
				<section class="content-header">
					<h1>대리운전 회사 관리</h1>
					<ol class="breadcrumb">
						<li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active">대리운전 회사 관리</li>
					</ol>
				</section>
				<!-- Main content -->
				<section class="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="box box-default box-solid">
								<div class="box-header with-border">
									<h3 class="box-title">대리운전 회사 리스트</h3>
								</div>
								<div class="box-body">
									<button type="button" class="btn btn-primary" id="btnCreateCompany">대리운전 회사 등록</button>
								</div>
								<div class="box-body">
									<ul class="nav nav-tabs">
									<?php 
										$i = 0;
										foreach ($REGION as $key => $row) {
									?>
										<li <?php echo $i == 0 ? "class='active'" : ""; ?>>
											<a data-toggle="tab" href="<?php echo $row; ?>"><?php echo $row; ?></a>
										</li>
									<?php
											$i++;
										}
									?>
									</ul>
									<div class="tab-content box-body">
										<table id="company_list" class="table table-bordered table-hover">
											<thead>
												<tr>
													<th>company_id</th>
													<th>회사명</th>
													<th>전화번호</th>
													<th>지역</th>
													<th>기본 가격</th>
													<th>경유비</th>
													<th class="width-50">삭제</th>
												</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<div class="modal fade" id="modal_popup" tabindex="-1" role="dialog" aria-labelledby="modal_popup_label" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="modal_popup_label"></h4>
							</div>
							<div class="modal-body" id="modal_popup_content"></div>
							<div class="modal-footer" id="modal_popup_footer"></div>
						</div>
					</div>
				</div>
			</div>
			<?php include_once CONF_URL_FOOTER; ?>
		</div>
	</body>
</html>
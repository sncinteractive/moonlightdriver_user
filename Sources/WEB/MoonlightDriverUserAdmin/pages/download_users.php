<?php
	header("Content-Type:text/html; charset=utf-8");

	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager", "CUserManager", "php-export-data.class"));
		
		$session = new CSession();
		
		if(!$session->isLogin()) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		$permissions = array("excel_export" => array("inquiry", "create"));
		if(!$session->checkPermission($permissions)) {
			echo "<script>";
			echo "alert('접근권한이 없습니다. test1');";
			echo "location.href = '" . CONF_URL_ROOT . "';";
			echo "</script>";
			exit;
		}
		
		$exportPassword = $_GET["exportPassword"];
				
		if(!isset($exportPassword) || empty($exportPassword) || $exportPassword != EXCEL_EXPORT_PASSWORD) {
			echo "<script>";
			echo "alert('비밀번호가 일치하지 않습니다. test2');";
			echo "location.href = '" . CONF_URL_ROOT . "';";
			echo "</script>";
			exit;
		}
		
		$database_manager = new CDatabaseManager();
		$database = $database_manager->getDb();
		
		$user_manager = new CUserManager($database->users);
		$allUserList = $user_manager->getAllUserList();
			
		$fileName = EXCEL_EXPORT_FILENAME . "_" . date("YmdHis") . ".xls";
		
		$exporter = new ExportDataExcel("browser", $fileName);
		$exporter->initialize(); // starts streaming data to web browser
		
		$exporter->addRow(array("Phone"));
		foreach ($allUserList as $row) {
			$phone = $row["phone"];
			
			if(!isset($phone) || empty($phone)) {
				continue;
			}
			
			if(startsWith($phone, "010")) {
				$phone = "+8210" . substr($phone, 3);
			}
			
			$exporter->addRow(array($phone));
		}
		
		$exporter->finalize();
	} catch (Exception $e) {
		echo $e->getMessage();
 		snc_error($e->getMessage());
 		moveToSpecificPage(CONF_URL_ERROR);
		exit;
	}
?>

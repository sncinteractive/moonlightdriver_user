<?php
	header("Content-Type: text/html; charset=UTF-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager"));
	
		$database_manager = new CDatabaseManager();
		$session = new CSession();
		
		$id = isset($_POST["id"]) ? $_POST["id"] : "";
		$pw = isset($_POST["pw"]) ? $_POST["pw"] : "";
		
		if(!empty($id) && !empty($pw)) {
			$database = $database_manager->getDb();
			
			$ret = $session->login($database->useraccounts, $id, $pw);
			if(!$ret) {
				echo "<script>";
				echo "alert('아이디 또는 비밀번호가 틀립니다.');";
				echo "location.href = '" . CONF_URL_ROOT . "';";
				echo "</script>";
				exit;
			}
		}

		moveToSpecificPage(CONF_URL_ROOT);
		exit;
	} catch (Exception $e) {
		echo $e->getMessage();
		exit;
		snc_error($e->getMessage());
		moveToSpecificPage(CONF_URL_ERROR);
		exit;
	}
?>

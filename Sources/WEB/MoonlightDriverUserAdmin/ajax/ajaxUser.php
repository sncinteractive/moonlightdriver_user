<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager", "CUserManager", "CCouponManager", "CUserPointHistManager"));
	
		$session = new CSession();
	
		$retResult = array("result" => "OK", "msg" => "SUCCESS", "data" => array());
	
		if(!$session->isLogin()) {
			$retResult["result"] = "NOT_LOGIN";
			$retResult["msg"] = "로그인 후 사용가능합니다.";
	
			echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
			exit;
		}
	
		$database_manager = new CDatabaseManager();
		$database = $database_manager->getDb();
		
		$user_manager = new CUserManager($database->users);
		$couponManager = new CCouponManager($database->coupons, $database->couponpushes, $database->usercoupons);
		$userPointHistManager = new CUserPointHistManager($database->useradditionals, $database->userpointhists);
	
		$type = $_POST["type"];
		
		if($type == "getUserList") {
			$permissions = array("user" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$draw = $_POST["draw"];
			$columns = $_POST["columns"];
			$order = $_POST["order"];
			$start = $_POST["start"];
			$length = $_POST["length"];
			$search = isset($_POST["search"]) ? $_POST["search"] : "";
			
			$sort_direction = $order[0]['dir'] == "asc" ? 1 : -1;
			$sort = array($columns[$order[0]['column']]['data'] => $sort_direction);
			
			$find = array();
			$findItem = array();
			if(!empty($search['value'])) {
				$regex = new MongoRegex("/" . $search['value'] . "/");
				
				foreach ($columns as $row) {
					if($row["searchable"] == "true") {
						$findItem[] = array($row["data"] => $regex);
					}
				}
				
				$find = array('$or' => $findItem);
			}
			
			$totalCount = $user_manager->getTotalUserCount();
			$filteredTotalCount = $user_manager->getTotalFilteredUserCount($find);
			$userList = $user_manager->getUserList($find, $start, $length, $sort);
			$ret = array(
				"data" => $userList,
				"draw" => intval($draw),
				"recordsFiltered" => $filteredTotalCount,
				"recordsTotal" => $totalCount
			);
			
			echo json_encode($ret, JSON_UNESCAPED_UNICODE);
			exit;
		} else if($type == "send_push") {
			$push_title = $_POST["push_title"];
			$push_message = $_POST["push_message"];
			$recv_user = isset($_POST["recv_user"]) ? $_POST["recv_user"] : "";
			$recv_user_list = explode(",", $recv_user);
			$recv_user_type = $_POST["recv_user_type"];
			
			$message_length = strlen($push_message);
			if($message_length > MAX_PUS_MESSAGE_LENGTH) {
				$retResult["result"] = "ERROR";
				$retResult["msg"] = "메시지 최대 길이를 넘었습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$imgUrl = "";
			if (isset($_FILES['push_img']) && is_uploaded_file($_FILES['push_img']['tmp_name'])) {
				$fileNameArr = explode(".", basename($_FILES['push_img']['name']));
				$upload_file_path = uniqid() . "." . $fileNameArr[1];
					
				if (move_uploaded_file($_FILES['push_img']['tmp_name'], CONF_PATH_FILE_PUSH . $upload_file_path)) {
					$imgUrl = CONF_URL_FILE_PUSH . $upload_file_path;
				}
			}
			
			$push_content = array();
			$push_content["data"] = array(
				"type" => "notice",
				"title" => $push_title,
				"message" => $push_message,
				"imgUrl" => $imgUrl
			);
			
			// 헤더 부분
			$headers = array(
				'Content-Type:application/json',
				'Authorization:key=' . GOOGLE_SERVER_KEY
			);
			
			$limit = 1000;
			$skip = 0;
			$retPush = 0;
			$hasNext = true;
			while($hasNext) {
				$ret_gcm_list = $user_manager->getRegIdList($recv_user_type, $recv_user_list, $skip, $limit);
				
				if($ret_gcm_list == null || count($ret_gcm_list) <= 0) {
					$hasNext = false;
					break;
				}
					
				$push_content['registration_ids'] = $ret_gcm_list;
								
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($push_content));
				$response = curl_exec($ch);
				curl_close($ch);
			
				// 푸시 전송 결과 반환.
				$obj = json_decode($response);
			
				if($obj != null) {
					// 푸시 전송시 성공 수량 반환.
					$retPush += $obj->success;

					if($obj->failure > 0) {
						debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $response);
					}
				} else {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $response);
				}
				
				$skip += $limit;
			}
			
			$retResult["data"] = $retPush;
		} else if($type == "user_detail") {
			$permissions = array("user" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$user_id = $_POST["user_id"];
			
			$user_manager = new CUserManager($database->users);
			$retResult["data"] = $user_manager->getUserInfo($user_id);
		} else if($type == "search_user_phone") {
			$permissions = array("user_pos" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$search_text = $_POST["search_text"];
				
			$user_manager = new CUserManager($database->users);
			$retResult["data"] = $user_manager->getUserListByPhoneNumberReg($search_text);
		} else if($type == "send_coupon") {
			$permissions = array("coupon" => array("inquiry", "create"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$couponId = $_POST["couponId"];
			$pushTitle = $_POST["push_title"];
			$pushMessage = $_POST["push_message"];
			$recvUser = isset($_POST["recv_user"]) ? $_POST["recv_user"] : "";
			$recvUserList = explode(",", $recvUser);
			$selectRecvUserList = $_POST["selectRecvUserList"];
			$selectPushImg = $_POST["selectPushImg"];
			$useCouponImgYn = $selectPushImg == "1" ? "Y" : "N";
			$lat = isset($_POST["lat"]) ? $_POST["lat"] : 0;
			$lng = isset($_POST["lng"]) ? $_POST["lng"] : 0;
			
			$recvUserType = "";
			if($selectRecvUserList == "0") {
				$recvUserType = "selected";
			} else if($selectRecvUserList == "1") {
				$recvUserType = "all";
			} else if($selectRecvUserList == "2") {
				$recvUserType = "500";
			} else if($selectRecvUserList == "3") {
				$recvUserType = "1000";
			}
			
			$messageLength = strlen($pushMessage);
			if($messageLength > MAX_PUS_MESSAGE_LENGTH) {
				$retResult["result"] = "ERROR";
				$retResult["msg"] = "메시지 최대 길이를 넘었습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$imgUrl = "";
			if ($selectPushImg == "2" && isset($_FILES['push_img']) && is_uploaded_file($_FILES['push_img']['tmp_name'])) {
				$fileNameArr = explode(".", basename($_FILES['push_img']['name']));
				$upload_file_path = uniqid() . "." . $fileNameArr[1];
					
				if (move_uploaded_file($_FILES['push_img']['tmp_name'], CONF_PATH_FILE_PUSH . $upload_file_path)) {
					$imgUrl = CONF_URL_FILE_PUSH . $upload_file_path;
				}
			} else if($selectPushImg == "1") {
				$couponInfo = $couponManager->getCouponInfo($couponId);
				if(isset($couponInfo)) {
					$imgUrl = $couponInfo["couponImgPath"];
				}
			}
			
			$couponManager->addUserCoupons($database->users, $couponId, $recvUserType, $recvUserList, $lat, $lng);
			$couponManager->addCouponPushes($couponId, $pushTitle, $pushMessage, $recvUserType, $recvUserList, $useCouponImgYn, $imgUrl);
			
			$retPush = sendPushNotification($database->users, "coupon", $pushTitle, $pushMessage, $imgUrl, $recvUserType, $recvUserList);
			
			$retResult["data"] = $retPush;
		} else if($type == "send_point") {
			$permissions = array("coupon" => array("inquiry", "create"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
		
			$pushTitle = "적립금 지급";
			$point = $_POST["point"];
			$pushMessage = $_POST["push_message"];
			$sendPushYn = isset($_POST["sendPushYn"]) ? $_POST["sendPushYn"] : "N";
			$recvUser = isset($_POST["recv_user"]) ? $_POST["recv_user"] : "";
			$recvUserList = explode(",", $recvUser);
			$selectRecvUserList = $_POST["selectRecvUserList"];
			$recvUserType = $selectRecvUserList == '0' ? "selected" : "all";
		
			$messageLength = strlen($pushMessage);
			if($messageLength > MAX_PUS_MESSAGE_LENGTH) {
				$retResult["result"] = "ERROR";
				$retResult["msg"] = "메시지 최대 길이를 넘었습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
		
			$userPointHistManager->addUserPoint($database->users, $point, $pushMessage, $recvUserType, $recvUserList);
			
			$retResult["data"] = 0;
			if($sendPushYn == "Y") {
				$sendMessage = "적립금 " . number_format($point) . "포인트 지급.";
				$retPush = sendPushNotification($database->users, "point", $pushTitle, $sendMessage, "", $recvUserType, $recvUserList);
				$retResult["data"] = $retPush;
			}
		} else if($type == "export_excel") {
			$permissions = array("excel_export" => array("inquiry", "create"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$exportPassword = $_POST["exportPassword"];

			if(!isset($exportPassword) || empty($exportPassword) || md5($exportPassword) != EXCEL_EXPORT_PASSWORD) {
				$retResult["result"] = "UNAUTHORIZED";
				$retResult["msg"] = "비밀번호가 일치하지 않습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$retResult["data"] = md5($exportPassword);
		}
	} catch (Exception $e) {
		$retResult["result"] = "ERROR";
		$retResult["msg"] = $e->getMessage();
	}
	
	echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
	exit;

	function sendPushNotification($_users, $_pushType, $_pushTitle, $_pushMessage, $_imgUrl, $_recvUserType, $_recvUserList) {
		$user_manager = new CUserManager($_users);
		
		$push_content = array();
		$push_content["data"] = array(
			"type" => $_pushType,
			"title" => $_pushTitle,
			"message" => $_pushMessage,
			"imgUrl" => $_imgUrl
		);
			
		// 헤더 부분
		$headers = array(
			'Content-Type:application/json',
			'Authorization:key=' . GOOGLE_SERVER_KEY
		);
			
		$limit = 1000;
		$skip = 0;
		$retPush = 0;
		$hasNext = true;
		while($hasNext) {
			$ret_gcm_list = $user_manager->getRegIdList($_recvUserType, $_recvUserList, $skip, $limit);
		
			if($ret_gcm_list == null || count($ret_gcm_list) <= 0) {
				$hasNext = false;
				break;
			}
				
			$push_content['registration_ids'] = $ret_gcm_list;
		
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($push_content));
			$response = curl_exec($ch);
			curl_close($ch);
				
			// 푸시 전송 결과 반환.
			$obj = json_decode($response);
				
			if($obj != null) {
				// 푸시 전송시 성공 수량 반환.
				$retPush += $obj->success;
		
				if($obj->failure > 0) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $response);
				}
			} else {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $response);
			}
		
			$skip += $limit;
		}
		
		return $retPush;
	}

?>
<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager", "CAccountManager"));
	
		$session = new CSession();
	
		$retResult = array("result" => "OK", "msg" => "SUCCESS", "data" => array());
	
		if(!$session->isLogin()) {
			$retResult["result"] = "NOT_LOGIN";
			$retResult["msg"] = "로그인 후 사용가능합니다.";
	
			echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
			exit;
		}
	
		$database_manager = new CDatabaseManager();
		$database = $database_manager->getDb();
	
		$type = $_POST["type"];
		
		if($type == "getAccountList") {
			$permissions = array("account" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$draw = $_POST["draw"];
			$columns = $_POST["columns"];
			$order = $_POST["order"];
			$start = $_POST["start"];
			$length = $_POST["length"];
			$search = isset($_POST["search"]) ? $_POST["search"] : "";
			
			$sort_direction = $order[0]['dir'] == "asc" ? 1 : -1;
			$sort = array($columns[$order[0]['column']]['data'] => $sort_direction);
			
			$find = array();
			$findItem = array();
			if(!empty($search['value'])) {
				$regex = new MongoRegex("/" . $search['value'] . "/");
				
				foreach ($columns as $row) {
					if($row["searchable"] == "true") {
						$findItem[] = array($row["data"] => $regex);
					}
				}
				
				$find = array('$or' => $findItem);
			}
			
			$account_manager = new CAccountManager();
			
			$totalCount = $database->useraccounts->count();
			$filteredTotalCount = $database->useraccounts->count($find);
			$accountList = $account_manager->getAccountList($database->useraccounts, $find, $start, $length, $sort);
			$ret = array(
				"data" => $accountList,
				"draw" => intval($draw),
				"recordsFiltered" => $filteredTotalCount,
				"recordsTotal" => $totalCount
			);
			
			echo json_encode($ret, JSON_UNESCAPED_UNICODE);
			exit;
		} else if($type == "add_account") {
			$permissions = array("account" => array("inquiry", "create"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$id = $_POST["id"];
			$pw = $_POST["pw"];
			$name = $_POST["name"];
			$phone = $_POST["phone"];
			$email = isset($_POST["email"]) ? $_POST["email"] : "";
			$level = $_POST["level"];
				
			$account_manager = new CAccountManager();
			if($account_manager->isDuplicatedId($database->useraccounts, $id, null)) {
				$retResult["result"] = "DUPLICATE_ID";
				$retResult["msg"] = "이미 사용중인 아이디입니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$account_manager->addAccount($database->useraccounts, $id, $pw, $name, $phone, $email, $level);
		} else if($type == "edit_account") {
			$permissions = array("account" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$account_id = $_POST["account_id"];
			$id = $_POST["id"];
			$pw = isset($_POST["pw"]) ? $_POST["pw"] : "";
			$name = $_POST["name"];
			$phone = $_POST["phone"];
			$email = isset($_POST["email"]) ? $_POST["email"] : "";
			$level = $_POST["level"];
				
			$account_manager = new CAccountManager();
			if($account_manager->isDuplicatedId($database->useraccounts, $id, $account_id)) {
				$retResult["result"] = "DUPLICATE_ID";
				$retResult["msg"] = "이미 사용중인 아이디입니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$account_manager->updateAccount($database->useraccounts, $account_id, $id, $pw, $name, $phone, $email, $level);
		} else if($type == "delete_account") {
			$permissions = array("account" => array("inquiry", "delete"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$account_id = $_POST["account_id"];
				
			$account_manager = new CAccountManager();
			$account_manager->deleteAccount($database->useraccounts, $account_id);
		} else if($type == "account_detail") {
			$permissions = array("account" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$account_id = $_POST["account_id"];
			
			$account_manager = new CAccountManager();
			$retResult["data"] = $account_manager->getAccountInfo($database->useraccounts, $account_id);
		}
	} catch (Exception $e) {
		$retResult["result"] = "ERROR";
		$retResult["msg"] = $e->getMessage();
	}
	
	echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
	exit;

?>
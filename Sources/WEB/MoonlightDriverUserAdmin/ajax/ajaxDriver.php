<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager", "CDriverManager"));
	
		$session = new CSession();
	
		$retResult = array("result" => "OK", "msg" => "SUCCESS", "data" => array());
	
		if(!$session->isLogin()) {
			$retResult["result"] = "NOT_LOGIN";
			$retResult["msg"] = "로그인 후 사용가능합니다.";
	
			echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
			exit;
		}
	
		$database_manager = new CDatabaseManager();
		$database = $database_manager->getDb();
		
		$driver_manager = new CDriverManager();
	
		$type = $_POST["type"];
		
		if($type == "getDriverList") {
			$permissions = array("driver" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$draw = $_POST["draw"];
			$columns = $_POST["columns"];
			$order = $_POST["order"];
			$start = $_POST["start"];
			$length = $_POST["length"];
			$search = isset($_POST["search"]) ? $_POST["search"] : "";
			
			$sort_direction = $order[0]['dir'] == "asc" ? 1 : -1;
			$sort = array($columns[$order[0]['column']]['data'] => $sort_direction);
			
			$find = array();
			$findItem = array();
			if(!empty($search['value'])) {
				$regex = new MongoRegex("/" . $search['value'] . "/");
				
				foreach ($columns as $row) {
					if($row["searchable"] == "true") {
						$findItem[] = array($row["data"] => $regex);
					}
				}
				
				$find = array('$or' => $findItem);
			}
			
			$totalCount = $database->cardaridrivers->count();
			$filteredTotalCount = $database->cardaridrivers->count($find);
			$driverList = $driver_manager->getDriverList($database->cardaridrivers, $find, $start, $length, $sort);
			$ret = array(
				"data" => $driverList,
				"draw" => intval($draw),
				"recordsFiltered" => $filteredTotalCount,
				"recordsTotal" => $totalCount
			);
			
			echo json_encode($ret, JSON_UNESCAPED_UNICODE);
			exit;
		} else if($type == "add_driver") {
			$permissions = array("driver" => array("inquiry", "create"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$name = $_POST["name"];
			$phone = $_POST["phone"];
			$socialNumber = $_POST["socialNumber"];
			$licenseType = $_POST["licenseType"];
			$licenseNumber = $_POST["licenseNumber"];
			$email = $_POST["email"];
				
			$driverInfo = $driver_manager->addDriver($database->cardaridrivers, $name, $phone, $socialNumber, $licenseType, $licenseNumber, $email);
			if(isset($driverInfo["driverId"]) && !empty($driverInfo["driverId"])) {
				$headers = array(
					'Content-Type:application/json'
				);
				
				$params = array(
					"company_number" => "",
					"biz_driver_id" => $driverInfo["driverId"],
					"cell" => encrypt(INSURANCE_CRYPT_KEY, $driverInfo["phone"]),
					"cell2" => encrypt(INSURANCE_CRYPT_KEY, ""),
					"name" => $driverInfo["name"],
					"social_number" => encrypt(INSURANCE_CRYPT_KEY, $driverInfo["socialNumber"]),
					"license_type" => $driverInfo["licenseType"],
					"license_number" => $driverInfo["licenseNumber"],
					"email" => $driverInfo["email"],
					"memo" => ""
				);
				
				$reqParams = array(
					"apiKey" => INSURANCE_API_KEY,
					"request_data" => array($params)
				); 
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, 'http://125.209.197.202:3000');
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($reqParams));
				$response = curl_exec($ch);
				curl_close($ch);
					
				$resData = json_decode($response);
				if($resData != null) {
					if(isset($resData->Message) && $resData->Message == "success") {
						
					} else {
						$retResult["result"] = "FAIL";
						$retResult["msg"] = "보험 심사 요청에 실패했습니다.[" . $resData->Message . "]";
						
						echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
						exit;
					}
				} else {
					$retResult["result"] = "FAIL";
					$retResult["msg"] = "보험 심사 요청에 실패했습니다.";
						
					echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
					exit;
				}
			} else {
				$retResult["result"] = "FAIL";
				$retResult["msg"] = "기사 등록에 실패했습니다.";
				
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
		} else if($type == "edit_driver") {
			$permissions = array("driver" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$driverId = $_POST["driverId"];
			$name = $_POST["name"];
			$phone = $_POST["phone"];
			$socialNumber = $_POST["socialNumber"];
			$licenseType = $_POST["licenseType"];
			$licenseNumber = $_POST["licenseNumber"];
			$email = $_POST["email"];
				
			$driver_manager->updateDriver($database->cardaridrivers, $driverId, $name, $phone, $socialNumber, $licenseType, $licenseNumber, $email);
		} else if($type == "update_driver_status") {
			$permissions = array("driver" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$driverId = $_POST["driverId"];
			$driverStatus = $_POST["driverStatus"];
				
			$driver_manager->updateDriverStatus($database->cardaridrivers, $driverId, $driverStatus);
		} else if($type == "driver_detail") {
			$permissions = array("driver" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$driverId = $_POST["driverId"];
			
			$retResult["data"] = $driver_manager->getDriverInfo($database->cardaridrivers, $driverId);
		} else if($type == "driver_balance_list") {
			$permissions = array("driver" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$driverId = $_POST["driverId"];
			
			$retResult["data"] = array();
			$retResult["data"]["balanceList"] = $driver_manager->getDriverBalanceList($database->_logdriverbalances, $driverId);
			$retResult["data"]["driverInfo"] = $driver_manager->getDriverInfo($database->cardaridrivers, $driverId);
		} else if($type == "add_balance") {
			$permissions = array("driver" => array("modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$driverId = $_POST["driverId"];
			$balance = $_POST["balance"];
			
			$ret = $driver_manager->updateDriverBalance($database->cardaridrivers, $database->_logdriverbalances, $driverId, $balance);
			if($ret['ok'] != 1) {
				$retResult["result"] = $ret["err"];
				$retResult["msg"] = $ret["errmsg"];
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
		} else if($type == "driver_history_list") {
			$permissions = array("driver" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
		
			$driverId = $_POST["driverId"];
				
			$retResult["data"] = $driver_manager->getDriverHistoryList($database->calls, $driverId);
		} else {
			$retResult["result"] = "PERMISSION_DENIED";
			$retResult["msg"] = "접근권한이 없습니다.";
				
			echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
			exit;
		}
	} catch (Exception $e) {
		$retResult["result"] = "ERROR";
		$retResult["msg"] = $e->getMessage();
	}
	
	echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
	exit;
?>
<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager", "CCrackdownRankMonthlyManager", "CCrackdownRankWeeklyManager"));
	
		$session = new CSession();
	
		$retResult = array("result" => "OK", "msg" => "SUCCESS", "data" => array());
	
		if(!$session->isLogin()) {
			$retResult["result"] = "NOT_LOGIN";
			$retResult["msg"] = "로그인 후 사용가능합니다.";
	
			echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
			exit;
		}
	
		$database_manager = new CDatabaseManager();
		$database = $database_manager->getDb();
		
		$crackdown_rank_monthly_manager = new CCrackdownRankMonthlyManager($database->crackdownrankmonthlies);
		$crackdown_rank_weekly_manager = new CCrackdownRankWeeklyManager($database->crackdownrankweeklies);
	
		$type = $_POST["type"];
		
		if($type == "getCrackdownRankList") {
			$permissions = array("crackdown_rank" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$rank_type = $_POST["rank_type"];
			
			$draw = $_POST["draw"];
			$columns = $_POST["columns"];
			$order = $_POST["order"];
			$start = $_POST["start"];
			$length = $_POST["length"];
			$search = isset($_POST["search"]) ? $_POST["search"] : "";
			
			$sort_direction = $order[0]['dir'] == "asc" ? 1 : -1;
			$sort = array($columns[$order[0]['column']]['data'] => $sort_direction);
			
			$find = array();
			$findItem = array();
			if(!empty($search['value'])) {
				$regex = new MongoRegex("/" . $search['value'] . "/");
				
				foreach ($columns as $row) {
					if($row["searchable"] == "true") {
						$findItem[] = array($row["data"] => $regex);
					}
				}
				
				$find = array('$or' => $findItem);
			}
			
			
			$totalCount = 0;
			$filteredTotalCount = 0;
			$crackdown_rankList = array();
			if($rank_type == "monthly") {
				$totalCount = $crackdown_rank_monthly_manager->getTotalCrackdownRankMonthlyCount();
				$filteredTotalCount = $crackdown_rank_monthly_manager->getTotalFilteredCrackdownRankMonthlyCount($find);
				$crackdown_rankList = $crackdown_rank_monthly_manager->getCrackdownRankMonthlyList($find, $start, $length, $sort);
			} else if($rank_type == "weekly") {
				$totalCount = $crackdown_rank_weekly_manager->getTotalCrackdownRankWeeklyCount();
				$filteredTotalCount = $crackdown_rank_weekly_manager->getTotalFilteredCrackdownRankWeeklyCount($find);
				$crackdown_rankList = $crackdown_rank_weekly_manager->getCrackdownRankWeeklyList($find, $start, $length, $sort);
			}
			
			$ret = array(
				"data" => $crackdown_rankList,
				"draw" => intval($draw),
				"recordsFiltered" => $filteredTotalCount,
				"recordsTotal" => $totalCount
			);
			
			echo json_encode($ret, JSON_UNESCAPED_UNICODE);
			exit;
		} else if($type == "add_crackdown_rank") {
			$permissions = array("crackdown_rank" => array("inquiry", "create"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$rank_type = $_POST["rank_type"];
			
			$month = isset($_POST["month"]) ? $_POST["month"] : "";
			$userId = isset($_POST["userId"]) ? $_POST["userId"] : "";
			$phone = isset($_POST["phone"]) ? $_POST["phone"] : "";
			$photoCount = isset($_POST["photoCount"]) ? $_POST["photoCount"] : 0;
			$normalCount = isset($_POST["normalCount"]) ? $_POST["normalCount"] : 0;
			$voiceCount = isset($_POST["voiceCount"]) ? $_POST["voiceCount"] : 0;
			
			$weekStartDate = isset($_POST["weekStartDate"]) ? $_POST["weekStartDate"] : "";
			$weekOfYear = isset($_POST["weekOfYear"]) ? $_POST["weekOfYear"] : 0;
			
			if($rank_type == "monthly") {
				$crackdown_rank_monthly_manager->addCrackdownRankMonthly($month, $userId, $phone, $photoCount, $normalCount, $voiceCount);
			} else if($rank_type == "weekly") {
				$crackdown_rank_weekly_manager->addCrackdownRankWeekly($weekStartDate, $weekOfYear, $userId, $phone, $photoCount, $normalCount, $voiceCount);
			}
		} else if($type == "edit_crackdown_rank") {
			$permissions = array("crackdown_rank" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$crackdown_rank_id = $_POST["crackdown_rank_id"];
			$rank_type = $_POST["rank_type"];
			
			$month = isset($_POST["month"]) ? $_POST["month"] : "";
			$userId = isset($_POST["userId"]) ? $_POST["userId"] : "";
			$phone = isset($_POST["phone"]) ? $_POST["phone"] : "";
			$photoCount = isset($_POST["photoCount"]) ? $_POST["photoCount"] : 0;
			$normalCount = isset($_POST["normalCount"]) ? $_POST["normalCount"] : 0;
			$voiceCount = isset($_POST["voiceCount"]) ? $_POST["voiceCount"] : 0;
			
			$weekStartDate = isset($_POST["weekStartDate"]) ? $_POST["weekStartDate"] : "";
			$weekOfYear = isset($_POST["weekOfYear"]) ? $_POST["weekOfYear"] : 0;
			
			if($rank_type == "monthly") {
				$crackdown_rank_monthly_manager->updateCrackdownRankMonthly($crackdown_rank_id, $month, $userId, $phone, $photoCount, $normalCount, $voiceCount);
			} else if($rank_type == "weekly") {
				$crackdown_rank_weekly_manager->updateCrackdownRankWeekly($crackdown_rank_id, $weekStartDate, $weekOfYear, $userId, $phone, $photoCount, $normalCount, $voiceCount);
			}
		} else if($type == "delete_crackdown_rank") {
			$permissions = array("crackdown_rank" => array("inquiry", "delete"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$crackdown_rank_id = $_POST["crackdown_rank_id"];
			$rank_type = $_POST["rank_type"];
				
			if($rank_type == "monthly") {
				$crackdown_rank_monthly_manager->deleteCrackdownRankMonthly($crackdown_rank_id);
			} else if($rank_type == "weekly") {
				$crackdown_rank_weekly_manager->deleteCrackdownRankWeekly($crackdown_rank_id);
			}
		} else if($type == "crackdown_rank_detail") {
			$permissions = array("crackdown_rank" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$crackdown_rank_id = $_POST["crackdown_rank_id"];
			$rank_type = $_POST["rank_type"];
			
			if($rank_type == "monthly") {
				$retResult["data"] = $crackdown_rank_monthly_manager->getCrackdownRankMonthlyInfo($crackdown_rank_id);
			} else if($rank_type == "weekly") {
				$retResult["data"] = $crackdown_rank_weekly_manager->getCrackdownRankWeeklyInfo($crackdown_rank_id);
			}
		}
	} catch (Exception $e) {
		$retResult["result"] = "ERROR";
		$retResult["msg"] = $e->getMessage();
	}
	
	echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
	exit;
?>
<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager", "CNoticeManager"));
	
		$session = new CSession();
	
		$retResult = array("result" => "OK", "msg" => "SUCCESS", "data" => array());
	
		if(!$session->isLogin()) {
			$retResult["result"] = "NOT_LOGIN";
			$retResult["msg"] = "로그인 후 사용가능합니다.";
	
			echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
			exit;
		}
	
		$database_manager = new CDatabaseManager();
		$database = $database_manager->getDb();
		
		$notice_manager = new CNoticeManager($database->usernotices);
	
		$type = $_POST["type"];
		
		if($type == "getNoticeList") {
			$permissions = array("notice" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$draw = $_POST["draw"];
			$columns = $_POST["columns"];
			$order = $_POST["order"];
			$start = $_POST["start"];
			$length = $_POST["length"];
			$search = isset($_POST["search"]) ? $_POST["search"] : "";
			
			$sort_direction = $order[0]['dir'] == "asc" ? 1 : -1;
			$sort = array($columns[$order[0]['column']]['data'] => $sort_direction);
			
			$find = array();
			$findItem = array();
			if(!empty($search['value'])) {
				$regex = new MongoRegex("/" . $search['value'] . "/");
				
				foreach ($columns as $row) {
					if($row["searchable"] == "true") {
						$findItem[] = array($row["data"] => $regex);
					}
				}
				
				$find = array('$or' => $findItem);
			}
			
			$totalCount = $notice_manager->getTotalNoticeCount();
			$filteredTotalCount = $notice_manager->getTotalFilteredNoticeCount($find);
			$noticeList = $notice_manager->getNoticeList($find, $start, $length, $sort);
			$ret = array(
				"data" => $noticeList,
				"draw" => intval($draw),
				"recordsFiltered" => $filteredTotalCount,
				"recordsTotal" => $totalCount
			);
			
			echo json_encode($ret, JSON_UNESCAPED_UNICODE);
			exit;
		} else if($type == "add_notice") {
			$permissions = array("notice" => array("inquiry", "create"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$form_data = isset($_POST["form_data"]) ? $_POST["form_data"] : null;
			if($form_data == null) {
				throw new Exception("업데이트 정보가 없습니다.");
			}
			
			$form_data = json_decode($form_data, true);
				
			$title = $form_data["title"];
			$contents = $form_data["contents"];
			$linkUrl = $form_data["linkUrl"];
			$startDate = $form_data["startDate"];
			$endDate = $form_data["endDate"];
			
			$imagePath = "";
			$orgImagePath = "";
			if(isset($_FILES['notice_image']) && !empty($_FILES['notice_image'])) {
				$imagePath = uploadFile($_FILES['notice_image']);
				$orgImagePath = $_FILES['notice_image']["name"];
			}
			
			$notice_manager->addNotice($title, $contents, $imagePath, $orgImagePath, $linkUrl, $startDate, $endDate);
		} else if($type == "edit_notice") {
			$permissions = array("notice" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$form_data = isset($_POST["form_data"]) ? $_POST["form_data"] : null;
			if($form_data == null) {
				throw new Exception("업데이트 정보가 없습니다.");
			}
				
			$form_data = json_decode($form_data, true);
			
			$notice_id = $form_data["notice_id"];
			$title = $form_data["title"];
			$contents = $form_data["contents"];
			$linkUrl = $form_data["linkUrl"];
			$startDate = $form_data["startDate"];
			$endDate = $form_data["endDate"];
			
			$imagePath = "";
			$orgImagePath = "";
			if(isset($_FILES['notice_image']) && !empty($_FILES['notice_image'])) {
				$imagePath = uploadFile($_FILES['notice_image']);
				$orgImagePath = $_FILES['notice_image']["name"];
			}
				
			$notice_manager->updateNotice($notice_id, $title, $contents, $imagePath, $orgImagePath, $linkUrl, $startDate, $endDate);
		} else if($type == "delete_notice") {
			$permissions = array("notice" => array("inquiry", "delete"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$notice_id = $_POST["notice_id"];
				
			$notice_manager->deleteNotice($notice_id);
		} else if($type == "notice_detail") {
			$permissions = array("notice" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$notice_id = $_POST["notice_id"];
			$retResult["data"] = $notice_manager->getNoticeInfo($notice_id);
		}
	} catch (Exception $e) {
		$retResult["result"] = "ERROR";
		$retResult["msg"] = $e->getMessage();
	}
	
	echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
	exit;

	function uploadFile($_files) {
		if(!startsWith($_files['type'], "image")) {
			throw new Exception("이미지 파일만 업로드 가능합니다.");
		}
	
		$file_ext = explode(".", basename($_files['name']));
		$upload_file_path = uniqid() . "." . $file_ext[1];
	
		if (move_uploaded_file($_files['tmp_name'], CONF_PATH_FILE_NOTICE . $upload_file_path)) {
			return CONF_URL_FILE_NOTICE . $upload_file_path;
		} else {
			throw new Exception("파일 업로드 중 오류가 발생 하였습니다.");
		}
	}
?>
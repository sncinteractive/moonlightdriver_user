<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager", "CCrackdownManager"));
	
		$session = new CSession();
	
		$retResult = array("result" => "OK", "msg" => "SUCCESS", "data" => array());
	
		if(!$session->isLogin()) {
			$retResult["result"] = "NOT_LOGIN";
			$retResult["msg"] = "로그인 후 사용가능합니다.";
	
			echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
			exit;
		}
	
		$database_manager = new CDatabaseManager();
		$database = $database_manager->getDb();
		
		$crackdown_manager = new CCrackdownManager($database->crackdowns);
	
		$type = $_POST["type"];
		
		if($type == "getCrackdownList") {
			$permissions = array("crackdown" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$draw = $_POST["draw"];
			$columns = $_POST["columns"];
			$order = $_POST["order"];
			$start = $_POST["start"];
			$length = $_POST["length"];
			$search = isset($_POST["search"]) ? $_POST["search"] : "";
			
			$sort_direction = $order[0]['dir'] == "asc" ? 1 : -1;
			$sort = array($columns[$order[0]['column']]['data'] => $sort_direction);
			
			$find = array();
			$findItem = array();
			if(!empty($search['value'])) {
				$regex = new MongoRegex("/" . $search['value'] . "/");
				
				foreach ($columns as $row) {
					if($row["searchable"] == "true") {
						$findItem[] = array($row["data"] => $regex);
					}
				}
				
				$find = array('$or' => $findItem);
			}
			
			$totalCount = $crackdown_manager->getTotalCrackdownCount();
			$filteredTotalCount = $crackdown_manager->getTotalFilteredCrackdownCount($find);
			$crackdownList = $crackdown_manager->getCrackdownList($find, $start, $length, $sort);
			$ret = array(
				"data" => $crackdownList,
				"draw" => intval($draw),
				"recordsFiltered" => $filteredTotalCount,
				"recordsTotal" => $totalCount
			);
			
			echo json_encode($ret, JSON_UNESCAPED_UNICODE);
			exit;
		} else if($type == "add_crackdown") {
			$permissions = array("crackdown" => array("inquiry", "create"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$phone = $_POST["phone"];
			$address = $_POST["address"];
			$sido = $_POST["sido"];
			$gugun = $_POST["gugun"];
			$lat = $_POST["lat"];
			$lng = $_POST["lng"];
			$reportType = $_POST["reportType"];
			
			$crackdown_manager->addCrackdown($database->users, $reportType, $phone, $sido, $gugun, $address, $lat, $lng);
		} else if($type == "edit_crackdown") {
			$permissions = array("crackdown" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$crackdown_id = $_POST["crackdown_id"];
			$phone = $_POST["phone"];
			$address = $_POST["address"];
			$sido = $_POST["sido"];
			$gugun = $_POST["gugun"];
			$lat = $_POST["lat"];
			$lng = $_POST["lng"];
			$reportType = $_POST["reportType"];
				
			$crackdown_manager->updateCrackdown($database->users, $crackdown_id, $reportType, $phone, $sido, $gugun, $address, $lat, $lng);
		} else if($type == "delete_crackdown") {
			$permissions = array("crackdown" => array("inquiry", "delete"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$crackdown_id = $_POST["crackdown_id"];
				
			$crackdown_manager->deleteCrackdown($crackdown_id);
		} else if($type == "crackdown_detail") {
			$permissions = array("crackdown" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$crackdown_id = $_POST["crackdown_id"];
			$retResult["data"] = $crackdown_manager->getCrackdownInfo($crackdown_id);
		} else if($type == "delete_selected_crackdown") {
			$permissions = array("crackdown" => array("inquiry", "delete"), "crackdown_blacklist" => array("inquiry", "create"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$delete_id_list = $_POST["delete_id_list"];
			$wrong_id_list = $_POST["wrong_id_list"];
			
			$crackdown_manager->deleteSelectedCrackdown($database->crackdownblacklists, $delete_id_list, $wrong_id_list);
		}
	} catch (Exception $e) {
		$retResult["result"] = "ERROR";
		$retResult["msg"] = $e->getMessage();
	}
	
	echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
	exit;

	function uploadFile($_files) {
		if(!startsWith($_files['type'], "image")) {
			throw new Exception("이미지 파일만 업로드 가능합니다.");
		}
	
		$file_ext = explode(".", basename($_files['name']));
		$upload_file_path = uniqid() . "." . $file_ext[1];
	
		if (move_uploaded_file($_files['tmp_name'], CONF_PATH_FILE_NOTICE . $upload_file_path)) {
			return CONF_URL_FILE_NOTICE . $upload_file_path;
		} else {
			throw new Exception("파일 업로드 중 오류가 발생 하였습니다.");
		}
	}
?>
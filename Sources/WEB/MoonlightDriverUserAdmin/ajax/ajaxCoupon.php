<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager", "CCouponManager"));
	
		$session = new CSession();
	
		$retResult = array("result" => "OK", "msg" => "SUCCESS", "data" => array());
	
		if(!$session->isLogin()) {
			$retResult["result"] = "NOT_LOGIN";
			$retResult["msg"] = "로그인 후 사용가능합니다.";
	
			echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
			exit;
		}
	
		$database_manager = new CDatabaseManager();
		$database = $database_manager->getDb();
		
		$couponManager = new CCouponManager($database->coupons, $database->couponpushes, $database->usercoupons);
	
		$type = $_POST["type"];
		
		if($type == "getCouponList") {
			$permissions = array("coupon" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$draw = $_POST["draw"];
			$columns = $_POST["columns"];
			$order = $_POST["order"];
			$start = $_POST["start"];
			$length = $_POST["length"];
			$search = isset($_POST["search"]) ? $_POST["search"] : "";
			
			$sort_direction = $order[0]['dir'] == "asc" ? 1 : -1;
			$sort = array($columns[$order[0]['column']]['data'] => $sort_direction);
			
			$find = array();
			$findItem = array();
			if(!empty($search['value'])) {
				$regex = new MongoRegex("/" . $search['value'] . "/");
				
				foreach ($columns as $row) {
					if($row["searchable"] == "true") {
						$findItem[] = array($row["data"] => $regex);
					}
				}
				
				$find = array('$or' => $findItem);
			}
			
			$totalCount = $couponManager->getTotalCouponCount();
			$filteredTotalCount = $couponManager->getTotalFilteredCouponCount($find);
			$couponList = $couponManager->getCouponList($find, $start, $length, $sort);
			$ret = array(
				"data" => $couponList,
				"draw" => intval($draw),
				"recordsFiltered" => $filteredTotalCount,
				"recordsTotal" => $totalCount
			);
			
			echo json_encode($ret, JSON_UNESCAPED_UNICODE);
			exit;
		} else if($type == "add_coupon") {
			$permissions = array("coupon" => array("inquiry", "create"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$form_data = isset($_POST["form_data"]) ? $_POST["form_data"] : null;
			if($form_data == null) {
				throw new Exception("업데이트 정보가 없습니다.");
			}
				
			$form_data = json_decode($form_data, true);
				
			$couponName = $form_data["couponName"];
			$couponDesc = $form_data["couponDesc"];
			$couponStartDate = $form_data["couponStartDate"];
			$couponEndDate = $form_data["couponEndDate"];
			$delYn = "N";
			
			$couponImgPath = "";
			$couponOrgImgPath = "";
			if(isset($_FILES['couponImage']) && !empty($_FILES['couponImage'])) {
				$couponImgPath = uploadFile($_FILES['couponImage']);
				$couponOrgImgPath = $_FILES['couponImage']["name"];
			}
				
			$couponManager->addCoupon($couponName, $couponDesc, $couponImgPath, $couponOrgImgPath, $couponStartDate, $couponEndDate, $delYn);
		} else if($type == "edit_coupon") {
			$permissions = array("coupon" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$form_data = isset($_POST["form_data"]) ? $_POST["form_data"] : null;
			if($form_data == null) {
				throw new Exception("업데이트 정보가 없습니다.");
			}
			
			$form_data = json_decode($form_data, true);
			
			$couponId = $form_data["couponId"];
			$couponName = $form_data["couponName"];
			$couponDesc = $form_data["couponDesc"];
			$couponStartDate = $form_data["couponStartDate"];
			$couponEndDate = $form_data["couponEndDate"];
				
			$couponImgPath = "";
			$couponOrgImgPath = "";
			if(isset($_FILES['couponImage']) && !empty($_FILES['couponImage'])) {
				$couponImgPath = uploadFile($_FILES['couponImage']);
				$couponOrgImgPath = $_FILES['couponImage']["name"];
			}
			
			
			$couponManager->updateCoupon($couponId, $couponName, $couponDesc, $couponImgPath, $couponOrgImgPath, $couponStartDate, $couponEndDate);
		} else if($type == "delete_coupon") {
			$permissions = array("coupon" => array("inquiry", "delete"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$couponId = $_POST["couponId"];
				
			$couponManager->deleteCoupon($couponId);
		} else if($type == "coupon_detail") {
			$permissions = array("coupon" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$couponId = $_POST["couponId"];
			
			$retResult["data"] = $couponManager->getCouponInfo($couponId);
		} else if($type == "select_coupon_list") {
			$permissions = array("coupon" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$retResult["data"] = $couponManager->getSelectCouponList();
		}
	} catch (Exception $e) {
		$retResult["result"] = "ERROR";
		$retResult["msg"] = $e->getMessage();
	}
	
	echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
	exit;

	function uploadFile($_files) {
		if(!startsWith($_files['type'], "image")) {
			throw new Exception("이미지 파일만 업로드 가능합니다.");
		}
	
		$file_ext = explode(".", basename($_files['name']));
		$upload_file_path = uniqid() . "." . $file_ext[1];
	
		if (move_uploaded_file($_files['tmp_name'], CONF_PATH_FILE_COUPON . $upload_file_path)) {
			return CONF_URL_FILE_COUPON . $upload_file_path;
		} else {
			throw new Exception("파일 업로드 중 오류가 발생 하였습니다.");
		}
	}
?>
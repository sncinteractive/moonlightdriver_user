<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager", "CCompanyManager"));
	
		$session = new CSession();
	
		$retResult = array("result" => "OK", "msg" => "SUCCESS", "data" => array());
	
		if(!$session->isLogin()) {
			$retResult["result"] = "NOT_LOGIN";
			$retResult["msg"] = "로그인 후 사용가능합니다.";
	
			echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
			exit;
		}
	
		$database_manager = new CDatabaseManager();
		$database = $database_manager->getDb();
		
		$company_manager = new CCompanyManager();
	
		$type = $_POST["type"];
		
		if($type == "getCompanyList") {
			$permissions = array("company" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$selected_region = $_POST["selected_region"];
			$draw = $_POST["draw"];
			$columns = $_POST["columns"];
			$order = $_POST["order"];
			$start = $_POST["start"];
			$length = $_POST["length"];
			$search = isset($_POST["search"]) ? $_POST["search"] : "";
			
			$sort_direction = $order[0]['dir'] == "asc" ? 1 : -1;
			$sort = array($columns[$order[0]['column']]['data'] => $sort_direction);
			
			$find = array();
			$findItem = array();
			if(!empty($search['value'])) {
				$regex = new MongoRegex("/" . $search['value'] . "/");
				
				foreach ($columns as $row) {
					if($row["searchable"] == "true") {
						$findItem[] = array($row["data"] => $regex);
					}
				}
				
				$find = array('$or' => $findItem);
			}
			
			$find['$and'] = array(array("region" => $selected_region));
			
			$totalCount = $database->chauffeurcompanies->count();
			$filteredTotalCount = $database->chauffeurcompanies->count($find);
			$companyList = $company_manager->getCompanyList($database->chauffeurcompanies, $find, $start, $length, $sort);
			$ret = array(
				"data" => $companyList,
				"draw" => intval($draw),
				"recordsFiltered" => $filteredTotalCount,
				"recordsTotal" => $totalCount
			);
			
			echo json_encode($ret, JSON_UNESCAPED_UNICODE);
			exit;
		} else if($type == "add_company") {
			$permissions = array("company" => array("inquiry", "create"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$companyName = $_POST["companyName"];
			$telNumber = $_POST["telNumber"];
			$region = $_POST["region"];
			$price = $_POST["price"];
			$stopByFee = $_POST["stopByFee"];
				
			$company_manager->addCompany($database->chauffeurcompanies, $companyName, $telNumber, $region, $price, $stopByFee);
		} else if($type == "edit_company") {
			$permissions = array("company" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$company_id = $_POST["company_id"];
			$companyName = $_POST["companyName"];
			$telNumber = $_POST["telNumber"];
			$region = $_POST["region"];
			$price = $_POST["price"];
			$stopByFee = $_POST["stopByFee"];
				
			$company_manager->updateCompany($database->chauffeurcompanies, $company_id, $companyName, $telNumber, $region, $price, $stopByFee);
		} else if($type == "delete_company") {
			$permissions = array("company" => array("inquiry", "delete"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$company_id = $_POST["company_id"];
				
			$company_manager->deleteCompany($database->chauffeurcompanies, $company_id);
		} else if($type == "company_detail") {
			$permissions = array("company" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$company_id = $_POST["company_id"];
			$retResult["data"] = $company_manager->getCompanyInfo($database->chauffeurcompanies, $company_id);
		}
	} catch (Exception $e) {
		$retResult["result"] = "ERROR";
		$retResult["msg"] = $e->getMessage();
	}
	
	echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
	exit;

?>
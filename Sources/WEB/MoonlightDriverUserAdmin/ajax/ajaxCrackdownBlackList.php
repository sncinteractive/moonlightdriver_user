<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager", "CCrackdownBlackListManager"));
	
		$session = new CSession();
	
		$retResult = array("result" => "OK", "msg" => "SUCCESS", "data" => array());
	
		if(!$session->isLogin()) {
			$retResult["result"] = "NOT_LOGIN";
			$retResult["msg"] = "로그인 후 사용가능합니다.";
	
			echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
			exit;
		}
	
		$database_manager = new CDatabaseManager();
		$database = $database_manager->getDb();
		
		$crackdown_blacklist_manager = new CCrackdownBlackListManager($database->crackdownblacklists);
	
		$type = $_POST["type"];
		
		if($type == "getCrackdownBlackListList") {
			$permissions = array("crackdown_blacklist" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$draw = $_POST["draw"];
			$columns = $_POST["columns"];
			$order = $_POST["order"];
			$start = $_POST["start"];
			$length = $_POST["length"];
			$search = isset($_POST["search"]) ? $_POST["search"] : "";
			
			$sort_direction = $order[0]['dir'] == "asc" ? 1 : -1;
			$sort = array($columns[$order[0]['column']]['data'] => $sort_direction);
			
			$find = array();
			$findItem = array();
			if(!empty($search['value'])) {
				$regex = new MongoRegex("/" . $search['value'] . "/");
				
				foreach ($columns as $row) {
					if($row["searchable"] == "true") {
						$findItem[] = array($row["data"] => $regex);
					}
				}
				
				$find = array('$or' => $findItem);
			}
			
			$totalCount = $crackdown_blacklist_manager->getTotalCrackdownBlackListCount();
			$filteredTotalCount = $crackdown_blacklist_manager->getTotalFilteredCrackdownBlackListCount($find);
			$crackdownBlackListList = $crackdown_blacklist_manager->getCrackdownBlackListList($find, $start, $length, $sort);
			$ret = array(
				"data" => $crackdownBlackListList,
				"draw" => intval($draw),
				"recordsFiltered" => $filteredTotalCount,
				"recordsTotal" => $totalCount
			);
			
			echo json_encode($ret, JSON_UNESCAPED_UNICODE);
			exit;
		} else if($type == "delete_crackdown_blacklist") {
			$permissions = array("crackdown_blacklist" => array("inquiry", "delete"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$userId = $_POST["user_id"];
				
			$crackdown_blacklist_manager->deleteCrackdownBlackList($userId);
		} else if($type == "update_crackdown_block_state") {
			$permissions = array("crackdown_blacklist" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$userId = $_POST["user_id"];
			$blockState = $_POST["block_state"];
			
			$crackdown_blacklist_manager->updateCrackdownBlockState($userId, $blockState);
		}
	} catch (Exception $e) {
		$retResult["result"] = "ERROR";
		$retResult["msg"] = $e->getMessage();
	}
	
	echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
	exit;
?>
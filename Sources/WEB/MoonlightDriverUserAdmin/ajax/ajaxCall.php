<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager", "CCallManager", "CUserManager"));
	
		$session = new CSession();
	
		$retResult = array("result" => "OK", "msg" => "SUCCESS", "data" => array());
	
		if(!$session->isLogin()) {
			$retResult["result"] = "NOT_LOGIN";
			$retResult["msg"] = "로그인 후 사용가능합니다.";
	
			echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
			exit;
		}
	
		$database_manager = new CDatabaseManager();
		$database = $database_manager->getDb();
		
		$call_manager = new CCallManager($database->calls);
		$user_manager = new CUserManager($database->users);
	
		$type = $_POST["type"];
		
		if($type == "getCallList") {
			$permissions = array("call" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$draw = $_POST["draw"];
			$columns = $_POST["columns"];
			$order = $_POST["order"];
			$start = $_POST["start"];
			$length = $_POST["length"];
			$search = isset($_POST["search"]) ? $_POST["search"] : "";
			
			$sort_direction = $order[0]['dir'] == "asc" ? 1 : -1;
			$sort = array($columns[$order[0]['column']]['data'] => $sort_direction);
			
			$find = array();
			$findItem = array();
			if(!empty($search['value'])) {
				$regex = new MongoRegex("/" . $search['value'] . "/");
				
				foreach ($columns as $row) {
					if($row["searchable"] == "true") {
						$findItem[] = array($row["data"] => $regex);
					}
				}
				
				$find = array('$or' => $findItem);
			}
			
			$totalCount = $call_manager->getTotalCallCount();
			$filteredTotalCount = $call_manager->getTotalFilteredCallCount($find);
			$callList = $call_manager->getCallList($find, $start, $length, $sort);
			$ret = array(
				"data" => $callList,
				"draw" => intval($draw),
				"recordsFiltered" => $filteredTotalCount,
				"recordsTotal" => $totalCount
			);
			
			echo json_encode($ret, JSON_UNESCAPED_UNICODE);
			exit;
		} else if($type == "add_call") {
			$permissions = array("call" => array("inquiry", "create"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$phone = $_POST["phone"];
			
			$userInfo = $user_manager->getUserInfoByPhoneNumber($phone);
			
			$callSeq = getNextSequence($database->counters, "call_seq");
			$callType = $_POST["callType"];
			$money = $_POST["money"];
			$payment_option = $_POST["payment_option"];
			$etc = isset($_POST["etc"]) ? $_POST["etc"] : "";
			$status = $_POST["status"];
			$user = array(
				"userId" => isset($userInfo) ? $userInfo["user_id"] : "",
				"phone" => $_POST["phone"],
				"address" => isset($_POST["user_address"]) ? $_POST["user_address"] : "",
				"city_do" => isset($_POST["user_city_do"]) ? $_POST["user_city_do"] : "",
				"gu_gun" => isset($_POST["user_gu_gun"]) ? $_POST["user_gu_gun"] : "",
				"dong" => isset($_POST["user_dong"]) ? $_POST["user_dong"] : "",
				"bunji" => isset($_POST["user_bunji"]) ? $_POST["user_bunji"] : "",
				"location" => array(
					"type" => "Point",
					"coordinates" => array(isset($_POST["user_lng"]) ? floatval($_POST["user_lng"]) : 0, isset($_POST["user_lat"]) ? floatval($_POST["user_lat"]) : 0)
				)
			);
			$start = array(
				"address" => $_POST["start_address"],
				"poi_name" => $_POST["start_poi_name"],
				"location" => array(
					"type" => "Point",
					"coordinates" => array(floatval($_POST["start_lng"]), floatval($_POST["start_lat"]))
				)
			);
			$end = array(
				"address" => $_POST["end_address"],
				"poi_name" => $_POST["end_poi_name"],
				"location" => array(
					"type" => "Point",
					"coordinates" => array(floatval($_POST["end_lng"]), floatval($_POST["end_lat"]))
				)
			);
			$through = array(
				"address" => isset($_POST["through_address"]) ? $_POST["through_address"] : "",
				"poi_name" => isset($_POST["through_poi_name"]) ? $_POST["through_poi_name"] : "",
				"location" => array(
					"type" => "Point",
					"coordinates" => array(isset($_POST["through_lng"]) ? floatval($_POST["through_lng"]) : 0, isset($_POST["through_lat"]) ? floatval($_POST["through_lat"]) : 0)
				)
			);
			$through1 = array(
				"address" => "",
				"poi_name" => "",
				"location" => array(
					"type" => "Point",
					"coordinates" => array()
				)
			);
			$through2 = array(
				"address" => "",
				"poi_name" => "",
				"location" => array(
					"type" => "Point",
					"coordinates" => array()
				)
			);
			
			$driver = array();
				
			$call_manager->addCall($callSeq, $callType, $money, $payment_option, $etc, $status, $driver, $user, $start, $end, $through, $through1, $through2);
		} else if($type == "edit_call") {
			$permissions = array("call" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$callId = $_POST["callId"];
			$phone = $_POST["phone"];
			
			$userInfo = $user_manager->getUserInfoByPhoneNumber($phone);
			
			$callType = $_POST["callType"];
			$money = $_POST["money"];
			$payment_option = $_POST["payment_option"];
			$etc = isset($_POST["etc"]) ? $_POST["etc"] : "";
			$status = $_POST["status"];
			$user = array(
				"userId" => isset($userInfo) ? $userInfo["user_id"] : "",
				"phone" => $_POST["phone"],
				"address" => isset($_POST["user_address"]) ? $_POST["user_address"] : "",
				"city_do" => isset($_POST["user_city_do"]) ? $_POST["user_city_do"] : "",
				"gu_gun" => isset($_POST["user_gu_gun"]) ? $_POST["user_gu_gun"] : "",
				"dong" => isset($_POST["user_dong"]) ? $_POST["user_dong"] : "",
				"bunji" => isset($_POST["user_bunji"]) ? $_POST["user_bunji"] : "",
				"location" => array(
					"type" => "Point",
					"coordinates" => array(isset($_POST["user_lng"]) ? floatval($_POST["user_lng"]) : 0, isset($_POST["user_lat"]) ? floatval($_POST["user_lat"]) : 0)
				)
			);
			$start = array(
				"address" => $_POST["start_address"],
				"poi_name" => $_POST["start_poi_name"],
				"location" => array(
					"type" => "Point",
					"coordinates" => array(floatval($_POST["start_lng"]), floatval($_POST["start_lat"]))
				)
			);
			$end = array(
				"address" => $_POST["end_address"],
				"poi_name" => $_POST["end_poi_name"],
				"location" => array(
					"type" => "Point",
					"coordinates" => array(floatval($_POST["end_lng"]), floatval($_POST["end_lat"]))
				)
			);
			$through = array(
				"address" => isset($_POST["through_address"]) ? $_POST["through_address"] : "",
				"poi_name" => isset($_POST["through_poi_name"]) ? $_POST["through_poi_name"] : "",
				"location" => array(
					"type" => "Point",
					"coordinates" => array(isset($_POST["through_lng"]) ? floatval($_POST["through_lng"]) : 0, isset($_POST["through_lat"]) ? floatval($_POST["through_lat"]) : 0)
				)
			);
				
			$call_manager->updateCall($callId, $callType, $money, $payment_option, $etc, $status, $user, $start, $end, $through);
		} else if($type == "delete_call") {
			$permissions = array("call" => array("inquiry", "delete"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$callId = $_POST["callId"];
				
			$call_manager->deleteCall($callId);
		} else if($type == "call_detail") {
			$permissions = array("call" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$callId = $_POST["callId"];
			
			$retResult["data"] = $call_manager->getCallInfo($callId);
		} else if($type == "user_detail") {
			$permissions = array("user" => array("inquiry"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
				exit;
			}
				
			$phone = $_POST["phone"];
				
			$retResult["data"] = $user_manager->getUserListByPhoneNumberReg($phone);
		} else if($type == "peaktime_list") {
			$retResult["data"] = $call_manager->getPeaktimeList($database->peaktimes);
		} else if($type == "add_peaktime") {
			$startDate = $_POST["startDate"];
			$endDate = $_POST["endDate"];
			$retResult["data"] = $call_manager->addPeaktime($database->peaktimes, $startDate, $endDate);
		} else if($type == "delete_peaktime") {
			$peaktimeId = $_POST["peaktimeId"];
			$call_manager->deletePeaktime($database->peaktimes, $peaktimeId);
		}
	} catch (Exception $e) {
		$retResult["result"] = "ERROR";
		$retResult["msg"] = $e->getMessage();
	}
	
	echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
	exit;

?>
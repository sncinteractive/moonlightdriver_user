var list_table;
var listCountPerPage = 10;

$(document).ready(function() {
	list_table = $('#driver_list').DataTable({
		initComplete: function () {
			
		},
		drawCallback: function() {
			$('#driver_list > tbody').off("click").on('click', 'tr', function (event) {
				var index = $(this).index("#driver_list > tbody > tr");
				
				if($(event.target).is('#driver_list > tbody > tr:eq(' + index + ') > td:eq(0),#driver_list > tbody > tr:eq(' + index + ') > td:eq(1),#driver_list > tbody > tr:eq(' + index + ') > td:eq(2),#driver_list > tbody > tr:eq(' + index + ') > td:eq(4),#driver_list > tbody > tr:eq(' + index + ') > td:eq(5),#driver_list > tbody > tr:eq(' + index + ') > td:eq(6),#driver_list > tbody > tr:eq(' + index + ') > td:eq(7)')) {
					var row_data = list_table.row(this).data();
					
					if(row_data) {
						getDriverDetail(row_data.driverId);
					}
				}
			});

			$(".btnBlock").off("click").on("click", function() {
				var ret = confirm("사용중지 하시겠습니까?");
				if(ret) {
					var row_data = list_table.row($(this).parent("td").parent("tr")).data();
					
					if(row_data) {
						updateDriverStatus(row_data.driverId, "block");
					}
				}
			});
			
			$(".btnSuccess").off("click").on("click", function() {
				var ret = confirm("사용 가능하도록 하시겠습니까?");
				if(ret) {
					var row_data = list_table.row($(this).parent("td").parent("tr")).data();
					
					if(row_data) {
						updateDriverStatus(row_data.driverId, "success");
					}
				}
			});
			
			$(".btnBalance").off("click").on("click", function() {
				var row_data = list_table.row($(this).parent("td").parent("tr")).data();
				getDriverBalanceList(row_data.driverId);
			});
			
			$(".btnHistory").off("click").on("click", function() {
				var row_data = list_table.row($(this).parent("td").parent("tr")).data();
				getDriverHistoryList(row_data.driverId);
			});
		},
		columns: [
		    { data: "driverId", visible: false, orderable: false, searchable: false },
		    { data: "createdDate" },
		    { data: "name" },
		    { data: "phone" },
			{ data: "balance", class: "text-right" },
			{ data: "driverStatus" },
			{ data: "insuranceStatus" },
			{ data: "lastLoginDate" },
			{ data: "availableDate" },
			{ data: "btnHistory" },
			{ data: "btnBlock" }
		],
        serverSide: true,
		ajax: {
			url :"/ajax/ajaxDriver.php", // json datasource
			type: "post",  // type of method  , by default would be get
			data: function (d) {
				d.type = "getDriverList";
			},
			error: function() {  // error handling code
			}
		},
		order: [[ 1, "desc" ]]
	});
	
	$('#btnCreateDriver').off("click").on('click', function() {
		showDriverDetailPopup(null);
	});
});

function getDriverDetail(_driverId) {
	var params = {};
	params.type = "driver_detail";
	params.driverId = _driverId;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxDriver.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			showDriverDetailPopup(ret_data.data);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function showDriverDetailPopup(_data) {
	var mode = _data ? "edit" : "create";
	
	if(mode == "edit") {
		$("#modal_popup_label").text("기사 상세보기");
	} else {
		$("#modal_popup_label").text("기사 등록");
	}
	
	$("#modal_popup_content").html(getDriverContentsHtml(mode));
	
	var footerHtml = "";
	
	if(mode == "edit") {
		$("#name").val(_data.name);
		$("#socialNumber").val(_data.socialNumber);
		$("#phone").val(_data.phone);
		$("#licenseType").val(_data.licenseType);
		$("#licenseNumber").val(_data.licenseNumber);
		$("#email").val(_data.email);
		$("#driverStatus").val(_data.driverStatus);
		$("#insuranceStatus").val(_data.insuranceStatus);
		$("#apiDriverId").val(_data.apiDriverId);
		$("#balance").val(_data.balance);
		
		footerHtml += "<button type='button' id='btnEdit' class='btn btn-primary'>수정</button>";
	} else {
		footerHtml += "<button type='button' id='btnAdd' class='btn btn-primary'>등록</button>";
	}
	
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	$("#modal_popup_footer").html(footerHtml);
	
	$("#modal_popup > .modal-dialog").removeClass("modal-lg").removeClass('modal-xlg');
	$("#modal_popup").modal('show');
	
	if(mode == "edit") {
		$("#btnEdit").off("click").on("click", function() {
			editDriver(_data);
		});
	} else {
		$("#btnAdd").off("click").on("click", function() {
			addDriver();
		});
	}
}

function getDriverContentsHtml(_mode) {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form class='form-horizontal'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='name'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>이름</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='name'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='socialNumber'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>주민번호</label>";
	html += "			<div class='col-sm-8'><input type='password' class='form-control' id='socialNumber' maxLength=13/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='phone'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>휴대폰</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='phone'></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='email'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>면허종류</label></label>";
	html += "			<div class='col-sm-8'>";
	html += "				<select id='licenseType'>";
	html += "					<option value='1종보통'>1종보통</option>";
	html += "					<option value='2종보통'>2종보통</option>";
	html += "					<option value='2종자동'>2종자동</option>";
	html += "					<option value='대형면허'>대형면허</option>";
	html += "					<option value='특수면허'>특수면허</option>";
	html += "				</select>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='licenseNumber'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>면허번호</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='licenseNumber'></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='email'>이메일</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='email'></div>";
	html += "		</div>";
	
	if(_mode == "edit") {
		html += "		<div class='form-group form-group-md'>";
		html += "			<label class='col-sm-3 control-label' for='driverStatus'>상태</label>";
		html += "			<div class='col-sm-8'><input type='text' class='form-control' id='driverStatus' readonly></div>";
		html += "		</div>";
		html += "		<div class='form-group form-group-md'>";
		html += "			<label class='col-sm-3 control-label' for='insuranceStatus'>보험상태</label>";
		html += "			<div class='col-sm-8'><input type='text' class='form-control' id='insuranceStatus' readonly></div>";
		html += "		</div>";
		html += "		<div class='form-group form-group-md'>";
		html += "			<label class='col-sm-3 control-label' for='api_driverId'>보험사 기사 ID</label>";
		html += "			<div class='col-sm-8'><input type='text' class='form-control' id='api_driverId' readonly></div>";
		html += "		</div>";
		html += "		<div class='form-group form-group-md'>";
		html += "			<label class='col-sm-3 control-label' for='insureNumber'>보험번호</label>";
		html += "			<div class='col-sm-8'><input type='text' class='form-control' id='insureNumber' readonly></div>";
		html += "		</div>";
		html += "		<div class='form-group form-group-md'>";
		html += "			<label class='col-sm-3 control-label' for='balance'>적립금</label>";
		html += "			<div class='col-sm-8'><input type='text' class='form-control' id='balance' readonly></div>";
		html += "		</div>";
	}
	
	html += "	</form>";
	html += "</div>";
	
	return html;
}

function showDriverBalancePopup(data) {
	$("#modal_popup_label").text("기사 적립금 내역");	
	$("#modal_popup_content").html(getDriverBalanceHtml(data.driverInfo));
	$("#modal_popup_footer").html("<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>");
	$("#modal_popup > .modal-dialog").removeClass('modal-xlg').addClass("modal-lg");
	$("#modal_popup").modal('show');
	
	var listTable = $('#balanceList').DataTable({
		data : data.balanceList,
		columns: [
		    { 
		    	data: "driverId",
		    	visible: false,
		    	orderable: false,
		    	searchable: false
		    },
		    { 
		    	data: "createdTime",
		    	class: "text-center"
		    },
		    { 
		    	data: "balance",
		    	render: function ( data, type, row, meta ) {
		    		if(row.orgBalance > 0) {
		    			return "<span style='color: red;'>" + data + "</span>";
		    		} else if(row.orgBalance < 0) {
		    			return "<span style='color: blue;'>" + data + "</span>";
		    		} else {
		    			return data;
		    		}
		    		
	    	    },
		    	class: "text-center"
		    },
		    {
		    	data: "afterBalance",
		    	class: "text-center"
		    },
			{
		    	data: "reason",
		    	class: "text-center"
		    }
		],
		order: [[ 1, "desc" ]]
	});
	
	$("#btnAddBalance").off("click").on("click", function() {
		var addBalance = $.trim($("#addBalance").val());
		if(addBalance.length <= 0 || addBalance == 0) {
			alert("추가할 적립금을 입력해 주세요.");
			return;
		}
		
		var driverId = $(this).attr("data-driverId");
		var driverName = $(this).attr("data-driverName");
		var ret = confirm(driverName + "기사님에게 적립금 " + addBalance + "원을 추가 적립 하시겠습니까?");
		if(ret) {
			balanceAdd(driverId, addBalance);
		}
	});
}

function balanceAdd(driverId, balance) {
	var params = {};
	params.type = "add_balance";
	params.driverId = driverId;
	params.balance = balance;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxDriver.php", params, function(_ret_data) {
		$(".loading").hide();
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("적립 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function getDriverBalanceList(driverId) {
	var params = {};
	params.type = "driver_balance_list";
	params.driverId = driverId;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxDriver.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			showDriverBalancePopup(ret_data.data);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function getDriverBalanceHtml(driverInfo) {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
	html += "		<form class='form-horizontal'>";
	html += "			<div class='form-group form-group-md'>";
	html += "				<div class='col-lg-2 col-md-2 col-sm-2 text-center' style='line-height: 30px;'>적립금 추가 : </div>";
	html += "				<div class='col-lg-2 col-md-2 col-sm-2'><input type='text' class='form-control' id='addBalance'/></div>";
	html += "				<div class='col-lg-2 col-md-2 col-sm-2'><button type='button' class='btn btn-primary' id='btnAddBalance' data-driverId='" + driverInfo.driverId + "' data-driverName='" + driverInfo.name + "'>적립</button></div>";
	html += "			</div>";
	html += "			<table id='balanceList' class='table table-bordered table-hover'>";
	html += "				<thead>";
	html += "					<tr>";
	html += "						<th>driver_id</th>";
	html += "						<th>날짜</th>";
	html += "						<th>입/출금</th>";
	html += "						<th>적립금 잔액</th>";
	html += "						<th>적요</th>";
	html += "					</tr>";
	html += "				</thead>";
	html += "			</table>";
	html += "		</form>";
	html += "	</div>";
	html += "</div>";
	
	return html;
}

function getDriverHistoryList(driverId) {
	var params = {};
	params.type = "driver_history_list";
	params.driverId = driverId;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxDriver.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			showDriverHistoryPopup(ret_data.data);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function showDriverHistoryPopup(data) {
	$("#modal_popup_label").text("기사 운행 내역");	
	$("#modal_popup_content").html(getDriverHistoryHtml());
	$("#modal_popup_footer").html("<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>");
	$("#modal_popup > .modal-dialog").addClass("modal-xlg")
	$("#modal_popup").modal('show');
	
	var listTable = $('#historyList').DataTable({
		data : data,
		columns: [
		    { 
		    	data: "callId",
		    	visible: false,
		    	orderable: false,
		    	searchable: false
		    },
		    { 
		    	data: "createdTime",
		    	class: "text-center"
		    },
		    { 
		    	data: "userPhone",
		    	class: "text-center",
		    	render: function ( data, type, row, meta ) {
		    		return row.user.phone;
	    	    }
		    },
		    {
		    	data: "start",
		    	class: "text-center",
		    	render: function ( data, type, row, meta ) {
		    		return row.start.address;
	    	    }
		    },
			{
		    	data: "end",
		    	class: "text-center",
		    	render: function ( data, type, row, meta ) {
		    		return row.end.address;
	    	    }
		    },
			{
		    	data: "money",
		    	class: "text-center",
		    	render: function ( data, type, row, meta ) {
		    		return data + "원";
	    	    }
		    },
			{
		    	data: "paymentOption",
		    	class: "text-center",
		    	render: function ( data, type, row, meta ) {
		    		if(data == "cash") {
		    			return "현금결제";
		    		} else if(data == "card") {
		    			return "카드결제";
		    		} else {
		    			return "";
		    		}
	    	    }
		    },
			{
		    	data: "premiums",
		    	class: "text-center",
		    	render: function ( data, type, row, meta ) {
		    		return data + "원";
	    	    }
		    },
			{
		    	data: "apiDrivingId",
		    	class: "text-center"
		    }
		],
		order: [[ 1, "desc" ]]
	});
}

function getDriverHistoryHtml() {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
	html += "		<form class='form-horizontal'>";
	html += "			<table id='historyList' class='table table-bordered table-hover'>";
	html += "				<thead>";
	html += "					<tr>";
	html += "						<th>callId</th>";
	html += "						<th>날짜</th>";
	html += "						<th>고객번호</th>";
	html += "						<th>출발지</th>";
	html += "						<th>도착지</th>";
	html += "						<th>금액</th>";
	html += "						<th>결제방법</th>";
	html += "						<th>보험료</th>";
	html += "						<th>보험사 운행번호</th>";
	html += "					</tr>";
	html += "				</thead>";
	html += "			</table>";
	html += "		</form>";
	html += "	</div>";
	html += "</div>";
	
	return html;
}

function editDriver(_driver_info) {
	var params = {};
	params.type = "edit_driver";
	params.driverId = _driver_info.driverId;
	params.name = $.trim($("#name").val());
	params.socialNumber = $.trim($("#socialNumber").val());
	params.phone = $.trim($("#phone").val());
	params.licenseType = $.trim($("#licenseType").val());
	params.licenseNumber = $.trim($("#licenseNumber").val());
	params.email = $.trim($("#email").val());
	
	if(params.name.length <= 0) {
		alert("이름을 입력해주세요.");
		$("#name").focus();
		return;
	}
	
	if(params.socialNumber.length <= 0) {
		alert("주민번호를 입력해주세요.");
		$("#socialNumber").focus();
		return;
	}

	if(params.phone.length <= 0) {
		alert("휴대폰 번호를 입력해주세요.");
		$("#phone").focus();
		return;
	}
	
	if(params.licenseType.length <= 0) {
		alert("면허종류를 입력해주세요.");
		$("#licenseType").focus();
		return;
	}
	
	if(params.licenseNumber.length <= 0) {
		alert("면허번호를 입력해주세요.");
		$("#licenseNumber").focus();
		return;
	}
	
	$(".loading").show();
	
	$.post("/ajax/ajaxDriver.php", params, function(_ret_data) {
		$(".loading").hide();
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("수정 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function addDriver() {
	var params = {};
	params.type = "add_driver";
	params.name = $.trim($("#name").val());
	params.socialNumber = $.trim($("#socialNumber").val());
	params.phone = $.trim($("#phone").val());
	params.licenseType = $.trim($("#licenseType").val());
	params.licenseNumber = $.trim($("#licenseNumber").val());
	params.email = $.trim($("#email").val());
	
	if(params.name.length <= 0) {
		alert("이름을 입력해주세요.");
		$("#name").focus();
		return;
	}
	
	if(params.socialNumber.length <= 0) {
		alert("주민번호를 입력해주세요.");
		$("#socialNumber").focus();
		return;
	}

	if(params.phone.length <= 0) {
		alert("휴대폰 번호를 입력해주세요.");
		$("#phone").focus();
		return;
	}
	
	if(params.licenseType.length <= 0) {
		alert("면허종류를 입력해주세요.");
		$("#licenseType").focus();
		return;
	}
	
	if(params.licenseNumber.length <= 0) {
		alert("면허번호를 입력해주세요.");
		$("#licenseNumber").focus();
		return;
	}
	
	$(".loading").show();
	
	$.post("/ajax/ajaxDriver.php", params, function(_ret_data) {
		$(".loading").hide();
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("등록 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function updateDriverStatus(_driverId, _status) {
	var params = {};
	params.type = "update_driver_status";
	params.driverId = _driverId;
	params.driverStatus = _status;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxDriver.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("수정 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function showSendPointPopup() {
	$("#modal_popup_label").text("적립금 지급");
	
	$("#modal_popup_content").html(getSendPointContentHtml());
	
	var footerHtml = "";
	footerHtml += "<button type='button' id='btnSend' class='btn btn-primary'>지급</button>";
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>취소</button>";
	
	$("#modal_popup_footer").html(footerHtml);
	
	$("#btnSend").off("click").on("click", function() {
		$('#sendPointPushForm').submit();
	});
	
	$("#recv_user").text(selected_recv_user.join());
	$("#selected_recv_user_container").show();
	$("#all_recv_user_container").hide();
	
	$("#modal_popup").modal('show');
	
	$("#selectRecvUserList").off("change").on("change", function() {
		var selectedType = $(this).val();
		if(selectedType == "0") {
			$("#selected_recv_user_container").show();
			$("#all_recv_user_container").hide();
		} else if(selectedType == "1") {
			$("#selected_recv_user_container").hide();
			$("#all_recv_user_container").show();
		}
	});
	
	$("#sendPushYnTemp").off("change").on("change", function() {
		var sendPushYn = $(this).prop("checked") ? "Y" : "N";
		$("#sendPushYn").val(sendPushYn);
	});
	
	$('#sendPointPushForm').ajaxForm({
		url: "/ajax/ajaxUser.php",
		dataType:  'json',
		type: "POST",
		beforeSubmit: function (data, form, option) {
			var point = $.trim($("#point").val());
			var push_message = $.trim($("#push_message").val());
			var recv_user = $.trim($("#recv_user").val());
			var recv_user_type = $.trim($("#recv_user_type").val());
			var selectRecvUserList = $.trim($("#selectRecvUserList").val());
			
			if(point.length <= 0) {
				alert("적립금을 입력해주세요.");
				$("#point").focus();
				return false;
			}
			
			if(selectRecvUserList == "0" && recv_user.length <= 0) {
				alert("받는사람을 선택해 주세요.");
				$("#recv_user").focus();
				return false;
			}			
			
            return true;
        },
        success: function(ret_data){
            if(ret_data.result == "OK") {
            	alert("적립금이 지급 되었습니다.");    			
    			$("#modal_popup").modal('hide');
    		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
    			alert(ret_data.msg);
    			location.href = "/";
    		} else {
    			alert(ret_data.msg);
    		}
        },
        error: function(){
            //에러발생을 위한 code페이지
        	alert("서버 오류.");
        }                               
    });
}

function getSendPointContentHtml() {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form class='form-horizontal' id='sendPointPushForm'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='point'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>적립금</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='point' name='point'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='push_message'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>지급사유</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<textarea class='form-control' id='push_message' name='push_message' rows='3'/>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='selectRecvUserList'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>받는 사람 선택</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<select class='form-control' id='selectRecvUserList' name='selectRecvUserList'>";
	html += "					<option value='0'>사용자 수동 입력</option>";
	html += "					<option value='1'>전체 사용자</option>";
	html += "				</select>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='recv_user'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>받는 사람</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<div id='selected_recv_user_container'>";
	html += "					<textarea class='form-control' id='recv_user' name='recv_user' rows='10'/>";
	html += "					<p class='help-block'>콤마(,)로 구분된 전화번호.</p>";
	html += "					<p class='help-block'>e.g. 01012345678,+821012345678,01012345678</p>";
	html += "				</div>";
	html += "				<div id='all_recv_user_container'>";
	html += "					<textarea class='form-control' rows='1' readOnly>전체 사용자</textarea>";
	html += "				</div>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='selectRecvUserList'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>푸시 여부</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<div class='checkbox'>";
	html += "					<label>";
	html += "						<input type='checkbox' id='sendPushYnTemp' checked/>푸시 전송";
	html += "						<input type='hidden' id='sendPushYn' name='sendPushYn' value='Y'/>";
	html += "					</label>";
	html += "				</div>";
	html += "			</div>";
	html += "		</div>";
	html += "		<input type='hidden' name='type' value='send_point' />";
	html += "	</form>";
	html += "</div>";
	
	return html;
}


var list_table;
var selected_region = "";

$(document).ready(function() {
	selected_region = $(".nav.nav-tabs > li.active > a").attr("href");
	
	list_table = $('#company_list').DataTable({
		initComplete: function () {

		},
		drawCallback: function() {
			$('#company_list > tbody').off("click").on('click', 'tr', function (event) {
				var index = $(this).index("#company_list > tbody > tr");
				
				if($(event.target).is('#company_list > tbody > tr:eq(' + index + ') > td:eq(0),#company_list > tbody > tr:eq(' + index + ') > td:eq(1),#company_list > tbody > tr:eq(' + index + ') > td:eq(2),#company_list > tbody > tr:eq(' + index + ') > td:eq(3),#company_list > tbody > tr:eq(' + index + ') > td:eq(4)')) {
					var row_data = list_table.row(this).data();
					
					if(row_data) {
						getCompanyDetail(row_data.company_id);
					}
				}
			});

			$(".btn_delete_company").off("click").on("click", function() {
				var ret = confirm("삭제하시겠습니까?");
				if(ret) {
					var row_data = list_table.row($(this).parent("td").parent("tr")).data();
					
					if(row_data) {
						deleteCompany(row_data.company_id);
					}
				}
			});
		},
		columns: [
		    { data: "company_id", visible: false, orderable: false, searchable: false },
		    { data: "companyName" },
			{ data: "telNumber" },
			{ data: "region" },
			{ data: "price" },
			{ data: "stopByFee" },
			{ data: "deletion", orderable: false, searchable: false, class: "text-center" }
		],
        serverSide: true,
		ajax: {
			url :"/ajax/ajaxCompany.php", // json datasource
			type: "post",  // type of method  , by default would be get
			data: function (d) {
				d.type = "getCompanyList";
				d.selected_region = selected_region;
			},
			error: function() {  // error handling code
			}
		}
	});
	
	$('#btnCreateCompany').off("click").on('click', function() {
		showCompanyDetailPopup(null);
	});
	
	$(".nav.nav-tabs > li").off("click").on("click", "a", function(event) {
		selected_region = $(this).attr("href");
		list_table.columns.adjust().draw();
	});
});

function getCompanyDetail(_company_id) {
	var params = {};
	params.type = "company_detail";
	params.company_id = _company_id;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCompany.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			showCompanyDetailPopup(ret_data.data);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function showCompanyDetailPopup(_data) {
	var mode = _data ? "edit" : "create";
	
	if(mode == "edit") {
		$("#modal_popup_label").text("대리운전 회사 상세보기");
	} else {
		$("#modal_popup_label").text("대리운전 회사 등록");
	}
	
	$("#modal_popup_content").html(getCompanyContentsHtml(mode));
	
	var footerHtml = "";
	
	initRegionData();
	
	if(mode == "edit") {
		$("#companyName").val(_data.companyName);
		$("#telNumber").val(_data.telNumber);
		$("#region").val(_data.region);
		$("#price").val(_data.price);
		$("#stopByFee").val(_data.stopByFee);
		
		footerHtml += "<button type='button' id='btnEdit' class='btn btn-primary'>수정</button>";
	} else {
		footerHtml += "<button type='button' id='btnAdd' class='btn btn-primary'>등록</button>";
	}
	
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	$("#modal_popup_footer").html(footerHtml);
	
	$("#modal_popup").modal('show');
	
	if(mode == "edit") {
		$("#btnEdit").off("click").on("click", function() {
			editCompany(_data);
		});
	} else {
		$("#btnAdd").off("click").on("click", function() {
			addCompany();
		});
	}
}

function initRegionData() {
	var html = "";
	
	html += "<option value='0'>지역 선택</option>";
	$.each(company_region, function(_key, _val) {
		html += "<option value='" + _val + "'>" + _val + "</option>";
	});
	
	$("#region").html(html);
}

function getCompanyContentsHtml(_mode) {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form class='form-horizontal'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='companyName'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>회사명</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='companyName'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='telNumber'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>전화번호</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='text' class='form-control' id='telNumber'/>";
	html += "				<p class='help-block'>(-)제외. eg.07012345678</p>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='region'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>지역</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<select class='form-control' id='region'></select>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='price'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>기본 가격</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='price'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='stopByFee'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>경유비</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='stopByFee'></div>";
	html += "		</div>";
	html += "	</form>";
	html += "</div>";
	
	return html;
}

function editCompany(_company_info) {
	var params = {};
	params.type = "edit_company";
	params.company_id = _company_info.company_id;
	params.companyName = $.trim($("#companyName").val());
	params.telNumber = $.trim($("#telNumber").val());
	params.region = $.trim($("#region").val());
	params.price = $.trim($("#price").val());
	params.stopByFee = $.trim($("#stopByFee").val());
	
	if(params.companyName.length <= 0) {
		alert("회사명을 입력해주세요.");
		$("#companyName").focus();
		return;
	}
	
	if(params.telNumber.length <= 0) {
		alert("전화번호를 입력해주세요.");
		$("#telNumber").focus();
		return;
	}
	
	if(params.region == 0 || params.region.length <= 0) {
		alert("지역을 선택해주세요.");
		$("#region").focus();
		return;
	}
	
	if(params.price == 0 || params.price.length <= 0) {
		alert("가격을 입력해주세요.");
		$("#price").focus();
		return;
	}
	
	if(params.stopByFee.length <= 0) {
		alert("경유비를 입력해주세요.");
		$("#stopByFee").focus();
		return;
	}
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCompany.php", params, function(_ret_data) {
		$(".loading").hide();
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("수정 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function addCompany() {
	var params = {};
	params.type = "add_company";
	params.companyName = $.trim($("#companyName").val());
	params.telNumber = $.trim($("#telNumber").val());
	params.region = $.trim($("#region").val());
	params.price = $.trim($("#price").val());
	params.stopByFee = $.trim($("#stopByFee").val());
	
	if(params.companyName.length <= 0) {
		alert("회사명을 입력해주세요.");
		$("#companyName").focus();
		return;
	}
	
	if(params.telNumber.length <= 0) {
		alert("전화번호를 입력해주세요.");
		$("#telNumber").focus();
		return;
	}
	
	if(params.region == 0 || params.region.length <= 0) {
		alert("지역을 선택해주세요.");
		$("#region").focus();
		return;
	}
	
	if(params.price == 0 || params.price.length <= 0) {
		alert("가격을 입력해주세요.");
		$("#price").focus();
		return;
	}
	
	if(params.stopByFee.length <= 0) {
		alert("경유비를 입력해주세요.");
		$("#stopByFee").focus();
		return;
	}
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCompany.php", params, function(_ret_data) {
		$(".loading").hide();
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("등록 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function deleteCompany(_company_id) {
	var params = {};
	params.type = "delete_company";
	params.company_id = _company_id;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCompany.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("삭제 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}
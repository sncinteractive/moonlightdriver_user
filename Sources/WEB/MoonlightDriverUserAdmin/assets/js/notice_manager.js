var list_table;

$(document).ready(function() {
	list_table = $('#notice_list').DataTable({
		initComplete: function () {

		},
		drawCallback: function() {
			$('#notice_list > tbody').off("click").on('click', 'tr', function (event) {
				var index = $(this).index("#notice_list > tbody > tr");
				
				if($(event.target).is('#notice_list > tbody > tr:eq(' + index + ') > td:eq(0),#notice_list > tbody > tr:eq(' + index + ') > td:eq(1),#notice_list > tbody > tr:eq(' + index + ') > td:eq(2),#notice_list > tbody > tr:eq(' + index + ') > td:eq(3),#notice_list > tbody > tr:eq(' + index + ') > td:eq(4),#notice_list > tbody > tr:eq(' + index + ') > td:eq(5),#notice_list > tbody > tr:eq(' + index + ') > td:eq(6),#notice_list > tbody > tr:eq(' + index + ') > td:eq(7)')) {
					var row_data = list_table.row(this).data();
					
					if(row_data) {
						getNoticeDetail(row_data.notice_id);
					}
				}
			});

			$(".btn_delete_notice").off("click").on("click", function() {
				var ret = confirm("삭제하시겠습니까?");
				if(ret) {
					var row_data = list_table.row($(this).parent("td").parent("tr")).data();
					
					if(row_data) {
						deleteNotice(row_data.notice_id);
					}
				}
			});
		},
		columns: [
		    { data: "notice_id", visible: false, orderable: false, searchable: false },
		    { data: "seq" },
		    { 
		    	data: "title", 
		    	render: function(data, type, row, meta) {
		    		return "<div class='text-ellipsis'>" + data + "</div>";
		    	}
		    },
			{ 
		    	data: "contents",
		    	render: function(data, type, row, meta) {
		    		return "<div class='text-ellipsis'>" + data + "</div>";
		    	}
		    },
			{ data: "orgImagePath" },
			{ data: "linkUrl" },
			{ data: "startDate" },
			{ data: "endDate" },
			{ data: "createdDate" },
			{ data: "deletion", orderable: false, searchable: false, class: "text-center" }
		],
		order: [[ 1, "desc" ]],
        serverSide: true,
		ajax: {
			url :"/ajax/ajaxNotice.php", // json datasource
			type: "post",  // type of method  , by default would be get
			data: function (d) {
				d.type = "getNoticeList";
			},
			error: function() {  // error handling code
			}
		}
	});
	
	$('#btnCreateNotice').off("click").on('click', function() {
		showNoticeDetailPopup(null);
	});
});

function getNoticeDetail(_notice_id) {
	var params = {};
	params.type = "notice_detail";
	params.notice_id = _notice_id;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxNotice.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			showNoticeDetailPopup(ret_data.data);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function showNoticeDetailPopup(_data) {
	var mode = _data ? "edit" : "create";
	
	if(mode == "edit") {
		$("#modal_popup_label").text("공지사항 상세보기");
	} else {
		$("#modal_popup_label").text("공지사항 등록");
	}
	
	$("#modal_popup_content").html(getNoticeContentsHtml(mode));
	
	var footerHtml = "";
	
	if(mode == "edit") {
		$("#type").val("edit_notice");
		$("#notice_id").val(_data.notice_id);
		$("#title").val(_data.title);
		$("#contents").val(_data.contents);
		$("#orgImagePath").val(_data.orgImagePath);
		$("#linkUrl").val(_data.linkUrl);
		$("#startDate").val(_data.startDate);
		$("#endDate").val(_data.endDate);
		
		footerHtml += "<button type='button' id='btnEdit' class='btn btn-primary'>수정</button>";
	} else {
		$("#type").val("add_notice");
		$("#notice_id").val("");
		footerHtml += "<button type='button' id='btnAdd' class='btn btn-primary'>등록</button>";
	}
	
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	$("#modal_popup_footer").html(footerHtml);
	
	$("#modal_popup").modal('show');
	
	if(mode == "edit") {
		$("#btnEdit").off("click").on("click", function() {
			sendFormData(_data.notice_id);
		});
	} else {
		$("#btnAdd").off("click").on("click", function() {
			sendFormData();
		});
	}
	
	$('#noticeInputForm').submit(function() {
		$(this).ajaxSubmit({
			beforeSubmit: function(formData, jqForm, options) {
				$('.loading').show();
				
				return true;
			},
			success : function(_ret_data, statusText, xhr, $form) {
				$('.loading').hide();
				$("#modal_popup").modal('hide');
				
				var type = $("#noticeInputForm > #type").val();
								
				var ret_data = JSON.parse(_ret_data);
				if(ret_data.result == "OK") {
					list_table.columns.adjust().draw();
					if(type == "add_notice") {
						alert("적용 되었습니다.");
					} else {
						alert("수정 되었습니다.");
					}
				} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
					alert(ret_data.msg);
					location.href = "/";
				} else {
					alert(ret_data.msg);
				}
			}
		});
		
		return false;
	});
	
	$("#notice_image").off("change").on("change", function(event) {
		var selectedFiles = event.target.files;
		if(selectedFiles.length <= 0) {
			return;
		}
		
		$("#orgImagePath").val(selectedFiles[0].name);
	});
	
	$('#startDateTimePicker').datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss",
		locale: "ko"
	});
	$('#endDateTimePicker').datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss",
		locale: "ko",
		useCurrent: false //Important! See issue #1075
	});
	
	$("#startDateTimePicker").on("dp.change", function (e) {
		$('#endDateTimePicker').data("DateTimePicker").minDate(e.date);
	});
	
	$("#endDateTimePicker").on("dp.change", function (e) {
		$('#startDateTimePicker').data("DateTimePicker").maxDate(e.date);
	});
}

function sendFormData(_notice_id) {
	var params = {};
	params.notice_id = $.trim($("#notice_id").val());
	params.title = $.trim($("#title").val());
	params.contents = $.trim($("#contents").val());
	params.orgImagePath = $.trim($("#orgImagePath").val());
	params.linkUrl = $.trim($("#linkUrl").val());
	params.startDate = $.trim($("#startDate").val());
	params.endDate = $.trim($("#endDate").val());
	
	if(params.title.length <= 0) {
		alert("제목을 입력해주세요.");
		$("#noticeName").focus();
		return;
	}
	
	if(params.contents.length <= 0) {
		alert("내용을 입력해주세요.");
		$("#contents").focus();
		return;
	}
	
	if(params.startDate.length <= 0) {
		alert("시작일을 선택해주세요.");
		$("#startDate").focus();
		return;
	}
	
	if(params.endDate.length <= 0) {
		alert("종료일을 선택해주세요.");
		$("#endDate").focus();
		return;
	}
	
	$("#form_data").val(JSON.stringify(params));
	
	$('#noticeInputForm').submit();
}

function getNoticeContentsHtml(_mode) {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form id='noticeInputForm' class='form-horizontal' action='/ajax/ajaxNotice.php' method='POST' enctype='multipart/form-data'>";
	html += "		<input type='hidden' name='form_data' id='form_data' />";
	html += "		<input type='hidden' name='type' id='type' />";
	html += "		<input type='hidden' id='notice_id' />";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='title'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>제목</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='title'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='contents'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>내용</label>";
	html += "			<div class='col-sm-8'><textarea class='form-control' id='contents' rows='10'></textarea></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='notice_image'>이미지</label>";
	html += "			<div class='col-sm-5'>";
	html += "				<input type='text' class='form-control' id='orgImagePath' readOnly/>";
	html += "			</div>";
	html += "			<div class='col-sm-3'>";
	html += "				<label id='btnAddNoticeImage' class='btn btn-info'>이미지 선택<input type='file' name='notice_image' id='notice_image' class='hidden' accept='image/*'/></label>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='linkUrl'>링크 URL</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='linkUrl'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='startDate'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>시작일</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<div class='input-group date' id='startDateTimePicker'>";
	html += "					<input type='text' class='form-control' id='startDate' />";
	html += "					<span class='input-group-addon'>";
	html += "						<span class='glyphicon glyphicon-calendar'></span>";
	html += "					</span>";
	html += "				</div>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='endDate'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>종료일</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<div class='input-group date' id='endDateTimePicker'>";
	html += "					<input type='text' class='form-control' id='endDate' />";
	html += "					<span class='input-group-addon'>";
	html += "						<span class='glyphicon glyphicon-calendar'></span>";
	html += "					</span>";
	html += "				</div>";
	html += "			</div>";
	html += "		</div>";
	html += "	</form>";
	html += "</div>";
	
	return html;
}

function deleteNotice(_notice_id) {
	var params = {};
	params.type = "delete_notice";
	params.notice_id = _notice_id;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxNotice.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("삭제 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}
var list_table;
var rank_type = "";

$(document).ready(function() {
	list_table = $('#crackdown_blacklist_list').DataTable({
		initComplete: function () {

		},
		drawCallback: function() {
			$(".btn_delete_crackdown_blacklist").off("click").on("click", function() {
				var ret = confirm("삭제하시겠습니까?");
				if(ret) {
					var row_data = list_table.row($(this).parent("td").parent("tr")).data();
					
					if(row_data) {
						deleteCrackdownBlackList(row_data.userId);
					}
				}
			});
			
			$('.btnBlockState').bootstrapToggle({
				on: '제보가능',
				off: '제보금지'
			}).off("change").on("change", function() {
				var block_state = $(this).is(':checked') ? "nonblock" : "block";
				var row_data = list_table.row($(this).parent("div").parent("td").parent("tr")).data();

				updateBlockState(row_data.userId, block_state);
			});
		},
		columns: [
		    { data: "userId", visible: false, orderable: false, searchable: false },
		    { data: "phone" },
			{ data: "wrongReportCount" },
			{ data: "lastWrongCrackdownReportTime" },
			{
				targets: 4,
				searchable: false,
				orderable: false,
				width: '1%',
				className: 'dt-body-center',
				render: function (data, type, full, meta) {
					return "<input type='checkbox' class='btnBlockState' " + (full.blockState == "nonblock" ? "checked" : "") + " data-onstyle='primary' data-offstyle='danger'/>";
				}
			},
			{
				targets: 5,
				searchable: false,
				orderable: false,
				width: '1%',
				className: 'dt-body-center',
				render: function (data, type, full, meta) {
					return "<button type='button' class='btn btn-danger btn-sm btn_delete_crackdown_blacklist'>삭제</button>";
				}
			}
		],
        serverSide: true,
		ajax: {
			url :"/ajax/ajaxCrackdownBlackList.php", // json datasource
			type: "post",  // type of method  , by default would be get
			data: function (d) {
				d.type = "getCrackdownBlackListList";
			},
			error: function() {  // error handling code
			}
		}
	});
});

function updateBlockState(_user_id, _block_state) {
	var params = {};
	params.type = "update_crackdown_block_state";
	params.user_id = _user_id;
	params.block_state = _block_state;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCrackdownBlackList.php", params, function(_ret_data) {
		$(".loading").hide();
		
		list_table.ajax.reload( null, false );
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			alert("변경 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function deleteCrackdownBlackList(_user_id) {
	var params = {};
	params.type = "delete_crackdown_blacklist";
	params.user_id = _user_id;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCrackdownBlackList.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.ajax.reload( null, false );
			alert("삭제 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}
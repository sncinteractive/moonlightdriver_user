var LIMIT_BOUNDS_NE = { lat : 83.91177715928563, lng : -175.8688546344638 };
var LIMIT_BOUNDS_SW = { lat : -43.27909581852835, lng : 57.56865844130516 };
var DEFAULT_ZOOM = 18;
var DEFAULT_LAT = 37.566702;
var DEFAULT_LNG = 126.978309;

var map;
var map_center;
var lastCenter;
var markerList = [];

function initMap() {
	if(!map_center) {
		map_center = new google.maps.LatLng(DEFAULT_LAT, DEFAULT_LNG);
	}
	
	map = new google.maps.Map(document.getElementById('map'), {
		center: map_center,
		zoom: DEFAULT_ZOOM
	});
	
	google.maps.event.addListener(map, 'idle', function() {
		if(lastCenter != null) {
			var distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lastCenter.lat(), lastCenter.lng()), new google.maps.LatLng(map.getCenter().lat(), map.getCenter().lng()));
			
			if(distance <= 500) {
				return;
			}
		}
		
		lastCenter = map.getCenter();
	});
}

function searchUserPhone() {
	var search_text = $.trim($("#search_user_phone").val());
	if(search_text.length < 4) {
		alert("4자이상 입력하세요.");
		return;
	}
	
	removeAllMarkers();
	
	var params = {};
	params.type = "search_user_phone";
	params.search_text = search_text;
	
	$.post("/ajax/ajaxUser.php", params, function(_ret_data) {
		console.log(_ret_data);
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			if(ret_data.data && ret_data.data.length > 0 && map) {
				var latlngBounds = new google.maps.LatLngBounds();
				var userPos;
				
				$.each(ret_data.data, function(_key, _val) {
					userPos = new google.maps.LatLng(_val.locations.coordinates[1], _val.locations.coordinates[0]);
					
					showMarker(userPos, _val);
					
					latlngBounds.extend(userPos);
				});
				
				if(ret_data.data.length > 1) {
					map.fitBounds(latlngBounds);
					map.panToBounds(latlngBounds);	
				} else {
					map.panTo(userPos);
					setTimeout(function() {
						$(".userInfo").click();
					}, 500);
				}
			} else {
				alert("결과가 없습니다.");
			}
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function showMarker(_position, _userData) {
	var userCurrentPosMarker = new google.maps.Marker({
		position: _position,
		icon: "/assets/images/pin_driver_40.png",
		map: map
	});
	
	markerList.push(userCurrentPosMarker);

	var userCurrentPosInfoWindow = new google.maps.InfoWindow({
		disableAutoPan: true,
		content: "<div class='userInfo' data-index='" + (markerList.length - 1) + "'>" + _userData.phone + "님의 현위치<br/>(최종 로그인 : " + _userData.lastLoginDate + ")</div>"
	});

	userCurrentPosMarker.addListener('click', function() {
		userCurrentPosInfoWindow.open(map, userCurrentPosMarker);
	});
	
	userCurrentPosInfoWindow.open(map, userCurrentPosMarker);
	
	setTimeout(function() {
		$(".userInfo").off("click").on("click", function() {
			if($(this).children(".addressArea").length > 0) {
				return;
			}
			
			var clickedIndex = $(this).attr("data-index");
			map.panTo(markerList[clickedIndex].getPosition());
			map.setZoom(DEFAULT_ZOOM);
			
			getReverseGeoCoding(markerList[clickedIndex].getPosition().lat(), markerList[clickedIndex].getPosition().lng(), $(this));
		});
	}, 500);
}

function moveToUser(_position) {
	map.panTo(_position);
	map.setZoom(DEFAULT_ZOOM);
}

function removeAllMarkers() {
	$.each(markerList, function(k, v) {
		v.setMap(null);
	});
	
	markerList = [];
}

function getReverseGeoCoding(_lat, _lng, _element) {
	$(".loading").show();

	$.getJSON("https://api2.sktelecom.com/tmap/geo/reversegeocoding?version=1&appKey=" + TMAP_API_KEY + "&lat=" + _lat + "&lon=" + _lng + "&addressType=A02&coordType=WGS84GEO", function( _data ) {
		$(".loading").hide();
		
		if(!_data.addressInfo) {
			alert("검색 결과가 없습니다.");
			return;
		}
		
		var addressInfo = _data.addressInfo;
		_element.children(".addressArea").remove();
		_element.append("<div class='addressArea' style='color: #0000ff; font-weight: bold;'>" + addressInfo.fullAddress + "</div>");
	});
}

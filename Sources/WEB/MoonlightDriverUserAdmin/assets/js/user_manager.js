var list_table;
var rows_selected = [];
var selected_recv_user = [];

var DEFAULT_ZOOM = 17;
var DEFAULT_LAT = 37.566702;
var DEFAULT_LNG = 126.978309;

var map;
var map_center;
var mapCenterCircle;
var listCountPerPage = 10;

$(document).ready(function() {
	rows_selected = [];
	selected_recv_user = [];
	
	list_table = $('#user_list').DataTable({
		initComplete: function () {
			
		},
		drawCallback: function() {

		},
		columns: [
		    { data: "selectCheckbox", orderable: false, searchable: false, class: "text-center" },
		    { data: "user_id", visible: false, orderable: false, searchable: false },
		    { data: "createdDate" },
		    { data: "phone" },
			{ data: "appVersion" },
			{ data: "lastLoginDate" }
		],
        serverSide: true,
		ajax: {
			url :"/ajax/ajaxUser.php", // json datasource
			type: "post",  // type of method  , by default would be get
			data: function (d) {
				d.type = "getUserList";
			},
			error: function() {  // error handling code
			}
		},
		order: [[ 2, "desc" ]],
		rowCallback: function(row, data, dataIndex){
			// Get row ID
			var rowId = data.user_id;
			
			// If row ID is in the list of selected row IDs
			if($.inArray(rowId, rows_selected) !== -1){
				$(row).find('input[type="checkbox"]').prop('checked', true);
				$(row).addClass('selected');
			}
		},
	});
	
	$('#user_list tbody').on('click', 'input[type="checkbox"]', function(e){
		var $row = $(this).closest('tr');

		// Get row data
		var data = list_table.row($row).data();

		// Get row ID
		var rowId = data.user_id;

		// Determine whether row ID is in the list of selected row IDs
		var index = $.inArray(rowId, rows_selected);

		// If checkbox is checked and row ID is not in list of selected row IDs
		if(this.checked && index === -1) {
			rows_selected.push(rowId);

			// Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
		} else if (!this.checked && index !== -1) {
			rows_selected.splice(index, 1);
		}
		
		if(this.checked){
			$row.addClass('selected');
			selected_recv_user.push(data.phone);
		} else {
			$row.removeClass('selected');
			selected_recv_user = _.without(selected_recv_user, data.phone);
		}

		// Update state of "Select all" control
		updateDataTableSelectAllCtrl(list_table);

		// Prevent click event from propagating to parent
		e.stopPropagation();
	});

	// Handle click on table cells with checkboxes
	$('#driver_list').on('click', 'tbody td, thead th:first-child', function(e){
		$(this).parent().find('input[type="checkbox"]').trigger('click');
	});

	// Handle click on "Select all" control
	$('thead input[name="select_all"]', list_table.table().container()).on('click', function(e){
		if(this.checked){
			$('#user_list tbody input[type="checkbox"]:not(:checked)').trigger('click');
		} else {
			$('#user_list tbody input[type="checkbox"]:checked').trigger('click');
		}

		// Prevent click event from propagating to parent
		e.stopPropagation();
	});
	
	list_table.on('draw', function(){
		// Update state of "Select all" control
		updateDataTableSelectAllCtrl(list_table);
	});
	
	$("#btnShowPushPopup").off("click").on("click", function() {
		showPushPopup("selected");
	});
	
	$("#btnShowAllPushPopup").off("click").on("click", function() {
		showPushPopup("all");
	});
	
	$("#btnSendCoupon").off("click").on("click", function() {
		showSendCouponPopup();
	});
	
	$("#btnSendPoint").off("click").on("click", function() {
		showSendPointPopup();
	});
	
	$("#btnExportExcel").off("click").on("click", function() {
		checkExportAuth();
	});
});

function initMap() {
	
}

function updateDataTableSelectAllCtrl(table){
	var $table             = table.table().node();
	var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
	var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
	var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);
	
	// If none of the checkboxes are checked
	if($chkbox_checked.length === 0) {
		chkbox_select_all.checked = false;
		if('indeterminate' in chkbox_select_all){
			chkbox_select_all.indeterminate = false;
		}
		
		// If all of the checkboxes are checked
	} else if ($chkbox_checked.length === $chkbox_all.length) {
		chkbox_select_all.checked = true;
		if('indeterminate' in chkbox_select_all) {
			chkbox_select_all.indeterminate = false;
		}
		
		// If some of the checkboxes are checked
	} else {
		chkbox_select_all.checked = true;
		if('indeterminate' in chkbox_select_all) {
			chkbox_select_all.indeterminate = true;
		}
	}
}

function showPushPopup(_mode) {
	$("#modal_popup_label").text("푸시 보내기");
	
	$("#modal_popup_content").html(getPushContentHtml());
	
	var footerHtml = "";
	footerHtml += "<button type='button' id='btnSendPush' class='btn btn-primary'>보내기</button>";
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>취소</button>";
	
	$("#modal_popup_footer").html(footerHtml);
		
	$("#btnSendPush").off("click").on("click", function() {
		$('#sendPushForm').submit();
	});
	
	if(_mode == "selected") {
		$("#recv_user").text(selected_recv_user.join());
		$("#selected_recv_user_container").show();
		$("#all_recv_user_container").hide();
	} else {
		$("#selected_recv_user_container").hide();
		$("#all_recv_user_container").show();
	}
	
	$("#recv_user_type").val(_mode);
	$("#push_message_length").text(MAX_LENGTH + "Bytes");
	
	$("#modal_popup").modal('show');
	
	$("#push_img").off("change").on("change", function(event) {
		var selectedFiles = event.target.files;
		if(selectedFiles.length <= 0) {
			return;
		}
		
		$("#pushOrgImgPath").val(selectedFiles[0].name);
	});
	
	$("#push_message").off("keyup").on("keyup", function() {
		var push_message = $.trim($(this).val());
		var stringByteLength = MAX_LENGTH - getStringBytes(push_message);
		
		$("#push_message_length").text(stringByteLength + "Bytes");
	});
	
	$('#sendPushForm').ajaxForm({
		url: "/ajax/ajaxUser.php",
		dataType:  'json',
		type: "POST",
		beforeSubmit: function (data, form, option) {
			var push_title = $.trim($("#push_title").val());
			var push_message = $.trim($("#push_message").val());
			var recv_user = $.trim($("#recv_user").val());
			var recv_user_type = $.trim($("#recv_user_type").val());
			
			if(push_title.length <= 0) {
				alert("제목을 입력해주세요.");
				$("#push_title").focus();
				return false;
			}
			
			if(push_message.length <= 0) {
				alert("내용을 입력해주세요.");
				$("#push_message").focus();
				return false;
			} else {
				var stringByteLength = getStringBytes(push_message);
				if(stringByteLength > MAX_LENGTH) {
					alert("메시지 최대 길이를 초과했습니다.");
					$("#push_message").focus();
					return false;
				}
			}
			
			if(recv_user_type == "selected" && recv_user.length <= 0) {
				alert("받는사람을 선택해 주세요.");
				$("#recv_user").focus();
				return false;
			}			
			
            return true;
        },
        success: function(ret_data){
            if(ret_data.result == "OK") {
    			alert("총 " + ret_data.data + "명에게 전송 되었습니다.");
    			$("#modal_popup").modal('hide');
    		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
    			alert(ret_data.msg);
    			location.href = "/";
    		} else {
    			alert(ret_data.msg);
    		}
        },
        error: function(){
            //에러발생을 위한 code페이지
        }                               
    });
}

function getPushContentHtml() {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form class='form-horizontal' id='sendPushForm'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='push_title'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>제목</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='push_title' name='push_title'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='push_message'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>내용</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<textarea class='form-control' id='push_message' name='push_message' rows='10'/>";
	html += "				<p class='help-block' id='push_message_length'></p>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='recv_user'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>받는 사람</label>";
	html += "			<div class='col-sm-8' id='selected_recv_user_container'>";
	html += "				<textarea class='form-control' id='recv_user' name='recv_user' rows='10'/>";
	html += "				<p class='help-block'>콤마(,)로 구분된 전화번호.</p>";
	html += "				<p class='help-block'>e.g. 01012345678,+821012345678,01012345678</p>";
	html += "			</div>";
	html += "			<div class='col-sm-8' id='all_recv_user_container'>";
	html += "				<textarea class='form-control' rows='1' readOnly>전체 사용자</textarea>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='push_img'>이미지</label>";
	html += "			<div class='col-sm-5'>";
	html += "				<input type='text' class='form-control' id='pushOrgImgPath' readOnly/>";
	html += "			</div>";
	html += "			<div class='col-sm-3'>";
	html += "				<label id='btnPushImage' class='btn btn-info'>이미지 선택<input type='file' name='push_img' id='push_img' class='hidden' accept='image/*'/></label>";
	html += "			</div>";
	html += "		</div>";
	html += "		<input type='hidden' name='type' value='send_push'>";
	html += "		<input type='hidden' id='recv_user_type' name='recv_user_type'>";
	html += "	</form>";
	html += "</div>";
	
	return html;
}

function getStringBytes(_message, b, i, c) {
	for(b = i = 0; c = _message.charCodeAt(i++); b += c >> 11 ? 3 : c >> 7 ? 2 : 1);
    return b;
}

function showSendCouponPopup() {
	$("#modal_popup_label").text("쿠폰 지급");
	
	$("#modal_popup_content").html(getSendCouponContentHtml());
	
	var footerHtml = "";
	footerHtml += "<button type='button' id='btnSend' class='btn btn-primary'>지급</button>";
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>취소</button>";
	
	$("#modal_popup_footer").html(footerHtml);
	
	$("#btnSend").off("click").on("click", function() {
		$('#sendCouponPushForm').submit();
	});
	
	$("#push_message_length").text(MAX_LENGTH + "Bytes");
	
	$("#recv_user").text(selected_recv_user.join());
	$("#selected_recv_user_container").show();
	$("#all_recv_user_container").hide();
	$("#map_recv_user_container").hide();
	
	getSelectCouponList();
	
	$("#modal_popup").modal('show');
	
	$("#selectRecvUserList").off("change").on("change", function() {
		var selectedType = $(this).val();
		if(selectedType == "0") {
			$("#selected_recv_user_container").show();
			$("#all_recv_user_container").hide();
			$("#map_recv_user_container").hide();
			
			map = null;
		} else if(selectedType == "1") {
			$("#selected_recv_user_container").hide();
			$("#all_recv_user_container").show();
			$("#map_recv_user_container").hide();
			
			map = null;
		} else if(selectedType == "2" || selectedType == "3") {
			$("#selected_recv_user_container").hide();
			$("#all_recv_user_container").hide();
			$("#map_recv_user_container").show();
			
			initializeMap(selectedType);
		}
	});
	
	$("#selectCouponList").off("change").on("change", function() {
		var selectedCouponId = $(this).val();
		$("#couponId").val("");
		
		if(selectedCouponId) {
			getSelectedCouponInfo(selectedCouponId);	
		}
	});
	
	$("#push_img").off("change").on("change", function(event) {
		var selectedFiles = event.target.files;
		if(selectedFiles.length <= 0) {
			return;
		}
		
		$("#pushOrgImgPath").val(selectedFiles[0].name);
	});
	
	$("#push_message").off("keyup").on("keyup", function() {
		var push_message = $.trim($(this).val());
		var stringByteLength = MAX_LENGTH - getStringBytes(push_message);
		
		$("#push_message_length").text(stringByteLength + "Bytes");
	});
	
	$("#selectPushImg").off("change").on("change", function(event) {
		var selectedPushImg = $(this).val();
		if(selectedPushImg == "2") {
			$("#selectPushImgContainer").show();
		} else {
			$("#selectPushImgContainer").hide();
		}
	});
	
	$("#searchText").off("keyup").on("keyup", function(event) {
		if(event.which == 13) {
			var searchText = $.trim($(this).val());
			searchAddress(searchText, 1);
			
			event.preventDefault();
			return false;
		}
	});
	
	$("#btnSearchText").off("click").on("click", function(event) {
		var searchText = $.trim($("#searchText").val());
		
		if(searchText.length <= 0) {
			alert("검색 위치를 입력해주세요.");
			$("#searchText").focus();
			return false;
		}
		
		searchAddress(searchText, 1);
		
		event.preventDefault();
		return false;
	});
	
	$('#sendCouponPushForm').ajaxForm({
		url: "/ajax/ajaxUser.php",
		dataType:  'json',
		type: "POST",
		beforeSubmit: function (data, form, option) {
			var push_title = $.trim($("#push_title").val());
			var push_message = $.trim($("#push_message").val());
			var recv_user = $.trim($("#recv_user").val());
			var selectRecvUserList = $.trim($("#selectRecvUserList").val());
			if(selectRecvUserList == "2" || selectRecvUserList == "3") {
				if(map) {
					var mapCenter = map.getCenter();
					$("#lat").val(mapCenter.lat());
					$("#lng").val(mapCenter.lng());
				}
			}
			
			if(push_title.length <= 0) {
				alert("제목을 입력해주세요.");
				$("#push_title").focus();
				return false;
			}
			
			if(push_message.length <= 0) {
				alert("내용을 입력해주세요.");
				$("#push_message").focus();
				return false;
			} else {
				var stringByteLength = getStringBytes(push_message);
				if(stringByteLength > MAX_LENGTH) {
					alert("메시지 최대 길이를 초과했습니다.");
					$("#push_message").focus();
					return false;
				}
			}
			
			if(selectRecvUserList == "0" && recv_user.length <= 0) {
				alert("받는사람을 선택해 주세요.");
				$("#recv_user").focus();
				return false;
			}			
			
            return true;
        },
        success: function(ret_data){
            if(ret_data.result == "OK") {
    			alert("총 " + ret_data.data + "명에게 전송 되었습니다.");
    			$("#modal_popup").modal('hide');
    		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
    			alert(ret_data.msg);
    			location.href = "/";
    		} else {
    			alert(ret_data.msg);
    		}
        },
        error: function(){
            //에러발생을 위한 code페이지
        	alert("서버 오류.");
        }                               
    });
}

function getSendCouponContentHtml() {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form class='form-horizontal' id='sendCouponPushForm'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='selectCouponList'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>쿠폰</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<select class='form-control' id='selectCouponList'></select>";
	html += "				<input type='hidden' id='couponId' name='couponId'/>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='push_title'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>제목</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='push_title' name='push_title'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='push_message'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>내용</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<textarea class='form-control' id='push_message' name='push_message' rows='10'/>";
	html += "				<p class='help-block' id='push_message_length'></p>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='selectRecvUserList'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>받는 사람 선택</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<select class='form-control' id='selectRecvUserList' name='selectRecvUserList'>";
	html += "					<option value='0'>사용자 수동 입력</option>";
	html += "					<option value='1'>전체 사용자</option>";
	html += "					<option value='2'>반경 500m이내 사용자</option>";
	html += "					<option value='3'>반경 1km이내 사용자</option>";
	html += "				</select>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='recv_user'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>받는 사람</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<div id='selected_recv_user_container'>";
	html += "					<textarea class='form-control' id='recv_user' name='recv_user' rows='10'/>";
	html += "					<p class='help-block'>콤마(,)로 구분된 전화번호.</p>";
	html += "					<p class='help-block'>e.g. 01012345678,+821012345678,01012345678</p>";
	html += "				</div>";
	html += "				<div id='all_recv_user_container'>";
	html += "					<textarea class='form-control' rows='1' readOnly>전체 사용자</textarea>";
	html += "				</div>";
	html += "				<div id='map_recv_user_container'>";
	html += "					<div class='row col-sm-12'>";
	html += "						<div class='col-sm-10' style='padding-left: 0px; padding-right: 0px;'>";
	html += "							<input type='text' class='form-control' id='searchText' placeholder='위치 검색'/>";
	html += "							<ul class='dropdown-menu col-sm-12'></ul>";
	html += "							</div>";
	html += "						<div class='col-sm-2'>";
	html += "							<button type='button' class='btn btn-primary' id='btnSearchText'>검색</button>";
	html += "						</div>";
	html += "					</div>";
	html += "					<div style='clear: both;'></div>";
	html += "					<div style='border: 1px solid #d2d6de; position: relative; width: 390px; height: 200px; margin-top: 10px;'>";
	html += "						<img src='/assets/images/ic_map_center.png' style='position: absolute; top: 100px; left: 185px; z-index: 1; width: 20px;'/>";
	html += "						<div id='map' style='height: 200px; width: 100%; position: absolute; top: 0px; left: 0px;'></div>";
	html += "					</div>";
	html += "				</div>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='useCouponImgYn'>푸시 이미지</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<select class='form-control' id='selectPushImg' name='selectPushImg'>";
	html += "					<option value='0'>사용 안함</option>";
	html += "					<option value='1'>쿠폰 이미지 사용</option>";
	html += "					<option value='2'>이미지 선택</option>";
	html += "				</select>";
	html += "				<div class='col-sm-12' id='selectPushImgContainer' style='padding: 10px 0px 0px 0px; display: none;'>";
	html += "					<div class='col-sm-8' style='padding: 0px;'>";
	html += "						<input type='text' class='form-control' id='pushOrgImgPath' readOnly/>";
	html += "					</div>";
	html += "					<div class='col-sm-4' style='padding-right: 0px'>";
	html += "						<label id='btnPushImage' class='btn btn-info'>이미지 선택<input type='file' name='push_img' id='push_img' class='hidden' accept='image/*'/></label>";
	html += "					</div>";
	html += "				</div>";
	html += "			</div>";
	html += "		</div>";
	html += "		<input type='hidden' name='type' value='send_coupon' />";
	html += "		<input type='hidden' name='lat' id='lat' />";
	html += "		<input type='hidden' name='lng' id='lng' />";
	html += "	</form>";
	html += "</div>";
	
	return html;
}

function getSelectCouponList() {
	var params = {};
	params.type = "select_coupon_list";
	
	$.post("/ajax/ajaxCoupon.php", params, function(_ret_data) {
		var ret_data = JSON.parse(_ret_data);
		$("#selectCouponList").append("<option value=''>쿠폰 선택...</option>");
		
		$.each(ret_data.data, function(_key, _val) {
			$("#selectCouponList").append("<option value='" + _val.couponId + "'>" + _val.couponName + "</option>");
		});
	}, "text");
}

function getSelectedCouponInfo(_couponId) {
	var params = {};
	params.type = "coupon_detail";
	params.couponId = _couponId;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCoupon.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			$("#couponId").val(_couponId);
			$("#push_title").val(ret_data.data.couponName);
			$("#push_message").text(ret_data.data.couponDesc);
			$("#pushOrgImgPath").val(ret_data.data.couponOrgImgPath);
			
			var push_message = $.trim(ret_data.data.couponDesc);
			var stringByteLength = MAX_LENGTH - getStringBytes(push_message);
			
			$("#push_message_length").text(stringByteLength + "Bytes");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function initializeMap(_selectedType) {
	if(!map_center) {
		map_center = new google.maps.LatLng(DEFAULT_LAT, DEFAULT_LNG);
	}
	
	if(mapCenterCircle) {
		mapCenterCircle.setMap(null);
	}
	
	map = new google.maps.Map(document.getElementById('map'), {
		center: map_center,
		zoom: DEFAULT_ZOOM
	});
	
	var circleRadius = 1000;
	if(_selectedType == 2) {
		circleRadius = 500;
	} else if(_selectedType == 3) {
		circleRadius = 1000;
	}
	
	mapCenterCircle = new google.maps.Circle({
		strokeColor: '#FF0000',
		strokeOpacity: 0.5,
		strokeWeight: 2,
		fillColor: '#FF0000',
		fillOpacity: 0.2,
		map: map,
		center: map.getCenter(),
		radius: circleRadius
	});

	google.maps.event.addListener(map, 'idle', function() {
		mapCenterCircle.setCenter(map.getCenter());
	});
	
	var containerHeight = $("#map_recv_user_container").height();
	var containerWidth = $("#map_recv_user_container").width();
	
	var centerImgHeight = $("#map_recv_user_container > img").height();
	var centerImgWidth = $("#map_recv_user_container > img").width();
	
	var centerLeft = (containerWidth - centerImgWidth) / 2;
	var centerTop = (containerHeight - centerImgHeight) / 2;
	
	$("#map_recv_user_container > img").css({
		"left" : centerLeft + "px",
		"top" : centerTop + "px"
	});
}

function showSendPointPopup() {
	$("#modal_popup_label").text("적립금 지급");
	
	$("#modal_popup_content").html(getSendPointContentHtml());
	
	var footerHtml = "";
	footerHtml += "<button type='button' id='btnSend' class='btn btn-primary'>지급</button>";
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>취소</button>";
	
	$("#modal_popup_footer").html(footerHtml);
	
	$("#btnSend").off("click").on("click", function() {
		$('#sendPointPushForm').submit();
	});
	
	$("#recv_user").text(selected_recv_user.join());
	$("#selected_recv_user_container").show();
	$("#all_recv_user_container").hide();
	
	$("#modal_popup").modal('show');
	
	$("#selectRecvUserList").off("change").on("change", function() {
		var selectedType = $(this).val();
		if(selectedType == "0") {
			$("#selected_recv_user_container").show();
			$("#all_recv_user_container").hide();
		} else if(selectedType == "1") {
			$("#selected_recv_user_container").hide();
			$("#all_recv_user_container").show();
		}
	});
	
	$("#sendPushYnTemp").off("change").on("change", function() {
		var sendPushYn = $(this).prop("checked") ? "Y" : "N";
		$("#sendPushYn").val(sendPushYn);
	});
	
	$('#sendPointPushForm').ajaxForm({
		url: "/ajax/ajaxUser.php",
		dataType:  'json',
		type: "POST",
		beforeSubmit: function (data, form, option) {
			var point = $.trim($("#point").val());
			var push_message = $.trim($("#push_message").val());
			var recv_user = $.trim($("#recv_user").val());
			var recv_user_type = $.trim($("#recv_user_type").val());
			var selectRecvUserList = $.trim($("#selectRecvUserList").val());
			
			if(point.length <= 0) {
				alert("적립금을 입력해주세요.");
				$("#point").focus();
				return false;
			}
			
			if(selectRecvUserList == "0" && recv_user.length <= 0) {
				alert("받는사람을 선택해 주세요.");
				$("#recv_user").focus();
				return false;
			}			
			
            return true;
        },
        success: function(ret_data){
            if(ret_data.result == "OK") {
            	alert("적립금이 지급 되었습니다.");    			
    			$("#modal_popup").modal('hide');
    		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
    			alert(ret_data.msg);
    			location.href = "/";
    		} else {
    			alert(ret_data.msg);
    		}
        },
        error: function(){
            //에러발생을 위한 code페이지
        	alert("서버 오류.");
        }                               
    });
}

function getSendPointContentHtml() {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form class='form-horizontal' id='sendPointPushForm'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='point'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>적립금</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='point' name='point'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='push_message'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>지급사유</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<textarea class='form-control' id='push_message' name='push_message' rows='3'/>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='selectRecvUserList'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>받는 사람 선택</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<select class='form-control' id='selectRecvUserList' name='selectRecvUserList'>";
	html += "					<option value='0'>사용자 수동 입력</option>";
	html += "					<option value='1'>전체 사용자</option>";
	html += "				</select>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='recv_user'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>받는 사람</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<div id='selected_recv_user_container'>";
	html += "					<textarea class='form-control' id='recv_user' name='recv_user' rows='10'/>";
	html += "					<p class='help-block'>콤마(,)로 구분된 전화번호.</p>";
	html += "					<p class='help-block'>e.g. 01012345678,+821012345678,01012345678</p>";
	html += "				</div>";
	html += "				<div id='all_recv_user_container'>";
	html += "					<textarea class='form-control' rows='1' readOnly>전체 사용자</textarea>";
	html += "				</div>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='selectRecvUserList'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>푸시 여부</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<div class='checkbox'>";
	html += "					<label>";
	html += "						<input type='checkbox' id='sendPushYnTemp' checked/>푸시 전송";
	html += "						<input type='hidden' id='sendPushYn' name='sendPushYn' value='Y'/>";
	html += "					</label>";
	html += "				</div>";
	html += "			</div>";
	html += "		</div>";
	html += "		<input type='hidden' name='type' value='send_point' />";
	html += "	</form>";
	html += "</div>";
	
	return html;
}

function checkExportAuth() {
	$("#modal_popup_label").text("엑셀 다운로드 인증");
	
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form class='form-horizontal'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='point'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>비밀번호</label>";
	html += "			<div class='col-sm-8'><input type='password' class='form-control' id='exportPassword' name='exportPassword'/></div>";
	html += "		</div>";
	html += "	</form>";
	html += "</div>";
	
	$("#modal_popup_content").html(html);
	
	var footerHtml = "";
	footerHtml += "<button type='button' id='btnExportAuth' class='btn btn-primary'>확인</button>";
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>취소</button>";
	
	$("#modal_popup_footer").html(footerHtml);
	
	$("#exportPassword").off("keypress").on("keypress", function(event) {
		if(event.which == 13) {
			var exportPassword = $(this).val();
			exportExcel(exportPassword);
			
			event.preventDefault();
			return false;
		}
	});
		
	$("#btnExportAuth").off("click").on("click", function() {
		var exportPassword = $("#exportPassword").val();
		
		exportExcel(exportPassword);
	});
	
	$("#modal_popup").modal('show');
}

function exportExcel(_exportPassword) {
	if(!_exportPassword) {
		alert("비밀번호를 입력해 주세요.");
		$("#exportPassword").focus();
		return;
	}
	
	var params = {};
	params.type = "export_excel";
	params.exportPassword = _exportPassword;
	
	$.post("/ajax/ajaxUser.php", params, function(_ret_data) {
		$("#modal_popup").modal('hide');

		var ret_data = JSON.parse(_ret_data);
		
        var tempLink = document.createElement('a');
        tempLink.href = "/pages/download_users.php?exportPassword=" + ret_data.data;
        tempLink.setAttribute('download', '');
        tempLink.click();
	}, "text");
}


function searchAddress(_searchText, _page) {
	if(_searchText.length <= 0) {
		alert("검색어를 입력해 주세요");
		$("#searchText").focus();
		return;
	}
	
	$(".loading").show();

	$.getJSON("https://api2.sktelecom.com/tmap/pois?version=1&appKey=" + TMAP_API_KEY + "&page=" + _page + "&count=" + listCountPerPage + "&searchKeyword=" + encodeURI(_searchText) + "&searchType=all&resCoordType=WGS84GEO", function( _data ) {
		$(".loading").hide();
		
		$(".dropdown-menu").empty();
		
		if(!(_data && _data.searchPoiInfo)) {
			alert("검색 결과가 없습니다.");
			return;
		}
		
		var searchPoiInfo = _data.searchPoiInfo;
		var count = parseInt(searchPoiInfo.count);
		var page = parseInt(searchPoiInfo.page);
		var totalCount = parseInt(searchPoiInfo.totalCount);
		var totalPage = Math.ceil(totalCount / listCountPerPage);
		
		var html = "";
		
		$.each(searchPoiInfo.pois.poi, function(key, val) {
			var address = val.upperAddrName + " " + val.middleAddrName + " " + val.lowerAddrName + " " + val.roadName + " " + val.firstBuildNo + (val.secondBuildNo != '-0' ? val.secondBuildNo : "");
			html += "<li>";
			html += "	<a href='#' data-lat='" + val.noorLat + "' data-lng='" + val.noorLon + "' data-address='" + address + "' data-poi-name='" + val.name + "'>" + val.name + "</a>";
			html += "</li>";
		});
		
		html += "<li class='text-center' data-page='" + page + "' data-search-text='" + _searchText + "' style='margin-top: 5px; padding-top: 5px; border-top: 1px solid #eee;'>";
		html += "	<button class='btn btn-default btnPrev' " + (page == 1 ? "disabled" : "") + ">이전</button>";
		html += "	<span style='margin-left: 10px;'>" + page + " / " + totalPage + "</span>";
		html += "	<button class='btn btn-default btnNext' " + (page == totalPage ? "disabled" : "") + " style='margin-left: 10px;'>다음</button>";
		html += "</li>";
		
		$(".dropdown-menu").html(html);
		$(".dropdown-menu").show();
		
		$(".btnPrev").off("click").on("click", function(event) {
			event.preventDefault ? event.preventDefault() : (event.returnValue = false);
			
			var page = parseInt($(this).parent("li").attr("data-page"), 10) - 1;
			var search_text = $(this).parent("li").attr("data-search-text");
			
			searchAddress(search_text, page);
		});
		
		$(".btnNext").off("click").on("click", function(event) {
			event.preventDefault ? event.preventDefault() : (event.returnValue = false);
			
			var page = parseInt($(this).parent("li").attr("data-page"), 10) + 1;
			var search_text = $(this).parent("li").attr("data-search-text");
			
			searchAddress(search_text, page);
		});
		
		$(".dropdown-menu" + " > li").off("click").on("click", "a", function(event) {
			$(".dropdown-menu").hide();
			
			var address = $(this).attr("data-address");
			var lat = parseFloat($(this).attr("data-lat"));
			var lng = parseFloat($(this).attr("data-lng"));
			var poi_name = $(this).attr("data-poi-name");
			
			$("#searchText").val(poi_name);
			
			var userPos = new google.maps.LatLng(lat, lng);
			map.panTo(userPos);
		});
	});
}

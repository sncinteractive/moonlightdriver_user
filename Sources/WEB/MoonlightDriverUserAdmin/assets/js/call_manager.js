var list_table;
var call_map_window;
var map;
var center = { lat : 37.566221, lng : 126.977921 };
var start_marker;
var end_marker;
var through_marker;
var start_infoWindow;
var end_infoWindow;
var through_infoWindow;
var isMaploaded = false;
var listCountPerPage = 10;

$(document).ready(function() {
	list_table = $('#call_list').DataTable({
		initComplete: function () {
			
		},
		drawCallback: function() {
			$('#call_list > tbody').off("click").on('click', 'tr', function (event) {
				var index = $(this).index("#call_list > tbody > tr");
				
				if($(event.target).is('#call_list > tbody > tr:eq(' + index + ') > td:eq(0),#call_list > tbody > tr:eq(' + index + ') > td:eq(1),#call_list > tbody > tr:eq(' + index + ') > td:eq(2),#call_list > tbody > tr:eq(' + index + ') > td:eq(3),#call_list > tbody > tr:eq(' + index + ') > td:eq(4),#call_list > tbody > tr:eq(' + index + ') > td:eq(5),#call_list > tbody > tr:eq(' + index + ') > td:eq(6),#call_list > tbody > tr:eq(' + index + ') > td:eq(7)')) {
					var row_data = list_table.row(this).data();
					
					if(row_data) {
						getCallDetail(row_data.callId);
					}
				}
			});
			
			$(".btn_map_location").off("click").on("click", function() {
				var callId = $(this).attr("data-callid");
				showCallLocation("/pages/call_map_location.php?callId=" + callId);
			});

			$(".btn_delete_call").off("click").on("click", function() {
				var ret = confirm("삭제하시겠습니까?");
				if(ret) {
					var row_data = list_table.row($(this).parent("td").parent("tr")).data();
					
					if(row_data) {
						deleteCall(row_data.callId);
					}
				}
			});
		},
		columns: [
		    { data: "callId", visible: false, orderable: false, searchable: false },
			{ data: "createdTime" },
			{ data: "userPhone" },
			{ data: "startPosition" },
			{ data: "endPosition" },
			{ data: "throughPosition" },
			{ data: "status" },
			{ 
				data: "money",
				render: function ( data, type, row, meta ) {
					return data + "원";
				} 
			},
			{ data: "etc" },
			{ data: "mapLocation", orderable: false, searchable: false, class: "text-center" },
			{ data: "deletion", orderable: false, searchable: false, class: "text-center" }
		],
		order: [[ 1, "desc" ]],
        serverSide: true,
		ajax: {
			url :"/ajax/ajaxCall.php", // json datasource
			type: "post",  // type of method  , by default would be get
			data: function (d) {
				d.type = "getCallList";
			},
			error: function() {  // error handling code
			}
		}
	});
	
	$('#btnCreateCall').off("click").on('click', function() {
		showCallDetailPopup(null);
	});
	
	$('#btnPeaktime').off("click").on('click', function() {
		getPeaktimeList();
	});
	
	$(document).on("click", function() {
		$(".dropdown-menu").hide();
	});
});

function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		mapTypeControl: false,
		center: center,
		zoom: 15
	});
	
	start_infoWindow = new google.maps.InfoWindow({
		content: ""
	});

	end_infoWindow = new google.maps.InfoWindow({
		content: ""
	});
	
	through_infoWindow = new google.maps.InfoWindow({
		content: ""
	});
	
	start_marker = new google.maps.Marker({
		position: center,
		draggable: true
	});
	
	through_marker = new google.maps.Marker({
		position: center,
		draggable: true
	});
	
	end_marker = new google.maps.Marker({
		position: center,
		draggable: true
	});
	
	start_marker.addListener('click', function() {
		if(start_infoWindow) {
			start_infoWindow.open(map, start_marker);
		}
	});
	
	start_marker.addListener('dragend', function(_event) {
		console.log(_event);
		getReverseGeoCoding("start-search", _event.latLng.lat(), _event.latLng.lng(), "");
	});
	
	end_marker.addListener('click', function() {
		if(end_infoWindow) {
			end_infoWindow.open(map, end_marker);
		}
	});
	
	end_marker.addListener('dragend', function(_event) {
		getReverseGeoCoding("end-search", _event.latLng.lat(), _event.latLng.lng(), "");
	});
	
	through_marker.addListener('click', function() {
		if(through_infoWindow) {
			through_infoWindow.open(map, through_marker);
		}
	});
	
	through_marker.addListener('dragend', function(_event) {
		getReverseGeoCoding("through-search", _event.latLng.lat(), _event.latLng.lng(), "");
	});
	
	google.maps.event.addListener(map, 'idle', function() {
//		console.log("zoom : " + map.getZoom());
	});
}

function showCallLocation(_url) {
	if(call_map_window && call_map_window.closed == false) {
		call_map_window.close();
	}
	
	call_map_window = window.open(_url, "call_map_window");
}

function getCallDetail(_callId) {
	var params = {};
	params.type = "call_detail";
	params.callId = _callId;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCall.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			showCallDetailPopup(ret_data.data);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function showCallDetailPopup(_data) {
	var mode = _data ? "edit" : "create";
	
	if(mode == "edit") {
		$("#modal_popup_label").text("콜 상세보기");
	} else {
		$("#modal_popup_label").text("콜 등록");
	}
	
	$("#modal_popup_content").html(getCallContentsHtml(mode));
	
	setCallStatus();
	
	var footerHtml = "";
	
	if(mode == "edit") {
		if(_data.user) {
			$("#phone").val(_data.user.phone ? _data.user.phone : "");
			$("#user_position").text(_data.user.address ? "현재 위치 : " + _data.user.address : "");
			
			$("#user_address").val(_data.user.address);
			$("#user_lat").val(_data.user.location.coordinates[1]);
			$("#user_lng").val(_data.user.location.coordinates[0]);
			$("#user_city_do").val(_data.user.city_do);
			$("#user_gu_gun").val(_data.user.gu_gun);
			$("#user_dong").val(_data.user.dong);
			$("#user_bunji").val(_data.user.bunji);
		}
		
		if(_data.start && _data.start.address) {
			$("#startSearch").val((_data.start.poi_name && _data.start.poi_name.length > 0 ? (_data.start.poi_name + " : ") : "") + _data.start.address);
			$("#start_address").val(_data.start.address);
			$("#start_lat").val(_data.start.location.coordinates[1]);
			$("#start_lng").val(_data.start.location.coordinates[0]);
			$("#start_poi_name").val(_data.start.poi_name);
		}
		
		if(_data.end && _data.end.address) {
			$("#endSearch").val((_data.end.poi_name && _data.end.poi_name.length > 0 ? (_data.end.poi_name + " : ") : "") + _data.end.address);
			$("#end_address").val(_data.end.address);
			$("#end_lat").val(_data.end.location.coordinates[1]);
			$("#end_lng").val(_data.end.location.coordinates[0]);
			$("#end_poi_name").val(_data.end.poi_name);
		}
		
		if(_data.through && _data.through.address) {
			$("#throughSearch").val((_data.through.poi_name && _data.through.poi_name.length > 0 ? (_data.through.poi_name + " : ") : "") + _data.through.address);
			$("#through_address").val(_data.through.address);
			$("#through_lat").val(_data.through.location.coordinates[1]);
			$("#through_lng").val(_data.through.location.coordinates[0]);
			$("#through_poi_name").val(_data.through.poi_name);
			
			$("#throughSearchWrapper").removeClass("hidden");
			
			$("#btnAddStopByPosition").parent("div").parent("div").hide();
		}
		
		$("#money").val(_data.money);
		$("#callType").val(_data.callType);
		$("#etc").val(_data.etc);
		$("#payment_option").val(_data.payment_option);
		
		$("#status").val(_data.status);
		
		footerHtml += "<button type='button' id='btnEdit' class='btn btn-primary'>수정</button>";
	} else {
		$("#payment_option").val("cash");
		$("#callType").val("normal");
		
		footerHtml += "<button type='button' id='btnAdd' class='btn btn-primary'>등록</button>";
	}
	
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	$("#modal_popup_footer").html(footerHtml);
	
	$("#modal_popup").modal('show');
	
	if(mode == "edit") {
		$("#btnEdit").off("click").on("click", function() {
			editCall(_data.callId);
		});
	} else {
		$("#btnAdd").off("click").on("click", function() {
			addCall();
		});
	}
	
	$("#btnStartSearch").off("click").on("click", function(event) {
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		
		var search_text = $.trim($("#startSearch").val());
		searchAddress("start-search", search_text, 1);
	});
	
	$("#startSearch").off("keypress").on("keypress", function(event) {
		if(event.which == 13) {
			event.preventDefault ? event.preventDefault() : (event.returnValue = false);
			var search_text = $.trim($("#startSearch").val());
			searchAddress("start-search", search_text, 1);
		}
	});
	
	$("#btnEndSearch").off("click").on("click", function(event) {
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		
		var search_text = $.trim($("#endSearch").val());
		searchAddress("end-search", search_text, 1);
	});
	
	$("#endSearch").off("keypress").on("keypress", function(event) {
		if(event.which == 13) {
			event.preventDefault ? event.preventDefault() : (event.returnValue = false);
			var search_text = $.trim($("#endSearch").val());
			searchAddress("end-search", search_text, 1);
		}
	});
	
	$("#btnThroughSearch").off("click").on("click", function(event) {
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		
		var search_text = $.trim($("#throughSearch").val());
		searchAddress("through-search", search_text, 1);
	});
	
	$("#throughSearch").off("keypress").on("keypress", function(event) {
		if(event.which == 13) {
			event.preventDefault ? event.preventDefault() : (event.returnValue = false);
			var search_text = $.trim($("#throughSearch").val());
			searchAddress("through-search", search_text, 1);
		}
	});
	
	$("#btnAddStopByPosition").off("click").on("click", function(event) {
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		
		$("#throughSearchWrapper").removeClass("hidden");
		
		$(this).parent("div").parent("div").hide();
	});
	
	$("#btnDeleteThrough").off("click").on("click", function(event) {
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		
		$("#throughSearch").val("");
		$("#through_address").val("");
		$("#through_lat").val("");
		$("#through_lng").val("");
		$("#through_poi_name").val("");
		
		$("#throughSearchWrapper").addClass("hidden");
		
		$("#btnAddStopByPosition").parent("div").parent("div").show();
	});
	
	$("#btnUserPositionSearch").off("click").on("click", function(event) {
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		
		var phone = $.trim($("#phone").val());
		if(phone.length <= 0) {
			alert("고객 전화번호를 입력해 주세요.");
			return;
		} else if(phone.length < 4) {
			alert("고객 전화번호를 4자이상 입력해 주세요.");
			return;
		} else {
			getUserList(phone);
		}
	});
	
	setTimeout(function() {
		if(!isMaploaded) {
			$.getScript("https://maps.googleapis.com/maps/api/js?key=" + google_api_key + "&callback=initMap").done(function() {
				isMaploaded = true;
				if(mode == "edit") {
					var bounds = new google.maps.LatLngBounds(null);
					
					if(_data.start && _data.start.address) {
						drawMarker("start-search", _data.start.location.coordinates[1], _data.start.location.coordinates[0], _data.start.address);
						bounds.extend({lat: _data.start.location.coordinates[1], lng : _data.start.location.coordinates[0]});
					}
					
					if(_data.end && _data.end.address) {
						drawMarker("end-search", _data.end.location.coordinates[1], _data.end.location.coordinates[0], _data.end.address);
						bounds.extend({lat: _data.end.location.coordinates[1], lng : _data.end.location.coordinates[0]});
					}

					if(_data.through && _data.through.address) {
						drawMarker("through-search", _data.through.location.coordinates[1], _data.through.location.coordinates[0], _data.through.address);
						bounds.extend({lat: _data.through.location.coordinates[1], lng : _data.through.location.coordinates[0]});
					}
					
					map.fitBounds(bounds);
				}
			}).fail(function() {
				alert('통신 상태를 확인해주세요.');
			});	
		} else {
			initMap();
			
			if(mode == "edit") {
				var bounds = new google.maps.LatLngBounds(null);
				
				if(_data.start && _data.start.address) {
					drawMarker("start-search", _data.start.location.coordinates[1], _data.start.location.coordinates[0], _data.start.address);
					bounds.extend({lat: _data.start.location.coordinates[1], lng : _data.start.location.coordinates[0]});
				}
				
				if(_data.end && _data.end.address) {
					drawMarker("end-search", _data.end.location.coordinates[1], _data.end.location.coordinates[0], _data.end.address);
					bounds.extend({lat: _data.end.location.coordinates[1], lng : _data.end.location.coordinates[0]});
				}

				if(_data.through && _data.through.address) {
					drawMarker("through-search", _data.through.location.coordinates[1], _data.through.location.coordinates[0], _data.through.address);
					bounds.extend({lat: _data.through.location.coordinates[1], lng : _data.through.location.coordinates[0]});
				}
				
				map.fitBounds(bounds);
			}
		}
		
		$("#phone").focus();
	}, 500);
}

function setCallStatus() {
	var html = "";
	
	$.each(call_status_list, function(key, val) {
		html += "<option value='" + key + "'>" + val + "</option>";
	});
	
	$("#status").html(html);
}

function getCallContentsHtml(_mode) {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form class='form-horizontal'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-2 control-label' for='phone'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>고객 전화번호</label>";
	html += "			<div class='col-sm-7'>";
	html += "				<input type='text' class='form-control' id='phone'/>";
	html += "				<ul class='dropdown-menu col-sm-11 user-search' style='left: 15px; margin-top: 0px; overflow-y: auto; height: 250px;'></ul>";
	html += "				<p class='help-block' id='user_position' style='margin: 0px;'></p>";
	html += "				<input type='hidden' class='form-control' id='user_address' />";
	html += "				<input type='hidden' class='form-control' id='user_city_do' />";
	html += "				<input type='hidden' class='form-control' id='user_gu_gun' />";
	html += "				<input type='hidden' class='form-control' id='user_dong' />";
	html += "				<input type='hidden' class='form-control' id='user_bunji' />";
	html += "				<input type='hidden' class='form-control' id='user_lat' />";
	html += "				<input type='hidden' class='form-control' id='user_lng' />";
	html += "			</div>";
	html += "			<div class='col-sm-2'>";
	html += "				<button id='btnUserPositionSearch' class='btn btn-info'>검색</button>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-2 control-label' for='startSearch'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>출발지</label>";
	html += "			<div class='col-sm-7'>";
	html += "				<input type='text' class='form-control' id='startSearch'/>";
	html += "				<input type='hidden' class='form-control' id='start_address' />";
	html += "				<input type='hidden' class='form-control' id='start_lat' />";
	html += "				<input type='hidden' class='form-control' id='start_lng' />";
	html += "				<input type='hidden' class='form-control' id='start_poi_name' />";
	html += "				<ul class='dropdown-menu col-sm-11 start-search' style='left: 15px;'></ul>";
	html += "			</div>";
	html += "			<div class='col-sm-2'>";
	html += "				<button id='btnStartSearch' class='btn btn-info'>검색</button>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div id='throughSearchWrapper' class='form-group form-group-md hidden'>";
	html += "			<label class='col-sm-2 control-label' for='throughSearch'>경유지</label>";
	html += "			<div class='col-sm-7'>";
	html += "				<div class='input-group input-group-sm'><input type='text' class='form-control' id='throughSearch'/><span class='input-group-btn'><button type='button' class='btn btn-danger btn-flat' id='btnDeleteThrough'><i class='fa fa-times'></i></button></span></div>";
	html += "				<input type='hidden' class='form-control' id='through_address' />";
	html += "				<input type='hidden' class='form-control' id='through_lat' />";
	html += "				<input type='hidden' class='form-control' id='through_lng' />";
	html += "				<input type='hidden' class='form-control' id='through_poi_name' />";
	html += "				<ul class='dropdown-menu col-sm-11 through-search' style='left: 15px;'></ul>";
	html += "			</div>";
	html += "			<div class='col-sm-2'>";
	html += "				<button id='btnThroughSearch' class='btn btn-info'>검색</button>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-2 control-label' for='endSearch'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>도착지</label>";
	html += "			<div class='col-sm-7'>";
	html += "				<input type='text' class='form-control' id='endSearch'/>";
	html += "				<input type='hidden' class='form-control' id='end_address' />";
	html += "				<input type='hidden' class='form-control' id='end_lat' />";
	html += "				<input type='hidden' class='form-control' id='end_lng' />";
	html += "				<input type='hidden' class='form-control' id='end_poi_name' />";
	html += "				<ul class='dropdown-menu col-sm-11 end-search' style='left: 15px;'></ul>";
	html += "			</div>";
	html += "			<div class='col-sm-2'>";
	html += "				<button id='btnEndSearch' class='btn btn-info'>검색</button>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-2 control-label'></label>";
	html += "			<div class='col-sm-2'>";
	html += "				<button id='btnAddStopByPosition' class='btn btn-success'>경유지 추가</button>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div id='mapWrapper' class='form-group form-group-md'>";
	html += "			<label class='col-sm-2 control-label'></label>";
	html += "			<div class='col-sm-9'>";
	html += "				<div id='map' style='height: 400px;'></div>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-2 control-label' for='money'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>대리요금</label>";
	html += "			<div class='col-sm-9'><input type='text' class='form-control' id='money'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-2 control-label' for='status'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>배차상태</label>";
	html += "			<div class='col-sm-9'><select class='form-control' id='status'></select></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-2 control-label' for='etc'>비고</label>";
	html += "			<div class='col-sm-9'><textarea class='form-control' id='etc' rows='3'></textarea></div>";
	html += "		</div>";
	html += "		<input type='hidden' id='callType'>";
	html += "		<input type='hidden' id='payment_option'>";
	html += "	</form>";
	html += "</div>";
	
	return html;
}

function searchAddress(_search_type, _search_text, _page) {
	if(_search_text.length <= 0) {
		alert("검색어를 입력해 주세요");
		$("#searchText").focus();
		return;
	}
	
	$(".loading").show();

	$.getJSON("https://api2.sktelecom.com/tmap/pois?version=1&appKey=" + tmap_api_key + "&page=" + _page + "&count=" + listCountPerPage + "&searchKeyword=" + encodeURI(_search_text) + "&searchType=all&resCoordType=WGS84GEO", function( _data ) {
		$(".loading").hide();
		
		$(".dropdown-menu." + _search_type).empty();
		
		if(!(_data && _data.searchPoiInfo)) {
			alert("검색 결과가 없습니다.");
			return;
		}
		
		var searchPoiInfo = _data.searchPoiInfo;
		var count = parseInt(searchPoiInfo.count);
		var page = parseInt(searchPoiInfo.page);
		var totalCount = parseInt(searchPoiInfo.totalCount);
		var totalPage = Math.ceil(totalCount / listCountPerPage);
		
		var html = "";
		
		$.each(searchPoiInfo.pois.poi, function(key, val) {
			var address = val.upperAddrName + " " + val.middleAddrName + " " + val.lowerAddrName + " " + val.roadName + " " + val.firstBuildNo + (val.secondBuildNo != '-0' ? val.secondBuildNo : "");
			html += "<li>";
			html += "	<a href='#' data-lat='" + val.noorLat + "' data-lng='" + val.noorLon + "' data-address='" + address + "' data-poi-name='" + val.name + "'>" + val.name + "</a>";
			html += "</li>";
		});
		
		html += "<li class='text-center' data-page='" + page + "' data-search-text='" + _search_text + "' style='margin-top: 5px; padding-top: 5px; border-top: 1px solid #eee;'>";
		html += "	<button class='btn btn-default btnPrev' " + (page == 1 ? "disabled" : "") + ">이전</button>";
		html += "	<span style='margin-left: 10px;'>" + page + " / " + totalPage + "</span>";
		html += "	<button class='btn btn-default btnNext' " + (page == totalPage ? "disabled" : "") + " style='margin-left: 10px;'>다음</button>";
		html += "</li>";
		
		$(".dropdown-menu." + _search_type).html(html);
		$(".dropdown-menu." + _search_type).show();
		
		$(".btnPrev").off("click").on("click", function(event) {
			event.preventDefault ? event.preventDefault() : (event.returnValue = false);
			
			var page = parseInt($(this).parent("li").attr("data-page")) - 1;
			var search_text = $(this).parent("li").attr("data-search-text");
			
			searchAddress(_search_type, search_text, page);
		});
		
		$(".btnNext").off("click").on("click", function(event) {
			event.preventDefault ? event.preventDefault() : (event.returnValue = false);
			
			var page = parseInt($(this).parent("li").attr("data-page")) + 1;
			var search_text = $(this).parent("li").attr("data-search-text");
			
			searchAddress(_search_type, search_text, page);
		});
		
		$(".dropdown-menu." + _search_type + " > li").off("click").on("click", "a", function(event) {
			$(".dropdown-menu." + _search_type).hide();
			
			var address = $(this).attr("data-address");
			var lat = parseFloat($(this).attr("data-lat"));
			var lng = parseFloat($(this).attr("data-lng"));
			var poi_name = $(this).attr("data-poi-name");
			
			getReverseGeoCoding(_search_type, lat, lng, poi_name);
		});
	});
}

function getReverseGeoCoding(_search_type, _lat, _lng, _poi_name) {
	$(".loading").show();

	$.getJSON("https://api2.sktelecom.com/tmap/geo/reversegeocoding?version=1&appKey=" + tmap_api_key + "&lat=" + _lat + "&lon=" + _lng + "&addressType=A02&coordType=WGS84GEO", function( _data ) {
		$(".loading").hide();
		
		if(!_data.addressInfo) {
			alert("검색 결과가 없습니다.");
			return;
		}
		
		var addressInfo = _data.addressInfo;
		
		drawMarker(_search_type, _lat, _lng, addressInfo.fullAddress);
		
		if(_search_type == "start-search") {
			$("#startSearch").val((_poi_name && _poi_name.length > 0 ? _poi_name + " : " : "") + addressInfo.fullAddress);
			$("#start_address").val(addressInfo.fullAddress);
			$("#start_lat").val(_lat);
			$("#start_lng").val(_lng);
			$("#start_poi_name").val(_poi_name);
		} else if(_search_type == "end-search") {
			$("#endSearch").val((_poi_name && _poi_name.length > 0 ? _poi_name + " : " : "") + addressInfo.fullAddress);
			$("#end_address").val(addressInfo.fullAddress);
			$("#end_lat").val(_lat);
			$("#end_lng").val(_lng);
			$("#end_poi_name").val(_poi_name);
		} else if(_search_type == "through-search") {
			$("#throughSearch").val((_poi_name && _poi_name.length > 0 ? _poi_name + " : " : "") + addressInfo.fullAddress);
			$("#through_address").val(addressInfo.fullAddress);
			$("#through_lat").val(_lat);
			$("#through_lng").val(_lng);
			$("#through_poi_name").val(_poi_name);
		} else if(_search_type == "user-search") {
			$("#user_position").text("현재 위치 : " + addressInfo.fullAddress);
			$("#user_address").val(addressInfo.fullAddress);
			$("#user_city_do").val(addressInfo.city_do);
			$("#user_gu_gun").val(addressInfo.gu_gun);
			$("#user_dong").val(addressInfo.legalDong);
			$("#user_bunji").val(addressInfo.bunji);
			$("#user_lat").val(_lat);
			$("#user_lng").val(_lng);
		}
	});
}

function drawMarker(_search_type, _lat, _lng, _address) {
	if(_search_type == "start-search") {
		start_marker.setPosition({ lat: _lat, lng: _lng });
		start_infoWindow.setContent("출발 : " + _address);
		
		start_marker.setMap(map);
		
		if(start_infoWindow) {
			start_infoWindow.open(map, start_marker);
		}
	} else if(_search_type == "end-search") {
		end_marker.setPosition({ lat: _lat, lng: _lng });
		end_infoWindow.setContent("도착 : " + _address);
		
		end_marker.setMap(map);
		
		if(end_infoWindow) {
			end_infoWindow.open(map, end_marker);
		}
	} else if(_search_type == "through-search") {
		through_marker.setPosition({ lat: _lat, lng: _lng });
		through_infoWindow.setContent("경유 : " + _address);
		
		through_marker.setMap(map);
		
		if(through_infoWindow) {
			through_infoWindow.open(map, through_marker);
		}
	}
}

function addCall() {
	var params = getFormData();
	if(!params) {
		return;
	}
	
	params.type = "add_call";
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCall.php", params, function(_ret_data) {
		$(".loading").hide();
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("등록 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function editCall(_callId) {
	var params = getFormData();
	if(!params) {
		return;
	}
	
	params.type = "edit_call";
	params.callId = _callId;

	$(".loading").show();
	
	$.post("/ajax/ajaxCall.php", params, function(_ret_data) {
		$(".loading").hide();
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("수정 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function getFormData() {
	var params = {};
	params.phone = $.trim($("#phone").val());
	params.start_address = $.trim($("#start_address").val());
	params.start_lat = $.trim($("#start_lat").val());
	params.start_lng = $.trim($("#start_lng").val());
	params.start_poi_name = $.trim($("#start_poi_name").val());
	params.end_address = $.trim($("#end_address").val());
	params.end_lat = $.trim($("#end_lat").val());
	params.end_lng = $.trim($("#end_lng").val());
	params.end_poi_name = $.trim($("#end_poi_name").val());
	params.through_address = $.trim($("#through_address").val());
	params.through_lat = $.trim($("#through_lat").val());
	params.through_lng = $.trim($("#through_lng").val());
	params.through_poi_name = $.trim($("#through_poi_name").val());
	params.user_address = $.trim($("#user_address").val());
	params.user_lat = $.trim($("#user_lat").val());
	params.user_lng = $.trim($("#user_lng").val());
	params.user_city_do = $.trim($("#user_city_do").val());
	params.user_gu_gun = $.trim($("#user_gu_gun").val());
	params.user_dong = $.trim($("#user_dong").val());
	params.user_bunji = $.trim($("#user_bunji").val());
	params.money = $.trim($("#money").val());
	params.etc = $.trim($("#etc").val());
	params.callType = $.trim($("#callType").val());
	params.payment_option = $.trim($("#payment_option").val());
	params.status = $.trim($("#status").val());
	
	if(params.phone.length <= 0) {
		alert("전화번호를 입력해주세요.");
		$("#phone").focus();
		return;
	}
	
	if(params.user_lat.length <= 0 || params.user_lng.length <= 0) {
		alert("고객 위치 검색을 해주세요.");
		$("#phone").focus();
		return;
	}
	
	if(params.start_address.length <= 0 || params.start_lat.length <= 0 || params.start_lng.length <= 0) {
		alert("출발지를 입력해주세요.");
		$("#startSearch").focus();
		return;
	}
	
	if(params.end_address.length <= 0 || params.end_lat.length <= 0 || params.end_lng.length <= 0) {
		alert("도착지를 입력해주세요.");
		$("#endSearch").focus();
		return;
	}
	
	if(params.money.length <= 0) {
		alert("대리요금을 입력해주세요.");
		$("#money").focus();
		return;
	}
	
	if(params.status.length <= 0) {
		alert("배차 상태를 입력해주세요.");
		$("#status").focus();
		return;
	}
	
	return params;
}

function deleteCall(_callId) {
	var params = {};
	params.type = "delete_call";
	params.callId = _callId;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCall.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("삭제 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function getUserList(_phone) {
	var params = {};
	params.type = "user_detail";
	params.phone = _phone;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCall.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		console.log(ret_data);
		
		if(ret_data.result == "OK") {
			if(ret_data.data) {
				var html = "";
				$.each(ret_data.data, function(key, val) {
					html += "<li>";
					html += "	<a href='#' data-lat='" + val.locations.coordinates[1] + "' data-lng='" + val.locations.coordinates[0] + "' data-phone='" + val.phone + "'>" + val.phone + "</a>";
					html += "</li>";
				});
				
				$(".dropdown-menu.user-search").html(html);
				$(".dropdown-menu.user-search").show();
				
				$(".dropdown-menu.user-search > li").off("click").on("click", "a", function(event) {
					$(".dropdown-menu.user-search").hide();
					
					var lat = parseFloat($(this).attr("data-lat"));
					var lng = parseFloat($(this).attr("data-lng"));
					var phone = $(this).attr("data-phone");
					
					$("#phone").val(phone);
					
					getReverseGeoCoding("user-search", lat, lng, "");
				});
			} else {
				alert("고객 정보를 확인 할 수 없습니다.");
			}
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function getPeaktimeList() {
	var params = {};
	params.type = "peaktime_list";
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCall.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			showPeaktimePopup(ret_data.data);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function showPeaktimePopup(data) {
	$("#modal_popup_label").text("피크타임 관리");	
	$("#modal_popup_content").html(getPeaktimeHtml());
	$("#modal_popup_footer").html("<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>");
	$("#modal_popup").modal('show');
	
	var listTable = $('#peaktimeList').DataTable({
		data : data,
		drawCallback: function() {
			$(".btnDeletePeaktime").off("click").on("click", function() {
				var ret = confirm("삭제하시겠습니까?");
				if(ret) {
					var $_trParent = $(this).parent("td").parent("tr");
					var row_data = $('#peaktimeList').DataTable().row($_trParent).data();
					
					if(row_data) {
						deletePeaktime($_trParent, row_data.peaktimeId);
					}
				}
			});
		},
		columns: [
		    { 
		    	data: "peaktimeId",
		    	visible: false,
		    	orderable: false,
		    	searchable: false
		    },
		    { 
		    	data: "startDate",
		    	class: "text-center"
		    },
		    { 
		    	data: "endDate",
		    	class: "text-center"
		    },
			{
		    	data: "btnDelete",
		    	class: "text-center"
		    }
		],
		order: [[ 1, "desc" ]]
	});
	
	$("#btnAddPeaktime").off("click").on("click", function() {
		var ret = confirm("등록 하시겠습니까?");
		if(ret) {
			var startDate = $.trim($("#startDate").val());
			var endDate = $.trim($("#endDate").val());
			
			if(startDate.length <= 0) {
				alert("시작날짜를 입력해 주세요.");
				return;
			}
			
			if(endDate.length <= 0) {
				alert("종료날짜를 입력해 주세요.");
				return;
			}
			
			addPeaktime(startDate, endDate);
		}
	});
	
	$('#startDateTimePicker').datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss",
		locale: "ko"
	});
	
	$('#endDateTimePicker').datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss",
		locale: "ko",
		useCurrent: false //Important! See issue #1075
	});
	
	$("#startDateTimePicker").on("dp.change", function (e) {
		$('#endDateTimePicker').data("DateTimePicker").minDate(e.date);
	});
	
	$("#endDateTimePicker").on("dp.change", function (e) {
		$('#startDateTimePicker').data("DateTimePicker").maxDate(e.date);
	});
}

function getPeaktimeHtml() {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
	html += "		<form class='form-horizontal'>";
	html += "			<div class='form-group form-group-md'>";
	html += "				<div class='col-lg-2 col-md-2 col-sm-2 text-center' style='line-height: 30px;'>피크타임 등록 : </div>";
	html += "				<div class='col-lg-3 col-md-3 col-sm-3'>";
	html += "					<div class='input-group date' id='startDateTimePicker'>";
	html += "						<input type='text' class='form-control' id='startDate' />";
	html += "						<span class='input-group-addon'>";
	html += "							<span class='glyphicon glyphicon-calendar'></span>";
	html += "						</span>";
	html += "					</div>";
	html += "				</div>";
	html += "				<span style='float: left; line-height: 30px;'> ~ </span>";
	html += "				<div class='col-lg-3 col-md-3 col-sm-3'>";
	html += "					<div class='input-group date' id='endDateTimePicker'>";
	html += "						<input type='text' class='form-control' id='endDate' />";
	html += "						<span class='input-group-addon'>";
	html += "							<span class='glyphicon glyphicon-calendar'></span>";
	html += "						</span>";
	html += "					</div>";
	html += "				</div>";
	html += "				<div class='col-lg-2 col-md-2 col-sm-2'><button type='button' class='btn btn-primary' id='btnAddPeaktime'>등록</button></div>";
	html += "			</div>";
	html += "			<table id='peaktimeList' class='table table-bordered table-hover'>";
	html += "				<thead>";
	html += "					<tr>";
	html += "						<th>peaktimeId</th>";
	html += "						<th>시작시간</th>";
	html += "						<th>종료시간</th>";
	html += "						<th>삭제</th>";
	html += "					</tr>";
	html += "				</thead>";
	html += "			</table>";
	html += "		</form>";
	html += "	</div>";
	html += "</div>";
	
	return html;
}

function deletePeaktime($_trParent, _peaktimeId) {
	var params = {};
	params.type = "delete_peaktime";
	params.peaktimeId = _peaktimeId;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCall.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			$_trParent.remove();
			alert("삭제 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function addPeaktime(startDate, endDate) {
	var params = {};
	params.type = "add_peaktime";
	params.startDate = startDate;
	params.endDate = endDate;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCall.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			$('#peaktimeList').DataTable().row.add({
				peaktimeId : ret_data.data.peaktimeId,
				startDate : ret_data.data.startDate,
				endDate : ret_data.data.endDate,
				btnDelete : "<button type='button' class='btn btn-danger btn-sm btnDeletePeaktime'>삭제</button>"
			}).draw();
			alert("등록 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

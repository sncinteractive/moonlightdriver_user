var list_table;
var crackdown_map_window;
var map;
var center = { lat : 37.566221, lng : 126.977921 };
var marker;
var infoWindow;
var isMaploaded = false;
var listCountPerPage = 10;

var rows_selected = [];
var selected_remove_wrong = [];

$(document).ready(function() {
	list_table = $('#crackdown_list').DataTable({
		initComplete: function () {
			var html = "";
			html += "<div style='margin-top: 10px;'>";
			html += "	<button class='btn btn-danger btnDeleteSelected'>선택 삭제</button>";
			html += "</div>";
			
			$('#crackdown_list_length').append(html);
			
			$(".btnDeleteSelected").off("click").on("click", function() {
				if(rows_selected.length <= 0) {
					alert("삭제할 음주단속제보를 선택해주세요.");
					return;
				}
				
				var ret = confirm("선택한 음주단속제보 " + rows_selected.length + "개를 삭제하시겠습니까?");
				if(ret) {
					deleteSelectedCrackdown(rows_selected, selected_remove_wrong);
				}
			});
		},
		drawCallback: function() {
			$('#crackdown_list > tbody').off("click").on('click', 'tr', function (event) {
				var index = $(this).index("#crackdown_list > tbody > tr");
				
				if($(event.target).is('#crackdown_list > tbody > tr:eq(' + index + ') > td:eq(0),#crackdown_list > tbody > tr:eq(' + index + ') > td:eq(1),#crackdown_list > tbody > tr:eq(' + index + ') > td:eq(2),#crackdown_list > tbody > tr:eq(' + index + ') > td:eq(3),#crackdown_list > tbody > tr:eq(' + index + ') > td:eq(4),#crackdown_list > tbody > tr:eq(' + index + ') > td:eq(6)')) {
					var row_data = list_table.row(this).data();
					
					if(row_data) {
						getCrackdownDetail(row_data.crackdown_id);
					}
				}
			});

			$(".btn_map_location").off("click").on("click", function() {
				var crackdown_id = $(this).attr("data-crackdownid");
				showCrackdownLocation("/pages/crackdown_map_location.php?crackdown_id=" + crackdown_id);
			});
			
			$(".btn_delete_crackdown").off("click").on("click", function() {
				var ret = confirm("삭제하시겠습니까?");
				if(ret) {
					var row_data = list_table.row($(this).parent("td").parent("tr")).data();
					
					if(row_data) {
						deleteCrackdown(row_data.crackdown_id);
					}
				}
			});
			
			$('#crackdown_list tbody input.checkbox_remove').off("click").on('click', function(e){
				var $row = $(this).closest('tr');

				// Get row data
				var data = list_table.row($row).data();
				
				// Get row ID
				var rowId = data.crackdown_id;

				// Determine whether row ID is in the list of selected row IDs
				var index = $.inArray(rowId, rows_selected);

				// If checkbox is checked and row ID is not in list of selected row IDs
				if(this.checked && index === -1) {
					rows_selected.push(rowId);
					// Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
				} else if (!this.checked && index !== -1) {
					rows_selected.splice(index, 1);
				}

				var selected_wrong_index = $.inArray(rowId, selected_remove_wrong);
				if(this.checked){
					$row.addClass('selected');
					$(this).parent().parent().find('input.checkbox_wrong').prop('checked', true);
					
					if(selected_wrong_index === -1) {
						selected_remove_wrong.push(rowId);
					}
				} else {
					$row.removeClass('selected');
					$(this).parent().parent().find('input.checkbox_wrong').prop('checked', false);
					
					if(selected_wrong_index !== -1) {
						selected_remove_wrong.splice(selected_wrong_index, 1);
					}
				}

				// Update state of "Select all" control
				updateDataTableSelectAllCtrl(list_table);

				// Prevent click event from propagating to parent
				e.stopPropagation();
			});
			
			$('#crackdown_list tbody input.checkbox_wrong').off("click").on('click', function(e){
				var $row = $(this).closest('tr');

				// Get row data
				var data = list_table.row($row).data();
				
				// Get row ID
				var rowId = data.crackdown_id;

				// Determine whether row ID is in the list of selected row IDs
				var selected_wrong_index = $.inArray(rowId, selected_remove_wrong);
				if(this.checked && selected_wrong_index === -1) {
					selected_remove_wrong.push(rowId);
				} else if(!this.checked && selected_wrong_index !== -1) {
					selected_remove_wrong.splice(selected_wrong_index, 1);
				}
				
				// Prevent click event from propagating to parent
				e.stopPropagation();
			});
		},
		columns: [
		    { data: "crackdown_id", visible: false, orderable: false, searchable: false },
		    { data: "createTime" },
		    { data: "from" },
			{ data: "reportType" },
			{ data: "address" },
			{ data: "phone" },
			{ data: "mapLocation", orderable: false, searchable: false, class: "text-center" },
			{ data: "updatedTime" },
			{
				targets: 8,
				searchable: false,
				orderable: false,
				width: '1%',
				className: 'dt-body-center',
				render: function (data, type, full, meta) {
					return '<input type="checkbox" value="' + full.crackdown_id + '" class="checkbox_remove">';
				}
			},
			{ data: "deletion", orderable: false, searchable: false, class: "text-center" },
			{
				targets: 10,
				searchable: false,
				orderable: false,
				width: '1%',
				className: 'dt-body-center',
				render: function (data, type, full, meta) {
					return '<input type="checkbox" value="' + full.crackdown_id + '" class="checkbox_wrong">';
				}
			}
		],
		order: [[ 1, "desc" ]],
		rowCallback: function(row, data, dataIndex){
			// Get row ID
			var rowId = data.crackdown_id;
			
			// If row ID is in the list of selected row IDs
			if($.inArray(rowId, rows_selected) !== -1){
				$(row).find('input.checkbox_remove').prop('checked', true);
				$(row).addClass('selected');
			}
			
			if($.inArray(rowId, selected_remove_wrong) !== -1){
				$(row).find('input.checkbox_wrong').prop('checked', true);
			}
		},
        serverSide: true,
		ajax: {
			url :"/ajax/ajaxCrackdown.php", // json datasource
			type: "post",  // type of method  , by default would be get
			data: function (d) {
				d.type = "getCrackdownList";
			},
			error: function() {  // error handling code
			}
		}
	});

	// Handle click on table cells with checkboxes
//	$('#crackdown_list').on('click', 'tbody td, thead th:first-child', function(e){
//		$(this).parent().find('input[type="checkbox"]').trigger('click');
//	});

	// Handle click on "Select all" control
	$('thead input[name="select_all_remove"]', list_table.table().container()).on('click', function(e){
		if(this.checked){
			$('#crackdown_list tbody input.checkbox_remove:not(:checked)').trigger('click');
		} else {
			$('#crackdown_list tbody input.checkbox_remove:checked').trigger('click');
		}

		// Prevent click event from propagating to parent
		e.stopPropagation();
	});
	
	list_table.on('draw', function(){
		// Update state of "Select all" control
		updateDataTableSelectAllCtrl(list_table);
	});
	
	$('#btnCreateCrackdown').off("click").on('click', function() {
		showCrackdownDetailPopup(null);
	});
	
	$(document).on("click", function() {
		$(".dropdown-menu").hide();
	});
});

function updateDataTableSelectAllCtrl(table){
	var $table             = table.table().node();
	var $chkbox_all        = $('tbody input.checkbox_remove', $table);
	var $chkbox_checked    = $('tbody input.checkbox_remove:checked', $table);
	var chkbox_select_all_remove  = $('thead input[name="select_all_remove"]', $table).get(0);
	
	// If none of the checkboxes are checked
	if($chkbox_checked.length === 0) {
		chkbox_select_all_remove.checked = false;
		if('indeterminate' in chkbox_select_all_remove){
			chkbox_select_all_remove.indeterminate = false;
		}
		
		// If all of the checkboxes are checked
	} else if ($chkbox_checked.length === $chkbox_all.length) {
		chkbox_select_all_remove.checked = true;
		if('indeterminate' in chkbox_select_all_remove) {
			chkbox_select_all_remove.indeterminate = false;
		}
		
		// If some of the checkboxes are checked
	} else {
		chkbox_select_all_remove.checked = true;
		if('indeterminate' in chkbox_select_all_remove) {
			chkbox_select_all_remove.indeterminate = true;
		}
	}
}

function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		mapTypeControl: false,
		center: center,
		zoom: 15
	});
	
	marker = new google.maps.Marker({
		position: center,
		draggable: true
	});
	
	infoWindow = new google.maps.InfoWindow({
		content: ""
	});
	
	marker.addListener('click', function() {
		if(infoWindow) {
			infoWindow.open(map, marker);
		}
	});
	
	marker.addListener('dragend', function(_event) {
		getReverseGeoCoding(_event.latLng.lat(), _event.latLng.lng());
	});
	
	google.maps.event.addListener(map, 'idle', function() {
		console.log("zoom : " + map.getZoom());
	});
	
	google.maps.event.addListener(map, 'click', function(_event) {
		getReverseGeoCoding(_event.latLng.lat(), _event.latLng.lng());
	});
}

function showCrackdownLocation(_url) {
	if(crackdown_map_window && crackdown_map_window.closed == false) {
		crackdown_map_window.close();
	}
	
	crackdown_map_window = window.open(_url, "crackdown_map_window");
}

function getCrackdownDetail(_crackdown_id) {
	var params = {};
	params.type = "crackdown_detail";
	params.crackdown_id = _crackdown_id;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCrackdown.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			showCrackdownDetailPopup(ret_data.data);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function showCrackdownDetailPopup(_data) {
	var mode = _data ? "edit" : "create";
	
	if(mode == "edit") {
		$("#modal_popup_label").text("음주단속 지점 상세보기");
	} else {
		$("#modal_popup_label").text("음주단속 지점 등록");
	}
	
	$("#modal_popup_content").html(getCrackdownContentsHtml(mode));
	
	var footerHtml = "";
	
	if(mode == "edit") {
		$("#phone").val(_data.phone);
		$("#searchText").val(_data.sido + " " + _data.sigugun + " " + _data.address);
		$("#resultSido").val(_data.sido);
		$("#resultGugun").val(_data.sigugun);
		$("#resultAddress").val(_data.address);
		$("#resultLat").val(_data.locations.coordinates[1]);
		$("#resultLng").val(_data.locations.coordinates[0]);
		
		footerHtml += "<button type='button' id='btnEdit' class='btn btn-primary'>수정</button>";
	} else {
		footerHtml += "<button type='button' id='btnAdd' class='btn btn-primary'>등록</button>";
	}
	
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	$("#modal_popup_footer").html(footerHtml);
	
	$("#modal_popup").modal('show');
	
	if(mode == "edit") {
		$("#btnEdit").off("click").on("click", function() {
			editCrackdown(_data.crackdown_id);
		});
	} else {
		$("#btnAdd").off("click").on("click", function() {
			addCrackdown();
		});
	}
	
	$("#btnSearchAddress").off("click").on("click", function(event) {
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		
		var search_text = $.trim($("#searchText").val());
		searchAddress(search_text, 1);
	});
	
	$("#searchText").off("keypress").on("keypress", function(event) {
		if(event.which == 13) {
			event.preventDefault ? event.preventDefault() : (event.returnValue = false);
			var search_text = $.trim($("#searchText").val());
			searchAddress(search_text, 1);
		}
	});
	
	setTimeout(function() {
		if(!isMaploaded) {
			$.getScript("https://maps.googleapis.com/maps/api/js?key=" + google_api_key + "&callback=initMap").done(function() {
				isMaploaded = true;
				if(mode == "edit") {
					drawMarker(_data.locations.coordinates[1], _data.locations.coordinates[0], _data.sido + " " + _data.sigugun + " " + _data.address);
				}
			}).fail(function() {
				alert('통신 상태를 확인해주세요.');
			});	
		} else {
			initMap();
			
			if(mode == "edit") {
				drawMarker(_data.locations.coordinates[1], _data.locations.coordinates[0], _data.sido + " " + _data.sigugun + " " + _data.address);
			}
		}
		
		$("#phone").focus();
	}, 500);
}

function getCrackdownContentsHtml() {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form class='form-horizontal'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='phone'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>제보자 전화번호</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='text' class='form-control' id='phone'/>";
	html += "				<p class='help-block'>(-)제외. eg.01012345678</p>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='searchText'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>단속 위치</label>";
	html += "			<div class='col-sm-6'>";
	html += "				<input type='text' class='form-control' id='searchText'/>";
	html += "				<ul class='dropdown-menu col-sm-11' style='left: 15px;'></ul>";
	html += "			</div>";
	html += "			<div class='col-sm-2'>";
	html += "				<button id='btnSearchAddress' class='btn btn-info'>검색</button>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div id='mapWrapper' class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label'></label>";
	html += "			<div class='col-sm-8'>";
	html += "				<div id='map' style='height: 400px;'></div>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label'></label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='hidden' class='form-control' id='resultSido' />";
	html += "				<input type='hidden' class='form-control' id='resultGugun' />";
	html += "				<input type='hidden' class='form-control' id='resultAddress' />";
	html += "				<input type='hidden' class='form-control' id='resultLat' />";
	html += "				<input type='hidden' class='form-control' id='resultLng' />";
	html += "			</div>";
	html += "		</div>";
	html += "	</form>";
	html += "</div>";
	
	return html;
}

function searchAddress(_search_text, _page) {
	if(_search_text.length <= 0) {
		alert("검색어를 입력해 주세요");
		$("#searchText").focus();
		return;
	}
	
	$(".loading").show();

	$.getJSON("https://apis.skplanetx.com/tmap/pois?version=1&appKey=" + tmap_api_key + "&page=" + _page + "&count=" + listCountPerPage + "&searchKeyword=" + encodeURI(_search_text) + "&searchType=all&resCoordType=WGS84GEO", function( _data ) {
		$(".loading").hide();
		
		$(".dropdown-menu").empty();
		
		if(!(_data && _data.searchPoiInfo)) {
			alert("검색 결과가 없습니다.");
			return;
		}
		
		var searchPoiInfo = _data.searchPoiInfo;
		var count = parseInt(searchPoiInfo.count);
		var page = parseInt(searchPoiInfo.page);
		var totalCount = parseInt(searchPoiInfo.totalCount);
		var totalPage = Math.ceil(totalCount / listCountPerPage);
		
		var html = "";
		
		$.each(searchPoiInfo.pois.poi, function(key, val) {
			var address = val.upperAddrName + " " + val.middleAddrName + " " + val.lowerAddrName + " " + val.roadName + " " + val.firstBuildNo + (val.secondBuildNo != '-0' ? val.secondBuildNo : "");
			html += "<li>";
			html += "	<a href='#' data-lat='" + val.noorLat + "' data-lng='" + val.noorLon + "' data-address='" + address + "'>" + val.name + "</a>";
			html += "</li>";
		});
		
		html += "<li class='text-center' data-page='" + page + "' data-search-text='" + _search_text + "' style='margin-top: 5px; padding-top: 5px; border-top: 1px solid #eee;'>";
		html += "	<button class='btn btn-default btnPrev' " + (page == 1 ? "disabled" : "") + ">이전</button>";
		html += "	<span style='margin-left: 10px;'>" + page + " / " + totalPage + "</span>";
		html += "	<button class='btn btn-default btnNext' " + (page == totalPage ? "disabled" : "") + " style='margin-left: 10px;'>다음</button>";
		html += "</li>";
		
		$(".dropdown-menu").html(html);
		$(".dropdown-menu").show();
		
		$(".btnPrev").off("click").on("click", function(event) {
			event.preventDefault ? event.preventDefault() : (event.returnValue = false);
			
			var page = parseInt($(this).parent("li").attr("data-page")) - 1;
			var search_text = $(this).parent("li").attr("data-search-text");
			
			searchAddress(search_text, page);
		});
		
		$(".btnNext").off("click").on("click", function(event) {
			event.preventDefault ? event.preventDefault() : (event.returnValue = false);
			
			var page = parseInt($(this).parent("li").attr("data-page")) + 1;
			var search_text = $(this).parent("li").attr("data-search-text");
			
			searchAddress(search_text, page);
		});
		
		$(".dropdown-menu > li").off("click").on("click", "a", function(event) {
			$(".dropdown-menu").hide();
			
			var address = $(this).attr("data-address");
			var lat = parseFloat($(this).attr("data-lat"));
			var lng = parseFloat($(this).attr("data-lng"));
			
			getReverseGeoCoding(lat, lng);
		});
	});
}

function getReverseGeoCoding(_lat, _lng) {
	$(".loading").show();

	$.getJSON("https://apis.skplanetx.com/tmap/geo/reversegeocoding?version=1&appKey=" + tmap_api_key + "&lat=" + _lat + "&lon=" + _lng + "&addressType=A02&coordType=WGS84GEO", function( _data ) {
		$(".loading").hide();
		
		if(!_data.addressInfo) {
			alert("검색 결과가 없습니다.");
			return;
		}
		
		var addressInfo = _data.addressInfo;
		
		drawMarker(_lat, _lng, addressInfo.fullAddress);

		$("#searchText").val(addressInfo.fullAddress);
		$("#resultSido").val(addressInfo.city_do);
		$("#resultGugun").val(addressInfo.gu_gun);
		$("#resultAddress").val(addressInfo.legalDong + " " + addressInfo.bunji);
		$("#resultLat").val(_lat);
		$("#resultLng").val(_lng);
	});
}

function drawMarker(_lat, _lng, _address) {
	marker.setPosition({ lat: _lat, lng: _lng });
	infoWindow.setContent(_address);
	
	marker.setMap(map);

	if(infoWindow) {
		infoWindow.open(map, marker);
	}
}

function addCrackdown() {
	var params = getFormData();
	if(!params) {
		return;
	}
	
	params.type = "add_crackdown";
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCrackdown.php", params, function(_ret_data) {
		$(".loading").hide();
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("등록 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function editCrackdown(_crackdown_id) {
	var params = getFormData();
	if(!params) {
		return;
	}
	
	params.type = "edit_crackdown";
	params.crackdown_id = _crackdown_id;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCrackdown.php", params, function(_ret_data) {
		$(".loading").hide();
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("수정 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function getFormData() {
	var params = {};
	params.phone = $.trim($("#phone").val());
	params.address = $.trim($("#resultAddress").val());
	params.sido = $.trim($("#resultSido").val());
	params.gugun = $.trim($("#resultGugun").val());
	params.lat = $.trim($("#resultLat").val());
	params.lng = $.trim($("#resultLng").val());
	params.reportType = "voice";
	
	if(params.phone.length <= 0) {
		alert("제보자 전화번호를 입력해주세요.");
		$("#phone").focus();
		return;
	}
	
	if(params.address.length <= 0 || params.lat.length <= 0 || params.lng.length <= 0) {
		alert("단속위치를 입력해주세요.");
		$("#searchText").focus();
		return;
	}
	
	return params;
}

function deleteCrackdown(_crackdown_id) {
	var params = {};
	params.type = "delete_crackdown";
	params.crackdown_id = _crackdown_id;
	
	var current_page = list_table.page();
	
	console.log("current_page : " + current_page);
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCrackdown.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.ajax.reload( null, false );
			alert("삭제 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function deleteSelectedCrackdown(_delete_id_list, _wrong_id_list) {
	var params = {};
	params.type = "delete_selected_crackdown";
	params.delete_id_list = _delete_id_list;
	params.wrong_id_list = _wrong_id_list;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCrackdown.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.ajax.reload( null, false );
			rows_selected = [];
			selected_remove_wrong = [];
			alert("삭제 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
} 

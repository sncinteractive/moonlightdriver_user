var map;
var center = { lat : 37.566221, lng : 126.977921 };

$(document).ready(function() {

});

function initMap() {
	center = {
		lat : crackdownDetail.locations.coordinates[1],
		lng : crackdownDetail.locations.coordinates[0]
	};
	
	map = new google.maps.Map(document.getElementById('map'), {
		center: center,
		zoom: 17
	});
	
	var marker = new google.maps.Marker({
		position: center,
		map: map
	});
	
	var infoWindowContent = "";
	infoWindowContent += "출처 : " + crackdownDetail.from + "</br>";
	infoWindowContent += "등록자 : " + (crackdownDetail.phone ? crackdownDetail.phone : crackdownDetail.registrant) + "</br>";
	infoWindowContent += "위치 : " + (crackdownDetail.sido  + " " + crackdownDetail.address) + "</br>";
	
	var infowindow = new google.maps.InfoWindow({
		content: infoWindowContent
	});
	
	marker.addListener('click', function() {
		if(infowindow) {
			infowindow.open(map, marker);
		}
	});

	if(infowindow) {
		infowindow.open(map, marker);
	}
	
	google.maps.event.addListener(map, 'idle', function() {
		console.log("zoom : " + map.getZoom());
	});
}

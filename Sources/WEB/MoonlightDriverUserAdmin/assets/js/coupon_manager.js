var list_table;

$(document).ready(function() {
	list_table = $('#couponList').DataTable({
		initComplete: function () {
			
		},
		drawCallback: function() {
			$('#couponList > tbody').off("click").on('click', 'tr', function (event) {
				var index = $(this).index("#couponList > tbody > tr");
				
				if($(event.target).is('#couponList > tbody > tr:eq(' + index + ') > td:eq(0),#couponList > tbody > tr:eq(' + index + ') > td:eq(1),#couponList > tbody > tr:eq(' + index + ') > td:eq(2),#couponList > tbody > tr:eq(' + index + ') > td:eq(3),#couponList > tbody > tr:eq(' + index + ') > td:eq(4),#couponList > tbody > tr:eq(' + index + ') > td:eq(5)')) {
					var row_data = list_table.row(this).data();
					
					if(row_data) {
						getCouponDetail(row_data.couponId);
					}
				}
			});

			$(".btnDeleteCoupon").off("click").on("click", function() {
				var ret = confirm("삭제하시겠습니까?");
				if(ret) {
					var row_data = list_table.row($(this).parent("td").parent("tr")).data();
					
					if(row_data) {
						deleteCoupon(row_data.couponId);
					}
				}
			});
		},
		columns: [
		    { data: "couponId", visible: false, orderable: false, searchable: false },
		    { data: "couponName" },
			{ 
		    	data: "couponDesc",
		    	render: function(data, type, row, meta) {
		    		return "<div class='text-ellipsis'>" + data + "</div>";
		    	}
		    },
			{ data: "couponOrgImgPath" },
			{ data: "couponStartDate" },
			{ data: "couponEndDate" },
			{ data: "createdDate" },
			{ data: "deletion", orderable: false, searchable: false, class: "text-center" }
		],
        serverSide: true,
		ajax: {
			url :"/ajax/ajaxCoupon.php", // json datasource
			type: "post",  // type of method  , by default would be get
			data: function (d) {
				d.type = "getCouponList";
			},
			error: function() {  // error handling code
			}
		}
	});
	
	$('#btnCreateCoupon').off("click").on('click', function() {
		showCouponDetailPopup(null);
	});
});

function getCouponDetail(_couponId) {
	var params = {};
	params.type = "coupon_detail";
	params.couponId = _couponId;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCoupon.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			showCouponDetailPopup(ret_data.data);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function showCouponDetailPopup(_data) {
	var mode = _data ? "edit" : "create";
	
	if(mode == "edit") {
		$("#modal_popup_label").text("쿠폰 상세보기");
	} else {
		$("#modal_popup_label").text("쿠폰 등록");
	}
	
	$("#modal_popup_content").html(getCouponContentsHtml(mode));
	
	var footerHtml = "";
	
	if(mode == "edit") {
		$("#type").val("edit_coupon");
		$("#couponId").val(_data.couponId);
		$("#couponName").val(_data.couponName);
		$("#couponDesc").val(_data.couponDesc);
		$("#couponOrgImgPath").val(_data.couponOrgImgPath);
		$("#couponStartDate").val(_data.couponStartDate);
		$("#couponEndDate").val(_data.couponEndDate);
		
		footerHtml += "<button type='button' id='btnEdit' class='btn btn-primary'>수정</button>";
	} else {
		$("#type").val("add_coupon");
		$("#couponId").val("");
		footerHtml += "<button type='button' id='btnAdd' class='btn btn-primary'>등록</button>";
	}
	
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	$("#modal_popup_footer").html(footerHtml);
	
	$("#modal_popup").modal('show');
	
	if(mode == "edit") {
		$("#btnEdit").off("click").on("click", function() {
			sendFormData(_data.notice_id);
		});
	} else {
		$("#btnAdd").off("click").on("click", function() {
			sendFormData();
		});
	}
	
	$('#couponInputForm').submit(function() {
		$(this).ajaxSubmit({
			beforeSubmit: function(formData, jqForm, options) {
				$('.loading').show();
				
				return true;
			},
			success : function(_ret_data, statusText, xhr, $form) {
				$('.loading').hide();
				$("#modal_popup").modal('hide');
				
				var type = $("#couponInputForm > #type").val();
								
				var ret_data = JSON.parse(_ret_data);
				if(ret_data.result == "OK") {
					list_table.columns.adjust().draw();
					if(type == "add_coupon") {
						alert("적용 되었습니다.");
					} else {
						alert("수정 되었습니다.");
					}
				} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
					alert(ret_data.msg);
					location.href = "/";
				} else {
					alert(ret_data.msg);
				}
			}
		});
		
		return false;
	});
	
	$("#couponImage").off("change").on("change", function(event) {
		var selectedFiles = event.target.files;
		if(selectedFiles.length <= 0) {
			return;
		}
		
		$("#couponOrgImgPath").val(selectedFiles[0].name);
	});
	
	$('#startDateTimePicker').datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss",
		locale: "ko"
	});
	
	$('#endDateTimePicker').datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss",
		locale: "ko",
		useCurrent: false //Important! See issue #1075
	});
	
	$("#startDateTimePicker").on("dp.change", function (e) {
		$('#endDateTimePicker').data("DateTimePicker").minDate(e.date);
	});
	
	$("#endDateTimePicker").on("dp.change", function (e) {
		$('#startDateTimePicker').data("DateTimePicker").maxDate(e.date);
	});
}

function sendFormData(_couponId) {
	var params = {};
	params.couponId = $.trim($("#couponId").val());
	params.couponName = $.trim($("#couponName").val());
	params.couponDesc = $.trim($("#couponDesc").val());
	params.couponOrgImgPath = $.trim($("#couponOrgImgPath").val());
	params.couponStartDate = $.trim($("#couponStartDate").val());
	params.couponEndDate = $.trim($("#couponEndDate").val());
	
	if(params.couponName.length <= 0) {
		alert("쿠폰명을 입력해주세요.");
		$("#couponName").focus();
		return;
	}
	
	if(params.couponDesc.length <= 0) {
		alert("쿠폰설명을 입력해주세요.");
		$("#couponDesc").focus();
		return;
	}
	
	if(params.couponStartDate.length <= 0) {
		alert("시작일을 선택해주세요.");
		$("#couponStartDate").focus();
		return;
	}
	
	if(params.couponEndDate.length <= 0) {
		alert("종료일을 선택해주세요.");
		$("#couponEndDate").focus();
		return;
	}
	
	$("#form_data").val(JSON.stringify(params));
	
	$('#couponInputForm').submit();
}

function getCouponContentsHtml(_mode) {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form id='couponInputForm' class='form-horizontal' action='/ajax/ajaxCoupon.php' method='POST' enctype='multipart/form-data'>";
	html += "		<input type='hidden' name='form_data' id='form_data' />";
	html += "		<input type='hidden' name='type' id='type' />";
	html += "		<input type='hidden' id='couponId' />";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='couponName'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>쿠폰명</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='couponName'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='couponDesc'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>쿠폰설명</label>";
	html += "			<div class='col-sm-8'><textarea class='form-control' rows='3' id='couponDesc'></textarea></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='couponImage'>이미지</label>";
	html += "			<div class='col-sm-5'>";
	html += "				<input type='text' class='form-control' id='couponOrgImgPath' readOnly/>";
	html += "			</div>";
	html += "			<div class='col-sm-3'>";
	html += "				<label id='btnAddCouponImage' class='btn btn-info'>이미지 선택<input type='file' name='couponImage' id='couponImage' class='hidden' accept='image/*'/></label>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='couponStartDate'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>사용 시작일</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<div class='input-group date' id='startDateTimePicker'>";
	html += "					<input type='text' class='form-control' id='couponStartDate' />";
	html += "					<span class='input-group-addon'>";
	html += "						<span class='glyphicon glyphicon-calendar'></span>";
	html += "					</span>";
	html += "				</div>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='couponEndDate'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>사용 종료일</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<div class='input-group date' id='endDateTimePicker'>";
	html += "					<input type='text' class='form-control' id='couponEndDate' />";
	html += "					<span class='input-group-addon'>";
	html += "						<span class='glyphicon glyphicon-calendar'></span>";
	html += "					</span>";
	html += "				</div>";
	html += "			</div>";
	html += "		</div>";
	html += "	</form>";
	html += "</div>";
	
	return html;
}

function deleteCoupon(_couponId) {
	var params = {};
	params.type = "delete_coupon";
	params.couponId = _couponId;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCoupon.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("삭제 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}
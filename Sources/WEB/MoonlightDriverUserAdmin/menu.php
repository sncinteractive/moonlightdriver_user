<header class="main-header">
	<a href="/" class="logo">
		<span class="logo-mini">카대리</span>
		<span class="logo-lg"><b>카대리</b></span>
	</a>
	<nav class="navbar navbar-static-top">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		<span class="sr-only">Toggle navigation</span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<li>
					<a href="<?php echo CONF_URL_LOGOUT; ?>">로그아웃</a>
				</li>
			</ul>
		</div>
	</nav>
</header>
<aside class="main-sidebar">
	<section class="sidebar">
		<ul class="sidebar-menu">
			<li class="header">MAIN NAVIGATION</li>
			<li class="<?php if($_SERVER["REQUEST_URI"] == "/") echo "active"; ?>">
				<a href="/">
					<i class="fa fa-home"></i><span>Home</span>
				</a>
			</li>
		<?php 
			$permissions = array("user" => array("inquiry"));
			if($session->checkPermission($permissions)) {
		?>
			<li class="<?php if($_SERVER["REQUEST_URI"] == CONF_URL_USER_MANAGEMENT) echo "active"; ?>">
				<a href="<?php echo CONF_URL_USER_MANAGEMENT; ?>">
					<i class="ion ion-person-add"></i><span>고객 관리</span>
				</a>
			</li>
		<?php 
			}
		?>
		<?php 
			$permissions = array("user_pos" => array("inquiry"));
			if($session->checkPermission($permissions)) {
		?>
			<li class="<?php if($_SERVER["REQUEST_URI"] == CONF_URL_USER_POS_MANAGEMENT) echo "active"; ?>">
				<a href="<?php echo CONF_URL_USER_POS_MANAGEMENT; ?>">
					<i class="ion ion-person-add"></i><span>고객 위치 조회</span>
				</a>
			</li>
		<?php 
			}
		?>
		<?php 
			$permissions = array("driver" => array("inquiry"));
			if($session->checkPermission($permissions)) {
		?>
			<li class="<?php if($_SERVER["REQUEST_URI"] == CONF_URL_DRIVER_MANAGEMENT) echo "active"; ?>">
				<a href="<?php echo CONF_URL_DRIVER_MANAGEMENT; ?>">
					<i class="ion ion-person-add"></i><span>기사 관리</span>
				</a>
			</li>
		<?php 
			}
		?>
			<li class="<?php if($_SERVER["REQUEST_URI"] == CONF_URL_CALL_MANAGEMENT) echo "active"; ?>">
				<a href="<?php echo CONF_URL_CALL_MANAGEMENT; ?>">
					<i class="fa fa-book"></i><span>콜 관리</span>
				</a>
			</li>
			<li class="<?php if($_SERVER["REQUEST_URI"] == CONF_URL_CRACKDOWN_MANAGEMENT) echo "active"; ?>">
				<a href="<?php echo CONF_URL_CRACKDOWN_MANAGEMENT; ?>">
					<i class="fa fa-book"></i><span>음주단속관리</span>
				</a>
			</li>
			<li class="<?php if($_SERVER["REQUEST_URI"] == CONF_URL_CRACKDOWN_RANK_MANAGEMENT) echo "active"; ?>">
				<a href="<?php echo CONF_URL_CRACKDOWN_RANK_MANAGEMENT; ?>">
					<i class="fa fa-book"></i><span>음주단속 랭킹 관리</span>
				</a>
			</li>
			<li class="<?php if($_SERVER["REQUEST_URI"] == CONF_URL_CRACKDOWN_BLACKLIST_MANAGEMENT) echo "active"; ?>">
				<a href="<?php echo CONF_URL_CRACKDOWN_BLACKLIST_MANAGEMENT; ?>">
					<i class="fa fa-book"></i><span>음주단속 허위제보자 관리</span>
				</a>
			</li>
			<li class="<?php if($_SERVER["REQUEST_URI"] == CONF_URL_COMPANY_MANAGEMENT) echo "active"; ?>">
				<a href="<?php echo CONF_URL_COMPANY_MANAGEMENT; ?>">
					<i class="fa fa-book"></i><span>대리회사관리</span>
				</a>
			</li>
			<li class="<?php if($_SERVER["REQUEST_URI"] == CONF_URL_NOTICE_MANAGEMENT) echo "active"; ?>">
				<a href="<?php echo CONF_URL_NOTICE_MANAGEMENT; ?>">
					<i class="fa fa-book"></i><span>공지사항 관리</span>
				</a>
			</li>
			<li class="<?php if($_SERVER["REQUEST_URI"] == CONF_URL_COUPON_MANAGEMENT) echo "active"; ?>">
				<a href="<?php echo CONF_URL_COUPON_MANAGEMENT; ?>">
					<i class="fa fa-book"></i><span>쿠폰 관리</span>
				</a>
			</li>
		<?php 
			$permissions = array("account" => array("inquiry"));
			if($session->checkPermission($permissions)) {
		?>
			<li class="<?php if($_SERVER["REQUEST_URI"] == "/pages/account_management.php") echo "active"; ?>">
				<a href="<?php echo CONF_URL_ACCOUNT_MANAGEMENT; ?>">
					<i class="fa fa-book"></i><span>계정 관리</span>
				</a>
			</li>
		<?php
			}
		?>
		</ul>
	</section>
</aside>
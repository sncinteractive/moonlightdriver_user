<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script>
			function callbackToApp(_ret, _phoneNumber, _ci, _di) {
				if(window.Android) {
					console.log("ANDROID toApp:d:callbackFromWeb::" + _ret + "::" + _phoneNumber + "::" + _ci + "::" + _di);
					Android.callbackFromWeb(_ret, _phoneNumber, _ci, _di);
				} else {
					// IOS Native 전환 호출
					document.location = "toApp::callbackFromWeb::" + _ret + "::" + _phoneNumber + "::" + _ci + "::" + _di;
				}
			}
		</script>
	</head>
</html>

<?php
	header('Content-Type: text/html; charset=utf-8');
	
	require_once ("../inc/config.php");
	require_once_classes(array("CDatabaseManager", "CAccountManager"));
	
	$database_manager = new CDatabaseManager();
	$database = $database_manager->getDb();
	
	$account_manager = new CAccountManager();
	
	/**************************************************************************
		파일명 : popup3.php
		
		본인확인서비스 결과 화면(return url)
	**************************************************************************/
	
	/* 공통 리턴 항목 */
	$rqstSiteNm	=	$_POST["rqst_site_nm"];			// 접속도메인	
	$rqstCausCd	=	$_POST["hs_cert_rqst_caus_cd"];	// 인증요청사유코드 2byte  (00:회원가입, 01:성인인증, 02:회원정보수정, 03:비밀번호찾기, 04:상품구매, 99:기타)

	/**************************************************************************
	 * 모듈 호출	; 본인확인서비스 결과 데이터를 복호화한다.
	 **************************************************************************/

	// 인증결과 암호화 데이터
	$encInfo = $_POST["encInfo"];
	//KCB서버 공개키
	$WEBPUBKEY = trim($_POST["WEBPUBKEY"]);
	//KCB서버 서명값
	$WEBSIGNATURE = trim($_POST["WEBSIGNATURE"]);

	/**************************************************************************
	 * 파라미터에 대한 유효성여부를 검증한다.
	 **************************************************************************/
	if(preg_match('~[^0-9a-zA-Z+/=]~', $encInfo, $match)) {echo "입력 값 확인이 필요합니다"; exit;}
	if(preg_match('~[^0-9a-zA-Z+/=]~', $WEBPUBKEY, $match)) {echo "입력 값 확인이 필요합니다"; exit;}
	if(preg_match('~[^0-9a-zA-Z+/=]~', $WEBSIGNATURE, $match)) {echo "입력 값 확인이 필요합니다"; exit;}

	// ########################################################################
	// # KCB로부터 부여받은 회원사코드(아이디) 설정 (12자리)
	// ########################################################################
	$memId = OK_NAME_ID;										// 회원사코드(아이디)

	// ########################################################################
	// # 운영전환시 변경 필요
	// ########################################################################
// 	if(PRODUCTION_CODE == "REAL") {
// 		$endPointURL = OK_NAME_END_POINT_URL; // 운영 서버
// 	} else {
		$endPointURL = OK_NAME_END_POINT_URL_TEST;	// 테스트 서버
// 	}
		  
	// ########################################################################
	// # 암호화키 파일 설정 (절대경로) - 파일은 주어진 파일명으로 자동 생성되며 생성되지 않으면 S211오류가 발생됨
	// # 파일은 매월초에 갱신되며 만일 파일이 갱신되지 않으면 복화화데이터가 깨지는 현상이 발생됨.
	// ########################################################################
// 	if(PRODUCTION_CODE == "REAL") {
// 		$keyPath = KEY_PATH . "/safecert_".$memId.".key";	// 운영  키파일
// 	} else {
		$keyPath = KEY_PATH . "/safecert_".$memId."_test.key";	// 테스트  키파일
// 	}

	// ########################################################################
	// # 로그 경로 지정 및 권한 부여 (popup2.asp에서 설정된 값과 동일하게 설정)
	// ########################################################################
	$logPath = LOG_PATH;

	// ########################################################################
	// # 옵션값에 'D','L'을 추가하는 경우 로그(logPath변수에 설정된)가 생성됨.
	// # 시스템(환경변수 LANG설정)이 UTF-8인 경우 'U'옵션 추가 ex)$option='SLU'
	// ########################################################################
	$options = "S";	// S:인증결과복호화
		
	// 명령어
	$cmd = array($keyPath, $memId, $endPointURL, $WEBPUBKEY, $WEBSIGNATURE, $encInfo, $logPath, $options);
//	echo "$cmd<br/>";
	
	/**************************************************************************
	okname 실행
	**************************************************************************/
	$output = NULL;
	// 실행
	$ret = okname($cmd, $output);
    //echo "ret=$ret<br/>";
    
	$retcode = "";

	if($ret == 0) {
		$result = explode("\n", $output);
		$retcode = $result[0];
	}
	else {
		if($ret <=200)
			$retcode=sprintf("B%03d", $ret);
		else
			$retcode=sprintf("S%03d", $ret);
	}
	
	$result[1] = iconv("EUC-KR", "UTF-8", $result[1]);
	$result[7] = iconv("EUC-KR", "UTF-8", $result[7]);
	
	if($ret == 0) {
		//인증결과 복호화 성공
		// 인증결과를 확인하여 페이지분기등의 처리를 수행해야한다.
		if ($retcode == "B000") {
			$account_manager->insertUserAuthResult($database->userauthresults, $rqstCausCd, $retcode, $result[1], $result[2], $result[3], $result[4], $result[5], $result[7], $result[8], $result[9], $result[10],
					$result[11], $result[12], $result[16]);
			echo "<script>";
			echo "callbackToApp('OK', '" . $result[12] . "', '" . $result[5] . "', '" . $result[4] . "');";
			echo "</script>";
		}
		else {
			echo "<script>";
			echo "callbackToApp('" . $retcode . "', '', '', '');";
			echo "</script>";
		}
	} else {
		//인증결과 복호화 실패
		echo "<script>";
		echo "callbackToApp('" . $ret . "', '', '', '');";
		echo "</script>";
	}
?>

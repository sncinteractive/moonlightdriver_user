package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.crackdown.CrackdownRankListViewModel;

/**
 * Created by youngmin on 2016-08-11.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CrackdownRankListAck extends NetAPI{
	@JsonProperty("crackdownWeeklyRankList")
	private ArrayList<CrackdownRankListViewModel> mCrackdownWeeklyRankList;
	@JsonProperty("crackdownMonthlyRankList")
	private ArrayList<CrackdownRankListViewModel> mCrackdownMonthlyRankList;

	public CrackdownRankListAck() {}

	public CrackdownRankListAck(ResultData mResultData, ArrayList<CrackdownRankListViewModel> mCrackdownWeeklyRankList, ArrayList<CrackdownRankListViewModel> mCrackdownMonthlyRankList) {
		super(mResultData);
		this.mCrackdownWeeklyRankList = mCrackdownWeeklyRankList;
		this.mCrackdownMonthlyRankList = mCrackdownMonthlyRankList;
	}

	public ArrayList<CrackdownRankListViewModel> getmCrackdownWeeklyRankList() {
		return mCrackdownWeeklyRankList;
	}

	public void setmCrackdownWeeklyRankList(ArrayList<CrackdownRankListViewModel> mCrackdownWeeklyRankList) {
		this.mCrackdownWeeklyRankList = mCrackdownWeeklyRankList;
	}

	public ArrayList<CrackdownRankListViewModel> getmCrackdownMonthlyRankList() {
		return mCrackdownMonthlyRankList;
	}

	public void setmCrackdownMonthlyRankList(ArrayList<CrackdownRankListViewModel> mCrackdownMonthlyRankList) {
		this.mCrackdownMonthlyRankList = mCrackdownMonthlyRankList;
	}
}

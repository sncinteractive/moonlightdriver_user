package kr.moonlightdriver.user.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.viewmodel.common.NoticeData;

/**
 * Created by kyo on 2017-04-26.
 */

public class NoticeListAdapter extends RecyclerView.Adapter<NoticeListAdapter.ViewHolder> {
    private List<NoticeData> mNoticeDataList;

    public NoticeListAdapter(List<NoticeData> noticeDataList) {
        mNoticeDataList = noticeDataList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_setting_notice_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(mNoticeDataList.get(position));
    }

    @Override
    public int getItemCount() {
        return mNoticeDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout mExpandLayout;
        private TextView mTitleTextView;
        private TextView mDateTextView;
        private TextView mContentTextView;
        private ImageView mExpandImageView;

        private ExpandableLayout mExpandableLayout;
        private TextView mContentDetailTextView;

        private ViewHolder(View itemView) {
            super(itemView);

            mExpandLayout = (LinearLayout) itemView.findViewById(R.id.ll_expand);
            mTitleTextView = (TextView) itemView.findViewById(R.id.tv_title);
            mDateTextView = (TextView) itemView.findViewById(R.id.tv_date);
            mContentTextView = (TextView) itemView.findViewById(R.id.tv_content);
            mExpandImageView = (ImageView) itemView.findViewById(R.id.iv_expansion);

            mExpandableLayout = (ExpandableLayout) itemView.findViewById(R.id.expandableLayout);
            mContentDetailTextView = (TextView) itemView.findViewById(R.id.tv_content_detail);

            Utils.setFontToTextViewRecursively(FontManager.getInstance().getFontSeoulNamsanJangM(), itemView);

            mExpandLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean isSelected = mExpandLayout.isSelected();

                    mExpandLayout.setSelected(!isSelected);
                    mExpandImageView.setSelected(!isSelected);

                    mExpandableLayout.toggle();
                }
            });
        }

        public void bind(NoticeData data) {
            mTitleTextView.setText(data.getmTitle());

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            mDateTextView.setText(simpleDateFormat.format(new Date(data.getmStartDate())));

            mContentTextView.setText(data.getmContents());
            mContentDetailTextView.setText(data.getmContents());
        }
    }
}
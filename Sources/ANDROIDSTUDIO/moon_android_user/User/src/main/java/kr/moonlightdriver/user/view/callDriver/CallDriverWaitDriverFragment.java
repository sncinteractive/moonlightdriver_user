package kr.moonlightdriver.user.view.callDriver;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.BusProvider;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.CustomInfoWindowAdapter;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetApi.TMapRouteModel;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.TMapRouteResultCallback;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewmodel.callDriver.DriverInfoViewModel;
import kr.moonlightdriver.user.viewmodel.common.LocationData;

public class CallDriverWaitDriverFragment extends Fragment implements OnMapReadyCallback {
    private static final String TAG = "CallDriverWaitDriverFragment";

    private MainActivity mMainActivity;

    private View mViewMyself;

    // 맵
    private GoogleMap mGoogleMap;
    private LatLng mCurrentLocation;
    private Marker mCurrentPositionMarker;
    private Marker mCurrentPositionMarkerInner;
    private Polyline mPolyline;

    private LatLng mDriverLocation;
    private Marker mDriverPositionMarker;

    private RelativeLayout mPopupElectronicBoard;
    private TextView mElectronicBoardDateTextView;

    private Handler mElectronicBoardDataUpdateHandler;
    private Runnable mElectronicBoardDataUpdateRunnable;
    private Handler mDriverPathUpdateHandler;
    private Runnable mDriverPathUpdateRunnable;

    private SimpleDateFormat mSimpleDateFormat;

    private LocationData mSourceLocationData;

    private boolean mDriverPathUpdateEnabled;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            mViewMyself = inflater.inflate(R.layout.fragment_call_driver_wait_driver, container, false);

            mMainActivity = (MainActivity) getActivity();

            SharedPreferences prefs = Utils.getGCMPreferences(mMainActivity);
            mSourceLocationData = new LocationData(prefs.getString(Configuration.PROPERTY_CALL_INFO_SOURCE_LOCATION, null));
            LocationData targetLocationData = new LocationData(prefs.getString(Configuration.PROPERTY_CALL_INFO_TARGET_LOCATION, null));
            DriverInfoViewModel driverInfo = new DriverInfoViewModel(prefs.getString(Configuration.PROPERTY_CALL_INFO_DRIVER_INFO, null));

            // 배차정보
            ImageView driverThumbnailImageView = (ImageView) mViewMyself.findViewById(R.id.iv_driver_thumbnail);
            String imageUrl = driverInfo.getmDriverPhoto();
            if (!Utils.isStringNullOrEmpty(imageUrl)) {
                new Utils.DownloadImageTask(driverThumbnailImageView).execute(Configuration.IMAGE_SERVER_HOST + imageUrl);
            }

            TextView pathInfoTextView = (TextView) mViewMyself.findViewById(R.id.tv_path_info);
            String sourceName = mSourceLocationData.getmLocationName();
            if (sourceName.length() > 3) {
                sourceName = sourceName.substring(0, 4);
                sourceName += "...";
            }
            String targetName = targetLocationData.getmLocationName();
            if (targetName.length() > 3) {
                targetName = targetName.substring(0, 4);
                targetName += "...";
            }
            pathInfoTextView.setText(getString(R.string.popup_call_driver_assigned_path_info_format, sourceName, targetName));

            TextView driverInfoTextView = (TextView) mViewMyself.findViewById(R.id.tv_driver_info);
            driverInfoTextView.setText(getString(R.string.popup_call_driver_assigned_driver_info_format,
                driverInfo.getMaskedDriverName(),
                Utils.convertDriverGradeAverageToString(driverInfo.getmDriverGradeAverage()),
                driverInfo.getmDriverPhoneNumber()));

            TextView messageTextView = (TextView) mViewMyself.findViewById(R.id.tv_message);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), pathInfoTextView, driverInfoTextView, messageTextView);

            // 현위치로
            ImageButton currentLocationButton = (ImageButton) mViewMyself.findViewById(R.id.ib_current_location);
            currentLocationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mGoogleMap != null) {
                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mCurrentLocation, Utils.GOOGLE_MAP_DEFAULT_ZOOM));
                    }
                }
            });

            // 줌인
            ImageButton zoomInButton = (ImageButton) mViewMyself.findViewById(R.id.ib_zoom_in);
            zoomInButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mGoogleMap != null) {
                        mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
                    }
                }
            });

            // 줌아웃
            ImageButton zoomOutButton = (ImageButton) mViewMyself.findViewById(R.id.ib_zoom_out);
            zoomOutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mGoogleMap != null) {
                        mGoogleMap.animateCamera(CameraUpdateFactory.zoomOut());
                    }
                }
            });

            // 전광판 버튼
            ImageButton electronicBoardButton = (ImageButton) mViewMyself.findViewById(R.id.ib_electronic_board);
            electronicBoardButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPopupElectronicBoard.setVisibility(View.VISIBLE);
                    mMainActivity.setTabMenuVisibility(View.GONE);
                }
            });

            // 전광판 팝업 레이아웃
            mPopupElectronicBoard = (RelativeLayout) mViewMyself.findViewById(R.id.popup_call_driver_electronic_board);
            mPopupElectronicBoard.setVisibility(View.GONE);

            TextView electronicBoardSourceTextView = (TextView) mPopupElectronicBoard.findViewById(R.id.tv_popup_call_driver_electronic_board_source);
            electronicBoardSourceTextView.setText(getString(R.string.popup_call_driver_electronic_board_source_format, mSourceLocationData.getmLocationName()));

            TextView electronicBoardTargetTextView = (TextView) mPopupElectronicBoard.findViewById(R.id.tv_popup_call_driver_electronic_board_target);
            electronicBoardTargetTextView.setText(getString(R.string.popup_call_driver_electronic_board_target_format, targetLocationData.getmLocationName()));

            mElectronicBoardDateTextView = (TextView) mPopupElectronicBoard.findViewById(R.id.tv_popup_call_driver_electronic_board_date);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanM(), electronicBoardSourceTextView, electronicBoardTargetTextView, mElectronicBoardDateTextView);

            TextView electronicBoardPhoneNumberTextView = (TextView) mPopupElectronicBoard.findViewById(R.id.tv_popup_call_driver_electronic_board_phone_number);
            electronicBoardPhoneNumberTextView.setText(driverInfo.getmDriverPhoneNumber());

            TextView electronicBoardDriverNameTextView = (TextView) mPopupElectronicBoard.findViewById(R.id.tv_popup_call_driver_electronic_board_driver_name);
            electronicBoardDriverNameTextView.setText(driverInfo.getmDriverName());

            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanVert(), electronicBoardSourceTextView, electronicBoardTargetTextView);

            // 전광판 팝업 닫기 버튼
            ImageButton electronicBoardPopupCloseButton = (ImageButton) mPopupElectronicBoard.findViewById(R.id.ib_popup_call_driver_electronic_board_close);
            electronicBoardPopupCloseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPopupElectronicBoard.setVisibility(View.GONE);
                    mMainActivity.setTabMenuVisibility(View.VISIBLE);
                }
            });

            mSimpleDateFormat = new SimpleDateFormat("yy/MM/dd hh:mm a");

            mCurrentLocation = MainActivity.mCurrentLocation;
            mDriverLocation = driverInfo.getmLocation();

            mElectronicBoardDataUpdateHandler = new Handler();
            mElectronicBoardDataUpdateRunnable = new Runnable() {
                @Override
                public void run() {
                    mElectronicBoardDateTextView.setText(mSimpleDateFormat.format(Calendar.getInstance().getTime()));
                    mElectronicBoardDataUpdateHandler.postDelayed(this, 1000);
                }
            };
            mElectronicBoardDataUpdateHandler.post(mElectronicBoardDataUpdateRunnable);

            mDriverPathUpdateHandler = new Handler();
            mDriverPathUpdateRunnable = new Runnable() {
                @Override
                public void run() {
                    updateDriverPath();
                    mDriverPathUpdateHandler.postDelayed(this, 1000 * 60);
                }
            };
            mDriverPathUpdateHandler.post(mDriverPathUpdateRunnable);

            mDriverPathUpdateEnabled = false;

            setUpMapIfNeeded();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mViewMyself;
    }

    private void setUpMapIfNeeded() {
        try {
            if (mGoogleMap == null) {
                SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_google_map);
                mapFragment.getMapAsync(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onMapReady(GoogleMap googleMap) {
        try {
            mGoogleMap = googleMap;

            mGoogleMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(getContext()));

            mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
            mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
            mGoogleMap.getUiSettings().setCompassEnabled(false);

            mCurrentPositionMarker = mGoogleMap.addMarker(new MarkerOptions().position(mCurrentLocation)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_current_location_outer))
                .anchor(0.5f, 0f));
            mCurrentPositionMarkerInner = mGoogleMap.addMarker(new MarkerOptions().position(mCurrentLocation)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_current_location_inner))
                .anchor(0.5f, -0.3739f));

            mDriverPositionMarker = mGoogleMap.addMarker(new MarkerOptions().position(mCurrentLocation)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_driver_blue_120)));
            //mDriverPositionMarker.showInfoWindow();

            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCurrentLocation, Utils.GOOGLE_MAP_DEFAULT_ZOOM));

            PolylineOptions polylineOptions = new PolylineOptions();
            polylineOptions.width(15f); // default width 10
            mPolyline = mGoogleMap.addPolyline(polylineOptions);
            mPolyline.setVisible(false);

            mDriverPathUpdateEnabled = true;

            mMainActivity.removeSplash();

            updateDriverPath();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateDriverPath() {
        try {
            if (!mDriverPathUpdateEnabled) {
                return;
            }

            NetClient.requestTMapRoute(getContext(), mDriverLocation, mSourceLocationData.getmLocation(), null, new TMapRouteResultCallback() {
                @Override
                public void onResult(boolean success, TMapRouteModel tMapRouteModel) {
                    if (success) {
                        setPolylinePoints(tMapRouteModel.getPointList());
                        setPolylineVisibility(true);
                        updateCameraBounds();

                        String snippet = getString(R.string.remaining_info_format,
                                Utils.convertTimeString(tMapRouteModel.getTotalTime()),
                                Utils.convertDistanceString(tMapRouteModel.getTotalDistance()));
                        mDriverPositionMarker.setSnippet(snippet);
                        mDriverPositionMarker.showInfoWindow();
                    } else {
                        setPolylineVisibility(false);
                    }

                    mMainActivity.hideLoadingAnim();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            mMainActivity.hideLoadingAnim();
        }
    }

    private void setPolylinePoints(List<LatLng> latLngList) {
        try {
            mPolyline.setPoints(latLngList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setPolylineVisibility(boolean visible) {
        mPolyline.setVisible(visible);
    }

    private void updateCameraBounds() {
        try {
            if (mGoogleMap == null) {
                return;
            }

            List<LatLng> latLngList = new ArrayList<>();
            if (mDriverLocation != null) {
                latLngList.add(mDriverLocation);
            }

            if (mSourceLocationData != null) {
                latLngList.add(mSourceLocationData.getmLocation());
            }

            if (latLngList.size() >= 2) {
                LatLngBounds.Builder builder = LatLngBounds.builder();
                for (LatLng latLng : latLngList) {
                    builder = builder.include(latLng);
                }

                int padding = 210;
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), padding));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        BusProvider.getInstance().unregister(this);
        super.onDestroyView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == Activity.RESULT_OK) {
                switch (requestCode) {
                    case Configuration.POSITION_CHANGED: {
                        onChangedMyLocation();
                    }
                    break;
                    case Configuration.CALL_DRIVER_POSITION_CHANGED: {
                        double lat = data.getDoubleExtra("lat", 0D);
                        double lng = data.getDoubleExtra("lng", 0D);

                        if (lat > 0 && lng > 0) {
                            mDriverLocation = new LatLng(lat, lng);

                            onChangedDriverLocation();
                        }
                    }
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onChangedMyLocation() {
        try {
            if (mGoogleMap != null) {
                mCurrentLocation = MainActivity.mCurrentLocation;

                if (mCurrentPositionMarker != null) {
                    mCurrentPositionMarker.setPosition(mCurrentLocation);
                }

                if (mCurrentPositionMarkerInner != null) {
                    mCurrentPositionMarkerInner.setPosition(mCurrentLocation);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onChangedDriverLocation() {
        try {
            if (mGoogleMap != null) {
                if (mDriverPositionMarker != null) {
                    mDriverPositionMarker.setPosition(mDriverLocation);
                }

                //mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(mDriverLocation));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        mElectronicBoardDataUpdateHandler.removeCallbacksAndMessages(null);
        mDriverPathUpdateHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onResume() {
        super.onResume();

        mElectronicBoardDataUpdateHandler.post(mElectronicBoardDataUpdateRunnable);
        mDriverPathUpdateHandler.post(mDriverPathUpdateRunnable);
    }
}

package kr.moonlightdriver.user.common;

import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;
import com.skt.Tmap.TMapAddressInfo;
import com.skt.Tmap.TMapData;

import java.util.HashMap;

/**
 * Created by youngmin on 2016-07-08.
 */
public class TmapManager {
    private static final String TAG = "TmapManager";
    private TMapData mTmapData;

    public TmapManager() {
        mTmapData = new TMapData();
    }

    public HashMap<String, Object> convertGpsToAddress(final LatLng _position, final ResultCallback _callback) {
        HashMap<String, Object> ret = new HashMap<String, Object>();

        try {
            new AsyncTask<Void, Integer, TMapAddressInfo>() {
                @Override
                protected TMapAddressInfo doInBackground(Void... voids) {
                    try {
                        TMapAddressInfo retAddressInfo = mTmapData.reverseGeocoding(_position.latitude, _position.longitude, "A03");

                        Utils.log("error", TAG, "result : " + retAddressInfo.strFullAddress);

                        return retAddressInfo;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(TMapAddressInfo _retAddressInfo) {
                    super.onPostExecute(_retAddressInfo);

                    try {
                        if (_retAddressInfo != null) {
                            HashMap<String, Object> ret = new HashMap<String, Object>();
                            ret.put("addressInfo", _retAddressInfo);

                            _callback.resultCallback(ret);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ret;
    }
}

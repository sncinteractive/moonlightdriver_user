package kr.moonlightdriver.user.view.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

import kr.moonlightdriver.user.Adapter.HistoryListAdapter;
import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.HistoryListAck;
import kr.moonlightdriver.user.common.network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewmodel.callDriver.CallInfoViewModel;
import kr.moonlightdriver.user.viewmodel.callDriver.DriverInfoViewModel;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.HistoryListViewModel;

/**
 * Created by youngmin on 2016-06-21.
 */
public class SettingHistoryFragment extends Fragment implements HistoryListAdapter.InteractionListener {

    private MainActivity mMainActivity;

    private View mViewMyself;

    private ListView mListView;
    private HistoryListAdapter mListAdapter;
    private List<HistoryListViewModel> mHistoryModelList;

    private int mHistoryIndex;

    private RelativeLayout mPopupEvaluateDriver;
    private ImageView mPopupEvaluateDirverThumbnailImageView;
    private List<ImageButton> mPopupEvaluateDriverStarButtonList;
    private TextView mPopupEvaluateDateTextView;
    private TextView mPopupEvaluatePathSourceTextView;
    private TextView mPopupEvaluatePathTargetTextView;
    private TextView mPopupEvaluatePriceTextView;
    private TextView mPopupEvaluateDriverInfoTextView;
    private TextView mPopupEvaluateCompanyTextView;

    private RelativeLayout mPopupReportUnfairness;
    private EditText mPopupReportUnfairnessEditText;
    private TextView mPopupReportUnfairnessDriverInfoTextView;
    private TextView mPopupReportUnfairnessCompanyTextView;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            mMainActivity = (MainActivity) getActivity();

            mHistoryModelList = new ArrayList<>();

            mViewMyself = inflater.inflate(R.layout.fragment_setting_history_manage, container, false);

            TextView headerTitleTextView = (TextView) mViewMyself.findViewById(R.id.tv_header_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanB(), headerTitleTextView);

            LinearLayout goBackButton = (LinearLayout) mViewMyself.findViewById(R.id.ib_setting_goback);
            goBackButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getActivity().getSupportFragmentManager().beginTransaction().remove(SettingHistoryFragment.this).commit();
                }
            });

            mListAdapter = new HistoryListAdapter(getContext(), R.layout.row_setting_history_list, mHistoryModelList, this);
            mListView = (ListView) mViewMyself.findViewById(R.id.lv_history);
            mListView.setAdapter(mListAdapter);

            initializeEvaluatePopup(mViewMyself);

            initializeReportUnfairnessPopup(mViewMyself);

            mHistoryIndex = -1;

            // 운행내역 조회 서버 연동시 주석 제거
//            testHistoryListResponse();
            requestHistoryList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mViewMyself;
    }

    private void initializeEvaluatePopup(View view) {
        try {
            mPopupEvaluateDriver = (RelativeLayout) view.findViewById(R.id.popup_evaluate_driver);
            mPopupEvaluateDriver.setVisibility(View.GONE);

            TextView titleTextView = (TextView) mPopupEvaluateDriver.findViewById(R.id.tv_popup_evaluate_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanVert(), titleTextView);

            TextView dateTitleTextView = (TextView) mPopupEvaluateDriver.findViewById(R.id.tv_popup_evaluate_date_title);
            TextView pathTitleTextView = (TextView) mPopupEvaluateDriver.findViewById(R.id.tv_popup_evaluate_path_title);
            TextView priceTitleTextView = (TextView) mPopupEvaluateDriver.findViewById(R.id.tv_popup_evaluate_price_title);
            TextView driverInfoTitleTextView = (TextView) mPopupEvaluateDriver.findViewById(R.id.tv_popup_evaluate_driver_info_title);
            TextView companyTitleTextView = (TextView) mPopupEvaluateDriver.findViewById(R.id.tv_popup_evaluate_company_title);
            TextView descriptionTextView = (TextView) mPopupEvaluateDriver.findViewById(R.id.tv_popup_evaluate_description);

            mPopupEvaluateDateTextView = (TextView) mPopupEvaluateDriver.findViewById(R.id.tv_popup_evaluate_date);
            mPopupEvaluatePathSourceTextView = (TextView) mPopupEvaluateDriver.findViewById(R.id.tv_popup_evaluate_path_source);
            mPopupEvaluatePathTargetTextView = (TextView) mPopupEvaluateDriver.findViewById(R.id.tv_popup_evaluate_path_target);
            mPopupEvaluatePriceTextView = (TextView) mPopupEvaluateDriver.findViewById(R.id.tv_popup_evaluate_price);
            mPopupEvaluateDriverInfoTextView = (TextView) mPopupEvaluateDriver.findViewById(R.id.tv_popup_evaluate_driver_info);
            mPopupEvaluateCompanyTextView = (TextView) mPopupEvaluateDriver.findViewById(R.id.tv_popup_evaluate_company);

            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(),
                dateTitleTextView, pathTitleTextView, priceTitleTextView, driverInfoTitleTextView, companyTitleTextView, descriptionTextView,
                mPopupEvaluateDateTextView, mPopupEvaluatePathSourceTextView, mPopupEvaluatePathTargetTextView, mPopupEvaluatePriceTextView, mPopupEvaluateDriverInfoTextView, mPopupEvaluateCompanyTextView);

            mPopupEvaluateDirverThumbnailImageView = (ImageView) mPopupEvaluateDriver.findViewById(R.id.iv_popup_evaluate_driver_thumbnail);

            mPopupEvaluateDriverStarButtonList = new ArrayList<>();
            LinearLayout starLayout = (LinearLayout) mPopupEvaluateDriver.findViewById(R.id.ll_popup_evaluate_driver_star_root);
            int childCount = starLayout.getChildCount();
            for (int i = 0; i < childCount; i++) {
                ImageButton starButton = (ImageButton) starLayout.getChildAt(i);
                if (starButton == null) {
                    continue;
                }
                starButton.setTag(i);
                starButton.setOnClickListener(new StarButtonClickListener());
                mPopupEvaluateDriverStarButtonList.add(starButton);
            }

            ImageButton evaluateDriverPopupCloseButton = (ImageButton) view.findViewById(R.id.ib_popup_evaluate_driver_close);
            evaluateDriverPopupCloseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPopupEvaluateDriver.setVisibility(View.GONE);
                }
            });

            ImageButton evaluateDriverPopupSubmitButton = (ImageButton) view.findViewById(R.id.ib_popup_evaluate_driver_submit);
            evaluateDriverPopupSubmitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    requestEvaluateDriver();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class StarButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int clickedPosition = (int) v.getTag();
            int size = mPopupEvaluateDriverStarButtonList.size();
            for (int i = 0; i < size; i++) {
                ImageButton starButton = mPopupEvaluateDriverStarButtonList.get(i);
                starButton.setSelected(i <= clickedPosition);
            }
        }
    }

    private void initializeReportUnfairnessPopup(View view) {
        try {
            mPopupReportUnfairness = (RelativeLayout) mViewMyself.findViewById(R.id.popup_report_unfairness);

            TextView titleTextView = (TextView) mPopupReportUnfairness.findViewById(R.id.tv_popup_report_unfairness_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanVert(), titleTextView);

            TextView driverInfoTitleTextView = (TextView) mPopupReportUnfairness.findViewById(R.id.tv_popup_report_unfairness_driver_info_title);
            TextView companyTitleTextView = (TextView) mPopupReportUnfairness.findViewById(R.id.tv_popup_report_unfairness_driver_company_title);

            mPopupReportUnfairnessDriverInfoTextView = (TextView) mPopupReportUnfairness.findViewById(R.id.tv_popup_report_unfairness_driver_info);
            mPopupReportUnfairnessCompanyTextView = (TextView) mPopupReportUnfairness.findViewById(R.id.tv_popup_report_unfairness_driver_company);
            mPopupReportUnfairnessEditText = (EditText) mPopupReportUnfairness.findViewById(R.id.et_popup_report_unfairness_report_message);

            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(),
                driverInfoTitleTextView, companyTitleTextView,
                mPopupReportUnfairnessDriverInfoTextView, mPopupReportUnfairnessCompanyTextView, mPopupReportUnfairnessEditText);

            ImageButton reportUnfairnessCloseButton = (ImageButton) mViewMyself.findViewById(R.id.ib_popup_report_unfairness_close);
            reportUnfairnessCloseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPopupReportUnfairness.setVisibility(View.GONE);
                }
            });

            ImageButton reportUnfairnessSubmitButton = (ImageButton) mViewMyself.findViewById(R.id.ib_popup_report_unfairness_submit);
            reportUnfairnessSubmitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HistoryListViewModel callHistoryInfo = mHistoryModelList.get(mHistoryIndex);
                    if (callHistoryInfo != null) {
                        String content = mPopupReportUnfairnessEditText.getText().toString();
                        if (!Utils.isStringNullOrEmpty(content)) {
                            // 부조리 신고 서버 연동시 주석 제거
//                            testReportUnfairnessResponse();
                            requestReportUnfairness(callHistoryInfo.getId(), callHistoryInfo.getDriverInfo().getmDriverId(), content);
                        } else {
                            CommonDialog.showSimpleDialog(getContext(), "신고할 내용을 적어주세요.");
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestHistoryList() {
        mMainActivity.showLoadingAnim();

        RequestParams requestParams = new RequestParams();
        requestParams.add("userId", mMainActivity.mUserDetail.getUserId());

        // 운행내역 조회 서버 연동시 주석 없애고 url 입력할 것
        NetClient.send(mMainActivity, Configuration.URL_CALL_DRIVER_HISTORY_CALL, "POST", requestParams, new HistoryListAck(), new NetResponseCallback(new HistoryListResponse()));
    }

    @Override
    public void OnReliefMessageClick(int position) {
        // 안심문자 이용 내역 보기 (기획서에 내용이 없어서 인터페이스만 만들어 놓음)
    }

    @Override
    public void OnEvaluateDriverClick(int position) {
        showEvaluateDriverPopup(position);
    }

    @Override
    public void OnReportUnfairnessClick(int position) {
        showReportUnfairnessPopup(position);
    }

    private class HistoryListResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            mMainActivity.hideLoadingAnim();

            if (_netAPI != null) {
                ResultData resultData = _netAPI.getmResultData();
                if (resultData.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                    CommonDialog.showSimpleDialog(mMainActivity, resultData.getmDetail());
                    return;
                }

                mHistoryModelList.clear();
                HistoryListAck ack = (HistoryListAck) _netAPI;
                for (CallInfoViewModel item : ack.getmHistoryList()) {
                    String phoneNumber = "";
                    if (item.getmDriver().getmDriverPhoneNumber().length() > 0) {
                        phoneNumber = item.getmDriver().getmDriverPhoneNumber().substring(item.getmDriver().getmDriverPhoneNumber().length() - 4);
                    }

                    DriverInfoViewModel driverInfoModel = new DriverInfoViewModel("", phoneNumber, "", 4.27f, 0d, 0d);
                    mHistoryModelList.add(new HistoryListViewModel(item.getmCallId(), item.getmCreatedTime(), item.getmUseReliefMessage(), item.getmStart().getmAddress(), item.getmEnd().getmAddress(), getString(R.string.history_list_driver_company_default), item.getmMoney(), item.getmEvaluatedYN().equals("Y"), item.getmReportedUnfairnessYN().equals("Y"), driverInfoModel));
                }

                mListAdapter.notifyDataSetChanged();
            }
        }

    }

    private void testHistoryListResponse() {
        List<HistoryListViewModel> historyList = new ArrayList<>();
        {
            DriverInfoViewModel driverInfoModel = new DriverInfoViewModel("임꺽정", "1111", "", 4.27f, 0d, 0d);
            historyList.add(new HistoryListViewModel("id1", 1496328300000L, "카대리로 가는중입니다", "문산", "서울시 강남구 대치동 포스코로 123 포스코빌딩 9999-999", "부자 대리운전", 18000, true, true, driverInfoModel));
        }
        {
            DriverInfoViewModel driverInfoModel = new DriverInfoViewModel("제갈공명", "2222", "", 4.531f, 0d, 0d);
            historyList.add(new HistoryListViewModel("id2", 1496414700000L, "", "서울", "문산", "따봉 대리운전", 30000, false, false, driverInfoModel));
        }
        {
            DriverInfoViewModel driverInfoModel = new DriverInfoViewModel("돌쇠", "3333", "", 3.1437f, 0d, 0d);
            historyList.add(new HistoryListViewModel("id3", 1496501100000L, "", "서울", "성남", "따봉 대리운전", 25000, true, false, driverInfoModel));
        }
        {
            DriverInfoViewModel driverInfoModel = new DriverInfoViewModel("임꺽정", "1111", "", 4.27f, 0d, 0d);
            historyList.add(new HistoryListViewModel("id4", 1496587500000L, "카대리로 가는중입니다", "문산", "서울시 강남구 대치동 포스코로 123 포스코빌딩 9999-999", "부자 대리운전", 18000, true, false, driverInfoModel));
        }
        {
            DriverInfoViewModel driverInfoModel = new DriverInfoViewModel("제갈공명", "2222", "", 4.531f, 0d, 0d);
            historyList.add(new HistoryListViewModel("id5", 1496673900000L, "", "서울", "문산", "따봉 대리운전", 30000, false, false, driverInfoModel));
        }
        {
            DriverInfoViewModel driverInfoModel = new DriverInfoViewModel("돌쇠", "3333", "", 3.1437f, 0d, 0d);
            historyList.add(new HistoryListViewModel("id6", 1496675700000L, "", "서울", "성남", "따봉 대리운전", 25000, true, false, driverInfoModel));
        }

//        HistoryListAck successAck = new HistoryListAck(new ResultData(Enums.NetResultCode.SUCCESS.getValue(), null, null), historyList);
//        new SettingHistoryFragment.HistoryListResponse().onResponse(successAck);
    }

    private void showEvaluateDriverPopup(int position) {
        try {
            mPopupEvaluateDriver.setVisibility(View.VISIBLE);

            mHistoryIndex = position;

            HistoryListViewModel model = mHistoryModelList.get(position);

            mPopupEvaluateDateTextView.setText(Utils.convertSimpleDateFormat("yyyy-MM-dd KK-mm a", model.getDate()));
            mPopupEvaluatePathSourceTextView.setText(getString(R.string.history_evaluate_driver_path_source, model.getFrom()));
            mPopupEvaluatePathTargetTextView.setText(getString(R.string.history_evaluate_driver_path_target, model.getTo()));
            mPopupEvaluatePriceTextView.setText(Utils.convertToPriceString(model.getPrice()));

            String driverInfo = getString(R.string.string_format_history_list_driver_info,
                model.getDriverInfo().getMaskedDriverName(),
                Utils.convertDriverGradeAverageToString(model.getDriverInfo().getmDriverGradeAverage()),
                model.getDriverInfo().getmDriverPhoneNumber());
            mPopupEvaluateDriverInfoTextView.setText(driverInfo);

            mPopupEvaluateCompanyTextView.setText(model.getCompanyName());

            String imageUrl = model.getDriverInfo().getmDriverPhoto();
            if (!Utils.isStringNullOrEmpty(imageUrl)) {
                new Utils.DownloadImageTask(mPopupEvaluateDirverThumbnailImageView).execute(Configuration.IMAGE_SERVER_HOST + imageUrl);
            }

        } catch (Exception e) {
            e.printStackTrace();
            mPopupEvaluateDriver.setVisibility(View.GONE);
        }
    }

    private void showReportUnfairnessPopup(int position) {
        try {
            mPopupReportUnfairness.setVisibility(View.VISIBLE);

            mHistoryIndex = position;

            HistoryListViewModel model = mHistoryModelList.get(position);

            mPopupReportUnfairnessEditText.setText("");
            String driverInfo = getString(R.string.string_format_history_list_driver_info,
                model.getDriverInfo().getMaskedDriverName(),
                Utils.convertDriverGradeAverageToString(model.getDriverInfo().getmDriverGradeAverage()),
                model.getDriverInfo().getmDriverPhoneNumber());
            mPopupReportUnfairnessDriverInfoTextView.setText(driverInfo);
            mPopupReportUnfairnessCompanyTextView.setText(model.getCompanyName());
        } catch (Exception e) {
            e.printStackTrace();
            mPopupReportUnfairness.setVisibility(View.GONE);
        }
    }

    private void requestEvaluateDriver() {
        mMainActivity.showLoadingAnim();
        try {
            HistoryListViewModel callHistoryInfo = mHistoryModelList.get(mHistoryIndex);
            if (callHistoryInfo != null) {
                int starNum = 0;
                for (ImageButton starButton : mPopupEvaluateDriverStarButtonList) {
                    if (starButton.isSelected()) {
                        starNum++;
                    }
                }

                RequestParams requestParams = new RequestParams();
                requestParams.put("userId", mMainActivity.mUserDetail.getUserId());
                requestParams.put("callId", callHistoryInfo.getId());
                requestParams.put("driverId", callHistoryInfo.getDriverInfo().getmDriverId());
                requestParams.put("starNum", starNum);

                // 서버 연동시 url 변경
                NetClient.send(mMainActivity, Configuration.URL_CALL_DRIVER_EVALUATE, "POST", requestParams, new NoResponseDataAck(), new NetResponseCallback(new EvaluateDriverResponse()));
            }
        } catch (Exception e) {
            e.printStackTrace();
            mMainActivity.hideLoadingAnim();
        }
    }

    private class EvaluateDriverResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            try {
                mMainActivity.hideLoadingAnim();

                if (_netAPI != null) {
                    ResultData resultData = _netAPI.getmResultData();
                    if (resultData.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                        CommonDialog.showSimpleDialog(mMainActivity, resultData.getmDetail());
                        return;
                    }

                    mPopupEvaluateDriver.setVisibility(View.GONE);

                    HistoryListViewModel model = mHistoryModelList.get(mHistoryIndex);
                    model.setEvaluated(true);

                    mListAdapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void testEvaluateDriverResponse() {
        NetAPI successAck = new NetAPI(new ResultData(Enums.NetResultCode.SUCCESS.getValue(), null, null));
        new EvaluateDriverResponse().onResponse(successAck);
    }

    private void requestReportUnfairness(String callId, String driverId, String content) {
        mMainActivity.showLoadingAnim();

        RequestParams requestParams = new RequestParams();
        requestParams.put("userId", mMainActivity.mUserDetail.getUserId());
        requestParams.put("callId", callId);
        requestParams.put("driverId", driverId);
        requestParams.put("unfairness", content);

        // 서버 연동시 url 변경
        NetClient.send(mMainActivity, Configuration.URL_CALL_DRIVER_REPORT_UNFAIRNESS, "POST", requestParams, new NoResponseDataAck(), new NetResponseCallback(new ReportUnfairnessResponse()));
    }

    private class ReportUnfairnessResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            try {
                mMainActivity.hideLoadingAnim();

                if (_netAPI != null) {
                    ResultData resultData = _netAPI.getmResultData();
                    if (resultData.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                        CommonDialog.showSimpleDialog(mMainActivity, resultData.getmDetail());
                        return;
                    }

                    mPopupReportUnfairness.setVisibility(View.GONE);

                    HistoryListViewModel model = mHistoryModelList.get(mHistoryIndex);
                    model.setReportedUnfairness(true);

                    mListAdapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void testReportUnfairnessResponse() {
        NetAPI successAck = new NetAPI(new ResultData(Enums.NetResultCode.SUCCESS.getValue(), null, null));
        new ReportUnfairnessResponse().onResponse(successAck);
    }
}

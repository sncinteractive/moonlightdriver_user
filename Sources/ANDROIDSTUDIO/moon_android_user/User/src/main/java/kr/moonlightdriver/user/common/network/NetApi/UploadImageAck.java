package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;

/**
 * Created by youngmin on 2016-08-18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UploadImageAck extends NetAPI {
	@JsonProperty("filePath")
	private String mFilePath;

	public UploadImageAck() {	}

	public UploadImageAck(ResultData mResultData, String mFilePath) {
		super(mResultData);
		this.mFilePath = mFilePath;
	}

	public String getmFilePath() {
		return mFilePath;
	}

	public void setmFilePath(String mFilePath) {
		this.mFilePath = mFilePath;
	}
}

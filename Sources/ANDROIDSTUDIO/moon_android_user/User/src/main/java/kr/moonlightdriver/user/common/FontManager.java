package kr.moonlightdriver.user.common;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by kyo on 2017-04-13.
 */

public class FontManager {
    private static final FontManager sInstance = new FontManager();

    public static FontManager getInstance() {
        return sInstance;
    }

    private FontManager() {
    }

    public Typeface getFontSeoulNamsanM() {
        return mFontSeoulNamsanM;
    }

    public Typeface getFontSeoulNamsanL() {
        return mFontSeoulNamsanL;
    }

    public Typeface getFontSeoulNamsanB() {
        return mFontSeoulNamsanB;
    }

    public Typeface getFontSeoulNamsanVert() {
        return mFontSeoulNamsanVert;
    }

    public Typeface getFontSeoulNamsanJangL() {
        return mFontSeoulNamsanJangL;
    }

    public Typeface getFontSeoulNamsanJangM() {
        return mFontSeoulNamsanJangM;
    }

    public Typeface getFontSeoulNamsanJangB() {
        return mFontSeoulNamsanJangB;
    }

    public void initialize(Context _context) {
        mFontSeoulNamsanM = Typeface.createFromAsset(_context.getAssets(), "fonts/SeoulNamsanM.otf");
        mFontSeoulNamsanL = Typeface.createFromAsset(_context.getAssets(), "fonts/SeoulNamsanL.otf");
        mFontSeoulNamsanB = Typeface.createFromAsset(_context.getAssets(), "fonts/SeoulNamsanB.otf");
        mFontSeoulNamsanVert = Typeface.createFromAsset(_context.getAssets(), "fonts/SeoulNamsanvert.otf");
        mFontSeoulNamsanJangL = Typeface.createFromAsset(_context.getAssets(), "fonts/SeoulNamsanJangL.otf");
        mFontSeoulNamsanJangM = Typeface.createFromAsset(_context.getAssets(), "fonts/SeoulNamsanJangM.otf");
        mFontSeoulNamsanJangB = Typeface.createFromAsset(_context.getAssets(), "fonts/SeoulNamsanJangB.otf");
    }

    private Typeface mFontSeoulNamsanM;
    private Typeface mFontSeoulNamsanL;
    private Typeface mFontSeoulNamsanB;
    private Typeface mFontSeoulNamsanVert;
    private Typeface mFontSeoulNamsanJangL;
    private Typeface mFontSeoulNamsanJangM;
    private Typeface mFontSeoulNamsanJangB;
}

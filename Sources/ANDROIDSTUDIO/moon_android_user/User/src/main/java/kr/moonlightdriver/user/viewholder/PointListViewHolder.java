package kr.moonlightdriver.user.viewholder;

import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by kyo on 2017-04-10.
 */

public class PointListViewHolder {
    public TextView mDateTextView;
    public TextView mPointTextView;
    public TextView mPathTextView;
    public TextView mPriceTextView;
    public TextView mChauffeurTextView;

    public LinearLayout mCallPointLayout;
    public LinearLayout mPromotionPointLayout;
    public TextView mPromotionPointTextView;
}

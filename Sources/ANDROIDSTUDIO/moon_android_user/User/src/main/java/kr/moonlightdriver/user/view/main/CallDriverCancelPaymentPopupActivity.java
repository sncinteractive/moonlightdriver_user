package kr.moonlightdriver.user.view.main;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.viewmodel.common.ResultData;

/**
 * Created by eklee on 2017. 6. 6..
 */

public class CallDriverCancelPaymentPopupActivity extends Activity {
    private static String TAG = "CallDriverCancelPaymentPopupActivity";

    private RelativeLayout mLoadingProgress;

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.popup_call_driver_cancel_payment);

            TextView titleTextView = (TextView) findViewById(R.id.tv_popup_call_driver_cancel_payment_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanVert(), titleTextView);

            TextView messageTextView = (TextView) findViewById(R.id.tv_popup_call_driver_cancel_payment_message);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), messageTextView);

            ImageButton noButton = (ImageButton) findViewById(R.id.ib_popup_call_driver_cancel_payment_no);
            noButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendResult(false);
                }
            });

            ImageButton yesButton = (ImageButton) findViewById(R.id.ib_popup_call_driver_cancel_payment_yes);
            yesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO: 서버 연동시 주석 제거
//                    testCancelPaymentResponse();
                    requestCancelPayment();
                }
            });

            ImageButton closeButton = (ImageButton) findViewById(R.id.ib_popup_call_driver_not_assigned_close);
            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendResult(false);
                }
            });

            mLoadingProgress = (RelativeLayout) findViewById(R.id.loading_progress);
            hideLoadingAnim();
            //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showLoadingAnim() {
        mLoadingProgress.setVisibility(View.VISIBLE);
    }

    private void hideLoadingAnim() {
        mLoadingProgress.setVisibility(View.GONE);
    }

    private void requestCancelPayment() {
        try {
            SharedPreferences prefs = Utils.getGCMPreferences(this);
            String userId = prefs.getString(Configuration.PROPERTY_USER_ID, null);

            showLoadingAnim();

            RequestParams requestParams = new RequestParams();
            requestParams.put("userId", userId);

            //TODO: url 수정 필요
            NetClient.send(this, Configuration.URL_CALL_DRIVER_CANCEL_PAYMENT, "POST", requestParams, new NoResponseDataAck(), new NetResponseCallback(new CancelPaymentResponse()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class CancelPaymentResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            hideLoadingAnim();

            if (_netAPI != null) {
                ResultData resultData = _netAPI.getmResultData();
                if (resultData.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                    CommonDialog.showSimpleDialog(CallDriverCancelPaymentPopupActivity.this, resultData.getmDetail());
                    return;
                }

                sendResult(true);
            }
        }
    }

    private void testCancelPaymentResponse() {
        showLoadingAnim();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                NoResponseDataAck successAck = new NoResponseDataAck(new ResultData(Enums.NetResultCode.SUCCESS.getValue(), null, null));
                NoResponseDataAck failAck = new NoResponseDataAck(new ResultData(Enums.NetResultCode.UNKNOWN_ERROR.getValue(), null, null));
                new CancelPaymentResponse().onResponse(successAck);
            }
        }, 500);
    }

    private void sendResult(boolean canceled) {
        try {
            if (canceled) {
                SharedPreferences prefs = Utils.getGCMPreferences(CallDriverCancelPaymentPopupActivity.this);
                prefs.edit().putString(Configuration.PROPERTY_CALL_INFO_STATUS, Enums.CallStatus.Cancel).apply();
            }

            Intent intent = new Intent();
            intent.putExtra("canceled", canceled);
            setResult(Activity.RESULT_OK, intent);
            overridePendingTransition(0, 0);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package kr.moonlightdriver.user.viewmodel.setting;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2016-12-29.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class CouponData {
    @JsonProperty("couponId")
    public String mCouponId;

    @JsonProperty("couponName")
    public String mCouponName;

    @JsonProperty("couponDesc")
    public String mCouponDesc;

    @JsonProperty("couponImgPath")
    public String mCouponImgPath;

    @JsonProperty("couponOrgImgPath")
    public String mCouponOrgImgPath;

    @JsonProperty("couponStartDate")
    public String mCouponStartDate;

    @JsonProperty("couponEndDate")
    public String mCouponEndDate;

    @JsonProperty("userCouponId")
    public String mUserCouponId;

    @JsonProperty("userId")
    public String mUserId;

    @JsonProperty("status")
    public String mStatus;

    @JsonProperty("createdDate")
    public String mCreatedDate;

    @JsonProperty("updatedDate")
    public String mUpdatedDate;

    public CouponData() {    }

    public CouponData(String mCouponId, String mCouponName, String mCouponDesc, String mCouponImgPath, String mCouponOrgImgPath, String mCouponStartDate, String mCouponEndDate, String mUserCouponId, String mUserId, String mStatus, String mCreatedDate, String mUpdatedDate) {
        this.mCouponId = mCouponId;
        this.mCouponName = mCouponName;
        this.mCouponDesc = mCouponDesc;
        this.mCouponImgPath = mCouponImgPath;
        this.mCouponOrgImgPath = mCouponOrgImgPath;
        this.mCouponStartDate = mCouponStartDate;
        this.mCouponEndDate = mCouponEndDate;
        this.mUserCouponId = mUserCouponId;
        this.mUserId = mUserId;
        this.mStatus = mStatus;
        this.mCreatedDate = mCreatedDate;
        this.mUpdatedDate = mUpdatedDate;
    }

    public String getmCouponId() {
        return mCouponId;
    }

    public void setmCouponId(String mCouponId) {
        this.mCouponId = mCouponId;
    }

    public String getmCouponName() {
        return mCouponName;
    }

    public void setmCouponName(String mCouponName) {
        this.mCouponName = mCouponName;
    }

    public String getmCouponDesc() {
        return mCouponDesc;
    }

    public void setmCouponDesc(String mCouponDesc) {
        this.mCouponDesc = mCouponDesc;
    }

    public String getmCouponImgPath() {
        return mCouponImgPath;
    }

    public void setmCouponImgPath(String mCouponImgPath) {
        this.mCouponImgPath = mCouponImgPath;
    }

    public String getmCouponOrgImgPath() {
        return mCouponOrgImgPath;
    }

    public void setmCouponOrgImgPath(String mCouponOrgImgPath) {
        this.mCouponOrgImgPath = mCouponOrgImgPath;
    }

    public String getmCouponStartDate() {
        return mCouponStartDate;
    }

    public void setmCouponStartDate(String mCouponStartDate) {
        this.mCouponStartDate = mCouponStartDate;
    }

    public String getmCouponEndDate() {
        return mCouponEndDate;
    }

    public void setmCouponEndDate(String mCouponEndDate) {
        this.mCouponEndDate = mCouponEndDate;
    }

    public String getmUserCouponId() {
        return mUserCouponId;
    }

    public void setmUserCouponId(String mUserCouponId) {
        this.mUserCouponId = mUserCouponId;
    }

    public String getmUserId() {
        return mUserId;
    }

    public void setmUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getmCreatedDate() {
        return mCreatedDate;
    }

    public void setmCreatedDate(String mCreatedDate) {
        this.mCreatedDate = mCreatedDate;
    }

    public String getmUpdatedDate() {
        return mUpdatedDate;
    }

    public void setmUpdatedDate(String mUpdatedDate) {
        this.mUpdatedDate = mUpdatedDate;
    }
}

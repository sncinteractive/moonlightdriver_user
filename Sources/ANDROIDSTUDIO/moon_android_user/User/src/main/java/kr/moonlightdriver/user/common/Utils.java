package kr.moonlightdriver.user.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.skt.Tmap.TMapPOIItem;
import com.skt.Tmap.TMapPoint;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.NodeList;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import kr.moonlightdriver.user.BuildConfig;
import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.network.NetApi.TMapRouteModel;
import kr.moonlightdriver.user.viewmodel.common.LocationData;

public class Utils {
    public static final String TAG = "Utils";
    public static final int CONVERT_TO_PRICE_VALUE_ERROR = -1;

    public static final float GOOGLE_MAP_DEFAULT_ZOOM = 16f;
    public static final float GOOGLE_MAP_MIN_DISTANCE = 1000f;
    public static final float GOOGLE_MAP_MAX_DISTANCE = 3000f;

    private static DecimalFormat sDecimalFormat;
    private static SimpleDateFormat sSimpleDateFormat;

    public static int GetLocationServiceStatus(Context cx) {
        try {
            LocationManager lm = (LocationManager) cx.getSystemService(Context.LOCATION_SERVICE);
            if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER) && lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                return 3;
            } else if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                return 2;
            } else if (lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                return 1;
            } else { // 위치 찾기 불가
                return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static boolean GetWifiOnStatus(Context cx) {
        try {
            ConnectivityManager cm = (ConnectivityManager) cx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork != null) {
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                    return activeNetwork.isAvailable();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static void log(String _logType, String _tag, String _message) {
        try {
            if (BuildConfig.DEBUG) {
                switch (_logType.toLowerCase()) {
                    case "verbose":
                        Log.v(_tag, _message);
                        break;
                    case "debug":
                        Log.d(_tag, _message);
                        break;
                    case "info":
                        Log.i(_tag, _message);
                        break;
                    case "warning":
                        Log.w(_tag, _message);
                        break;
                    case "error":
                        Log.e(_tag, _message);
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//	public static int dpFromPx(final Context context, final float px) {
//		return Math.round(px / context.getResources().getDisplayMetrics().density);
//	}

    public static int pxFromDp(final Context context, final float dp) {
        return Math.round(dp * context.getResources().getDisplayMetrics().density);
    }

    public static void finishActivity(Activity _activity) {
        try {
            _activity.moveTaskToBack(true);
            _activity.finish();
            android.os.Process.killProcess(android.os.Process.myPid());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getCommentRegisteredDate(long _createTime) {
        String regDateStr = "";

        try {
            Calendar regDate = Calendar.getInstance();
            regDate.setTimeInMillis(_createTime);

            int raw_month = regDate.get(Calendar.MONTH) + 1;

            String year = regDate.get(Calendar.YEAR) + "-";
            String month = (raw_month < 10 ? "0" + raw_month : raw_month) + "-";
            String dayofmonth = (regDate.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + regDate.get(Calendar.DAY_OF_MONTH) : regDate.get(Calendar.DAY_OF_MONTH)) + " ";
            String hours = (regDate.get(Calendar.HOUR) < 10 ? "0" + regDate.get(Calendar.HOUR) : regDate.get(Calendar.HOUR)) + ":";
            String minutes = (regDate.get(Calendar.MINUTE) < 10 ? "0" + regDate.get(Calendar.MINUTE) : regDate.get(Calendar.MINUTE)) + "" + (regDate.get(Calendar.HOUR_OF_DAY) < 12 ? "AM" : "PM");

            regDateStr = year + month + dayofmonth + hours + minutes;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return regDateStr;
    }

    public static String getElapsedTime(long _createTime) {
        String elapsedTime = "";

        try {
            long minutes = 60 * 1000;
            long hours = 60 * 60 * 1000;
            long days = 24 * 60 * 60 * 1000;
            long now = System.currentTimeMillis();
            long diff = now - _createTime;

            if (diff < minutes) {
                elapsedTime = "방금 전";
            } else if (diff >= minutes && diff < hours) {
                elapsedTime = ((int) (diff / minutes)) + "분 전";
            } else if (diff >= hours && diff < days) {
                elapsedTime = ((int) (diff / hours)) + "시간 전";
            } else if (diff >= days) {
                elapsedTime = ((int) (diff / days)) + "일 전";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return elapsedTime;
    }

    public static void setFontToView(Typeface _type_face, TextView... views) {
        for (TextView view : views)
            view.setTypeface(_type_face);
    }

    public static void setFontToTextViewRecursively(Typeface _type_face, View _view) {
        if (_view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) _view).getChildCount(); ++i) {
                setFontToTextViewRecursively(_type_face, ((ViewGroup) _view).getChildAt(i));
            }
        } else if (_view instanceof TextView) {
            ((TextView) _view).setTypeface(_type_face);
        }
    }

    public static SharedPreferences getGCMPreferences(Context _context) {
        return _context.getSharedPreferences(Configuration.PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public static String getDeviceUUID(final Context _context) {
        String uuid_str = "";

        try {
            final SharedPreferences prefs = Utils.getGCMPreferences(_context);
            final String deviceUUID = prefs.getString(Configuration.PROPERTY_DEVICE_UUID, "");

            UUID uuid = null;
            if (!Utils.isStringNullOrEmpty(deviceUUID)) {
                uuid = UUID.fromString(deviceUUID);
            } else {
                final String androidId = Settings.Secure.getString(_context.getContentResolver(), Settings.Secure.ANDROID_ID);
                if (!"9774d56d682e549c".equals(androidId)) {
                    uuid = UUID.nameUUIDFromBytes(androidId.getBytes("utf8"));
                } else {
                    final String deviceId = ((TelephonyManager) _context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
                    uuid = deviceId != null ? UUID.nameUUIDFromBytes(deviceId.getBytes("utf8")) : UUID.randomUUID();
                }

                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(Configuration.PROPERTY_DEVICE_UUID, uuid.toString());
                editor.apply();
            }

            uuid_str = uuid.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return uuid_str;
    }

    public static String getCrackdownRegistrantEncrypt(String _registrant) {
        String retRegistrant = "";

        try {
            StringBuilder replacedRegistrant = new StringBuilder(_registrant);
            replacedRegistrant.setCharAt(1, 'x');
            replacedRegistrant.setCharAt(2, 'x');

            retRegistrant = replacedRegistrant.toString();
            retRegistrant = retRegistrant.substring(0, 4) + "-" + retRegistrant.substring(4);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return retRegistrant;
    }

    public static void showSoftInput(Activity activity) {
        try {
            View view = activity.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(view, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void hideSoftInput(Activity activity) {
        try {
            View view = activity.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void initRegion() {
        try {
            Configuration.REGION = new JSONObject();

            Configuration.REGION.put("서울특별시", "서울경기");
            Configuration.REGION.put("경기도", "서울경기");
            Configuration.REGION.put("인천광역시", "서울경기");
            Configuration.REGION.put("강원도", "강원도");
            Configuration.REGION.put("충청도", "충청도");
            Configuration.REGION.put("충청남도", "충청도");
            Configuration.REGION.put("충청북도", "충청도");
            Configuration.REGION.put("대전광역시", "충청도");
            Configuration.REGION.put("세종특별자치시", "충청도");
            Configuration.REGION.put("경상도", "경상도");
            Configuration.REGION.put("경상남도", "경상도");
            Configuration.REGION.put("경상북도", "경상도");
            Configuration.REGION.put("울산광역시", "경상도");
            Configuration.REGION.put("부산광역시", "경상도");
            Configuration.REGION.put("전라도", "전라도");
            Configuration.REGION.put("전라남도", "전라도");
            Configuration.REGION.put("전라북도", "전라도");
            Configuration.REGION.put("광주광역시", "전라도");
            Configuration.REGION.put("제주도", "제주도");
            Configuration.REGION.put("제주특별자치도", "제주도");
            Configuration.REGION.put("대구광역시", "대구");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<ResolveInfo> getShareIntents(PackageManager _pm, String... _appNameList) {
        ArrayList<ResolveInfo> retSharedAppInfo = new ArrayList<>();

        try {
            for (String appName : _appNameList) {
                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("image/png");

                List<ResolveInfo> resInfo = _pm.queryIntentActivities(intent, 0);

                if (resInfo == null) {
                    return new ArrayList<>();
                }

                for (ResolveInfo info : resInfo) {
                    if (info.activityInfo.packageName.toLowerCase().contains(appName) || info.activityInfo.name.toLowerCase().contains(appName)) {
                        retSharedAppInfo.add(info);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return retSharedAppInfo;
    }

    public static String saveScreenshot(Bitmap _bitmap) {
        String saveFileName = "";

        try {
            Utils.log("error", TAG, "Environment.getExternalStorageDirectory(); : " + Environment.getExternalStorageDirectory());

            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
            String dateString = formatter.format(new Date());
            String fileName = "/moonlightdriver_shared_" + dateString + ".png";
            File dirs = new File(Environment.getExternalStorageDirectory(), Configuration.PATH_SHARED_IMAGE);

            if (!dirs.exists()) {
                dirs.mkdirs();
            }

            saveFileName = dirs.getPath() + fileName;

            FileOutputStream outStream = new FileOutputStream(saveFileName);
            _bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);

            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return saveFileName;
    }

    public static String convertToPriceString(int value) {
        try {
            DecimalFormat df = new DecimalFormat("#,##0");
            return df.format(value) + "원";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static int convertToPriceValue(String str) {
        if (isStringNullOrEmpty(str)) {
            return -1;
        }

        String replaceResult = str.replaceAll("[^\\d]", ""); // 숫자만 남기고 제거
        try {
            return Integer.parseInt(replaceResult);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public static class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String url = urls[0];
            Bitmap mIcon11 = null;

            try {
                InputStream in = new java.net.URL(url).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                bmImage.setImageBitmap(result);
                this.bmImage.setVisibility(View.VISIBLE);
            }
        }
    }

    public static Bitmap getImageDownload(String _imgUrl) {
        Bitmap img = null;
        try {
            URL url = new URL(_imgUrl);
            URLConnection conn = url.openConnection();
            conn.connect();

            BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
            img = BitmapFactory.decodeStream(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return img;
    }

    public static long getLocationUpdateTime(double _kmPerHour) {
        try {
            if (_kmPerHour >= 40) {
                return 1000;
            } else if (_kmPerHour >= 30) {
                return 1200;
            } else if (_kmPerHour >= 25) {
                return 1400;
            } else if (_kmPerHour >= 20) {
                return 1800;
            } else if (_kmPerHour >= 19) {
                return 1900;
            } else if (_kmPerHour >= 18) {
                return 2000;
            } else if (_kmPerHour >= 17) {
                return 2100;
            } else if (_kmPerHour >= 16) {
                return 2200;
            } else if (_kmPerHour >= 15) {
                return 2400;
            } else if (_kmPerHour >= 14) {
                return 2600;
            } else if (_kmPerHour >= 13) {
                return 2800;
            } else if (_kmPerHour >= 12) {
                return 3000;
            } else if (_kmPerHour >= 11) {
                return 3200;
            } else if (_kmPerHour >= 10) {
                return 3600;
            } else if (_kmPerHour >= 9) {
                return 4000;
            } else if (_kmPerHour >= 8) {
                return 4500;
            } else if (_kmPerHour >= 7) {
                return 5000;
            } else if (_kmPerHour >= 6) {
                return 6000;
            } else if (_kmPerHour >= 5) {
                return 7000;
            } else if (_kmPerHour >= 4) {
                return 9000;
            } else if (_kmPerHour >= 3) {
                return 12000;
            } else if (_kmPerHour >= 2) {
                return 18000;
            } else if (_kmPerHour >= 1) {
                return 36000;
            } else {
                return 60000;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 60000;
        }
    }

    public static boolean isStringNullOrEmpty(String _text) {
        return (_text == null || _text.isEmpty() || _text.toLowerCase().equals("null"));
    }

    public static String getPOIAddressExceptNullOrEmpty(@NonNull TMapPOIItem tMapPOIItem) {
        StringBuilder sb = new StringBuilder();

        if (isStringNullOrEmpty(tMapPOIItem.upperAddrName) == false) {
            sb.append(tMapPOIItem.upperAddrName);
        }

        if (isStringNullOrEmpty(tMapPOIItem.middleAddrName) == false) {
            sb.append(" ");
            sb.append(tMapPOIItem.middleAddrName);
        }

        if (isStringNullOrEmpty(tMapPOIItem.lowerAddrName) == false) {
            sb.append(" ");
            sb.append(tMapPOIItem.lowerAddrName);
        }

        if (isStringNullOrEmpty(tMapPOIItem.detailAddrName) == false) {
            sb.append(" ");
            sb.append(tMapPOIItem.detailAddrName);
        }

        return sb.toString();
    }

    public static float getGoogleMapScreenSize(LatLngBounds latLngBounds) {
        float distance = 0f;

        try {
            float[] dist = new float[3];
            Location.distanceBetween(latLngBounds.southwest.latitude,
                latLngBounds.southwest.longitude,
                latLngBounds.northeast.latitude,
                latLngBounds.northeast.longitude, dist);
            distance = dist[0];
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (distance > GOOGLE_MAP_MAX_DISTANCE) {
            distance = GOOGLE_MAP_MAX_DISTANCE;
        } else if (distance <= GOOGLE_MAP_MIN_DISTANCE) {
            distance = GOOGLE_MAP_MIN_DISTANCE;
        }

        return distance;
    }

    public static BitmapDescriptor getDriverIconByZoom(float zoom) {
        int resourceId;
        if (zoom >= GOOGLE_MAP_DEFAULT_ZOOM) {
            resourceId = R.drawable.pin_driver_blue_120;
        } else if (zoom >= 15) {
            resourceId = R.drawable.pin_driver_blue_75;
        } else if (zoom >= 13) {
            resourceId = R.drawable.pin_driver_blue_50;
        } else if (zoom >= 11) {
            resourceId = R.drawable.pin_driver_blue_40;
        } else {
            resourceId = R.drawable.pin_driver_blue_22;
        }

        return BitmapDescriptorFactory.fromResource(resourceId);
    }

    public static LatLng convertTMapPointToLatLng(TMapPoint tMapPoint) {
        return new LatLng(tMapPoint.getLatitude(), tMapPoint.getLongitude());
    }

    public static TMapPoint convertLatLngToTMapPoint(LatLng latLng) {
        return new TMapPoint(latLng.latitude, latLng.longitude);
    }

    public static List<LatLng> convertTMapPointListToLatLngList(List<TMapPoint> tMapPointList) {
        List<LatLng> result = new ArrayList<>(tMapPointList.size());
        for (TMapPoint item : tMapPointList) {
            result.add(new LatLng(item.getLatitude(), item.getLongitude()));
        }

        return result;
    }

    public static List<TMapPoint> convertLatLngListToTMapPointList(List<LatLng> latLngList) {
        List<TMapPoint> result = new ArrayList<>(latLngList.size());
        for (LatLng item : latLngList) {
            result.add(new TMapPoint(item.latitude, item.longitude));
        }

        return result;
    }

    public static String convertDistanceString(double distance) {
        if (distance < 1000) {
            return (int) distance + "m";
        } else {
            return Math.round(distance / 1000d) + "km";
        }
    }

    public static String convertTimeString(double timeSec) {
        double hour = timeSec / 3600;
        double min = (timeSec % 3600) / 60;
        if (hour >= 1 && min >= 1) {
            return String.format("%.0f시간 %.0f분", hour, min);
        } else if (hour >= 1) {
            return String.format("%.0f시간", hour);
        } else {
            return String.format("%.0f분", min);
        }
    }

    public static List<LatLng> parseCoordinateListToLatLngList(NodeList coordinateList) {
        List<LatLng> result = null;
        try {
            if (coordinateList != null && coordinateList.getLength() > 0) {
                int size = coordinateList.getLength();
                result = new ArrayList<>(size);
                for (int i = 0; i < size; i++) {
                    String content = coordinateList.item(i).getTextContent();
                    String[] coordinates = content.split(" ");
                    for (int j = 0; j < coordinates.length; j++) {
                        String[] latLngs = coordinates[j].split(",");
                        result.add(new LatLng(Double.parseDouble(latLngs[1]), Double.parseDouble(latLngs[0])));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public static TMapRouteModel parseJsonToTMapRouteModel(JsonNode jsonNode) {
        try {
            if (jsonNode == null) {
                return null;
            }

            JsonNode featuresNode = jsonNode.get("features");
            if (featuresNode == null) {
                return null;
            }

            /**
             * 첫 번째 feature의 properties에서 총 거리 및 소요시간을 구할 수 있다
             */
            int totalDistance = 0;
            int totalTime = 0;
            Iterator<JsonNode> featureIterator = featuresNode.iterator();
            if (featureIterator.hasNext()) {
                JsonNode firstFeature = featureIterator.next();
                JsonNode propertiesNode = firstFeature.get("properties");

                JsonNode totalDistanceNode = propertiesNode.get("totalDistance");
                if (totalDistanceNode != null) {
                    totalDistance = totalDistanceNode.intValue();
                }

                JsonNode totalTimeNode = propertiesNode.get("totalTime");
                if (totalTimeNode != null) {
                    totalTime = totalTimeNode.intValue();
                }
            }

            /**
             * 각 feature의 geometry에서 type이 LineString인 경우에 coordinates를 파싱해서 좌표 리스트에 추가한다.
             * 경유지와 연결된 가상의 라인 표시용 좌표는 포함하지 않는다.
             */
            List<LatLng> resultCoordinateList = new LinkedList<>();
            List<Integer> resultRemainDistanceList = new LinkedList<>();
            List<Integer> resultRemainTimeList = new LinkedList<>();
            int remainTotalDistance = totalDistance;
            int remainTotalTime = totalTime;
            while (featureIterator.hasNext()) {
                JsonNode featureNode = featureIterator.next();
                JsonNode geometryNode = featureNode.get("geometry");

                if (geometryNode == null) {
                    continue;
                }

                JsonNode typeNode = geometryNode.get("type");
                if (typeNode == null || !typeNode.textValue().equals("LineString")) {
                    continue;
                }

                JsonNode propertiesNode = featureNode.get("properties");
                if (propertiesNode == null) {
                    continue;
                }

                JsonNode descNode = propertiesNode.get("description");
                if (descNode != null && descNode.textValue().equals("경유지와 연결된 가상의 라인입니다")) {
                    continue;
                }

                int sectionTotalDistance = 0;
                int sectionTotalTime = 0;

                JsonNode sectionTotalDistanceNode = propertiesNode.get("distance");
                if (sectionTotalDistanceNode != null) {
                    sectionTotalDistance = sectionTotalDistanceNode.intValue();
                }

                JsonNode sectionTotalTimeNode = propertiesNode.get("time");
                if (sectionTotalTimeNode != null) {
                    sectionTotalTime = sectionTotalTimeNode.intValue();
                }

                JsonNode coordinatesNode = geometryNode.get("coordinates");
                List<Double> coordList = new LinkedList<>();
                parseCoordinatesNodeToLatLngListRecursively(coordList, coordinatesNode);
                int coordListSize = coordList.size() / 2;
                if (coordListSize > 0) {
                    LatLng latLngPrev = null;
                    if (resultCoordinateList.size() > 0) {
                        latLngPrev = resultCoordinateList.get(resultCoordinateList.size() - 1);
                    }

                    int lastIndex = coordListSize - 1;
                    int sectionDistance = (int) (sectionTotalDistance / (float) (lastIndex));
                    int sectionTime = (int) (sectionTotalTime / (float) (lastIndex));
                    for (int i = 0; i < coordListSize; ++i) {
                        LatLng latLngNew = new LatLng(coordList.get(i * 2 + 1), coordList.get(i * 2));
                        if (i == 0 && latLngPrev != null && latLngPrev.equals(latLngNew)) {
                            continue;
                        }

                        resultCoordinateList.add(latLngNew);

                        if (i == lastIndex) {
                            resultRemainDistanceList.add(remainTotalDistance - sectionTotalDistance);
                            resultRemainTimeList.add(remainTotalTime - sectionTotalTime);
                        } else {
                            resultRemainDistanceList.add(remainTotalDistance - (sectionDistance * i));
                            resultRemainTimeList.add(remainTotalTime - (sectionTime * i));
                        }
                    }
                    remainTotalDistance -= sectionTotalDistance;
                    remainTotalTime -= sectionTotalTime;
                }
            }

            if (resultCoordinateList.size() == 0) {
                return null;
            }

            return new TMapRouteModel(totalDistance, totalTime, resultCoordinateList, resultRemainDistanceList, resultRemainTimeList);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void parseCoordinatesNodeToLatLngListRecursively(List<Double> coordList, JsonNode coordinatesNode) {
        if (coordinatesNode == null) {
            return;
        }

        if (!coordinatesNode.isArray()) {
            return;
        }

        Iterator<JsonNode> it1 = coordinatesNode.iterator();
        while (it1.hasNext()) {
            JsonNode itemNode = it1.next();
            if (itemNode.isArray()) {
                parseCoordinatesNodeToLatLngListRecursively(coordList, itemNode);
            } else if (itemNode.isDouble()) {
                coordList.add(itemNode.doubleValue());
            }
        }
    }

    public static String convertDriverGradeAverageToString(float value) {
        if (sDecimalFormat == null) {
            sDecimalFormat = new DecimalFormat("#.#");
        }

        return sDecimalFormat.format(value);
    }

    public static String convertSimpleDateFormat(String pattern, long timeMilliseconds) {
        return convertSimpleDateFormat(pattern, new Date(timeMilliseconds));
    }

    public static String convertSimpleDateFormat(String pattern, Date date) {
        if (sSimpleDateFormat == null) {
            sSimpleDateFormat = new SimpleDateFormat();
        }

        sSimpleDateFormat.applyPattern(pattern);
        return sSimpleDateFormat.format(date);
    }

    public static int getNotificationId(String pushMsgType) {
        switch (pushMsgType) {
            case Configuration.PUSH_MSG_TYPE_CRACK_DOWN_WARNING:
                return Configuration.NOTIFICATION_ID_CRACKDOWN_WARNING;
            case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_ASSIGNED:
                return Configuration.NOTIFICATION_ID_CALL_DRIVER_ASSIGNED;
//            case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_NOT_ASSIGNED:
//                return Configuration.NOTIFICATION_ID_CALL_DRIVER_NOT_ASSIGNED;
            case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_PRICE_OFFER:
                return Configuration.NOTIFICATION_ID_CALL_DRIVER_PRICE_OFFER;
        }

        return 0;
    }

    public static String parseStopByLocationDataListToJSONString(List<LocationData> stopbyLocationDataList) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JSONArray jsonArray = new JSONArray();
            for (LocationData item : stopbyLocationDataList) {
                String jsonItem = mapper.writeValueAsString(item);
                jsonArray.put(jsonItem);
            }

            return jsonArray.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<LocationData> parseJSONStringToStopByLocationDataList(String jsonString) {
        try {
            if (isStringNullOrEmpty(jsonString)) {
                return null;
            }

            List<LocationData> resultList = new ArrayList<>();
            JSONArray jsonArray2 = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray2.length(); i++) {
                JSONObject jsonItem = new JSONObject(jsonArray2.getString(i));
                JSONObject jsonLatLng = jsonItem.getJSONObject("mLocation");
                LocationData locationData = new LocationData(new LatLng(jsonLatLng.getDouble("latitude"), jsonLatLng.getDouble("longitude")), jsonItem.getString("mLocationName"), jsonItem.getString("mAddress"));
                resultList.add(locationData);
            }
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static double distanceBetweenLocation(LatLng location1, LatLng location2) {
        return distanceBetweenLocation(location1.latitude, location1.longitude, location2.latitude, location2.longitude);
    }

    public static double distanceBetweenLocation(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));

        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515; // mile
        dist = dist * 1.609344; // kilometer
        dist = dist * 1000.0; // meter

        return dist;
    }

    public static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    public static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

    public static String convert2NumberFormat(int strNumber) {
        try {
            DecimalFormat formatter = new DecimalFormat("#,###,###,###");
            return formatter.format(strNumber);
        } catch (Exception e) {
            e.printStackTrace();
            return String.valueOf(strNumber);
        }
    }

    public static boolean isAvailableSMS(@NonNull Context context) {
        try {
            Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:"));
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}

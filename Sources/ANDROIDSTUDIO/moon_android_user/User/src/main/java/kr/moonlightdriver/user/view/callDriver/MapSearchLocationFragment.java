package kr.moonlightdriver.user.view.callDriver;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.skt.Tmap.TMapAddressInfo;

import java.util.HashMap;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.ResultCallback;
import kr.moonlightdriver.user.common.TmapManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewmodel.common.LocationData;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapSearchLocationFragment extends Fragment implements OnMapReadyCallback {
    private static final String TAG = "MapSearchLocationFragment";
    private final float DEFAULT_ZOOM = 16f;
    private MainActivity mMainActivity;

    private String mLocationType; // "source", "target", "stopby{index}"

    private GoogleMap mGoogleMap;

    private Marker mMarker;
    private LocationData mLocationData;

    private LinearLayout mLocationInfoLayout;
    private TextView mLocationNameTextView;
    private TextView mLocationAddressTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = null;
        try {
            mMainActivity = (MainActivity) getActivity();
            mMainActivity.setTabMenuVisibility(View.GONE);

            Bundle bundle = MapSearchLocationFragment.this.getArguments();
            mLocationType = bundle.getString("locationType", "");

            view = inflater.inflate(R.layout.fragment_map_search_location, container, false);
            initializeView(view);

            setUpMapIfNeeded();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return view;
        }
    }

    private void initializeView(View view) {
        try {
            TextView titleTextView = (TextView) view.findViewById(R.id.tv_title);
            switch (mLocationType) {
                case "source": {
                    titleTextView.setText(getString(R.string.common_search_source));
                }
                break;
                case "target": {
                    titleTextView.setText(getString(R.string.common_search_target));
                }
                break;
                default: {
                    titleTextView.setText(getString(R.string.common_search_stopby));
                }
                break;
            }
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), titleTextView);

            ImageButton currentLocationButton = (ImageButton) view.findViewById(R.id.ib_current_location);
            currentLocationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(MainActivity.mCurrentLocation, DEFAULT_ZOOM));
                }
            });

            ImageButton zoomInButton = (ImageButton) view.findViewById(R.id.ib_zoom_in);
            zoomInButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
                }
            });

            ImageButton zoomOutButton = (ImageButton) view.findViewById(R.id.ib_zoom_out);
            zoomOutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mGoogleMap.animateCamera(CameraUpdateFactory.zoomOut());
                }
            });

            mLocationInfoLayout = (LinearLayout) view.findViewById(R.id.ll_location_info);
            mLocationInfoLayout.setVisibility(View.INVISIBLE);
            mLocationNameTextView = (TextView) view.findViewById(R.id.tv_location_info_name);
            mLocationAddressTextView = (TextView) view.findViewById(R.id.tv_location_info_address);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), mLocationNameTextView, mLocationAddressTextView);

            ImageButton cancelButton = (ImageButton) view.findViewById(R.id.ib_cancel);
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getActivity().getSupportFragmentManager().beginTransaction().remove(MapSearchLocationFragment.this).commit();
                }
            });

            ImageButton confirmButton = (ImageButton) view.findViewById(R.id.ib_confirm);
            confirmButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    TmapManager tmapManager = new TmapManager();
                    tmapManager.convertGpsToAddress(mGoogleMap.getCameraPosition().target, new ResultCallback() {
                        @Override
                        public void resultCallback(HashMap<String, Object> params) {
                            TMapAddressInfo addressInfo = (TMapAddressInfo) params.get("addressInfo");
                            if (addressInfo != null) {
                                if (mLocationInfoLayout.getVisibility() == View.INVISIBLE) {
                                    mLocationInfoLayout.setVisibility(View.VISIBLE);
                                }

                                if (mLocationData == null) {
                                    mLocationData = new LocationData();
                                }

                                if (Utils.isStringNullOrEmpty(addressInfo.strBuildingName)) {
                                    mLocationNameTextView.setText(addressInfo.strFullAddress);
                                } else {
                                    mLocationNameTextView.setText(addressInfo.strBuildingName);
                                }

                                mLocationAddressTextView.setText(addressInfo.strFullAddress);
                                mLocationData.setmLocation(mGoogleMap.getCameraPosition().target);
                                mLocationData.setmLocationName(addressInfo.strBuildingName);
                                mLocationData.setmAddress(addressInfo.strFullAddress);
                            }

                            Intent intent = new Intent();
                            intent.putExtra("locationType", mLocationType);
                            intent.putExtra("locationData", mLocationData);
                            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                            getActivity().getSupportFragmentManager().beginTransaction().remove(MapSearchLocationFragment.this).commit();
                        }
                    });
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpMapIfNeeded() {
        try {
            if (mGoogleMap == null) {
                SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.search_map);
                mapFragment.getMapAsync(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void closeFragment() {
        try {
            getChildFragmentManager().beginTransaction().remove(MapSearchLocationFragment.this).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            mGoogleMap = googleMap;

            mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
            mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
            mGoogleMap.getUiSettings().setCompassEnabled(false);

            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MainActivity.mCurrentLocation, DEFAULT_ZOOM));

            mMarker = mGoogleMap.addMarker(new MarkerOptions().position(MainActivity.mCurrentLocation)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_crackdown_report_center))
                    .anchor(0.5f, 0.5f));

            mGoogleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
                @Override
                public void onCameraMove() {
                    mMarker.setPosition(mGoogleMap.getCameraPosition().target);
                }
            });

            mGoogleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {
//                    onCameraTargetChanged();
                }
            });

            mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    return true;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        try {
            mMainActivity.setTabMenuVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onCameraTargetChanged() {
        TmapManager tmapManager = new TmapManager();
        tmapManager.convertGpsToAddress(mGoogleMap.getCameraPosition().target, new ResultCallback() {
            @Override
            public void resultCallback(HashMap<String, Object> params) {
                TMapAddressInfo addressInfo = (TMapAddressInfo) params.get("addressInfo");
                if (addressInfo != null) {
                    if (mLocationInfoLayout.getVisibility() == View.INVISIBLE) {
                        mLocationInfoLayout.setVisibility(View.VISIBLE);
                    }

                    if (Utils.isStringNullOrEmpty(addressInfo.strBuildingName)) {
                        mLocationNameTextView.setText(addressInfo.strFullAddress);
                    } else {
                        mLocationNameTextView.setText(addressInfo.strBuildingName);
                    }
                    mLocationAddressTextView.setText(addressInfo.strFullAddress);

                    if (mLocationData == null) {
                        mLocationData = new LocationData();
                    }

                    mLocationData.setmLocation(mGoogleMap.getCameraPosition().target);
                    mLocationData.setmLocationName(addressInfo.strBuildingName);
                    mLocationData.setmAddress(addressInfo.strFullAddress);
                }
            }
        });
    }
}

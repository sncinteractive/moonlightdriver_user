package kr.moonlightdriver.user.viewholder;

import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by youngmin on 2016-08-30.
 */
public class CrackdownCommentListViewHolder {
	public LinearLayout mCrackdownSubCommentReplyLayout;
	public ImageView mCommentReplyIcon;
	public TextView mCommentReplyTitle;
	public TextView mCommentReplyContent;
	public ImageButton mBtnCommentReply;
	public EditText mCrackdownSubCommentEditText;
	public ImageButton mBtnCrackdownSubComment;
}

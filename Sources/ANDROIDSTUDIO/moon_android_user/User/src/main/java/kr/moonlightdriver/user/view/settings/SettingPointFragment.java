package kr.moonlightdriver.user.view.settings;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.PointChargeAck;
import kr.moonlightdriver.user.common.network.NetApi.PointListAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewholder.PointListViewHolder;
import kr.moonlightdriver.user.viewmodel.callDriver.DriverInfoViewModel;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.PointListViewModel;

/**
 * Created by youngmin on 2016-06-21.
 */
public class SettingPointFragment extends Fragment implements View.OnClickListener {
    private MainActivity mMainActivity;
    private SettingsFragment mFragment;

    //region - point manage fragment
    private ImageButton mBtnRecharge;
    private LinearLayout mBtnGoBack;
    private ListView mPointListView;
    private PointListAdapter mPointListAdapter;
    private List<PointListViewModel> mPointList;
    private TextView mOwnPoint;
    //endregion - point manage fragment

    //region - point recharge popup
    private RelativeLayout mPopupPointRechargeLayout;
    private Spinner mRechargePointSpinner;
    private Spinner mInstallmentSpinner;
    private ImageButton mBtnRechargePopupClose;
    private ImageButton mBtnCharge;
    private ImageButton mBtnChargeCancel;
    private TextView mRechargePopupOwnPoint;
    //endregion - point recharge popup

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            mMainActivity = (MainActivity) getActivity();

            view = inflater.inflate(R.layout.fragment_setting_point_manage, container, false);

            TextView headerTitleTextView = (TextView) view.findViewById(R.id.tv_header_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanB(), headerTitleTextView);

            //region - point manage fragment
            mBtnGoBack = (LinearLayout) view.findViewById(R.id.ib_setting_goback);
            mBtnGoBack.setOnClickListener(this);

            mBtnRecharge = (ImageButton) view.findViewById(R.id.ib_setting_point_recharge);
            mBtnRecharge.setOnClickListener(this);

            mPointList = new ArrayList<>();
            mPointListAdapter = new PointListAdapter(mMainActivity, R.layout.row_setting_point_list, mPointList);
            mPointListView = (ListView) view.findViewById(R.id.point_list);
            mPointListView.setAdapter(mPointListAdapter);

            mOwnPoint = (TextView) view.findViewById(R.id.own_point);
            mRechargePopupOwnPoint = (TextView) view.findViewById(R.id.rechargePopupOwnPoint);
            //endregion - point manage fragment

            //region - point recharge popup
            mPopupPointRechargeLayout = (RelativeLayout) view.findViewById(R.id.popup_point_recharge);
            mPopupPointRechargeLayout.setVisibility(View.GONE);

            mBtnRechargePopupClose = (ImageButton) view.findViewById(R.id.btn_recharge_popup_close);
            mBtnRechargePopupClose.setOnClickListener(this);

            mRechargePointSpinner = (Spinner) view.findViewById(R.id.spinner_recharge_point);
            ArrayAdapter<CharSequence> rechargePointAdapter = ArrayAdapter.createFromResource(mMainActivity, R.array.recharge_point_array, R.layout.spinner_item_point_recharge);
            rechargePointAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item_point_recharge);
            mRechargePointSpinner.setAdapter(rechargePointAdapter);

            mInstallmentSpinner = (Spinner) view.findViewById(R.id.spinner_installment);
            ArrayAdapter<CharSequence> installmentAdapter = ArrayAdapter.createFromResource(mMainActivity, R.array.installment_array, R.layout.spinner_item_point_recharge);
            installmentAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item_point_recharge);
            mInstallmentSpinner.setAdapter(installmentAdapter);

            mBtnCharge = (ImageButton) view.findViewById(R.id.ib_charge);
            mBtnCharge.setOnClickListener(this);

            mBtnChargeCancel = (ImageButton) view.findViewById(R.id.ib_charge_cancel);
            mBtnChargeCancel.setOnClickListener(this);

            // endregion - point recharge popup

            resetPointList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onClick(View _view) {
        try {
            if (_view == mBtnGoBack) {
                getActivity().getSupportFragmentManager().beginTransaction().remove(SettingPointFragment.this).commit();
            } else if (_view == mBtnRecharge) {
                String displayPoint = Utils.convert2NumberFormat(mMainActivity.mUserAdditionalData.getmPoint()) + " P";
                mRechargePopupOwnPoint.setText(displayPoint);
                mPopupPointRechargeLayout.setVisibility(View.VISIBLE);
            } else if (_view == mBtnRechargePopupClose || _view == mBtnChargeCancel) {
                mPopupPointRechargeLayout.setVisibility(View.GONE);
            } else if (_view == mBtnCharge) {
                // 포인트 충전 결제하기 버튼 눌렀을 때의 처리
                requestPointCharge();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void resetPointList() {
        String displayPoint = Utils.convert2NumberFormat(mMainActivity.mUserAdditionalData.getmPoint()) + " P";
        mOwnPoint.setText(displayPoint);

        mMainActivity.showLoadingAnim();

        requestPointList();
    }

    private void requestPointList() {
        RequestParams requestParams = new RequestParams();
        requestParams.add("userId", mMainActivity.mUserDetail.getUserId());

        // 포인트 리스트 조회 서버 연동시 주석 없애고 url 입력할 것
        NetClient.send(mMainActivity, Configuration.URL_POINT_LIST, "POST", requestParams, new PointListAck(), new NetResponseCallback(new PointListResponse()));
    }

    private class PointListResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            mMainActivity.hideLoadingAnim();

            if (_netAPI != null) {
                ResultData resultData = _netAPI.getmResultData();
                if (resultData.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                    CommonDialog.showSimpleDialog(mMainActivity, resultData.getmDetail());
                    return;
                }

                PointListAck pointListAck = (PointListAck) _netAPI;

                mPointList.clear();
                mPointList.addAll(pointListAck.getPointList());

                mPointListAdapter.notifyDataSetChanged();
            }
        }
    }

    private void requestPointCharge() {
        String selectedPoint = mRechargePointSpinner.getSelectedItem().toString();
        selectedPoint = selectedPoint.replaceAll("[,p]+", "");
        RequestParams requestParams = new RequestParams();
        requestParams.add("userId", mMainActivity.mUserDetail.getUserId());
        requestParams.add("point", selectedPoint);

        // 포인트 리스트 조회 서버 연동시 주석 없애고 url 입력할 것
        NetClient.send(mMainActivity, Configuration.URL_POINT_CHARGE, "POST", requestParams, new PointChargeAck(), new NetResponseCallback(new PointChargeResponse()));
    }

    private class PointChargeResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            mMainActivity.hideLoadingAnim();

            if (_netAPI != null) {
                ResultData resultData = _netAPI.getmResultData();
                if (resultData.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                    CommonDialog.showSimpleDialog(mMainActivity, resultData.getmDetail());
                    return;
                }

                PointChargeAck pointChargeAck = (PointChargeAck) _netAPI;

                mMainActivity.mUserAdditionalData = pointChargeAck.getmUserAdditionalData();

                CommonDialog.showSimpleDialog(mMainActivity, getString(R.string.point_popup_charge_complete));
                mPopupPointRechargeLayout.setVisibility(View.GONE);

                resetPointList();
            }
        }
    }

    private class PointListAdapter extends ArrayAdapter<PointListViewModel> {
        private int mResourceId;
        private List<PointListViewModel> mPointList;
        private PointListViewHolder mViewHolder;

        public PointListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<PointListViewModel> objects) {
            super(context, resource, objects);

            mPointList = objects;
            mResourceId = resource;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(mResourceId, null);

                mViewHolder = new PointListViewHolder();
                mViewHolder.mCallPointLayout = (LinearLayout) convertView.findViewById(R.id.callPointLayout);
                mViewHolder.mPromotionPointLayout = (LinearLayout) convertView.findViewById(R.id.promotionPointLayout);
                mViewHolder.mDateTextView = (TextView) convertView.findViewById(R.id.dateTextView);
                mViewHolder.mPointTextView = (TextView) convertView.findViewById(R.id.pointTextView);
                mViewHolder.mPathTextView = (TextView) convertView.findViewById(R.id.pathTextView);
                mViewHolder.mPriceTextView = (TextView) convertView.findViewById(R.id.priceTextView);
                mViewHolder.mChauffeurTextView = (TextView) convertView.findViewById(R.id.chauffeurTextView);
                mViewHolder.mPromotionPointTextView = (TextView) convertView.findViewById(R.id.promotionPointTextView);

                mViewHolder.mCallPointLayout.setVisibility(View.GONE);
                mViewHolder.mPromotionPointLayout.setVisibility(View.GONE);

                Utils.setFontToTextViewRecursively(FontManager.getInstance().getFontSeoulNamsanJangM(), convertView);

                convertView.setTag(mViewHolder);
            } else {
                mViewHolder = (PointListViewHolder) convertView.getTag();
                mViewHolder.mCallPointLayout.setVisibility(View.GONE);
                mViewHolder.mPromotionPointLayout.setVisibility(View.GONE);
            }

            PointListViewModel data = mPointList.get(position);
            if(data.getmPointType().equals("call")) {
                mViewHolder.mCallPointLayout.setVisibility(View.VISIBLE);

                mViewHolder.mPathTextView.setText(getString(R.string.string_format_point_list_path, data.getmFrom(), data.getmTo()));
                mViewHolder.mPriceTextView.setText(Utils.convertToPriceString(data.getmPrice()));
                mViewHolder.mChauffeurTextView.setText(getString(R.string.string_format_point_list_chauffeur_info, data.getmDriverInfo().getMaskedDriverName(), data.getmDriverInfo().getmDriverPhoneNumber()));
            } else if(data.getmPointType().equals("promotion")) {
                mViewHolder.mPromotionPointLayout.setVisibility(View.VISIBLE);
                mViewHolder.mPromotionPointTextView.setText(getString(R.string.point_list_reason_1));
            } else if(data.getmPointType().equals("charge")) {
                mViewHolder.mPromotionPointLayout.setVisibility(View.VISIBLE);
                mViewHolder.mPromotionPointTextView.setText(getString(R.string.point_list_reason_2));
            }

            String dateText = getString(R.string.string_format_point_list_date,
                    position + 1,
                    Utils.convertSimpleDateFormat("yyyy-MM-dd", data.getmCreatedDate()));
            mViewHolder.mDateTextView.setText(dateText);
            String displayPoint = Utils.convert2NumberFormat(data.getmPoint());
            if (data.getmPoint() >= 0) {
                mViewHolder.mPointTextView.setTextAppearance(getContext(), R.style.SettingsPointEarnText);
                mViewHolder.mPointTextView.setText(getString(R.string.string_format_point_list_earn, displayPoint));
            } else {
                mViewHolder.mPointTextView.setTextAppearance(getContext(), R.style.SettingsPointUseText);
                mViewHolder.mPointTextView.setText(getString(R.string.string_format_point_list_use, displayPoint));
            }

            return convertView;
        }
    }

    // TestCode => 포인트 리스트 조회 처리 테스트 함수
    private void testPointListResponse() {
        List<PointListViewModel> pointList = new ArrayList<>();
        {
            DriverInfoViewModel driverInfoViewModel = new DriverInfoViewModel();
            driverInfoViewModel.setmDriverName("임꺽정");
            driverInfoViewModel.setmDriverPhoneNumber("8888");
            pointList.add(new PointListViewModel(1496675100000L, -2000, "call", "문산", "서울", 18000, driverInfoViewModel));
        }
        {
            DriverInfoViewModel driverInfoViewModel = new DriverInfoViewModel();
            driverInfoViewModel.setmDriverName("홍길동");
            driverInfoViewModel.setmDriverPhoneNumber("9999");
            pointList.add(new PointListViewModel(1496761500000L, 2000, "call", "서울", "문산", 20000, driverInfoViewModel));
        }

        PointListAck ack = new PointListAck();
        ack.setmResultData(new ResultData(Enums.NetResultCode.SUCCESS.getValue(), null, null));
        ack.setPointList(pointList);
        new PointListResponse().onResponse(ack);
    }
    //endregion Test Code
}

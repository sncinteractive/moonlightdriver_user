package kr.moonlightdriver.user.view.settings;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.CreditCardListAck;
import kr.moonlightdriver.user.common.network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewholder.CardListViewHolder;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.CardListViewModel;

/**
 * Created by youngmin on 2016-06-21.
 */
public class SettingCardModifyFragment extends Fragment implements View.OnClickListener {
    public interface OnDeleteCardButtonClickListener {
        void onDeleteButtonClick(int position);
    }

    private MainActivity mMainActivity;
    private SettingsFragment mFragment;

    private ListView mCardListView;
    private CardModifyListAdapter mCardListAdapter;
    private ArrayList<CardListViewModel> mCardModifyList;
    private ArrayList<CardListViewModel> mRemovedModelList;
    private OnDeleteCardButtonClickListener mOnDeleteButtonClickListener;

    private TextView mEmptyListText;

    private int mLastSelectedItemIndex;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            mMainActivity = (MainActivity) getActivity();

            view = inflater.inflate(R.layout.fragment_setting_card_modify, container, false);

            TextView headerTitleTextView = (TextView)view.findViewById(R.id.tv_header_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanB(), headerTitleTextView);

            LinearLayout goBackButton = (LinearLayout) view.findViewById(R.id.ib_setting_goback);
            ImageButton completeButton = (ImageButton) view.findViewById(R.id.ib_complete);

            mEmptyListText = (TextView) view.findViewById(R.id.tv_card_list_empty);

            mCardModifyList = new ArrayList<>();
            mRemovedModelList = new ArrayList<>();
            mCardListAdapter = new CardModifyListAdapter(mMainActivity, R.layout.row_setting_card_modify_list, mCardModifyList);
            mCardListAdapter.setDeleteButtonClickListener(new SettingCardModifyFragment.OnDeleteCardButtonClickListener() {
                @Override
                public void onDeleteButtonClick(int position) {
                    CardListViewModel removedData = mCardModifyList.remove(position);
                    mRemovedModelList.add(removedData);
                    mCardListAdapter.notifyDataSetChanged();
                }
            });

            mCardListView = (ListView) view.findViewById(R.id.card_modify_list);
            mCardListView.setAdapter(mCardListAdapter);

            goBackButton.setOnClickListener(this);
            completeButton.setOnClickListener(this);

            requestCreditCardList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return view;
        }
    }

    @Override
    public void onClick(View _view) {
        try {
            switch (_view.getId()) {
                case R.id.ib_setting_goback: {
                    closeFragment();
                }
                break;
                case R.id.ib_complete: {
                    if (mRemovedModelList.size() > 0) {
                        // 서버 연동시 주석 제거
//                    testFavoriteListResponse();
                        CommonDialog.showDialogWithListener(mMainActivity, "카드 삭제", "카드를 삭제 하시겠습니까?", "삭제", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestCardListModify();
                            }
                        }, "취소", null);
                    } else {
                        closeFragment();
                    }
                }
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void closeFragment() {
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
        getActivity().getSupportFragmentManager().beginTransaction().remove(SettingCardModifyFragment.this).commit();
    }

    public class CardModifyListAdapter extends ArrayAdapter<CardListViewModel> {
        private ArrayList<CardListViewModel> mCardListViewModelList;
        private int mResourceId;
        private CardListViewHolder mViewHolder;

        public CardModifyListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull ArrayList<CardListViewModel> objects) {
            super(context, resource, objects);

            mCardListViewModelList = objects;
            mResourceId = resource;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(mResourceId, null);

                mViewHolder = new CardListViewHolder();
                mViewHolder.nameAndCardNumberTextView = (TextView) convertView.findViewById(R.id.tv_modify_card_number);
                mViewHolder.userEmailTextView = (TextView) convertView.findViewById(R.id.tv_modify_user_email);
                mViewHolder.btnDelete = (ImageButton) convertView.findViewById(R.id.btn_delete_card);

                mViewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mOnDeleteButtonClickListener != null) {
                            mOnDeleteButtonClickListener.onDeleteButtonClick(position);
                        }
                    }
                });

                convertView.setTag(mViewHolder);
            } else {
                mViewHolder = (CardListViewHolder) convertView.getTag();
            }

            CardListViewModel model = mCardListViewModelList.get(position);
            String displayCardNumber = model.getmUserName() + " " + model.getmCardNumber();
            mViewHolder.nameAndCardNumberTextView.setText(displayCardNumber);
            mViewHolder.userEmailTextView.setText(model.getmUserEmail());

            return convertView;
        }

        public void setDeleteButtonClickListener(OnDeleteCardButtonClickListener listener) {
            mOnDeleteButtonClickListener = listener;
        }
    }

    private void requestCreditCardList() {
        mMainActivity.showLoadingAnim();

        RequestParams requestParams = new RequestParams();
        requestParams.add("userId", mMainActivity.mUserDetail.getUserId());

        // 서버 연동시 주석 제거
        NetClient.send(mMainActivity, Configuration.URL_CREDIT_CARD_LIST, "POST", requestParams, new CreditCardListAck(), new NetResponseCallback(new CreditCardListResponse()));
    }

    private class CreditCardListResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            mMainActivity.hideLoadingAnim();

            mCardModifyList.clear();

            if (_netAPI != null) {
                ResultData resultData = _netAPI.getmResultData();
                if (resultData.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                    mCardModifyList.addAll(((CreditCardListAck)_netAPI).getmCreditCardList());
                } else { // rollback
                    CommonDialog.showSimpleDialog(mMainActivity, resultData.getmDetail());
                }
            }

            mCardListAdapter.notifyDataSetChanged();

            if(mCardModifyList.size() > 0) {
                mEmptyListText.setVisibility(View.GONE);
            } else {
                mEmptyListText.setVisibility(View.VISIBLE);
            }
        }
    }

    private void requestCardListModify() {
        mMainActivity.showLoadingAnim();

        String removedIds = "";
        for (CardListViewModel model : mRemovedModelList) {
            removedIds += model.getmCreditCardId() + ",";
        }

        if(removedIds.length() > 0) {
            removedIds = removedIds.substring(0, removedIds.length() - 1);
        }

        RequestParams requestParams = new RequestParams();
        requestParams.add("userId", mMainActivity.mUserDetail.getUserId());
        requestParams.add("removedList", removedIds);

        // 서버 연동시 주석 제거
        NetClient.send(mMainActivity, Configuration.URL_CREDIT_CARD_DELETE, "POST", requestParams, new NoResponseDataAck(), new NetResponseCallback(new CardListModifyResponse()));
    }

    private class CardListModifyResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            mMainActivity.hideLoadingAnim();

            if (_netAPI != null) {
                ResultData resultData = _netAPI.getmResultData();
                if (resultData.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                    mRemovedModelList.clear();

                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);

                    closeFragment();
                } else { // rollback
                    mRemovedModelList.clear();
                    mCardModifyList.clear();
                    mCardListAdapter.notifyDataSetChanged();

                    CommonDialog.showSimpleDialog(mMainActivity, resultData.getmDetail());
                }
            }
        }
    }
}

package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.crackdown.DriverPointViewModel;

/**
 * Created by youngmin on 2016-08-12.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NearDriverListAck extends NetAPI {
	@JsonProperty("driver_list")
	private ArrayList<DriverPointViewModel> mDriverPointViewModel;

	public NearDriverListAck() {}

	public NearDriverListAck(ResultData mResultData, ArrayList<DriverPointViewModel> mDriverPointViewModel) {
		super(mResultData);
		this.mDriverPointViewModel = mDriverPointViewModel;
	}

	public ArrayList<DriverPointViewModel> getmDriverPointViewModel() {
		return mDriverPointViewModel;
	}

	public void setmDriverPointViewModel(ArrayList<DriverPointViewModel> mDriverPointViewModel) {
		this.mDriverPointViewModel = mDriverPointViewModel;
	}
}

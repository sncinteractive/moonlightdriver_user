package kr.moonlightdriver.user.common.network;

import kr.moonlightdriver.user.common.network.NetApi.TMapRouteModel;

/**
 * Created by eklee on 2017.05.26..
 */
public class TMapResponseCallback {
    private TMapResponse mTMapResponse;

    public TMapResponseCallback(TMapResponse tMapResponse) {
        this.mTMapResponse = tMapResponse;
    }

    public void onResponse(TMapRouteModel tMapRouteModel) {
        this.mTMapResponse.onResponse(tMapRouteModel);
    }
}

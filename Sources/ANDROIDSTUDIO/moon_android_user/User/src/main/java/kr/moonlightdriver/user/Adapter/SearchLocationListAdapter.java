package kr.moonlightdriver.user.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.Enums.SearchLocationListItemType;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.viewmodel.callDriver.SearchLocationListViewModel;
import kr.moonlightdriver.user.viewmodel.common.LocationData;
import kr.moonlightdriver.user.viewmodel.setting.FavoriteListViewModel;

/**
 * Created by kyo on 2017-05-08.
 */

public class SearchLocationListAdapter extends ArrayAdapter<SearchLocationListViewModel> {
    public interface OnAddFavoriteClickListener {

        void onAddFavoriteClick(int position);
    }

    private OnAddFavoriteClickListener mOnAddFavoriteClickListener;

    public SearchLocationListAdapter(@NonNull Context context, @NonNull List<SearchLocationListViewModel> objects) {
        super(context, 0, objects);
    }

    public void setOnAddFavoriteClickListener(OnAddFavoriteClickListener onAddFavoriteClickListener) {
        mOnAddFavoriteClickListener = onAddFavoriteClickListener;
    }

    public void setList(List<FavoriteListViewModel> favoriteList, List<LocationData> latestList) {
        clear();

        if (favoriteList != null && favoriteList.size() > 0) {
            SearchLocationListViewModel favoriteTitleItem = new SearchLocationListViewModel(SearchLocationListItemType.TITLE, getContext().getString(R.string.search_location_favorite_title), null);
            add(favoriteTitleItem);

            for (FavoriteListViewModel model : favoriteList) {
                SearchLocationListViewModel favoriteItem = new SearchLocationListViewModel(SearchLocationListItemType.FAVORITE_ITEM, model.getName(), model.getLocationData());
                add(favoriteItem);
            }
        }

        if (latestList != null && latestList.size() > 0) {
            SearchLocationListViewModel latestTitleItem = new SearchLocationListViewModel(SearchLocationListItemType.TITLE, getContext().getString(R.string.search_location_latest_using_title), null);
            add(latestTitleItem);

            for (LocationData model : latestList) {
                SearchLocationListViewModel latestItem = new SearchLocationListViewModel(SearchLocationListItemType.LATEST_ITEM, null, model);
                add(latestItem);
            }
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            ISearchLocationViewHolder viewHolder = null;

            // inflate view and create view holder
            if (convertView == null) {
                SearchLocationListItemType type = SearchLocationListItemType.values()[getItemViewType(position)];
                switch (type) {
                    case TITLE: {
                        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_call_driver_search_location_title, parent, false);
                        viewHolder = new SearchLocationTitleViewHolderImpl();
                    }
                    break;
                    case FAVORITE_ITEM: {
                        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_call_driver_search_location_favorite, parent, false);
                        viewHolder = new SearchLocationFavoriteViewHolderImpl();
                    }
                    break;
                    case LATEST_ITEM: {
                        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_call_driver_search_location_latest_using, parent, false);
                        viewHolder = new SearchLocationLatestViewHolderImpl();
                        ((SearchLocationLatestViewHolderImpl) viewHolder).setOnAddFavoriteClickListener(new SearchLocationLatestViewHolderImpl.OnAddFavoriteClickListener() {
                            @Override
                            public void onAddFavoriteClick() {
                                if (mOnAddFavoriteClickListener != null) {
                                    mOnAddFavoriteClickListener.onAddFavoriteClick(position);
                                }
                            }
                        });
                    }
                    break;
                    default:
                        break;
                }

                if (viewHolder != null) {
                    viewHolder.onCreateViewHolder(convertView, position);
                }

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ISearchLocationViewHolder) convertView.getTag();
            }

            // bind
            if (viewHolder != null) {
                viewHolder.bind(getItem(position));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return SearchLocationListItemType.values().length;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getSearchLocationListItemType().getValue();
    }

    //region ViewHolder
    private interface ISearchLocationViewHolder {
        void onCreateViewHolder(View view, int position);

        void bind(SearchLocationListViewModel model);

        int getPosition();
    }

    private static class SearchLocationTitleViewHolderImpl implements ISearchLocationViewHolder {
        private int mPosition;
        private TextView mTitleTextView;

        @Override
        public void onCreateViewHolder(View view, int position) {
            mPosition = position;
            mTitleTextView = (TextView) view.findViewById(R.id.tv_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), mTitleTextView);
        }

        @Override
        public void bind(SearchLocationListViewModel model) {
            mTitleTextView.setText(model.getName());
        }

        @Override
        public int getPosition() {
            return mPosition;
        }
    }

    private static class SearchLocationFavoriteViewHolderImpl implements ISearchLocationViewHolder {
        private int mPosition;
        private TextView mContentTextView;

        @Override
        public void onCreateViewHolder(View view, int position) {
            mPosition = position;
            mContentTextView = (TextView) view.findViewById(R.id.tv_content);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanM(), mContentTextView);
        }

        @Override
        public void bind(SearchLocationListViewModel model) {
            mContentTextView.setText(mContentTextView.getContext().getString(R.string.search_location_favorite_content_format,
                model.getName(), model.getLocationData().getmAddress()));
        }

        @Override
        public int getPosition() {
            return mPosition;
        }
    }

    private static class SearchLocationLatestViewHolderImpl implements ISearchLocationViewHolder {
        public interface OnAddFavoriteClickListener {
            void onAddFavoriteClick();
        }

        private int mPosition;
        private TextView mNameTextView;
        private TextView mAddressTextView;
        private ImageView mAddFavoriteButton;

        private OnAddFavoriteClickListener mOnAddFavoriteClickListener;

        @Override
        public void onCreateViewHolder(View view, int position) {
            mPosition = position;
            mNameTextView = (TextView) view.findViewById(R.id.tv_name);
            mAddressTextView = (TextView) view.findViewById(R.id.tv_address);
            mAddFavoriteButton = (ImageView) view.findViewById(R.id.iv_add_favortie);
            mAddFavoriteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnAddFavoriteClickListener != null)
                        mOnAddFavoriteClickListener.onAddFavoriteClick();
                }
            });

            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanM(), mNameTextView, mAddressTextView);
        }

        @Override
        public void bind(SearchLocationListViewModel model) {
            mNameTextView.setText(model.getLocationData().getmLocationName());
            mAddressTextView.setText(model.getLocationData().getmAddress());

            if (model.getSearchLocationListItemType() == SearchLocationListItemType.LATEST_ITEM &&
                Utils.isStringNullOrEmpty(model.getName())) {
                mAddFavoriteButton.setEnabled(true);
            } else {
                mAddFavoriteButton.setEnabled(false);
            }
        }

        public void setOnAddFavoriteClickListener(OnAddFavoriteClickListener onAddFavoriteClickListener) {
            mOnAddFavoriteClickListener = onAddFavoriteClickListener;
        }

        @Override
        public int getPosition() {
            return mPosition;
        }
    }
    //endregion ViewHolder
}

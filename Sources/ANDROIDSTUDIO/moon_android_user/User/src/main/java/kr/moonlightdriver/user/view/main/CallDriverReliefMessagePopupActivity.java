package kr.moonlightdriver.user.view.main;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kakao.kakaolink.KakaoLink;
import com.kakao.kakaolink.KakaoTalkLinkMessageBuilder;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.viewmodel.callDriver.DriverInfoViewModel;
import kr.moonlightdriver.user.viewmodel.common.LocationData;
import kr.moonlightdriver.user.viewmodel.common.ResultData;

/**
 * Created by eklee on 2017. 6. 9..
 */

public class CallDriverReliefMessagePopupActivity extends Activity {
    private static String TAG = "CallDriverReliefMessagePopupActivity";

    private RelativeLayout mLoadingProgress;

    private String mSendTarget;

    private String mReliefMessage;

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);

            SharedPreferences prefs = Utils.getGCMPreferences(this);

            setContentView(R.layout.popup_call_driver_send_relief_message);

            TextView titleTextView = (TextView) findViewById(R.id.tv_popup_call_driver_send_relief_message_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanVert(), titleTextView);

            TextView sourceTextView = (TextView) findViewById(R.id.tv_popup_call_driver_send_relief_message_source_info);
            TextView targetTextView = (TextView) findViewById(R.id.tv_popup_call_driver_send_relief_message_target_info);
            TextView driverInfoTextView = (TextView) findViewById(R.id.tv_popup_call_driver_send_relief_message_driver_info);


            LocationData sourceLocationData = new LocationData(prefs.getString(Configuration.PROPERTY_CALL_INFO_SOURCE_LOCATION, null));
            String sourceName = sourceLocationData.getmLocationName();

            LocationData targetLocationData = new LocationData(prefs.getString(Configuration.PROPERTY_CALL_INFO_TARGET_LOCATION, null));
            String targetName = targetLocationData.getmLocationName();

            List<LocationData> stopbyLocationDataList = Utils.parseJSONStringToStopByLocationDataList(prefs.getString(Configuration.PROPERTY_CALL_INFO_STOPBY_LOCATION_LIST, null));
            List<String> stopbyNameList = new LinkedList<>();
            if (stopbyLocationDataList != null) {
                for (LocationData locationData : stopbyLocationDataList) {
                    stopbyNameList.add(locationData.getmLocationName());
                }
            }

            DriverInfoViewModel driverInfo = new DriverInfoViewModel(prefs.getString(Configuration.PROPERTY_CALL_INFO_DRIVER_INFO, null));
            String driverMaskedName = driverInfo.getMaskedDriverName();
            String driverLastPhoneNumber = driverInfo.getDriverLastPhoneNumber();

            long departureTime = prefs.getLong(Configuration.PROPERTY_CALL_INFO_DEPARTURE_TIME, 0);
            long arrivalTime = prefs.getLong(Configuration.PROPERTY_CALL_INFO_ESTIMATED_ARRIVAL_TIME, 0);

            mReliefMessage = generateReliefMessage(sourceName, targetName, stopbyNameList, driverMaskedName, driverLastPhoneNumber, departureTime, arrivalTime);

            sourceTextView.setText(getString(R.string.popup_call_driver_arrived_at_target_source_info_format, sourceName));
            targetTextView.setText(getString(R.string.popup_call_driver_arrived_at_target_target_info_format, targetName));
            driverInfoTextView.setText(getString(R.string.popup_call_driver_arrived_at_target_driver_info_format, driverMaskedName, driverLastPhoneNumber));

            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanM(), sourceTextView, targetTextView, driverInfoTextView);

            TextView descriptionTextView1 = (TextView) findViewById(R.id.tv_popup_call_driver_send_relief_message_description1);
            TextView descriptionTextView2 = (TextView) findViewById(R.id.tv_popup_call_driver_send_relief_message_description2);
            TextView descriptionTextView3 = (TextView) findViewById(R.id.tv_popup_call_driver_send_relief_message_description3);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), descriptionTextView1, descriptionTextView2, descriptionTextView3);

            ImageButton closeButton = (ImageButton) findViewById(R.id.ib_popup_call_driver_send_relief_message_close);
            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(CallDriverReliefMessagePopupActivity.this, MainActivity.class);
                    setResult(Activity.RESULT_OK, intent);
                    overridePendingTransition(0, 0);
                    finish();
                }
            });


            mSendTarget = "";

            ImageButton sendTextButton = (ImageButton) findViewById(R.id.ib_popup_call_driver_send_relief_message_send_text);
            ImageButton sendKakaoTalkButton = (ImageButton) findViewById(R.id.ib_popup_call_driver_send_relief_message_send_kakaotalk);
            sendTextButton.setVisibility(View.GONE);
            sendKakaoTalkButton.setVisibility(View.GONE);

            // check kakao talk
            ArrayList<ResolveInfo> sharedAppInfoList = Utils.getShareIntents(getPackageManager(), "com.kakao.talk");
            for (final ResolveInfo resolveInfo : sharedAppInfoList) {
                String packageName = resolveInfo.activityInfo.packageName.toLowerCase();
                if (packageName.equals("com.kakao.talk")) {
                    sendKakaoTalkButton.setVisibility(View.VISIBLE);
                    sendKakaoTalkButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                mSendTarget = "kakao";
                                requestUpdateReliefMessageState();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    break;
                }
            }

            // check sms
            if (Utils.isAvailableSMS(this)) {
                sendTextButton.setVisibility(View.VISIBLE);
                sendTextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSendTarget = "sms";
                        requestUpdateReliefMessageState();
                    }
                });
            }

            mLoadingProgress = (RelativeLayout) findViewById(R.id.loading_progress);
            hideLoadingAnim();
            //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestUpdateReliefMessageState() {
        try {
            showLoadingAnim();

            SharedPreferences prefs = Utils.getGCMPreferences(this);
            String userId = prefs.getString(Configuration.PROPERTY_USER_ID, null);
            String callId = prefs.getString(Configuration.PROPERTY_CALL_INFO_CALL_ID, null);
            if (!Utils.isStringNullOrEmpty(userId) && !Utils.isStringNullOrEmpty(callId)) {
                RequestParams requestParams = new RequestParams();
                requestParams.put("userId", userId);
                requestParams.put("callId", callId);
                requestParams.put("reliefMessage", mReliefMessage);
                NetClient.send(this, Configuration.URL_CALL_DRIVER_UPDATE_RELIEF_MESSAGE_STATE, "POST", requestParams, new NoResponseDataAck(), new NetResponseCallback(new CallDriverUpdateReliefMessageStateResponse()));
            } else {
                hideLoadingAnim();
            }
        } catch (Exception e) {
            hideLoadingAnim();
        }
    }

    private class CallDriverUpdateReliefMessageStateResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            try {
                hideLoadingAnim();

                if (_netAPI != null) {
                    ResultData resultData = _netAPI.getmResultData();
                    if (resultData.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                        CommonDialog.showSimpleDialog(CallDriverReliefMessagePopupActivity.this, resultData.getmDetail());
                        return;
                    }

                    switch (mSendTarget) {
                        case "sms": {
                            showSMSView();
                        }
                        break;
                        case "kakao": {
                            sendKakao();
                        }
                        break;
                        default:
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showLoadingAnim() {
        mLoadingProgress.setVisibility(View.VISIBLE);
    }

    private void hideLoadingAnim() {
        mLoadingProgress.setVisibility(View.GONE);
    }

    private void showSMSView() {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:"));
        intent.putExtra("subject", Configuration.SHARED_TEXT); // mms일때만 사용됨
        intent.putExtra("sms_body", mReliefMessage);
        startActivity(intent);

        sendResult("sms");
    }

    private void sendKakao() {
        try {
            KakaoLink kakaoLink = KakaoLink.getKakaoLink(this);
            KakaoTalkLinkMessageBuilder builder = kakaoLink.createKakaoTalkLinkMessageBuilder();

            builder.addText(Configuration.SHARED_TEXT + "\n" + mReliefMessage)
                .addAppButton("앱으로 이동");
            kakaoLink.sendMessage(builder, this);

            sendResult("kakao");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendResult(String result) {
        Intent intent = new Intent();
        intent.putExtra("result", result);
        setResult(Activity.RESULT_OK, intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private String generateReliefMessage(@NonNull String sourceName,
                                         @NonNull String targetName,
                                         @NonNull List<String> stopbyNameList,
                                         @NonNull String driverMaskedName,
                                         @NonNull String driverLastPhoneNumber,
                                         long departureTime,
                                         long arrivalTime) {
        StringBuilder builder = new StringBuilder();
        builder.append(getString(R.string.relief_message_title)).append("\n");
        builder.append(getString(R.string.relief_message_source_format, sourceName)).append("\n");
        if (stopbyNameList.size() > 0) {
            for (String stopbyName : stopbyNameList) {
                builder.append(getString(R.string.relief_message_stopby_format, stopbyName)).append("\n");
            }
        }
        builder.append(getString(R.string.relief_message_target_format, targetName)).append("\n");
        builder.append(getString(R.string.relief_message_departure_time_format, Utils.convertSimpleDateFormat("HH시MM분", departureTime))).append("\n");
        builder.append(getString(R.string.relief_message_arrival_time_format, Utils.convertSimpleDateFormat("HH시MM분", arrivalTime))).append("\n");
        builder.append(getString(R.string.relief_message_driver_info_format, driverMaskedName, driverLastPhoneNumber)).append("\n");

        return builder.toString();
    }
}

package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.NoticeData;
import kr.moonlightdriver.user.viewmodel.common.ResultData;

/**
 * Created by youngmin on 2016-08-18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NoticeListAck extends NetAPI {
	@JsonProperty("notices")
	private ArrayList<NoticeData> mNoticeDataList;

	public NoticeListAck() {}

	public NoticeListAck(ResultData mResultData, ArrayList<NoticeData> mNoticeDataList) {
		super(mResultData);
		this.mNoticeDataList = mNoticeDataList;
	}

	public ArrayList<NoticeData> getmNoticeDataList() {
		return mNoticeDataList;
	}

	public void setmNoticeDataList(ArrayList<NoticeData> mNoticeDataList) {
		this.mNoticeDataList = mNoticeDataList;
	}
}

package kr.moonlightdriver.user.viewmodel.setting;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by eklee on 2017. 4. 8..
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CardListViewModel {
    @JsonProperty("creditCardId")
    private String mCreditCardId;

    @JsonProperty("cardName")
    private String mCardName;

    @JsonProperty("cardNumber")
    private String mCardNumber;

    @JsonProperty("expirationDateMonth")
    private int mExpirationDateMonth;

    @JsonProperty("expirationDateYear")
    private int mExpirationDateYear;

    @JsonProperty("userName")
    private String mUserName;

    @JsonProperty("userEmail")
    private String mUserEmail;

    @JsonProperty("companyCardYN")
    private String mCompanyCardYN;

    public CardListViewModel() {}

    public CardListViewModel(String mCreditCardId, String mCardName, String mCardNumber, int mExpirationDateMonth, int mExpirationDateYear, String mUserName, String mUserEmail, String mCompanyCardYN) {
        this.mCreditCardId = mCreditCardId;
        this.mCardName = mCardName;
        this.mCardNumber = mCardNumber;
        this.mExpirationDateMonth = mExpirationDateMonth;
        this.mExpirationDateYear = mExpirationDateYear;
        this.mUserName = mUserName;
        this.mUserEmail = mUserEmail;
        this.mCompanyCardYN = mCompanyCardYN;
    }

    public String getmCreditCardId() {
        return mCreditCardId;
    }

    public void setmCreditCardId(String mCreditCardId) {
        this.mCreditCardId = mCreditCardId;
    }

    public String getmUserName() {
        return mUserName;
    }

    public void setmUserName(String mUserName) {
        this.mUserName = mUserName;
    }

    public String getmCardName() {
        return mCardName;
    }

    public void setmCardName(String mCardName) {
        this.mCardName = mCardName;
    }

    public String getmCardNumber() {
        return mCardNumber;
    }

    public void setmCardNumber(String mCardNumber) {
        this.mCardNumber = mCardNumber;
    }

    public int getmExpirationDateMonth() {
        return mExpirationDateMonth;
    }

    public void setmExpirationDateMonth(int mExpirationDateMonth) {
        this.mExpirationDateMonth = mExpirationDateMonth;
    }

    public int getmExpirationDateYear() {
        return mExpirationDateYear;
    }

    public void setmExpirationDateYear(int mExpirationDateYear) {
        this.mExpirationDateYear = mExpirationDateYear;
    }

    public String getmUserEmail() {
        return mUserEmail;
    }

    public void setmUserEmail(String mUserEmail) {
        this.mUserEmail = mUserEmail;
    }

    public String getmCompanyCardYN() {
        return mCompanyCardYN;
    }

    public void setmCompanyCardYN(String mCompanyCardYN) {
        this.mCompanyCardYN = mCompanyCardYN;
    }
}

package kr.moonlightdriver.user.viewholder;

import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by kyo on 2017-04-12.
 */

public class FavoriteModifyListViewHolder {
    public TextView nameTextView;
    public TextView addressTextView;
    public ImageButton deleteButton;
}

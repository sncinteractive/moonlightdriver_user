package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.FavoriteListViewModel;

/**
 * Created by kyo on 2017-04-13.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class FavoriteListAck extends NetAPI {
    @JsonProperty("favoriteList")
    private List<FavoriteListViewModel> mFavoriteList;

    public FavoriteListAck() {
    }

    public FavoriteListAck(ResultData mResultData, List<FavoriteListViewModel> favoriteModelList) {
        super(mResultData);
        mFavoriteList = favoriteModelList;
    }

    public List<FavoriteListViewModel> getFavoriteList() {
        return mFavoriteList;
    }

    public void setFavoriteList(List<FavoriteListViewModel> favoriteList) {
        mFavoriteList = favoriteList;
    }
}

package kr.moonlightdriver.user.view.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.view.main.MainActivity;

/**
 * Created by youngmin on 2016-06-21.
 */
public class SettingsFragment extends Fragment implements View.OnClickListener {

    private LinearLayout ll_mycar_info, ll_card_manage, ll_point_manage, ll_favorite_manage, ll_run_history, ll_notice, ll_coupon;
    private MainActivity mMainActivity;
    private SettingMycarFragment mycarFragment;
    private SettingNoticeFragment noticeFragment;
    private SettingPointFragment pointFragment;
    private SettingHistoryFragment historyFragment;
    private SettingCardFragment cardFragment;
    private SettingFavoriteFragment favoriteFragment;
    private SettingCouponFragment couponFragment;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            mMainActivity = (MainActivity) getActivity();

            view = inflater.inflate(R.layout.fragment_setting, container, false);

            ll_mycar_info = (LinearLayout) view.findViewById(R.id.ll_mycar_info);
            ll_card_manage = (LinearLayout) view.findViewById(R.id.ll_card_manage);
            ll_point_manage = (LinearLayout) view.findViewById(R.id.ll_point_manage);
            ll_favorite_manage = (LinearLayout) view.findViewById(R.id.ll_favorite_manage);
            ll_run_history = (LinearLayout) view.findViewById(R.id.ll_run_history);
            ll_notice = (LinearLayout) view.findViewById(R.id.ll_notice);
            ll_coupon = (LinearLayout) view.findViewById(R.id.ll_coupon);

            TextView headerTitleTextView = (TextView) view.findViewById(R.id.tv_header_title);

            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanB(), headerTitleTextView);
            Utils.setFontToTextViewRecursively(FontManager.getInstance().getFontSeoulNamsanM(), ll_mycar_info);
            Utils.setFontToTextViewRecursively(FontManager.getInstance().getFontSeoulNamsanM(), ll_card_manage);
            Utils.setFontToTextViewRecursively(FontManager.getInstance().getFontSeoulNamsanM(), ll_point_manage);
            Utils.setFontToTextViewRecursively(FontManager.getInstance().getFontSeoulNamsanM(), ll_favorite_manage);
            Utils.setFontToTextViewRecursively(FontManager.getInstance().getFontSeoulNamsanM(), ll_run_history);
            Utils.setFontToTextViewRecursively(FontManager.getInstance().getFontSeoulNamsanM(), ll_notice);
            Utils.setFontToTextViewRecursively(FontManager.getInstance().getFontSeoulNamsanM(), ll_coupon);

            ll_mycar_info.setOnClickListener(this);
            ll_card_manage.setOnClickListener(this);
            ll_point_manage.setOnClickListener(this);
            ll_favorite_manage.setOnClickListener(this);
            ll_run_history.setOnClickListener(this);
            ll_notice.setOnClickListener(this);
            ll_coupon.setOnClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return view;
        }
    }

    @Override
    public void onClick(View _view) {
        try {
            switch (_view.getId()) {
                case R.id.ll_mycar_info:
                    mycarFragment = new SettingMycarFragment();
                    mMainActivity.addFragment(new SettingMycarFragment());
                    break;
                case R.id.ll_card_manage:
                    cardFragment = new SettingCardFragment();
                    mMainActivity.addFragment(cardFragment);
                    break;
                case R.id.ll_point_manage:
                    pointFragment = new SettingPointFragment();
                    mMainActivity.addFragment(pointFragment);
                    break;
                case R.id.ll_favorite_manage:
                    favoriteFragment = new SettingFavoriteFragment();
                    mMainActivity.addFragment(favoriteFragment);
                    break;
                case R.id.ll_run_history:
                    historyFragment = new SettingHistoryFragment();
                    mMainActivity.addFragment(historyFragment);
                    break;
                case R.id.ll_notice:
                    noticeFragment = new SettingNoticeFragment();
                    mMainActivity.addFragment(noticeFragment);
                    break;
                case R.id.ll_coupon:
                    couponFragment = new SettingCouponFragment();
                    mMainActivity.addFragment(couponFragment);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
package kr.moonlightdriver.user.view.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.CouponDetailAck;
import kr.moonlightdriver.user.common.network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.CouponListViewModel;

/**
 * Created by youngmin on 2016-06-21.
 */
public class SettingCouponDetailFragment extends Fragment implements View.OnClickListener {
    public static final String PARAMS_1 = "userCouponId";
    public static final String PARAMS_2 = "couponId";

    private CouponListViewModel mCouponModelDetail;
    private String mUserCouponId;
    private String mCouponId;

    private MainActivity mMainActivity;

    private ImageView mCouponImage;
    private TextView mCouponDesc;
    private Button mBtnUseCoupon;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            mMainActivity = (MainActivity) getActivity();

            mUserCouponId = getArguments().getString(PARAMS_1);
            mCouponId = getArguments().getString(PARAMS_2);

            view = inflater.inflate(R.layout.fragment_setting_coupon_detail, container, false);

            TextView headerTitleTextView = (TextView)view.findViewById(R.id.tv_header_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanB(), headerTitleTextView);

            LinearLayout goBackButton = (LinearLayout) view.findViewById(R.id.ib_setting_goback);
            goBackButton.setOnClickListener(this);

            mCouponDesc = (TextView) view.findViewById(R.id.tv_coupon_desc);
            mCouponImage = (ImageView) view.findViewById(R.id.img_coupon_image);
            mBtnUseCoupon = (Button) view.findViewById(R.id.btn_use_coupon);
            mBtnUseCoupon.setOnClickListener(this);

            requestCouponDetailGet();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return view;
        }
    }

    @Override
    public void onClick(View _view) {
        switch (_view.getId()) {
            case R.id.ib_setting_goback: {
                getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
                break;
            }

            case R.id.btn_use_coupon : {
                if(mCouponModelDetail.getmStatus().equals("new")) {
                    mMainActivity.showConfirmPopup(getString(R.string.coupon_detail_use_confirm_title), getString(R.string.coupon_detail_use_confirm_desc), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mMainActivity.closeConfirmPopup();
                            requestUseCouponUpdate();
                        }
                    });
                } else if(mCouponModelDetail.getmStatus().equals("used")) {
                    CommonDialog.showSimpleDialog(mMainActivity, getString(R.string.coupon_detail_status_use_complete));
                } else {
                    CommonDialog.showSimpleDialog(mMainActivity, getString(R.string.coupon_detail_status_not_available));
                }
                break;
            }
        }
    }

    private void requestCouponDetailGet() {
        try {
            mMainActivity.showLoadingAnim();

            RequestParams requestParams = new RequestParams();
            requestParams.add("userCouponId", mUserCouponId);
            requestParams.add("userId", mMainActivity.mUserDetail.getUserId());
            requestParams.add("couponId", mCouponId);

            NetClient.send(mMainActivity, Configuration.URL_COUPON_DETAIL_GET, "POST", requestParams, new CouponDetailAck(), new NetResponseCallback(new CouponDetailGetResponse()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class CouponDetailGetResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            mMainActivity.hideLoadingAnim();

            if (_netAPI != null) {
                ResultData resultData = _netAPI.getmResultData();
                if (resultData.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                    mCouponModelDetail = ((CouponDetailAck)_netAPI).getCouponDetail();

                    renderCouponDetailData();
                } else { // rollback
                    CommonDialog.showSimpleDialog(mMainActivity, resultData.getmDetail());
                }
            }
        }
    }

    private void renderCouponDetailData() {
        try {
            mCouponImage.setVisibility(View.GONE);

            if (!Utils.isStringNullOrEmpty(mCouponModelDetail.getmCouponImgPath())) {
                new Utils.DownloadImageTask(mCouponImage).execute(Configuration.COUPON_IMAGE_SERVER_HOST + mCouponModelDetail.getmCouponImgPath());
            }

            mCouponDesc.setText(mCouponModelDetail.getmCouponDesc());

            if(isAvailableCouponDate()) {
                if(mCouponModelDetail.getmStatus().equals("new")) {
                    mBtnUseCoupon.setText(R.string.coupon_detail_btn_use);
                } else if(mCouponModelDetail.getmStatus().equals("used")) {
                    mBtnUseCoupon.setText(R.string.coupon_detail_btn_used);
                } else {
                    mBtnUseCoupon.setText(R.string.coupon_detail_btn_not_available);
                }
            } else {
                mBtnUseCoupon.setText(R.string.coupon_detail_btn_not_available);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestUseCouponUpdate() {
        try {
            mMainActivity.showLoadingAnim();

            RequestParams requestParams = new RequestParams();
            requestParams.add("userCouponId", mUserCouponId);
            requestParams.add("userId", mMainActivity.mUserDetail.getUserId());
            requestParams.add("status", "used");

            NetClient.send(mMainActivity, Configuration.URL_COUPON_USE_UPDATE, "POST", requestParams, new NoResponseDataAck(), new NetResponseCallback(new UseCouponUpdateResponse()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class UseCouponUpdateResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            mMainActivity.hideLoadingAnim();

            if (_netAPI != null) {
                ResultData resultData = _netAPI.getmResultData();
                if (resultData.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                    mBtnUseCoupon.setText(R.string.coupon_detail_btn_used);
                    CommonDialog.showSimpleDialog(mMainActivity, getString(R.string.coupon_detail_status_use_ok));
                } else { // rollback
                    CommonDialog.showSimpleDialog(mMainActivity, resultData.getmDetail());
                }
            }
        }
    }

    private boolean isAvailableCouponDate() {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.KOREA);

            long couponStartTime = format.parse(mCouponModelDetail.getmCouponStartDate()).getTime();
            long couponEndTime = format.parse(mCouponModelDetail.getmCouponEndDate()).getTime();

            Calendar nowCal = Calendar.getInstance();
            long nowDate = nowCal.getTimeInMillis();

            if(couponStartTime <= nowDate && nowDate <= couponEndTime) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
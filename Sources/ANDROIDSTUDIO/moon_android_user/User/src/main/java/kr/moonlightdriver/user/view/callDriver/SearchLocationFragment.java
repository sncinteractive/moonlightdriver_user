package kr.moonlightdriver.user.view.callDriver;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.RequestParams;
import com.skt.Tmap.TMapAddressInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import kr.moonlightdriver.user.Adapter.SearchLocationListAdapter;
import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.ResultCallback;
import kr.moonlightdriver.user.common.TTSManager;
import kr.moonlightdriver.user.common.TmapManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.view.settings.SearchPOIFragment;
import kr.moonlightdriver.user.viewmodel.callDriver.SearchLocationListViewModel;
import kr.moonlightdriver.user.viewmodel.common.LocationData;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.FavoriteListViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchLocationFragment extends Fragment implements SearchLocationListAdapter.OnAddFavoriteClickListener {
    private static final String TAG = "SearchLocationFragment";

    private static final int REQ_SEARCH_LOCATION = 1;
    private static final int REQ_SEARCH_MAP = 2;

    private static final int LATEST_LIST_ITEM_COUNT_MAX = 50;

    private MainActivity mMainActivity;

    private String mLocationType;

    private List<LocationData> mLatestList;
    private List<SearchLocationListViewModel> mSearchLocationListViewModelList;
    private SearchLocationListViewModel mSearchLocationViewModel;
    private SearchLocationListAdapter mListAdapter;
    ListView mListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = null;
        try {
            view = inflater.inflate(R.layout.fragment_search_location, container, false);

            Bundle bundle = SearchLocationFragment.this.getArguments();
            mLocationType = bundle.getString("locationType", "");

            mMainActivity = (MainActivity) getActivity();
            mMainActivity.setTabMenuVisibility(View.GONE);

            RelativeLayout searchKeywordLayout = (RelativeLayout) view.findViewById(R.id.rl_search_keyword);
            searchKeywordLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment fragment = new SearchPOIFragment();
                    fragment.setTargetFragment(SearchLocationFragment.this, REQ_SEARCH_LOCATION);
                    mMainActivity.addFragment(fragment);
                }
            });

            TextView searchLocationTextView = (TextView) view.findViewById(R.id.tv_search_location);

            LinearLayout setSourceFromCurrentLayout = (LinearLayout) view.findViewById(R.id.ll_set_source_from_current);
            setSourceFromCurrentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TmapManager tmapManager = new TmapManager();
                    tmapManager.convertGpsToAddress(MainActivity.mCurrentLocation, new ResultCallback() {
                        @Override
                        public void resultCallback(HashMap<String, Object> params) {
                            try {
                                TMapAddressInfo addressInfo = (TMapAddressInfo) params.get("addressInfo");
                                if (addressInfo != null) {
                                    LocationData locationData = new LocationData();
                                    locationData.setmLocation(MainActivity.mCurrentLocation);
                                    locationData.setmAddress(addressInfo.strFullAddress);

                                    if (Utils.isStringNullOrEmpty(addressInfo.strBuildingName)) {
                                        locationData.setmLocationName(addressInfo.strFullAddress);
                                    } else {
                                        locationData.setmLocationName(addressInfo.strBuildingName);
                                    }

                                    sendResult(locationData);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });

            SpannableStringBuilder prefixSpannableStringBuilder = new SpannableStringBuilder(getString(R.string.call_driver_set_source_from_current_prefix));
            prefixSpannableStringBuilder.setSpan(new ForegroundColorSpan(Color.rgb(220, 43, 0)), 0, prefixSpannableStringBuilder.length(), 0);
            SpannableStringBuilder postfixSpannableStringBuilder = new SpannableStringBuilder(getString(R.string.call_driver_set_source_from_current_postfix));
            postfixSpannableStringBuilder.setSpan(new ForegroundColorSpan(Color.BLACK), 0, postfixSpannableStringBuilder.length(), 0);
            TextView setSourceFromCurrentTextView = (TextView) view.findViewById(R.id.tv_content);
            setSourceFromCurrentTextView.setText(TextUtils.concat(prefixSpannableStringBuilder, postfixSpannableStringBuilder));

            if (mLocationType.equals("source")) {
                searchLocationTextView.setHint(getContext().getString(R.string.common_search_source));
                setSourceFromCurrentLayout.setVisibility(View.VISIBLE);
            } else if (mLocationType.equals("target")) {
                searchLocationTextView.setHint(getContext().getString(R.string.common_search_target));
                setSourceFromCurrentLayout.setVisibility(View.GONE);
            } else if (mLocationType.startsWith("stopby")) {
                searchLocationTextView.setHint(getContext().getString(R.string.common_search_stopby));
                setSourceFromCurrentLayout.setVisibility(View.GONE);
            }

            ImageButton voiceGuidanceIcon = (ImageButton) view.findViewById(R.id.ib_voice_guidance);
            voiceGuidanceIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (mLocationType) {
                        case "source":
                            TTSManager.getInstance().speak(getContext(), "출발지를 검색해주세요");
                            break;
                        case "target":
                            TTSManager.getInstance().speak(getContext(), "목적지를 검색해주세요");
                            break;
                        default:
                            TTSManager.getInstance().speak(getContext(), "경유지를 검색해주세요");
                            break;
                    }
                }
            });

            LinearLayout searchMapLayout = (LinearLayout) view.findViewById(R.id.ll_search_map);
            searchMapLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MapSearchLocationFragment mapFragment = new MapSearchLocationFragment();

                    Bundle bundle = new Bundle();
                    bundle.putString("locationType", mLocationType);

                    mapFragment.setArguments(bundle);
                    mapFragment.setTargetFragment(SearchLocationFragment.this, REQ_SEARCH_MAP);

                    mMainActivity.addFragment(mapFragment);
                }
            });

            loadLatestList();

            createSearchLocationViewModelList();
            mListAdapter = new SearchLocationListAdapter(getContext(), mSearchLocationListViewModelList);
            mListAdapter.setOnAddFavoriteClickListener(this);

            mListView = (ListView) view.findViewById(R.id.lv_favorite_and_latest);
            mListView.setAdapter(mListAdapter);
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    SearchLocationListViewModel item = (SearchLocationListViewModel) mListView.getItemAtPosition(position);
                    LocationData locationData = item.getLocationData();
                    switch (item.getSearchLocationListItemType()) {
                        case LATEST_ITEM: {
                            addLatestItem(locationData);
                        }
                        // fall through
                        case FAVORITE_ITEM: {
                            sendResult(locationData);
                        }
                        break;
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    //region latest list
    private void loadLatestList() {
        try {
            if (mLatestList == null) {
                mLatestList = new LinkedList<>();
            }

            SharedPreferences prefs = Utils.getGCMPreferences(mMainActivity);
            String json = prefs.getString(Configuration.PROPERTY_LATEST_LOCATION_LIST, null);
            if (!Utils.isStringNullOrEmpty(json)) {
                JSONObject jsonObject = new JSONObject(json);
                JSONArray jsonArray = jsonObject.getJSONArray("latestList");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonItem = new JSONObject(jsonArray.getString(i));
                    JSONObject jsonLatLng = jsonItem.getJSONObject("mLocation");
                    LocationData locationData = new LocationData(new LatLng(jsonLatLng.getDouble("latitude"), jsonLatLng.getDouble("longitude")), jsonItem.getString("mLocationName"), jsonItem.getString("mAddress"));
                    mLatestList.add(locationData);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addLatestItem(LocationData locationData) {
        try {
            if (mLatestList.size() >= LATEST_LIST_ITEM_COUNT_MAX) {
                mLatestList.remove(0);
            }

            if (mLatestList.contains(locationData)) {
                mLatestList.remove(locationData);
            }
            mLatestList.add(locationData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void removeLatestItem(LocationData locationData) {
        try {
            mLatestList.remove(locationData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveLatestList() {
        try {
            if (mLatestList == null || mLatestList.size() == 0) {
                return;
            }

            ObjectMapper mapper = new ObjectMapper();
            JSONArray jsonArray = new JSONArray();
            for (LocationData item : mLatestList) {
                String jsonItem = mapper.writeValueAsString(item);
                jsonArray.put(jsonItem);
            }

            SharedPreferences prefs = Utils.getGCMPreferences(mMainActivity);
            SharedPreferences.Editor editor = prefs.edit();
            JSONObject jsonObject = new JSONObject();
            editor.remove(Configuration.PROPERTY_LATEST_LOCATION_LIST);
            jsonObject.put("latestList", jsonArray);
            editor.putString(Configuration.PROPERTY_LATEST_LOCATION_LIST, jsonObject.toString());
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //endregion latest list

    private void createSearchLocationViewModelList() {
        mSearchLocationListViewModelList = new ArrayList<>();

        try {
            List<FavoriteListViewModel> favoriteList = mMainActivity.mUserAdditionalData.getFavoriteList();
            if (favoriteList.size() > 0) {
                SearchLocationListViewModel favoriteTitle = new SearchLocationListViewModel(Enums.SearchLocationListItemType.TITLE, getContext().getString(R.string.search_location_favorite_title), null);
                mSearchLocationListViewModelList.add(favoriteTitle);

                for (FavoriteListViewModel model : favoriteList) {
                    SearchLocationListViewModel favoriteItem = new SearchLocationListViewModel(Enums.SearchLocationListItemType.FAVORITE_ITEM, model.getName(), model.getLocationData());
                    mSearchLocationListViewModelList.add(favoriteItem);
                }
            }

            int latestListSize = mLatestList.size();
            if (latestListSize > 0) {
                SearchLocationListViewModel latestTitle = new SearchLocationListViewModel(Enums.SearchLocationListItemType.TITLE, getContext().getString(R.string.search_location_latest_using_title), null);
                mSearchLocationListViewModelList.add(latestTitle);

                for (int i = latestListSize - 1; i >= 0; i--) {
                    SearchLocationListViewModel latestItem = new SearchLocationListViewModel(Enums.SearchLocationListItemType.LATEST_ITEM, null, mLatestList.get(i));
                    mSearchLocationListViewModelList.add(latestItem);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        try {
            super.onDestroyView();

            mMainActivity.setTabMenuVisibility(View.VISIBLE);

            saveLatestList();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void closeFragment() {
        getChildFragmentManager().beginTransaction().remove(SearchLocationFragment.this).commit();
    }

    private void sendResult(LocationData locationData) {
        try {
            Intent intent = new Intent();
            intent.putExtra("locationType", mLocationType);
            intent.putExtra("locationData", locationData);

            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);

            getActivity().getSupportFragmentManager().beginTransaction().remove(SearchLocationFragment.this).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == Activity.RESULT_OK) {
                switch (requestCode) {
                    case REQ_SEARCH_LOCATION:
                        // fall through
                    case REQ_SEARCH_MAP: {
                        if (data != null) {
                            LocationData locationData = data.getParcelableExtra("locationData");
                            addLatestItem(locationData);
                            sendResult(locationData);
                        }
                    }
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAddFavoriteClick(int position) {
        try {
            mSearchLocationViewModel = (SearchLocationListViewModel) mListView.getItemAtPosition(position);
            String locationName = mSearchLocationViewModel.getLocationData().getmLocationName();

            for (FavoriteListViewModel favorite : mMainActivity.mUserAdditionalData.getFavoriteList()) {
                if (locationName.equalsIgnoreCase(favorite.getName())) {
                    return;
                }
            }

            requestAddFavorite();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestAddFavorite() {
        try {
            if (mSearchLocationViewModel == null) {
                return;
            }

            mMainActivity.showLoadingAnim();

            LocationData locationData = mSearchLocationViewModel.getLocationData();

            RequestParams requestParams = new RequestParams();
            requestParams.add("userId", mMainActivity.mUserDetail.getUserId());
            requestParams.put("favoriteName", locationData.getmLocationName());
            requestParams.put("locationName", locationData.getmLocationName());
            requestParams.put("locationAddress", locationData.getmAddress());
            requestParams.put("lat", Double.toString(locationData.getmLocation().latitude));
            requestParams.put("lng", Double.toString(locationData.getmLocation().longitude));

            //TODO: 서버 연동시 주석 제거
//            testFavoriteListAddResponse();
            NetClient.send(mMainActivity, Configuration.URL_FAVORITE_LIST_ADD, "POST", requestParams, new NoResponseDataAck(), new NetResponseCallback(new FavoriteListAddResponse()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class FavoriteListAddResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            try {
                mMainActivity.hideLoadingAnim();

                if (_netAPI != null) {
                    ResultData resultData = _netAPI.getmResultData();
                    if (resultData.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                        LocationData locationData = mSearchLocationViewModel.getLocationData();

                        FavoriteListViewModel favoriteModel = new FavoriteListViewModel(mSearchLocationViewModel.getLocationData().getmLocationName(), locationData);
                        mMainActivity.mUserAdditionalData.addFavoriteListItem(favoriteModel);

                        removeLatestItem(locationData);
                        mSearchLocationListViewModelList.remove(mSearchLocationViewModel);

                        SearchLocationListViewModel searchLocationModel = new SearchLocationListViewModel(Enums.SearchLocationListItemType.FAVORITE_ITEM, favoriteModel.getName(), favoriteModel.getLocationData());
                        int addIndex = mMainActivity.mUserAdditionalData.getFavoriteList().size();
                        mSearchLocationListViewModelList.add(addIndex, searchLocationModel);

                        mListAdapter.notifyDataSetChanged();

                        mSearchLocationViewModel = null;
                    } else { // rollback
                        CommonDialog.showSimpleDialog(mMainActivity, resultData.getmDetail());
                    }
                }
            } catch (Exception e) {

            }
        }
    }
}

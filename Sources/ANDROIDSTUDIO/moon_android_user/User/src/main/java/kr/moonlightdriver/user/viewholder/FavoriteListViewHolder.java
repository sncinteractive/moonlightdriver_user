package kr.moonlightdriver.user.viewholder;

import android.widget.TextView;

/**
 * Created by kyo on 2017-04-12.
 */

public class FavoriteListViewHolder {
    public TextView nameTextView;
    public TextView addressTextView;
}

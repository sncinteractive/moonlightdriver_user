package kr.moonlightdriver.user.view.main;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.viewmodel.common.ResultData;

/**
 * Created by eklee on 2017. 6. 6..
 */

public class CallDriverCardPaymentPopupActivity extends Activity {
    private static String TAG = "CallDriverCardPaymentPopupActivity";

    private RelativeLayout mLoadingProgress;

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences prefs = Utils.getGCMPreferences(this);
        int price = prefs.getInt(Configuration.PROPERTY_CALL_INFO_PRICE, Integer.MAX_VALUE);

        setContentView(R.layout.popup_call_driver_card_payment);

        TextView titleTextView = (TextView) findViewById(R.id.tv_popup_call_driver_card_payment_title);
        Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanVert(), titleTextView);

        ImageButton okButton = (ImageButton) findViewById(R.id.ib_popup_call_driver_card_payment_ok);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: PG 연동 필요. 임시로 결제 완료로 처리
                requestUpdatePayment();
            }
        });

        ImageButton cancelButton = (ImageButton) findViewById(R.id.ib_popup_call_driver_card_payment_cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult("cancel");
            }
        });

        ImageButton closeButton = (ImageButton) findViewById(R.id.ib_popup_call_driver_card_payment_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult("cancel");
            }
        });

        mLoadingProgress = (RelativeLayout) findViewById(R.id.loading_progress);
        hideLoadingAnim();
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

    private void showLoadingAnim() {
        mLoadingProgress.setVisibility(View.VISIBLE);
    }

    private void hideLoadingAnim() {
        mLoadingProgress.setVisibility(View.GONE);
    }

    private void requestUpdatePayment() {
        try {
            SharedPreferences prefs = Utils.getGCMPreferences(this);
            String callId = prefs.getString(Configuration.PROPERTY_CALL_INFO_CALL_ID, null);

            showLoadingAnim();

            RequestParams requestParams = new RequestParams();
            requestParams.put("callId", callId);

            NetClient.send(this, Configuration.URL_CALL_DRIVER_UPDATE_PAYMENT, "POST", requestParams, new NoResponseDataAck(), new NetResponseCallback(new UpdatePaymentResponse()));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private class UpdatePaymentResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            hideLoadingAnim();

            if (_netAPI != null) {
                ResultData resultData = _netAPI.getmResultData();
                if (resultData.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                    CommonDialog.showSimpleDialog(CallDriverCardPaymentPopupActivity.this, resultData.getmDetail());
                    return;
                }

                sendResult("complete");
            }
        }
    }


//    private void requestCancelPayment() {
//        try {
//            SharedPreferences prefs = Utils.getGCMPreferences(this);
//            String userId = prefs.getString(Configuration.PROPERTY_USER_ID, null);
//
//            showLoadingAnim();
//
//            RequestParams requestParams = new RequestParams();
//            requestParams.put("userId", userId);
//
//            //TODO: url 수정 필요
//            NetClient.send(this, Configuration.URL_CALL_DRIVER_CANCEL_PAYMENT, "POST", requestParams, new NoResponseDataAck(), new NetResponseCallback(new CancelPaymentResponse()));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    private class CancelPaymentResponse implements NetResponse {
//        @Override
//        public void onResponse(NetAPI _netAPI) {
//            hideLoadingAnim();
//
//            if (_netAPI != null) {
//                ResultData resultData = _netAPI.getmResultData();
//                if (resultData.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
//                    CommonDialog.showSimpleDialog(CallDriverCardPaymentPopupActivity.this, resultData.getmDetail());
//                    return;
//                }
//
//                SharedPreferences prefs = Utils.getGCMPreferences(CallDriverCardPaymentPopupActivity.this);
//                prefs.edit().putInt(Configuration.PROPERTY_CALL_INFO_STATUS, Enums.CallDriverState.IDLE.getValue()).apply();
//
//                sendResult("cancel");
//            }
//        }
//    }
//
//    private void testCancelPaymentResponse() {
//        showLoadingAnim();
//
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                NoResponseDataAck successAck = new NoResponseDataAck(new ResultData(Enums.NetResultCode.SUCCESS.getValue(), null, null));
//                NoResponseDataAck failAck = new NoResponseDataAck(new ResultData(Enums.NetResultCode.UNKNOWN_ERROR.getValue(), null, null));
//                new CancelPaymentResponse().onResponse(successAck);
//            }
//        }, 500);
//    }

    private void sendResult(String payResult) {
        try {
//            if (payResult.equals("complete")) {
//                SharedPreferences prefs = Utils.getGCMPreferences(CallDriverCardPaymentPopupActivity.this);
//                prefs.edit().putInt(Configuration.PROPERTY_CALL_INFO_STATUS, Enums.CallDriverState.PAY_COMPLETED.getValue()).apply();
//            }

            Intent intent = new Intent();
            intent.putExtra("payResult", payResult);
            setResult(Activity.RESULT_OK, intent);
            overridePendingTransition(0, 0);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

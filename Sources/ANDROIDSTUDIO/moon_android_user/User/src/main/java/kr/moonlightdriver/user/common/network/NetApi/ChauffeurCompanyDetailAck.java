package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.crackdown.ChauffeurCompanyViewModel;

/**
 * Created by youngmin on 2016-08-11.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChauffeurCompanyDetailAck extends NetAPI{
	@JsonProperty("chauffeur_company_detail")
	private ChauffeurCompanyViewModel mChauffeurCompanyViewModel;
	@JsonProperty("virtual_number")
	private String mVirtualNumber;

	public ChauffeurCompanyDetailAck() {}

	public ChauffeurCompanyDetailAck(ResultData mResultData, ChauffeurCompanyViewModel mChauffeurCompanyViewModel, String mVirtualNumber) {
		super(mResultData);
		this.mChauffeurCompanyViewModel = mChauffeurCompanyViewModel;
		this.mVirtualNumber = mVirtualNumber;
	}

	public ChauffeurCompanyViewModel getmChauffeurCompanyViewModel() {
		return mChauffeurCompanyViewModel;
	}

	public void setmChauffeurCompanyViewModel(ChauffeurCompanyViewModel mChauffeurCompanyViewModel) {
		this.mChauffeurCompanyViewModel = mChauffeurCompanyViewModel;
	}

	public String getmVirtualNumber() {
		return mVirtualNumber;
	}

	public void setmVirtualNumber(String mVirtualNumber) {
		this.mVirtualNumber = mVirtualNumber;
	}
}

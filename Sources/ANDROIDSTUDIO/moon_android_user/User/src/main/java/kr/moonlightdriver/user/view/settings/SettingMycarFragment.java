package kr.moonlightdriver.user.view.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.SettingsMyCarInfoAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.SettingsMyCarInfoViewModel;

/**
 * Created by youngmin on 2016-06-21.
 */
public class SettingMycarFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "SettingMycarFragment";

    private ImageButton ib_mycar_submit, ib_mycar_small, ib_mycar_middle, ib_mycar_large, ib_mycar_suv, ib_mycar_domestic, ib_mycar_foreign, ib_mycar_auto, ib_mycar_manual;
    private LinearLayout ib_mycar_goback;
    private MainActivity mMainActivity;
    private SettingsFragment mFragment;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            mMainActivity = (MainActivity) getActivity();

            view = inflater.inflate(R.layout.fragment_setting_mycar, container, false);

            TextView headerTitleTextView = (TextView) view.findViewById(R.id.tv_header_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanB(), headerTitleTextView);

            ib_mycar_goback = (LinearLayout) view.findViewById(R.id.ib_setting_goback);
            ib_mycar_submit = (ImageButton) view.findViewById(R.id.ib_mycar_submit);
            ib_mycar_small = (ImageButton) view.findViewById(R.id.ib_mycar_small);
            ib_mycar_middle = (ImageButton) view.findViewById(R.id.ib_mycar_middle);
            ib_mycar_large = (ImageButton) view.findViewById(R.id.ib_mycar_large);
            ib_mycar_suv = (ImageButton) view.findViewById(R.id.ib_mycar_suv);
            ib_mycar_domestic = (ImageButton) view.findViewById(R.id.ib_mycar_domestic);
            ib_mycar_foreign = (ImageButton) view.findViewById(R.id.ib_mycar_foreign);
            ib_mycar_auto = (ImageButton) view.findViewById(R.id.ib_mycar_auto);
            ib_mycar_manual = (ImageButton) view.findViewById(R.id.ib_mycar_manual);

            ib_mycar_goback.setOnClickListener(this);
            ib_mycar_submit.setOnClickListener(this);
            ib_mycar_small.setOnClickListener(this);
            ib_mycar_middle.setOnClickListener(this);
            ib_mycar_large.setOnClickListener(this);
            ib_mycar_suv.setOnClickListener(this);
            ib_mycar_domestic.setOnClickListener(this);
            ib_mycar_foreign.setOnClickListener(this);
            ib_mycar_auto.setOnClickListener(this);
            ib_mycar_manual.setOnClickListener(this);

            getMyCarInfo();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return view;
        }
    }

    @Override
    public void onClick(View _view) {
        try {
            if (_view == ib_mycar_goback) {
                closeFragment();
            } else if (_view == ib_mycar_submit) {
                saveMyCarInfo();
            } else if (_view == ib_mycar_small) {
                changeCarType("small");
            } else if (_view == ib_mycar_middle) {
                changeCarType("middle");
            } else if (_view == ib_mycar_large) {
                changeCarType("large");
            } else if (_view == ib_mycar_suv) {
                changeCarType("suv");
            } else if (_view == ib_mycar_domestic) {
                changeCarMadeType("domestic");
            } else if (_view == ib_mycar_foreign) {
                changeCarMadeType("foreign");
            } else if (_view == ib_mycar_auto) {
                changeCarTransmissionType("auto");
            } else if (_view == ib_mycar_manual) {
                changeCarTransmissionType("manual");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getSelectedCarType() {
        try {
            if(ib_mycar_small.isSelected()) {
                return "small";
            } else if(ib_mycar_middle.isSelected()) {
                return "middle";
            } else if(ib_mycar_large.isSelected()) {
                return "large";
            } else if(ib_mycar_suv.isSelected()) {
                return "suv";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    private void changeCarType(String _type) {
        try {
            ib_mycar_small.setImageResource(R.drawable.btn_setting_mycar_small_disable);
            ib_mycar_small.setSelected(false);
            ib_mycar_middle.setImageResource(R.drawable.btn_setting_mycar_middle_disable);
            ib_mycar_middle.setSelected(false);
            ib_mycar_large.setImageResource(R.drawable.btn_setting_mycar_large_disable);
            ib_mycar_large.setSelected(false);
            ib_mycar_suv.setImageResource(R.drawable.btn_setting_mycar_suv_disable);
            ib_mycar_suv.setSelected(false);

            switch (_type) {
                case "small" :
                    ib_mycar_small.setImageResource(R.drawable.btn_setting_mycar_small_enable);
                    ib_mycar_small.setSelected(true);
                    break;
                case "middle" :
                    ib_mycar_middle.setImageResource(R.drawable.btn_setting_mycar_middle_enable);
                    ib_mycar_middle.setSelected(true);
                    break;
                case "large" :
                    ib_mycar_large.setImageResource(R.drawable.btn_setting_mycar_large_enable);
                    ib_mycar_large.setSelected(true);
                    break;
                case "suv" :
                    ib_mycar_suv.setImageResource(R.drawable.btn_setting_mycar_suv_enable);
                    ib_mycar_suv.setSelected(true);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getSelectedCarMadeType() {
        try {
            if(ib_mycar_domestic.isSelected()) {
                return "domestic";
            } else if(ib_mycar_foreign.isSelected()) {
                return "foreign";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    private void changeCarMadeType(String _type) {
        try {
            ib_mycar_domestic.setImageResource(R.drawable.btn_setting_mycar_domestic_disable);
            ib_mycar_domestic.setSelected(false);
            ib_mycar_foreign.setImageResource(R.drawable.btn_setting_mycar_foreign_disable);
            ib_mycar_foreign.setSelected(false);

            switch (_type) {
                case "domestic" :
                    ib_mycar_domestic.setImageResource(R.drawable.btn_setting_mycar_domestic_enable);
                    ib_mycar_domestic.setSelected(true);
                    break;
                case "foreign" :
                    ib_mycar_foreign.setImageResource(R.drawable.btn_setting_mycar_foreign_enable);
                    ib_mycar_foreign.setSelected(true);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getSelectedCarTransmissionType() {
        try {
            if(ib_mycar_auto.isSelected()) {
                return "auto";
            } else if(ib_mycar_manual.isSelected()) {
                return "manual";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    private void changeCarTransmissionType(String _type) {
        try {
            ib_mycar_auto.setImageResource(R.drawable.btn_setting_mycar_auto_disable);
            ib_mycar_auto.setSelected(false);
            ib_mycar_manual.setImageResource(R.drawable.btn_setting_mycar_manual_disable);
            ib_mycar_manual.setSelected(false);

            switch (_type) {
                case "auto" :
                    ib_mycar_auto.setImageResource(R.drawable.btn_setting_mycar_auto_enable);
                    ib_mycar_auto.setSelected(true);
                    break;
                case "manual" :
                    ib_mycar_manual.setImageResource(R.drawable.btn_setting_mycar_manual_enable);
                    ib_mycar_manual.setSelected(true);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getMyCarInfo() {
        try {
            mMainActivity.showLoadingAnim();

            RequestParams params = new RequestParams();
            params.add("userId", mMainActivity.mUserDetail.getUserId());

            NetClient.send(mMainActivity, Configuration.URL_SETTINGS_GET_MY_CAR_INFO, "POST", params, new SettingsMyCarInfoAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    try {
                        mMainActivity.hideLoadingAnim();

                        if (_netAPI != null) {
                            SettingsMyCarInfoAck retNetApi = (SettingsMyCarInfoAck) _netAPI;
                            ResultData result = retNetApi.getmResultData();
                            if (result.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                                CommonDialog.showSimpleDialog(mMainActivity, result.getmDetail());
                                return;
                            }

                            SettingsMyCarInfoViewModel carInfo = retNetApi.getmCarInfo();

                            changeCarType(carInfo.getmCarType());
                            changeCarMadeType(carInfo.getmMadeType());
                            changeCarTransmissionType(carInfo.getmTransmissionType());
                        }
                    } catch (Exception e) {
                        mMainActivity.hideLoadingAnim();
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            mMainActivity.hideLoadingAnim();
            e.printStackTrace();
        }
    }

    private void saveMyCarInfo() {
        try {
            mMainActivity.showLoadingAnim();

            RequestParams params = new RequestParams();
            params.add("userId", mMainActivity.mUserDetail.getUserId());
            params.add("carType", getSelectedCarType());
            params.add("carMadeType", getSelectedCarMadeType());
            params.add("transmissionType", getSelectedCarTransmissionType());

            NetClient.send(mMainActivity, Configuration.URL_SETTINGS_SAVE_MY_CAR_INFO, "POST", params, new SettingsMyCarInfoAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    try {
                        mMainActivity.hideLoadingAnim();

                        if (_netAPI != null) {
                            SettingsMyCarInfoAck retNetApi = (SettingsMyCarInfoAck) _netAPI;
                            ResultData result = retNetApi.getmResultData();
                            if (result.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                                CommonDialog.showSimpleDialog(mMainActivity, result.getmDetail());
                                return;
                            }

                            closeFragment();
                        }
                    } catch (Exception e) {
                        mMainActivity.hideLoadingAnim();
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            mMainActivity.hideLoadingAnim();
            e.printStackTrace();
        }
    }

    private void closeFragment() {
        try {
            getActivity().getSupportFragmentManager().beginTransaction().remove(SettingMycarFragment.this).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package kr.moonlightdriver.user.common.network;

import android.content.Context;
import android.content.DialogInterface;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.ParseException;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetApi.TMapRouteModel;

/**
 * Created by youngmin on 2016-08-11.
 */
public class NetClient {
    private static final String TAG = "NetClient";

    private static AsyncHttpClient mAsyncHttpClient;

    public static class NetMessageContainer {
        public Context mContext;
        public String mUrl;
        public String mMethod;
        public RequestParams mRequestParams;
        public NetAPI mNetApiAck;
        public NetResponseCallback mCallback;

        public NetMessageContainer(Context _context, String _url, String _method, RequestParams _requestParams, NetAPI _netApiAck, NetResponseCallback _callback) {
            this.mContext = _context;
            this.mUrl = _url;
            this.mMethod = _method;
            this.mRequestParams = _requestParams;
            this.mNetApiAck = _netApiAck;
            this.mCallback = _callback;
        }
    }

    public static class TMapRouteNetMessageContainer {
        public Context mContext;
        public String mUrl;
        public String mMethod;
        public Header[] mHeaders;
        public RequestParams mRequestParams;
        public TMapResponseCallback mCallback;

        public TMapRouteNetMessageContainer(Context _context, String _url, String _method, Header[] _headers, RequestParams _requestParams, TMapResponseCallback _callback) {
            this.mContext = _context;
            this.mUrl = _url;
            this.mMethod = _method;
            this.mHeaders = _headers;
            this.mRequestParams = _requestParams;
            this.mCallback = _callback;
        }
    }

    public static AsyncHttpClient getAsyncHttpClient() {
        try {
            if (mAsyncHttpClient == null) {
                mAsyncHttpClient = new AsyncHttpClient();
            }

            mAsyncHttpClient.setLoggingEnabled(false);
            mAsyncHttpClient.setTimeout(60 * 1000);
            mAsyncHttpClient.setConnectTimeout(60 * 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mAsyncHttpClient;
    }

    public static void send(Context _context, String _url, String _method, RequestParams _requestParams, NetAPI _netApiAck, NetResponseCallback _callback) {
        try {
            Utils.log("error", TAG, "REQUEST URL[" + _method.toUpperCase() + "] : " + _url + (_requestParams != null ? "?" + _requestParams.toString() : ""));

            if (_method.toLowerCase().equals("get")) {
                NetClient.get(new NetMessageContainer(_context, _url, _method, _requestParams, _netApiAck, _callback));
            } else if (_method.toLowerCase().equals("post")) {
                NetClient.post(new NetMessageContainer(_context, _url, _method, _requestParams, _netApiAck, _callback));
            } else {
                _callback.onResponse(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void send(Context _context, String _url, String _method, Header[] _headers, RequestParams _requestParams, TMapResponseCallback _callback) {
        try {
            Utils.log("error", TAG, "REQUEST URL[" + _method.toUpperCase() + "] : " + _url + (_requestParams != null ? "?" + _requestParams.toString() : ""));

            if (_method.toLowerCase().equals("get")) {
                throw new RuntimeException("Not implemented..");
            } else if (_method.toLowerCase().equals("post")) {
                NetClient.post(new TMapRouteNetMessageContainer(_context, _url, _method, _headers, _requestParams, _callback));
            } else {
                _callback.onResponse(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void get(final NetMessageContainer _netMessageContainer) {
        AsyncHttpClient asyncHttpClient = getAsyncHttpClient();

        asyncHttpClient.get(_netMessageContainer.mUrl, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                NetAPI retNetApi = null;
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonParser jp = mapper.getFactory().createParser(responseBody);
                    retNetApi = mapper.readValue(jp, _netMessageContainer.mNetApiAck.getClass());
                    jp.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    _netMessageContainer.mCallback.onResponse(retNetApi);
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                Utils.log("error", TAG, "ERROR URL : " + _netMessageContainer.mUrl);
                error.printStackTrace();

                if (error instanceof java.net.SocketTimeoutException) {
                    CommonDialog.showTimeoutDialog(_netMessageContainer.mContext);
                } else {
                    CommonDialog.showDialogWithListener(_netMessageContainer.mContext, "알수없는 오류가 발생했습니다.\n다시 시도해 주십시요.", "확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            _netMessageContainer.mCallback.onResponse(null);
                        }
                    });
                }
            }
        });
    }

    public static void post(final NetMessageContainer _netMessageContainer) {
        AsyncHttpClient asyncHttpClient = getAsyncHttpClient();

        asyncHttpClient.post(_netMessageContainer.mContext, _netMessageContainer.mUrl, _netMessageContainer.mRequestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                NetAPI retNetApi = null;

                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonParser jp = mapper.getFactory().createParser(responseBody);
                    retNetApi = mapper.readValue(jp, _netMessageContainer.mNetApiAck.getClass());
                    jp.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    _netMessageContainer.mCallback.onResponse(retNetApi);
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                Utils.log("error", TAG, "ERROR URL : " + _netMessageContainer.mUrl + ", PARAMS : " + _netMessageContainer.mRequestParams.toString());

                error.printStackTrace();

                if (error instanceof java.net.SocketTimeoutException) {
                    CommonDialog.showTimeoutDialog(_netMessageContainer.mContext);
                } else {
                    CommonDialog.showDialogWithListener(_netMessageContainer.mContext, "알수없는 오류가 발생했습니다.\n다시 시도해 주십시요.", "확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            _netMessageContainer.mCallback.onResponse(null);
                        }
                    });
                }
            }
        });
    }

    public static void post(final TMapRouteNetMessageContainer _tMapNetMessageContainer) {
        AsyncHttpClient asyncHttpClient = getAsyncHttpClient();

        asyncHttpClient.post(_tMapNetMessageContainer.mContext, _tMapNetMessageContainer.mUrl, _tMapNetMessageContainer.mHeaders, _tMapNetMessageContainer.mRequestParams, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                TMapRouteModel tMapRouteModel = null;
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode jsonNode = mapper.readTree(responseBody);
                    tMapRouteModel = Utils.parseJsonToTMapRouteModel(jsonNode);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    _tMapNetMessageContainer.mCallback.onResponse(tMapRouteModel);
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                Utils.log("error", TAG, "ERROR URL : " + _tMapNetMessageContainer.mUrl + ", PARAMS : " + _tMapNetMessageContainer.mRequestParams.toString());

                error.printStackTrace();

                if (error instanceof java.net.SocketTimeoutException) {
                    CommonDialog.showTimeoutDialog(_tMapNetMessageContainer.mContext);
                } else {
                    CommonDialog.showDialogWithListener(_tMapNetMessageContainer.mContext, "알수없는 오류가 발생했습니다.\n다시 시도해 주십시요.", "확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            _tMapNetMessageContainer.mCallback.onResponse(null);
                        }
                    });
                }
            }
        });
    }

    /**
     * 실시간 자동차 경로 요청(TMap REST API 호출)
     * 총 거리, 소요시간, 출발지, 경유지, 도착지에 대한 모든 정보 제공
     */
    public static void requestTMapRoute(Context context, LatLng source, LatLng target, List<LatLng> stopbyList, final TMapRouteResultCallback callback) {
        try {
            //region set request params
            RequestParams requestParams = new RequestParams();
            requestParams.put("version", 1);
            requestParams.put("startX", source.longitude);
            requestParams.put("startY", source.latitude);
            requestParams.put("endX", target.longitude);
            requestParams.put("endY", target.latitude);
            requestParams.put("reqCoordType", "WGS84GEO");
            requestParams.put("resCoordType", "WGS84GEO");

            if (stopbyList != null) {
                int stopbyCount = stopbyList.size();
                if (stopbyCount > 0) {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < stopbyCount; i++) {
                        LatLng location = stopbyList.get(i);
                        if (location == null) {
                            continue;
                        }

                        if (sb.length() != 0)
                            sb.append('_');

                        sb.append(location.longitude);
                        sb.append(',');
                        sb.append(location.latitude);
                    }
                    requestParams.put("passList", sb.toString());
                }
            }
            //endregion set request params

            //region set headers
            List<Header> headerList = new ArrayList<>();
            headerList.add(new Header() {
                @Override
                public String getName() {
                    return "appKey";
                }

                @Override
                public String getValue() {
                    return Configuration.TMAP_API_KEY;
                }

                @Override
                public HeaderElement[] getElements() throws ParseException {
                    return new HeaderElement[0];
                }
            });

            headerList.add(new Header() {
                @Override
                public String getName() {
                    return "Accept";
                }

                @Override
                public String getValue() {
                    return "application/json";
                }

                @Override
                public HeaderElement[] getElements() throws ParseException {
                    return new HeaderElement[0];
                }
            });

            headerList.add(new Header() {
                @Override
                public String getName() {
                    return "Accept-Language";
                }

                @Override
                public String getValue() {
                    return "ko";
                }

                @Override
                public HeaderElement[] getElements() throws ParseException {
                    return new HeaderElement[0];
                }
            });
            //endregion set headers

            //region process request & response
            NetClient.send(context, "https://api2.sktelecom.com/tmap/routes", "POST", headerList.toArray(new Header[headerList.size()]), requestParams, new TMapResponseCallback(new TMapResponse() {
                @Override
                public void onResponse(TMapRouteModel tMapRouteModel) {
                    if (tMapRouteModel != null) {
                        callback.onResult(true, tMapRouteModel);
                    } else {
                        callback.onResult(false, null);
                    }
                }
            }));
            //endregion process request & response
        } catch (Exception e) {
            e.printStackTrace();
            callback.onResult(false, null);
        }
    }
}

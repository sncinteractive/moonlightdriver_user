package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.CardListViewModel;

/**
 * Created by kyo on 2017-04-13.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreditCardListAck extends NetAPI {
    @JsonProperty("creditCardList")
    private ArrayList<CardListViewModel> mCreditCardList;

    public CreditCardListAck() {
    }

    public CreditCardListAck(ArrayList<CardListViewModel> mCreditCardList) {
        this.mCreditCardList = mCreditCardList;
    }

    public CreditCardListAck(ResultData mResultData, ArrayList<CardListViewModel> mCreditCardList) {
        super(mResultData);
        this.mCreditCardList = mCreditCardList;
    }

    public ArrayList<CardListViewModel> getmCreditCardList() {
        return mCreditCardList;
    }

    public void setmCreditCardList(ArrayList<CardListViewModel> mCreditCardList) {
        this.mCreditCardList = mCreditCardList;
    }
}

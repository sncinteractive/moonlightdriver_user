package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.crackdown.CrackdownListViewModel;

/**
 * Created by youngmin on 2016-08-11.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CrackdownListAck extends NetAPI{
	@JsonProperty("crackdown_list")
	private ArrayList<CrackdownListViewModel> mCrackdownListViewModel;

	public CrackdownListAck() {}

	public CrackdownListAck(ResultData mResultData, ArrayList<CrackdownListViewModel> mCrackdownListViewModel) {
		super(mResultData);
		this.mCrackdownListViewModel = mCrackdownListViewModel;
	}

	public ArrayList<CrackdownListViewModel> getmCrackdownListViewModel() {
		return mCrackdownListViewModel;
	}

	public void setmCrackdownListViewModel(ArrayList<CrackdownListViewModel> mCrackdownListViewModel) {
		this.mCrackdownListViewModel = mCrackdownListViewModel;
	}
}

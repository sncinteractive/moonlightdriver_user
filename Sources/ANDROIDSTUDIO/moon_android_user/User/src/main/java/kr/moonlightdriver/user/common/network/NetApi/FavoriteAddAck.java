package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.FavoriteListViewModel;

/**
 * Created by kyo on 2017-04-13.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class FavoriteAddAck extends NetAPI {
    @JsonProperty("favorite")
    private FavoriteListViewModel mFavorite;

    public FavoriteAddAck() {
    }

    public FavoriteAddAck(ResultData mResultData, FavoriteListViewModel favoriteModel) {
        super(mResultData);
        mFavorite = favoriteModel;
    }

    public FavoriteListViewModel getFavorite() {
        return mFavorite;
    }

    public void setFavorite(FavoriteListViewModel favorite) {
        mFavorite = favorite;
    }
}

package kr.moonlightdriver.user.viewmodel.setting;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.viewmodel.callDriver.DriverInfoViewModel;

/**
 * Created by eklee on 2017. 4. 24..
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoryListViewModel {
    @JsonProperty("id")
    private String mId;

    @JsonProperty("date")
    private long mDate; // milliseconds

    @JsonProperty("reliefMessage")
    private String mReliefMessage;

    @JsonProperty("from")
    private String mFrom;

    @JsonProperty("to")
    private String mTo;

    @JsonProperty("companyName")
    private String mCompanyName;

    @JsonProperty("price")
    private int mPrice;

    @JsonProperty("evaluated")
    private boolean mEvaluated;

    @JsonProperty("unfairnessReported")
    private boolean mReportedUnfairness;

    @JsonProperty("driverInfo")
    private DriverInfoViewModel mDriverInfo;

    public HistoryListViewModel() {
    }

    public HistoryListViewModel(String id, long date, String reliefMessage, String from, String to, String companyName, int price, boolean evaluated, boolean reportedUnfairness, DriverInfoViewModel driverInfo) {
        mId = id;
        mDate = date;
        mReliefMessage = reliefMessage;
        mFrom = from;
        mTo = to;
        mCompanyName = companyName;
        mPrice = price;
        mEvaluated = evaluated;
        mReportedUnfairness = reportedUnfairness;
        mDriverInfo = driverInfo;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public long getDate() {
        return mDate;
    }

    public void setDate(long date) {
        mDate = date;
    }

    public String getReliefMessage() {
        return mReliefMessage;
    }

    public void setReliefMessage(String reliefMessage) {
        mReliefMessage = reliefMessage;
    }

    public String getFrom() {
        return mFrom;
    }

    public void setFrom(String from) {
        mFrom = from;
    }

    public String getTo() {
        return mTo;
    }

    public void setTo(String to) {
        mTo = to;
    }

    public String getCompanyName() {
        return mCompanyName;
    }

    public void setCompanyName(String companyName) {
        mCompanyName = companyName;
    }

    public int getPrice() {
        return mPrice;
    }

    public void setPrice(int price) {
        mPrice = price;
    }

    public boolean isEvaluated() {
        return mEvaluated;
    }

    public void setEvaluated(boolean evaluated) {
        mEvaluated = evaluated;
    }

    public DriverInfoViewModel getDriverInfo() {
        return mDriverInfo;
    }

    public boolean isReportedUnfairness() {
        return mReportedUnfairness;
    }

    public void setReportedUnfairness(boolean reportedUnfairness) {
        mReportedUnfairness = reportedUnfairness;
    }

    public void setDriverInfo(DriverInfoViewModel driverInfo) {
        mDriverInfo = driverInfo;
    }
}

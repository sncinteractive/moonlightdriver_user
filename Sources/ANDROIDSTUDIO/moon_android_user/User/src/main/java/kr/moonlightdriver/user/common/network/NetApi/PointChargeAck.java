package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.common.UserAdditionalViewModel;

/**
 * Created by kyo on 2017-04-10.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class PointChargeAck extends NetAPI {
    @JsonProperty("userAdditional")
    private UserAdditionalViewModel mUserAdditionalData;

    public PointChargeAck() {}

    public PointChargeAck(UserAdditionalViewModel mUserAdditionalData) {
        this.mUserAdditionalData = mUserAdditionalData;
    }

    public PointChargeAck(ResultData mResultData, UserAdditionalViewModel mUserAdditionalData) {
        super(mResultData);
        this.mUserAdditionalData = mUserAdditionalData;
    }

    public UserAdditionalViewModel getmUserAdditionalData() {
        return mUserAdditionalData;
    }

    public void setmUserAdditionalData(UserAdditionalViewModel mUserAdditionalData) {
        this.mUserAdditionalData = mUserAdditionalData;
    }
}

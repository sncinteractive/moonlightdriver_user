package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.CouponListViewModel;

/**
 * Created by kyo on 2017-04-13.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class CouponDetailAck extends NetAPI {
    @JsonProperty("couponDetail")
    private CouponListViewModel mCouponDetail;

    public CouponDetailAck() {
    }

    public CouponDetailAck(ResultData mResultData, CouponListViewModel couponModelDetail) {
        super(mResultData);
        mCouponDetail = couponModelDetail;
    }

    public CouponListViewModel getCouponDetail() {
        return mCouponDetail;
    }

    public void setCouponDetail(CouponListViewModel couponModelDetail) {
        mCouponDetail = couponModelDetail;
    }
}

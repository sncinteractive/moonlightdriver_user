package kr.moonlightdriver.user.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.viewholder.CouponListViewHolder;
import kr.moonlightdriver.user.viewmodel.setting.CouponListViewModel;

/**
 * Created by kyo on 2017-04-26.
 */

public class CouponListAdapter extends ArrayAdapter<CouponListViewModel>  {
    public interface InteractionListener {
        void OnCouponItemClick(int position);
    }

    private int mResourceId;
    private List<CouponListViewModel> mDataList;
    private CouponListViewHolder mViewHolder;
    private InteractionListener mInteractionListener;

    public CouponListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<CouponListViewModel> objects, InteractionListener interactionListener) {
        super(context, resource, objects);

        mResourceId = resource;
        mDataList = objects;
        mInteractionListener = interactionListener;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if( convertView == null ) {
            LayoutInflater layoutInflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(mResourceId, null);

            mViewHolder = new CouponListViewHolder();
            mViewHolder.couponListItemLayout = (LinearLayout) convertView.findViewById(R.id.ll_coupon_list_item);
            mViewHolder.couponNameTextView = (TextView)convertView.findViewById(R.id.tv_coupon_name);
            mViewHolder.couponDescTextView= (TextView)convertView.findViewById(R.id.tv_coupon_desc);

            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), mViewHolder.couponNameTextView, mViewHolder.couponDescTextView);

            convertView.setTag(mViewHolder);

        } else {
            mViewHolder = (CouponListViewHolder) convertView.getTag();
        }

        CouponListViewModel data = mDataList.get(position);
        mViewHolder.couponNameTextView.setText( data.getmCouponName());
        mViewHolder.couponDescTextView.setText(data.getmCouponDesc());

        mViewHolder.couponListItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mInteractionListener != null) {
                    mInteractionListener.OnCouponItemClick(position);
                }
            }
        });

        return convertView;
    }
}
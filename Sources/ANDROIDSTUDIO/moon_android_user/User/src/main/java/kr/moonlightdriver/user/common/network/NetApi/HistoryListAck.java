package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.callDriver.CallInfoViewModel;
import kr.moonlightdriver.user.viewmodel.common.ResultData;

/**
 * Created by kyo on 2017-04-10.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoryListAck extends NetAPI {
    @JsonProperty("callHistoryList")
    private List<CallInfoViewModel> mHistoryList;

    public HistoryListAck() {
    }

    public HistoryListAck(List<CallInfoViewModel> mHistoryList) {
        this.mHistoryList = mHistoryList;
    }

    public HistoryListAck(ResultData mResultData, List<CallInfoViewModel> mHistoryList) {
        super(mResultData);
        this.mHistoryList = mHistoryList;
    }

    public List<CallInfoViewModel> getmHistoryList() {
        return mHistoryList;
    }

    public void setmHistoryList(List<CallInfoViewModel> mHistoryList) {
        this.mHistoryList = mHistoryList;
    }
}

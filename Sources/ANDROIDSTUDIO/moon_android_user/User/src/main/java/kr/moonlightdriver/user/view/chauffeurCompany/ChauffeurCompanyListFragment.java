package kr.moonlightdriver.user.view.chauffeurCompany;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;
import com.skt.Tmap.TMapAddressInfo;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.BusProvider;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.ResultCallback;
import kr.moonlightdriver.user.common.TmapManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.ChauffeurCompanyDetailAck;
import kr.moonlightdriver.user.common.network.NetApi.ChauffeurCompanyListAck;
import kr.moonlightdriver.user.common.network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewholder.ChauffeurCompanyListViewHolder;
import kr.moonlightdriver.user.viewmodel.common.ActivityResultEvent;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.crackdown.ChauffeurCompanyViewModel;

public class ChauffeurCompanyListFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "ChauffeurCompanyListFragment";

    private MainActivity mMainActivity;

    private LinearLayout mPopupCompanyDetailLayout;
    private TextView mRegionsSelector;
    private ImageButton mBtnRegionDropdown;
    private ImageButton mBtnCompanyDetailClose;
    private TextView mCompanyName;
    private TextView mCompanyDetailRegionTitle;
    private TextView mCompanyDetailRegionText;
    private TextView mCompanyDetailPriceTitle;
    private TextView mCompanyDetailPriceText;
    private TextView mCompanyDetailStopbyTitle;
    private TextView mCompanyDetailStopbyText;
    private ListView mChauffeurCompanyListView;
    private ImageView mChauffeurCompanyIsBookmark;
    private ImageButton mBtnSecretCall;

    private ArrayList<ChauffeurCompanyViewModel> mChauffeurCompanyList;
    private ArrayList<ChauffeurCompanyViewModel> mChauffeurCompanySubList;
    private int mSelectedRegion;
    private ArrayList<String> mBookmarkList;
    private ChauffeurCompanyListAdapter mChauffeurCompanyListAdapter;

    private String mVirtualNumber;
    private PushAlertReceiver mPushAlertReceiver;

    private View mView;

    private boolean mIsSecretCalling;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            mView = inflater.inflate(R.layout.fragment_chauffeur_company_list, container, false);
            mMainActivity = (MainActivity) getActivity();
//			mMainActivity.setTabMenuVisibility(View.GONE);

            mChauffeurCompanyList = new ArrayList<>();
            mChauffeurCompanySubList = new ArrayList<>();
            mBookmarkList = new ArrayList<>();
            mSelectedRegion = 0;
            mIsSecretCalling = false;

            mRegionsSelector = (TextView) mView.findViewById(R.id.region_selector);
            TextView regionTitle = (TextView) mView.findViewById(R.id.region_title);
            TextView phoneListHeader_1 = (TextView) mView.findViewById(R.id.phone_list_header_1);
            TextView phoneListHeader_2 = (TextView) mView.findViewById(R.id.phone_list_header_2);
            TextView phoneListHeader_3 = (TextView) mView.findViewById(R.id.phone_list_header_3);
            mPopupCompanyDetailLayout = (LinearLayout) mView.findViewById(R.id.popup_company_detail_layout);
            mPopupCompanyDetailLayout.setVisibility(View.GONE);
            mBtnRegionDropdown = (ImageButton) mView.findViewById(R.id.btn_region_dropdown);
            mCompanyName = (TextView) mView.findViewById(R.id.company_name);
            mBtnCompanyDetailClose = (ImageButton) mView.findViewById(R.id.btn_company_detail_close);
            mChauffeurCompanyListView = (ListView) mView.findViewById(R.id.chauffeur_company_list);
            mChauffeurCompanyIsBookmark = (ImageView) mView.findViewById(R.id.chauffeur_company_is_bookmark);

            mCompanyDetailRegionTitle = (TextView) mView.findViewById(R.id.company_detail_region_title);
            mCompanyDetailRegionText = (TextView) mView.findViewById(R.id.company_detail_region_text);
            mCompanyDetailPriceTitle = (TextView) mView.findViewById(R.id.company_detail_price_title);
            mCompanyDetailPriceText = (TextView) mView.findViewById(R.id.company_detail_price_text);
            mCompanyDetailStopbyTitle = (TextView) mView.findViewById(R.id.company_detail_stopby_title);
            mCompanyDetailStopbyText = (TextView) mView.findViewById(R.id.company_detail_stopby_text);
            mBtnSecretCall = (ImageButton) mView.findViewById(R.id.btn_secret_call);

            mRegionsSelector.setOnClickListener(this);
            mBtnRegionDropdown.setOnClickListener(this);
            mBtnCompanyDetailClose.setOnClickListener(this);

            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanB(), regionTitle, mRegionsSelector);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanVert(), mCompanyName);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), mCompanyDetailRegionTitle, mCompanyDetailRegionText, mCompanyDetailStopbyTitle, mCompanyDetailStopbyText, mCompanyDetailPriceTitle, mCompanyDetailPriceText);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangB(), phoneListHeader_1, phoneListHeader_2, phoneListHeader_3);

            mChauffeurCompanyListAdapter = new ChauffeurCompanyListAdapter(mMainActivity, R.layout.row_chauffeur_company_list, mChauffeurCompanySubList);
            mChauffeurCompanyListView.setAdapter(mChauffeurCompanyListAdapter);
            mChauffeurCompanyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> _adapterView, View _view, int _position, long l) {
                    try {
                        Utils.log("error", TAG, "click position : " + _position);
                        if (mChauffeurCompanySubList.get(_position) != null) {
                            getChauffeurCompanyDetail(_position);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            getChauffeurCompanyList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mView;
    }

    private void getChauffeurCompanyDetail(final int _chauffeurCompanyIndex) {
        try {
            ChauffeurCompanyViewModel chauffeurCompanyData = mChauffeurCompanySubList.get(_chauffeurCompanyIndex);
            if (chauffeurCompanyData == null) {
                return;
            }

            mMainActivity.showLoadingAnim();

            RequestParams params = new RequestParams();
            params.add("userId", mMainActivity.mUserDetail.getUserId());
            params.add("chauffeurCompanyId", chauffeurCompanyData.getmChauffeurCompanyId());

            NetClient.send(mMainActivity, Configuration.URL_CHAUFFEUR_COMPANY_DETAIL, "POST", params, new ChauffeurCompanyDetailAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    try {
                        mMainActivity.hideLoadingAnim();

                        if (_netAPI != null) {
                            ChauffeurCompanyDetailAck retNetApi = (ChauffeurCompanyDetailAck) _netAPI;
                            ResultData result = retNetApi.getmResultData();
                            if (result.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                                CommonDialog.showSimpleDialog(mMainActivity, result.getmDetail());
                                return;
                            }

                            showChauffeurCompanyDetailPopup(_chauffeurCompanyIndex);
                        }
                    } catch (Exception e) {
                        mMainActivity.hideLoadingAnim();
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            mMainActivity.hideLoadingAnim();
            e.printStackTrace();
        }
    }

    private void showChauffeurCompanyDetailPopup(final int _chauffeurCompanyIndex) {
        try {
            ChauffeurCompanyViewModel chauffeurCompanyData = mChauffeurCompanySubList.get(_chauffeurCompanyIndex);
            if (chauffeurCompanyData == null) {
                return;
            }

            if (chauffeurCompanyData.ismIsBookmark()) {
                mChauffeurCompanyIsBookmark.setVisibility(View.VISIBLE);
            } else {
                mChauffeurCompanyIsBookmark.setVisibility(View.GONE);
            }

            mCompanyName.setText(chauffeurCompanyData.getmCompanyName());
            mCompanyDetailRegionText.setText(chauffeurCompanyData.getmRegion());
            mCompanyDetailPriceText.setText(Utils.convertToPriceString(chauffeurCompanyData.getmPrice()));
            mCompanyDetailStopbyText.setText(Utils.convertToPriceString(chauffeurCompanyData.getmStopByFee()));
            mBtnSecretCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    registerVirtualNumber(_chauffeurCompanyIndex);
                }
            });

            mPopupCompanyDetailLayout.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getChauffeurCompanyList() {
        try {
            mChauffeurCompanyList.clear();

            mMainActivity.showLoadingAnim();

            RequestParams params = new RequestParams();
            params.add("userId", mMainActivity.mUserDetail.getUserId());

            NetClient.send(mMainActivity, Configuration.URL_CHAUFFEUR_COMPANY_LIST, "POST", params, new ChauffeurCompanyListAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    try {
                        mMainActivity.hideLoadingAnim();

                        if (_netAPI != null) {
                            ChauffeurCompanyListAck retNetApi = (ChauffeurCompanyListAck) _netAPI;
                            ResultData result = retNetApi.getmResultData();
                            if (result.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                                CommonDialog.showSimpleDialog(mMainActivity, result.getmDetail());
                                return;
                            }

                            mBookmarkList = retNetApi.getmChauffeurCompanyBookmarkList();

                            ArrayList<ChauffeurCompanyViewModel> retData = retNetApi.getmChauffeurCompanyViewModel();
                            for (ChauffeurCompanyViewModel item : retData) {
                                item.setmIsBookmark(false);
                                if (mBookmarkList.contains(item.getmChauffeurCompanyId())) {
                                    item.setmIsBookmark(true);
                                }

                                mChauffeurCompanyList.add(item);
                            }

                            getAddress();
                        }
                    } catch (Exception e) {
                        mMainActivity.hideLoadingAnim();
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            mMainActivity.hideLoadingAnim();
            e.printStackTrace();
        }
    }

    private void drawChauffeurCompanyList() {
        try {
            mRegionsSelector.setText(Configuration.REGION_LIST[mSelectedRegion]);

            mChauffeurCompanySubList.clear();

            for (ChauffeurCompanyViewModel data : mChauffeurCompanyList) {
                if (data.getmRegion().equals(Configuration.REGION_LIST[mSelectedRegion])) {
                    mChauffeurCompanySubList.add(data);
                }
            }

            Collections.sort(mChauffeurCompanySubList, bookmarkSort);

            mChauffeurCompanyListAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateBookmark(final int _chauffeurCompanyIndex, final String _bookmarkType) {
        try {
            final ChauffeurCompanyViewModel chauffeurCompanyData = mChauffeurCompanySubList.get(_chauffeurCompanyIndex);
            if (chauffeurCompanyData == null) {
                return;
            }

            mMainActivity.showLoadingAnim();

            RequestParams params = new RequestParams();
            params.add("chauffeurCompanyId", chauffeurCompanyData.getmChauffeurCompanyId());
            params.add("userId", mMainActivity.mUserDetail.getUserId());
            params.add("bookmarkType", _bookmarkType);

            NetClient.send(mMainActivity, Configuration.URL_CHAUFFEUR_COMPANY_UPDATE_BOOKMARK, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    try {
                        mMainActivity.hideLoadingAnim();

                        if (_netAPI != null) {
                            NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
                            ResultData result = retNetApi.getmResultData();
                            if (result.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                                if (_bookmarkType.equals("register")) {
                                    chauffeurCompanyData.setmIsBookmark(true);
                                    mBookmarkList.add(chauffeurCompanyData.getmChauffeurCompanyId());
                                } else {
                                    chauffeurCompanyData.setmIsBookmark(false);
                                    mBookmarkList.remove(chauffeurCompanyData.getmChauffeurCompanyId());
                                }

                                mChauffeurCompanySubList.set(_chauffeurCompanyIndex, chauffeurCompanyData);

                                updateChauffeurCompanyListData(chauffeurCompanyData);

                                drawChauffeurCompanyList();
                            } else {
                                CommonDialog.showSimpleDialog(mMainActivity, result.getmDetail());
                            }
                        }
                    } catch (Exception e) {
                        mMainActivity.hideLoadingAnim();
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            mMainActivity.hideLoadingAnim();
            e.printStackTrace();
        }
    }

    private void registerVirtualNumber(final int _chauffeurCompanyIndex) {
        try {
            final ChauffeurCompanyViewModel chauffeurCompanyData = mChauffeurCompanySubList.get(_chauffeurCompanyIndex);
            if (chauffeurCompanyData == null) {
                return;
            }

            mMainActivity.showLoadingAnim();

            RequestParams params = new RequestParams();
            params.add("recvId", chauffeurCompanyData.getmChauffeurCompanyId());
            params.add("sendId", mMainActivity.mUserDetail.getUserId());
            params.add("type", "company");
            params.add("lat", String.valueOf(MainActivity.mCurrentLocation.latitude));
            params.add("lng", String.valueOf(MainActivity.mCurrentLocation.longitude));

            NetClient.send(mMainActivity, Configuration.URL_VIRTUAL_NUMBER_REGISTER, "POST", params, new ChauffeurCompanyDetailAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    try {
                        mMainActivity.hideLoadingAnim();

                        if (_netAPI != null) {
                            ChauffeurCompanyDetailAck retNetApi = (ChauffeurCompanyDetailAck) _netAPI;
                            ResultData result = retNetApi.getmResultData();
                            if (result.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                                CommonDialog.showSimpleDialog(mMainActivity, result.getmDetail());
                            }
                        }
                    } catch (Exception e) {
                        mMainActivity.hideLoadingAnim();
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            mMainActivity.hideLoadingAnim();
            e.printStackTrace();
        }
    }

    private void unregisterVirtualNumber() {
        try {
            RequestParams params = new RequestParams();
            params.add("type", "company");
            params.add("virtual_number", mVirtualNumber);
            params.add("sendId", mMainActivity.mUserDetail.getUserId());

            NetClient.send(mMainActivity, Configuration.URL_VIRTUAL_NUMBER_UNREGISTER, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    if (_netAPI != null) {
                        mIsSecretCalling = false;
                        mVirtualNumber = "";

                        NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
                        ResultData result = retNetApi.getmResultData();
                        if (result.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                            CommonDialog.showSimpleDialog(mMainActivity, result.getmDetail());
                        }
                    }
                }
            }));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateChauffeurCompanyListData(ChauffeurCompanyViewModel _chauffeurCompanyData) {
        try {
            int index = 0;
            for (ChauffeurCompanyViewModel data : mChauffeurCompanyList) {
                if (data.getmChauffeurCompanyId().equals(_chauffeurCompanyData.getmChauffeurCompanyId())) {
                    mChauffeurCompanyList.set(index, _chauffeurCompanyData);
                    break;
                }

                index++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showRegionList() {
        try {
            CommonDialog.showSingleChoiceItemDialog(mMainActivity, "지역 선택", Configuration.REGION_LIST, mSelectedRegion, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    mSelectedRegion = i;
                    drawChauffeurCompanyList();
                    dialogInterface.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class ChauffeurCompanyListAdapter extends ArrayAdapter<ChauffeurCompanyViewModel> {
        private ArrayList<ChauffeurCompanyViewModel> mChauffeurCompanyListData;
        private int mResourceId;
        private ChauffeurCompanyListViewHolder mViewHolder;

        public ChauffeurCompanyListAdapter(Context _context, int _resourceId, ArrayList<ChauffeurCompanyViewModel> _listData) {
            super(_context, _resourceId, _listData);

            this.mChauffeurCompanyListData = _listData;
            this.mResourceId = _resourceId;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;

            try {
                if (v == null) {
                    LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    v = li.inflate(mResourceId, null);

                    mViewHolder = new ChauffeurCompanyListViewHolder();
                    mViewHolder.mBtnChauffeurCompanyBookmarkLayout = (LinearLayout) v.findViewById(R.id.row_btn_chauffeur_company_bookmark_layout);
                    mViewHolder.mBtnChauffeurCompanyBookmark = (ImageView) v.findViewById(R.id.row_btn_chauffeur_company_bookmark);
                    mViewHolder.mChauffeurCompanyName = (TextView) v.findViewById(R.id.row_chauffeur_company_name);
                    mViewHolder.mChauffeurCompanyUseCount = (TextView) v.findViewById(R.id.row_chauffeur_company_use_count);
                    mViewHolder.mChauffeurCompanyDescription = (TextView) v.findViewById(R.id.row_chauffeur_company_description);
                    mViewHolder.mBtnChauffeurCompanySecretCallLayout = (LinearLayout) v.findViewById(R.id.row_btn_chauffeur_company_secret_call_layout);
                    mViewHolder.mBtnChauffeurCompanySecretCall = (ImageView) v.findViewById(R.id.row_btn_chauffeur_company_secret_call);

                    Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanM());
                    Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanB(), mViewHolder.mChauffeurCompanyName);
                    Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanM(), mViewHolder.mChauffeurCompanyUseCount);
                    Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanM(), mViewHolder.mChauffeurCompanyDescription);
                    Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM());

                    v.setTag(mViewHolder);
                } else {
                    mViewHolder = (ChauffeurCompanyListViewHolder) v.getTag();
                }

                ChauffeurCompanyViewModel chauffeurCompanyData = mChauffeurCompanyListData.get(position);

                if (mBookmarkList.contains(chauffeurCompanyData.getmChauffeurCompanyId())) {
                    mViewHolder.mBtnChauffeurCompanyBookmark.setSelected(true);
                    mViewHolder.mBtnChauffeurCompanyBookmarkLayout.setSelected(true);
                } else {
                    mViewHolder.mBtnChauffeurCompanyBookmark.setSelected(false);
                    mViewHolder.mBtnChauffeurCompanyBookmarkLayout.setSelected(false);
                }

                if (chauffeurCompanyData.getmUseCount() > 0) {
                    mViewHolder.mChauffeurCompanyUseCount.setVisibility(View.VISIBLE);
                    mViewHolder.mChauffeurCompanyUseCount.setText(Integer.toString(chauffeurCompanyData.getmUseCount()));
                } else {
                    mViewHolder.mChauffeurCompanyUseCount.setVisibility(View.GONE);
                }

                mViewHolder.mChauffeurCompanyName.setText(chauffeurCompanyData.getmCompanyName());

                if (Utils.isStringNullOrEmpty(chauffeurCompanyData.getmDescription())) {
                    mViewHolder.mChauffeurCompanyDescription.setVisibility(View.GONE);
                } else {
                    mViewHolder.mChauffeurCompanyDescription.setVisibility(View.VISIBLE);
                    mViewHolder.mChauffeurCompanyDescription.setText(chauffeurCompanyData.getmDescription());
                }

                mViewHolder.mBtnChauffeurCompanySecretCallLayout.setTag(position);
                mViewHolder.mBtnChauffeurCompanySecretCallLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int chauffeurCompanyIndex = (int) view.getTag();

                        registerVirtualNumber(chauffeurCompanyIndex);
                    }
                });

                mViewHolder.mBtnChauffeurCompanyBookmark.setTag(position);
                mViewHolder.mBtnChauffeurCompanyBookmark.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int chauffeurCompanyIndex = (int) view.getTag();

                        if (view.isSelected()) {
                            updateBookmark(chauffeurCompanyIndex, "unregister");
                        } else {
                            updateBookmark(chauffeurCompanyIndex, "register");
                        }
                    }
                });

                mViewHolder.mBtnChauffeurCompanyBookmarkLayout.setTag(position);
                mViewHolder.mBtnChauffeurCompanyBookmarkLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int chauffeurCompanyIndex = (int) view.getTag();

                        if (view.isSelected()) {
                            updateBookmark(chauffeurCompanyIndex, "unregister");
                        } else {
                            updateBookmark(chauffeurCompanyIndex, "register");
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

            return v;
        }
    }

    public void makeSecretPhoneCall() {
        try {
            if (mMainActivity.checkPhoneCallPermission(Configuration.SECRET_CALL_PHONE_REQ_CODE)) {
                Configuration.UPDATE_DISTANCE = 100;

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Configuration.UPDATE_DISTANCE = 1000;
                    }
                }, Configuration.RESET_UPDATE_DISTANCE_INTERVAL);

                String uri = "tel:" + mVirtualNumber;

                mIsSecretCalling = true;

                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse(uri));

                startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAddress() {
        try {
            TmapManager tmapManager = new TmapManager();
            tmapManager.convertGpsToAddress(MainActivity.mCurrentLocation, new ResultCallback() {
                @Override
                public void resultCallback(HashMap<String, Object> params) {
                    try {
                        TMapAddressInfo addressInfo = (TMapAddressInfo) params.get("addressInfo");
                        if (addressInfo != null) {
                            String city_do = Configuration.REGION.optString(addressInfo.strCity_do, "");
                            int indexOfCity = Arrays.asList(Configuration.REGION_LIST).indexOf(city_do);

                            Utils.log("error", TAG, "current address : " + addressInfo.strCity_do + ", city_do : " + city_do + ", indexOfCity : " + indexOfCity);
                            if (indexOfCity >= 0) {
                                mSelectedRegion = indexOfCity;
                            }
                        }

                        drawChauffeurCompanyList();
                    } catch (Exception e) {
                        mMainActivity.hideLoadingAnim();
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            mMainActivity.hideLoadingAnim();
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == Configuration.INCOMING_CALL) {
                    if (data != null) {
                        String call_state = data.getStringExtra("call_state");

                        Utils.log("error", TAG, "call_state : " + call_state);

                        if (mIsSecretCalling && call_state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                            unregisterVirtualNumber();
                        }
                    }
                } else if (requestCode == Configuration.SECRET_CALL_PHONE_REQ_CODE) {
                    makeSecretPhoneCall();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onActivityResult(ActivityResultEvent _activityResultEvent) {
        try {
            onActivityResult(_activityResultEvent.getRequestCode(), _activityResultEvent.getResultCode(), _activityResultEvent.getData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class PushAlertReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                String action = intent.getAction();

                switch (action) {
                    case Configuration.PUSH_INTENT_ACTION_VIRTUAL_NUMBER_RECEIVED:
                        mMainActivity.hideLoadingAnim();

                        mVirtualNumber = intent.getStringExtra("virtualNumber");

                        if (!Utils.isStringNullOrEmpty(mVirtualNumber)) {
                            makeSecretPhoneCall();
                        }

                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private final Comparator<ChauffeurCompanyViewModel> bookmarkSort = new Comparator<ChauffeurCompanyViewModel>() {
        @Override
        public int compare(ChauffeurCompanyViewModel box, ChauffeurCompanyViewModel box2) {
            int comp1 = box.ismIsBookmark() ? 0 : 1;
            int comp2 = box2.ismIsBookmark() ? 0 : 1;
            int comp = comp1 - comp2;

            if (comp == 0) {
                return box2.getmUseCount() - box.getmUseCount();
            }

            return comp;
        }
    };

    @Override
    public void onClick(View view) {
        try {
            if (view == mBtnRegionDropdown || view == mRegionsSelector) {
                showRegionList();
            } else if (view == mBtnCompanyDetailClose) {
                mPopupCompanyDetailLayout.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        try {
            mPushAlertReceiver = new PushAlertReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Configuration.PUSH_INTENT_ACTION_VIRTUAL_NUMBER_RECEIVED);

            getActivity().registerReceiver(mPushAlertReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        try {
            getActivity().unregisterReceiver(mPushAlertReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        BusProvider.getInstance().unregister(this);
        super.onDestroyView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BusProvider.getInstance().register(this);
    }
}

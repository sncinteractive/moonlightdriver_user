package kr.moonlightdriver.user.viewmodel.callDriver;

import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.viewmodel.common.LocationData;

/**
 * Created by kyo on 2017-05-08.
 */

public class SearchLocationListViewModel {
    private Enums.SearchLocationListItemType mSearchLocationListItemType;
    private String mName;
    private LocationData mLocationData;

    public SearchLocationListViewModel() {
    }

    public SearchLocationListViewModel(Enums.SearchLocationListItemType type, String name, LocationData locationData) {
        mSearchLocationListItemType = type;
        mName = name;
        mLocationData = locationData;
    }

    public Enums.SearchLocationListItemType getSearchLocationListItemType() {
        return mSearchLocationListItemType;
    }

    public void setSearchLocationListItemType(Enums.SearchLocationListItemType searchLocationListItemType) {
        mSearchLocationListItemType = searchLocationListItemType;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public LocationData getLocationData() {
        return mLocationData;
    }

    public void setLocationData(LocationData locationData) {
        mLocationData = locationData;
    }
}

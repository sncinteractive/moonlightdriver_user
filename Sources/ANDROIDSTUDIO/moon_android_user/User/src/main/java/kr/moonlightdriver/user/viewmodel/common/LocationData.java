package kr.moonlightdriver.user.viewmodel.common;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.viewmodel.callDriver.CallInfoViewModel;

/**
 * Created by youngmin on 2016-06-21.
 */
public class LocationData implements Parcelable {
    private LatLng mLocation;
    private String mLocationName;
    private String mAddress;

    public LocationData() {

    }

    public LocationData(CallInfoViewModel.CallInfoPositionViewModel callInfoPositionViewModel) {
        double lat = 0;
        double lng = 0;

        try {
            lat = callInfoPositionViewModel.getmLocation().getmCoordinates().get(1);
            lng = callInfoPositionViewModel.getmLocation().getmCoordinates().get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.mLocation = new LatLng(lat, lng);
        this.mLocationName = callInfoPositionViewModel.getmAddress();
        this.mAddress = callInfoPositionViewModel.getmAddress();
    }

    public LocationData(LatLng mLocation, String mLocationName, String mAddress) {
        this.mLocation = mLocation;
        this.mLocationName = mLocationName;
        this.mAddress = mAddress;
    }

    protected LocationData(Parcel in) {
        mLocation = in.readParcelable(LatLng.class.getClassLoader());
        mLocationName = in.readString();
        mAddress = in.readString();
    }

    public LocationData(String value) {
        if (!Utils.isStringNullOrEmpty(value)) {
            String[] values = value.split(",");
            if (values.length == 4) {
                mLocationName = values[0];
                mAddress = values[1];
                mLocation = new LatLng(Double.parseDouble(values[2]), Double.parseDouble(values[3]));
            }
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mLocation, flags);
        dest.writeString(mLocationName);
        dest.writeString(mAddress);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LocationData> CREATOR = new Creator<LocationData>() {
        @Override
        public LocationData createFromParcel(Parcel in) {
            return new LocationData(in);
        }

        @Override
        public LocationData[] newArray(int size) {
            return new LocationData[size];
        }
    };

    @Override
    public String toString() {
        return mLocationName + "," + mAddress + "," + mLocation.latitude + "," + mLocation.longitude;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public LatLng getmLocation() {
        return mLocation;
    }

    public void setmLocation(LatLng mLocation) {
        this.mLocation = mLocation;
    }

    public String getmLocationName() {
        if (!Utils.isStringNullOrEmpty(mLocationName)) {
            return mLocationName;
        } else if (!Utils.isStringNullOrEmpty(mAddress)) {
            return mAddress;
        } else {
            return "";
        }
    }

    public void setmLocationName(String mLocationName) {
        this.mLocationName = mLocationName;
    }

    public void set(LatLng location, String locationName, String address) {
        mLocation = location;
        mLocationName = locationName;
        mAddress = address;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof LocationData) {
            LocationData other = (LocationData) obj;

            boolean isEqual = (this == other);
            String otherLocationName = other.getmLocationName();
            if ((mLocationName != null && otherLocationName == null) || (mLocationName == null && otherLocationName != null)) {
                isEqual = false;
            } else if (mLocationName != null && !mLocationName.equals(otherLocationName)) {
                isEqual = false;
            }

            if (isEqual) {
                String otherAddress = other.getmAddress();
                if ((mAddress != null && otherAddress == null) || (mAddress == null && otherAddress != null)) {
                    isEqual = false;
                } else if (mAddress != null && !mAddress.equals(otherAddress)) {
                    isEqual = false;
                }
            }

            if (isEqual) {
                LatLng otherLocation = other.getmLocation();
                if ((mLocation != null && otherLocation == null) || (mLocation == null && otherLocation != null)) {
                    isEqual = false;
                } else if (mLocation != null && !mLocation.equals(otherLocation)) {
                    isEqual = false;
                }
            }

            return isEqual;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return mLocationName.hashCode() + mAddress.hashCode() + mLocation.hashCode();
    }
}

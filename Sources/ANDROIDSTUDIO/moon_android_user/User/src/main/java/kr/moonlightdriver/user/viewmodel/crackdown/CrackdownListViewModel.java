package kr.moonlightdriver.user.viewmodel.crackdown;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.user.viewmodel.common.CoordinatesData;

/**
 * Created by youngmin on 2016-08-11.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CrackdownListViewModel {
	@JsonProperty("crackdownId")
	private String mCrackdownId;

	@JsonProperty("userId")
	private String mUserId;

	@JsonProperty("registrant")
	private String mRegistrant;

	@JsonProperty("sigugun")
	private String mSigugun;

	@JsonProperty("sido")
	private String mSido;

	@JsonProperty("category")
	private String mCategory;

	@JsonProperty("dislikeCount")
	private int mDislikeCount;

	@JsonProperty("likeCount")
	private int mLikeCount;

	@JsonProperty("address")
	private String mAddress;

	@JsonProperty("from")
	private String mFrom;

	@JsonProperty("reportType")
	private String mReportType;

	@JsonProperty("picture")
	private String mPicture;

	@JsonProperty("locations")
	private CoordinatesData mLocation;

	@JsonProperty("comments")
	private ArrayList<CrackdownCommentListViewModel> mCommentList;

	@JsonProperty("commentCount")
	private int mCommentCount;

	@JsonProperty("createTime")
	private long mCreateTime;

	@JsonProperty("updatedTime")
	private long mUpdatedTime;

	@JsonProperty("isUserLike")
	private boolean mIsUserLike;

	@JsonProperty("isUserDislike")
	private boolean mIsUserDislike;

	public CrackdownListViewModel() {}

	public CrackdownListViewModel(String mAddress, String mCategory, int mCommentCount, ArrayList<CrackdownCommentListViewModel> mCommentList, String mCrackdownId, long mCreateTime, int mDislikeCount, String mFrom, boolean mIsUserDislike, boolean mIsUserLike, int mLikeCount, CoordinatesData mLocation, String mPicture, String mRegistrant, String mReportType, String mSido, String mSigugun, long mUpdatedTime, String mUserId) {
		this.mAddress = mAddress;
		this.mCategory = mCategory;
		this.mCommentCount = mCommentCount;
		this.mCommentList = mCommentList;
		this.mCrackdownId = mCrackdownId;
		this.mCreateTime = mCreateTime;
		this.mDislikeCount = mDislikeCount;
		this.mFrom = mFrom;
		this.mIsUserDislike = mIsUserDislike;
		this.mIsUserLike = mIsUserLike;
		this.mLikeCount = mLikeCount;
		this.mLocation = mLocation;
		this.mPicture = mPicture;
		this.mRegistrant = mRegistrant;
		this.mReportType = mReportType;
		this.mSido = mSido;
		this.mSigugun = mSigugun;
		this.mUpdatedTime = mUpdatedTime;
		this.mUserId = mUserId;
	}

	public String getmAddress() {
		return mAddress;
	}

	public void setmAddress(String mAddress) {
		this.mAddress = mAddress;
	}

	public String getmCategory() {
		return mCategory;
	}

	public void setmCategory(String mCategory) {
		this.mCategory = mCategory;
	}

	public String getmCrackdownId() {
		return mCrackdownId;
	}

	public void setmCrackdownId(String mCrackdownId) {
		this.mCrackdownId = mCrackdownId;
	}

	public long getmCreateTime() {
		return mCreateTime;
	}

	public void setmCreateTime(long mCreateTime) {
		this.mCreateTime = mCreateTime;
	}

	public int getmDislikeCount() {
		return mDislikeCount;
	}

	public void setmDislikeCount(int mDislikeCount) {
		this.mDislikeCount = mDislikeCount;
	}

	public String getmFrom() {
		return mFrom;
	}

	public void setmFrom(String mFrom) {
		this.mFrom = mFrom;
	}

	public int getmLikeCount() {
		return mLikeCount;
	}

	public void setmLikeCount(int mLikeCount) {
		this.mLikeCount = mLikeCount;
	}

	public CoordinatesData getmLocation() {
		return mLocation;
	}

	public void setmLocation(CoordinatesData mLocation) {
		this.mLocation = mLocation;
	}

	public String getmRegistrant() {
		return mRegistrant;
	}

	public void setmRegistrant(String mRegistrant) {
		this.mRegistrant = mRegistrant;
	}

	public String getmReportType() {
		return mReportType;
	}

	public void setmReportType(String mReportType) {
		this.mReportType = mReportType;
	}

	public String getmSido() {
		return mSido;
	}

	public void setmSido(String mSido) {
		this.mSido = mSido;
	}

	public String getmSigugun() {
		return mSigugun;
	}

	public void setmSigugun(String mSigugun) {
		this.mSigugun = mSigugun;
	}

	public long getmUpdatedTime() {
		return mUpdatedTime;
	}

	public void setmUpdatedTime(long mUpdatedTime) {
		this.mUpdatedTime = mUpdatedTime;
	}

	public String getmUserId() {
		return mUserId;
	}

	public void setmUserId(String mUserId) {
		this.mUserId = mUserId;
	}

	public boolean ismIsUserDislike() {
		return mIsUserDislike;
	}

	public void setmIsUserDislike(boolean mIsUserDislike) {
		this.mIsUserDislike = mIsUserDislike;
	}

	public boolean ismIsUserLike() {
		return mIsUserLike;
	}

	public void setmIsUserLike(boolean mIsUserLike) {
		this.mIsUserLike = mIsUserLike;
	}

	public ArrayList<CrackdownCommentListViewModel> getmCommentList() {
		return mCommentList;
	}

	public void setmCommentList(ArrayList<CrackdownCommentListViewModel> mCommentList) {
		this.mCommentList = mCommentList;
	}

	public int getmCommentCount() {
		return mCommentCount;
	}

	public void setmCommentCount(int mCommentCount) {
		this.mCommentCount = mCommentCount;
	}

	public String getmPicture() {
		return mPicture;
	}

	public void setmPicture(String mPicture) {
		this.mPicture = mPicture;
	}
}

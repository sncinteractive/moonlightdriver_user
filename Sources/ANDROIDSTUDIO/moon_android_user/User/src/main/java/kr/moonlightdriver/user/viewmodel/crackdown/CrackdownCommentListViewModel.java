package kr.moonlightdriver.user.viewmodel.crackdown;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by youngmin on 2016-08-11.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CrackdownCommentListViewModel {
	@JsonProperty("commentsId")
	private String mCommentsId;

	@JsonProperty("userId")
	private String mUserId;

	@JsonProperty("registrant")
	private String mRegistrant;

	@JsonProperty("contents")
	private String mContents;

	@JsonProperty("sub_comments")
	private ArrayList<CrackdownCommentListViewModel> mSubCommentsList;

	@JsonProperty("createTime")
	private long mCreateTime;

	private int mDepth;
	private int mParentIndex;

	public CrackdownCommentListViewModel() {}

	public CrackdownCommentListViewModel(String mCommentsId, String mContents, long mCreateTime, int mDepth, int mParentIndex, String mRegistrant, ArrayList<CrackdownCommentListViewModel> mSubCommentsList, String mUserId) {
		this.mCommentsId = mCommentsId;
		this.mContents = mContents;
		this.mCreateTime = mCreateTime;
		this.mDepth = mDepth;
		this.mParentIndex = mParentIndex;
		this.mRegistrant = mRegistrant;
		this.mSubCommentsList = mSubCommentsList;
		this.mUserId = mUserId;
	}

	public int getmDepth() {
		return mDepth;
	}

	public void setmDepth(int mDepth) {
		this.mDepth = mDepth;
	}

	public int getmParentIndex() {
		return mParentIndex;
	}

	public void setmParentIndex(int mParentIndex) {
		this.mParentIndex = mParentIndex;
	}

	public String getmCommentsId() {
		return mCommentsId;
	}

	public void setmCommentsId(String mCommentsId) {
		this.mCommentsId = mCommentsId;
	}

	public String getmContents() {
		return mContents;
	}

	public void setmContents(String mContents) {
		this.mContents = mContents;
	}

	public long getmCreateTime() {
		return mCreateTime;
	}

	public void setmCreateTime(long mCreateTime) {
		this.mCreateTime = mCreateTime;
	}

	public String getmRegistrant() {
		return mRegistrant;
	}

	public void setmRegistrant(String mRegistrant) {
		this.mRegistrant = mRegistrant;
	}

	public ArrayList<CrackdownCommentListViewModel> getmSubCommentsList() {
		return mSubCommentsList;
	}

	public void setmSubCommentsList(ArrayList<CrackdownCommentListViewModel> mSubCommentsList) {
		this.mSubCommentsList = mSubCommentsList;
	}

	public String getmUserId() {
		return mUserId;
	}

	public void setmUserId(String mUserId) {
		this.mUserId = mUserId;
	}
}

package kr.moonlightdriver.user.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.viewmodel.common.LocationData;

/**
 * Created by kyo on 2017-05-02.
 */

public class StopByListAdapter extends ArrayAdapter<LocationData> {
    public interface OnChildViewClickListener {
        void onSearchKeywordClick(int position);

        void onSearchMapClick(int position);

        void onRemoveClick(int position);
    }

    private int mResourceId;
    private OnChildViewClickListener mOnChildViewClickListener;

    public StopByListAdapter(Context context, int resource, ArrayList<LocationData> items) {
        super(context, resource, items);
        this.mResourceId = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        try {
            ViewHolder viewHolder = null;

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(mResourceId, null);

                viewHolder = new ViewHolder();

                viewHolder.mLocationTypeTextView = (TextView) convertView.findViewById(R.id.tv_location_type);
                Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangB(), viewHolder.mLocationTypeTextView);

                viewHolder.mSearchKeywordLayout = (RelativeLayout) convertView.findViewById(R.id.rl_search_keyword);
                viewHolder.mSearchKeywordLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            if (mOnChildViewClickListener != null) {
                                mOnChildViewClickListener.onSearchKeywordClick((int)view.getTag());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                viewHolder.mLocationTextView = (TextView) convertView.findViewById(R.id.tv_location);
                Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), viewHolder.mLocationTextView);

                viewHolder.mSearchMapButton = (ImageButton) convertView.findViewById(R.id.ib_search_map);
                viewHolder.mSearchMapButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            if (mOnChildViewClickListener != null) {
                                mOnChildViewClickListener.onSearchMapClick((int)view.getTag());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

//                viewHolder.mRemoveButton = (Button) convertView.findViewById(R.id.btn_remove);
//                viewHolder.mRemoveButton.setVisibility(View.VISIBLE);
//                viewHolder.mRemoveButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        remove(getItem((int)view.getTag()));
//
//                        if (mOnChildViewClickListener != null) {
//                            mOnChildViewClickListener.onRemoveClick((int)view.getTag());
//                        }
//                    }
//                });

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.mSearchKeywordLayout.setTag(position);
            viewHolder.mSearchMapButton.setTag(position);
//            viewHolder.mRemoveButton.setTag(position);

            viewHolder.mLocationTypeTextView.setText(getContext().getString(R.string.common_search_stopby_title));

            LocationData locationData = getItem(position);
            if (!Utils.isStringNullOrEmpty(locationData.getmLocationName())) {
                viewHolder.mLocationTextView.setText(locationData.getmLocationName());
            } else if (!Utils.isStringNullOrEmpty(locationData.getmAddress())) {
                viewHolder.mLocationTextView.setText(locationData.getmAddress());
            } else {
                viewHolder.mLocationTextView.setText(getContext().getString(R.string.common_search_stopby));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public void setOnChildViewClickListener(OnChildViewClickListener onChildViewClickListener) {
        mOnChildViewClickListener = onChildViewClickListener;
    }

    private static class ViewHolder {
        public TextView mLocationTypeTextView;
        public RelativeLayout mSearchKeywordLayout;
        public TextView mLocationTextView;
        public ImageButton mSearchMapButton;
//        public Button mRemoveButton;
    }
}

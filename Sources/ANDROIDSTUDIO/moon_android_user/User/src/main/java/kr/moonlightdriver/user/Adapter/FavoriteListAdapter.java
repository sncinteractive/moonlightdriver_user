package kr.moonlightdriver.user.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.viewholder.FavoriteListViewHolder;
import kr.moonlightdriver.user.viewmodel.setting.FavoriteListViewModel;

/**
 * Created by kyo on 2017-04-12.
 */

public class FavoriteListAdapter extends ArrayAdapter<FavoriteListViewModel> {

    private int mResourceId;
    private List<FavoriteListViewModel> mDataList;
    private FavoriteListViewHolder mViewHolder;

    public FavoriteListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<FavoriteListViewModel> objects) {
        super(context, resource, objects);

        mResourceId = resource;
        mDataList = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if( convertView == null ) {
            LayoutInflater layoutInflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(mResourceId, null);

            mViewHolder = new FavoriteListViewHolder();
            mViewHolder.nameTextView = (TextView)convertView.findViewById(R.id.tv_favorite_name);
            mViewHolder.addressTextView= (TextView)convertView.findViewById(R.id.tv_favorite_address);

            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), mViewHolder.nameTextView, mViewHolder.addressTextView);

            convertView.setTag(mViewHolder);

        } else {
            mViewHolder = (FavoriteListViewHolder) convertView.getTag();
        }

        FavoriteListViewModel data = mDataList.get(position);
        mViewHolder.nameTextView.setText( data.getName());
        mViewHolder.addressTextView.setText(data.getLocationData().getmAddress());

        return convertView;
    }
}

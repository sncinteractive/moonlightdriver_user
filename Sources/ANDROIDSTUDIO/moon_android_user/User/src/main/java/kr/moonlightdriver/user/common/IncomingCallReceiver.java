package kr.moonlightdriver.user.common;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

import kr.moonlightdriver.user.viewmodel.common.ActivityResultEvent;

/**
 * Created by youngmin on 2016-11-24.
 */

public class IncomingCallReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context _context, Intent _intent){
        try{
            String state = _intent.getStringExtra(TelephonyManager.EXTRA_STATE);

            Intent incomingCallIntent = new Intent();
            incomingCallIntent.putExtra("call_state", state);

            BusProvider.getInstance().post(new ActivityResultEvent(Configuration.INCOMING_CALL, Activity.RESULT_OK, incomingCallIntent));
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}

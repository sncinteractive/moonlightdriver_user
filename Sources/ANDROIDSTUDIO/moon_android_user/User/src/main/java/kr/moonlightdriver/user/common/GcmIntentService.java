package kr.moonlightdriver.user.common;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.view.main.MainActivity;

/**
 * Created by youngmin on 2016-10-31.
 */

public class GcmIntentService extends IntentService {
    private static final String TAG = "GcmIntentService";

    public GcmIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            Bundle extras = intent.getExtras();

            if (extras != null) {
                GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

                String messageType = gcm.getMessageType(intent);

                if (!extras.isEmpty()) {
                    Utils.log("error", TAG, "Received: " + extras.toString());

                    if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                        sendNotification(extras);
                    }
                }

                GcmBroadcastReceiver.completeWakefulIntent(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendNotification(Bundle _args) {
        try {
            String pushMsgType = _args.getString("type", "");

            String title = getNotificationTitle(pushMsgType);
            if (title.isEmpty()) {
                return;
            }

            title = _args.getString("title", title);

            String imgUrl = _args.getString("imgUrl", "");
            Bitmap notifyImg = null;
            if (!Utils.isStringNullOrEmpty(imgUrl)) {
                notifyImg = Utils.getImageDownload(Configuration.IMAGE_SERVER_HOST + imgUrl);
            }

            String content = _args.getString("message", "");
            Intent intent = createIntent(_args);

            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
            builder.setSmallIcon(R.drawable.ic_launcher);
            builder.setWhen(System.currentTimeMillis());

            if (pushMsgType.equals(Configuration.PUSH_MSG_TYPE_CRACK_DOWN_WARNING)) {
                builder.setTicker(content);
                RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.crackdown_notification);

                float lat = Float.parseFloat(_args.getString("lat", "0"));
                float lng = Float.parseFloat(_args.getString("lng", "0"));
                int distance = Integer.parseInt(_args.getString("distance", "0"));

                if(lat != 0 && lng != 0) {
                    float[] dist = new float[2];
                    Location.distanceBetween(MainActivity.mCurrentLocation.latitude, MainActivity.mCurrentLocation.longitude, lat, lng, dist);

                    distance = (int)dist[0];
                }

                if (distance <= 300) {
                    contentView.setImageViewResource(R.id.crackdown_notification_image, R.drawable.push_notice_300m);
                } else if (distance <= 500) {
                    contentView.setImageViewResource(R.id.crackdown_notification_image, R.drawable.push_notice_500m);
                } else if (distance <= 1000) {
                    contentView.setImageViewResource(R.id.crackdown_notification_image, R.drawable.push_notice_1000m);
                } else {
                    return;
                }

                builder.setCustomHeadsUpContentView(contentView);
            } else {
                builder.setTicker(title);
            }

            builder.setContentTitle(title);
            builder.setContentText(content);
            builder.setContentIntent(contentIntent);
            builder.setAutoCancel(true);
            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
            builder.setLargeIcon(bm);
            builder.setVibrate(getVibratePattern(_args));
            builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

            if (Build.VERSION.SDK_INT > 15) {
                builder.setPriority(Notification.PRIORITY_MAX);
            }

            if (notifyImg != null) {
                NotificationCompat.BigPictureStyle style = new NotificationCompat.BigPictureStyle();
                style.setBigContentTitle(title);
                style.setSummaryText(content);
                style.bigPicture(notifyImg);
                builder.setStyle(style);
            } else {
                NotificationCompat.BigTextStyle textStyle = new NotificationCompat.BigTextStyle();
                textStyle.setBigContentTitle(title);
                textStyle.bigText(content);
                textStyle.setSummaryText(content);
                builder.setStyle(textStyle);
            }

            Notification notification;
            if (Build.VERSION.SDK_INT > 15) {
                notification = builder.build();
            } else {
                notification = builder.getNotification();
            }

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(Utils.getNotificationId(pushMsgType), notification);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private long[] getVibratePattern(Bundle _args) {
        try {
            String pushMsgType = _args.getString("type", "");
            switch (pushMsgType) {
                case Configuration.PUSH_MSG_TYPE_CRACK_DOWN_WARNING: {
                    int distance = Integer.parseInt(_args.getString("distance", "0"));
                    if (distance == 300) {
                        return new long[]{200, 1000, 200, 1000, 200, 1000, 200, 1000, 200, 1000};
                    } else if (distance == 500) {
                        return new long[]{200, 1000, 200, 1000, 200, 1000};
                    } else if (distance == 1000) {
                        return new long[]{200, 2000};
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new long[]{200, 1000, 200, 1000};
    }

    private String getNotificationTitle(String pushMsgType) {
        try {
            switch (pushMsgType) {
                case Configuration.PUSH_MSG_TYPE_CRACK_DOWN_WARNING:
                    return "음주단속 알림";
//                case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_NOT_ASSIGNED:
//                    return "기사 호출 실패";
                case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_ASSIGNED:
                    return "기사 호출 성공";
                case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_PRICE_OFFER:
                    return "기사 금액 제안";
                case Configuration.PUSH_MSG_TYPE_NOTICE:
                    return "공지사항";
                case Configuration.PUSH_MSG_TYPE_COUPON:
                    return "쿠폰지급";
                case Configuration.PUSH_MSG_TYPE_POINT:
                    return "적립금 지급";
                default:
                    return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private Intent createIntent(Bundle bundle) {
        Intent intent;
        String pushMsgType = bundle.getString("type", "");
        switch (pushMsgType) {
            case Configuration.PUSH_MSG_TYPE_CRACK_DOWN_WARNING: {
                intent = new Intent(this, MainActivity.class);
                intent.putExtra("distance", bundle.getString("distance", "0"));
            }
            break;
//            case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_NOT_ASSIGNED: {
//                intent = new Intent(this, MainActivity.class);
//            }
//            break;
            case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_ASSIGNED: {
                intent = new Intent(this, MainActivity.class);
            }
            break;
            case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_PRICE_OFFER: {
                intent = new Intent(this, MainActivity.class);
            }
            break;
            case Configuration.PUSH_MSG_TYPE_NOTICE: {
                intent = new Intent(this, MainActivity.class);
            }
            break;
            case Configuration.PUSH_MSG_TYPE_COUPON: {
                intent = new Intent(this, MainActivity.class);
            }
            break;
            case Configuration.PUSH_MSG_TYPE_POINT: {
                intent = new Intent(this, MainActivity.class);
            }
            break;
            default:
                return null;
        }

        intent.putExtra("pushMsgType", pushMsgType);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        return intent;
    }
}

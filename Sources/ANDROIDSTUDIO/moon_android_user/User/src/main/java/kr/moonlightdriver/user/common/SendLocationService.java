package kr.moonlightdriver.user.common;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.RequestParams;

import java.util.Calendar;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.viewmodel.common.ActivityResultEvent;
import kr.moonlightdriver.user.viewmodel.common.ResultData;

/**
 * Created by youngmin on 2016-09-08.
 */
public class SendLocationService extends Service {
    private static final String TAG = "SendLocationService";
    private boolean mIsCheckGpsUpdateTime;
    private Context mContext;
    private GpsInfo mGpsInfo;

    private String mUserId = "";
    private LatLng mLastUpdateLocation;
    private double mLastUpdateLocationTime = 0;

    private class GpsLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            try {
                Utils.log("error", TAG, "provider : " + location.getProvider() + ", latlng : (" + location.getLatitude() + ", " + location.getLongitude() + ")");

                sendDataToMain(location.getLatitude(), location.getLongitude(), location.getAltitude());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        try {
            mContext = this;

            if (intent == null) {
                mIsCheckGpsUpdateTime = true;
            } else {
                mIsCheckGpsUpdateTime = false;
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                mIsCheckGpsUpdateTime = true;
                            }
                        }, 30000);
            }

            initializeLocationManager();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return START_STICKY;
    }

    private void initializeLocationManager() {
        try {
            stopLocationUpdates();

            startLocationUpdates();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startLocationUpdates() {
        try {
            mGpsInfo = new GpsInfo(mContext, new GpsLocationListener());
            mGpsInfo.connectGoogleApi();

            updateLastKnownLocation();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopLocationUpdates() {
        try {
            if (mGpsInfo != null) {
                if (mGpsInfo.isConnected()) {
                    mGpsInfo.stopLocationUpdates();
                    mGpsInfo.disconnectGoogleApi();
                }

                mGpsInfo = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getUserId() {
        String userId = "";

        try {
            SharedPreferences prefs = mContext.getSharedPreferences(Configuration.PREFERENCES_NAME, Context.MODE_PRIVATE);
            userId = prefs.getString(Configuration.PROPERTY_USER_ID, "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return userId;
    }

    private void updateLastKnownLocation() {
        try {
            if (mGpsInfo != null) {
                Location lastLocation = mGpsInfo.getLastLocation();

                if (lastLocation != null) {
                    sendDataToMain(lastLocation.getLatitude(), lastLocation.getLongitude(), lastLocation.getAltitude());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendDataToMain(double _lat, double _lng, double _alt) {
        try {
            Intent intent = new Intent();
            intent.putExtra("lat", _lat);
            intent.putExtra("lng", _lng);
            intent.putExtra("alt", _alt);

            BusProvider.getInstance().post(new ActivityResultEvent(Configuration.POSITION_CHANGED, Activity.RESULT_OK, intent));

            if (mUserId.isEmpty()) {
                mUserId = getUserId();
            }

            if (!mUserId.isEmpty()) {
                updateCurrentLocation(new LatLng(_lat, _lng));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateCurrentLocation(LatLng _newLocation) {
        try {
            if (mLastUpdateLocation == null) {
                updateUserLocation(_newLocation);
            } else {
                float[] dist = new float[2];
                Location.distanceBetween(mLastUpdateLocation.latitude, mLastUpdateLocation.longitude, _newLocation.latitude, _newLocation.longitude, dist);

                if (dist[0] >= Configuration.UPDATE_DISTANCE) {
                    updateUserLocation(_newLocation);
                }
            }

            setLocationUpdateTime(_newLocation);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setLocationUpdateTime(LatLng _newLocation) {
        try {
            if (mLastUpdateLocation != null && mLastUpdateLocationTime >= 0) {
                Calendar calendar = Calendar.getInstance();
                double nowTime = calendar.getTimeInMillis();
                double diffTimeInSeconds = ((nowTime - mLastUpdateLocationTime) / 1000d) / 3600d;

                float[] dist = new float[3];
                Location.distanceBetween(mLastUpdateLocation.latitude, mLastUpdateLocation.longitude, _newLocation.latitude, _newLocation.longitude, dist);

                double distance = dist[0] / 1000d;

                double kmPerHour = distance / diffTimeInSeconds;

                long uploadTime = Utils.getLocationUpdateTime(kmPerHour);

                Utils.log("error", TAG, "distance : " + distance + ", diffTimeInSeconds : " + diffTimeInSeconds + ", kmPerHour : " + kmPerHour + ", uploadTime : " + uploadTime + ", mIsCheckGpsUpdateTime : " + mIsCheckGpsUpdateTime);

                if (uploadTime > 1000 && mIsCheckGpsUpdateTime) {
                    stopLocationUpdates();

                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    startLocationUpdates();
                                }
                            }, uploadTime);
                }
            }

            Calendar calendar1 = Calendar.getInstance();
            mLastUpdateLocationTime = calendar1.getTimeInMillis();
            mLastUpdateLocation = new LatLng(_newLocation.latitude, _newLocation.longitude);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateUserLocation(final LatLng _newLocation) {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;

            Log.e(TAG, "updateUserLocation() version = " + version);

            RequestParams params = new RequestParams();
            params.add("userId", mUserId);
            params.add("lat", _newLocation.latitude + "");
            params.add("lng", _newLocation.longitude + "");
            params.add("version", version);

            NetClient.send(mContext, Configuration.URL_UPDATE_USER_LOCATION, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    try {
                        if (_netAPI != null) {
                            NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
                            ResultData result = retNetApi.getmResultData();

                            if (result.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {

                            } else {
                                Utils.log("error", TAG, result.getmDetail());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            stopLocationUpdates();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

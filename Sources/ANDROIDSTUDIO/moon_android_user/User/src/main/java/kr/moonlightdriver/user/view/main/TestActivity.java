package kr.moonlightdriver.user.view.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Utils;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        if (Utils.isAvailableSMS(this)) {
            Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:"));
            intent.putExtra("subject", Configuration.SHARED_TEXT); // mms일때만 사용됨
            intent.putExtra("sms_body", "홍길동 기사님이 대리로 운전하여 귀가중입니다.");
            startActivity(intent);
        }
    }
}




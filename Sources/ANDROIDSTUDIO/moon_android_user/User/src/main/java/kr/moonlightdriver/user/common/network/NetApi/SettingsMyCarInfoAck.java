package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.SettingsMyCarInfoViewModel;

/**
 * Created by eklee on 2017. 5. 11..
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SettingsMyCarInfoAck extends NetAPI {
    @JsonProperty("retMyCarInfo")
    private SettingsMyCarInfoViewModel mCarInfo;

    public SettingsMyCarInfoAck() {}

    public SettingsMyCarInfoAck(ResultData mResultData, SettingsMyCarInfoViewModel mCarInfo) {
        super(mResultData);
        this.mCarInfo = mCarInfo;
    }

    public SettingsMyCarInfoViewModel getmCarInfo() {
        return mCarInfo;
    }

    public void setmCarInfo(SettingsMyCarInfoViewModel mCarInfo) {
        this.mCarInfo = mCarInfo;
    }
}

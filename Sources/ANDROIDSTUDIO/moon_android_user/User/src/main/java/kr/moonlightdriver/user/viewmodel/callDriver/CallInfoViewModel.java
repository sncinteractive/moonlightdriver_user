package kr.moonlightdriver.user.viewmodel.callDriver;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.viewmodel.common.CoordinatesData;

/**
 * Created by youngmincho on 2017. 7. 31..
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CallInfoViewModel {
	@JsonProperty("callId")
	private String mCallId;
	@JsonProperty("callSeq")
	private int mCallSeq;
	@JsonProperty("callType")
	private String mCallType;
	@JsonProperty("status")
	private String mStatus;
	@JsonProperty("uniqueId")
	private String mUniqueId;
	@JsonProperty("start")
	private CallInfoPositionViewModel mStart;
	@JsonProperty("through")
	private CallInfoPositionViewModel mThrough;
	@JsonProperty("through1")
	private CallInfoPositionViewModel mThrough1;
	@JsonProperty("through2")
	private CallInfoPositionViewModel mThrough2;
	@JsonProperty("end")
	private CallInfoPositionViewModel mEnd;
	@JsonProperty("money")
	private int mMoney;
	@JsonProperty("paymentOption")
	private String mPaymentOption;
	@JsonProperty("user")
	private CallInfoUserViewModel mUser;
	@JsonProperty("driver")
	private DriverInfoViewModel mDriver;
	@JsonProperty("etc")
	private String mEtc;
	@JsonProperty("useReliefMessage")
	private String mUseReliefMessage;
	@JsonProperty("evaluatedYN")
	private String mEvaluatedYN;
	@JsonProperty("reportedUnfairnessYN")
	private String mReportedUnfairnessYN;
	@JsonProperty("createdTime")
	private long mCreatedTime;
	@JsonProperty("departureTime")
	private long mDepartureTime;
	@JsonProperty("estimateTime")
	private long mEstimateTime;


	public CallInfoViewModel() {}

	public CallInfoViewModel(String mCallId, int mCallSeq, String mCallType, String mStatus, String mUniqueId, CallInfoPositionViewModel mStart, CallInfoPositionViewModel mThrough, CallInfoPositionViewModel mThrough1, CallInfoPositionViewModel mThrough2, CallInfoPositionViewModel mEnd, int mMoney, String mPaymentOption, CallInfoUserViewModel mUser, DriverInfoViewModel mDriver, String mEtc, String mUseReliefMessage, String mEvaluatedYN, String mReportedUnfairnessYN, long mCreatedTime, long mDepartureTime, long mEstimateTime) {
		this.mCallId = mCallId;
		this.mCallSeq = mCallSeq;
		this.mCallType = mCallType;
		this.mStatus = mStatus;
		this.mUniqueId = mUniqueId;
		this.mStart = mStart;
		this.mThrough = mThrough;
		this.mThrough1 = mThrough1;
		this.mThrough2 = mThrough2;
		this.mEnd = mEnd;
		this.mMoney = mMoney;
		this.mPaymentOption = mPaymentOption;
		this.mUser = mUser;
		this.mDriver = mDriver;
		this.mEtc = mEtc;
		this.mUseReliefMessage = mUseReliefMessage;
		this.mEvaluatedYN = mEvaluatedYN;
		this.mReportedUnfairnessYN = mReportedUnfairnessYN;
		this.mCreatedTime = mCreatedTime;
		this.mDepartureTime = mDepartureTime;
		this.mEstimateTime = mEstimateTime;
	}

	public String getmCallId() {
		return mCallId;
	}

	public void setmCallId(String mCallId) {
		this.mCallId = mCallId;
	}

	public int getmCallSeq() {
		return mCallSeq;
	}

	public void setmCallSeq(int mCallSeq) {
		this.mCallSeq = mCallSeq;
	}

	public String getmCallType() {
		return mCallType;
	}

	public void setmCallType(String mCallType) {
		this.mCallType = mCallType;
	}

	public String getmStatus() {
		return mStatus;
	}

	public void setmStatus(String mStatus) {
		this.mStatus = mStatus;
	}

	public String getmUniqueId() {
		return mUniqueId;
	}

	public void setmUniqueId(String mUniqueId) {
		this.mUniqueId = mUniqueId;
	}

	public CallInfoPositionViewModel getmStart() {
		return mStart;
	}

	public void setmStart(CallInfoPositionViewModel mStart) {
		this.mStart = mStart;
	}

	public CallInfoPositionViewModel getmThrough() {
		return mThrough;
	}

	public void setmThrough(CallInfoPositionViewModel mThrough) {
		this.mThrough = mThrough;
	}

	public CallInfoPositionViewModel getmThrough1() {
		return mThrough1;
	}

	public void setmThrough1(CallInfoPositionViewModel mThrough1) {
		this.mThrough1 = mThrough1;
	}

	public CallInfoPositionViewModel getmThrough2() {
		return mThrough2;
	}

	public void setmThrough2(CallInfoPositionViewModel mThrough2) {
		this.mThrough2 = mThrough2;
	}

	public CallInfoPositionViewModel getmEnd() {
		return mEnd;
	}

	public void setmEnd(CallInfoPositionViewModel mEnd) {
		this.mEnd = mEnd;
	}

	public int getmMoney() {
		return mMoney;
	}

	public void setmMoney(int mMoney) {
		this.mMoney = mMoney;
	}

	public String getmPaymentOption() {
		return mPaymentOption;
	}

	public void setmPaymentOption(String mPaymentOption) {
		this.mPaymentOption = mPaymentOption;
	}

	public CallInfoUserViewModel getmUser() {
		return mUser;
	}

	public void setmUser(CallInfoUserViewModel mUser) {
		this.mUser = mUser;
	}

	public DriverInfoViewModel getmDriver() {
		return mDriver;
	}

	public void setmDriver(DriverInfoViewModel mDriver) {
		this.mDriver = mDriver;
	}

	public String getmEtc() {
		return mEtc;
	}

	public void setmEtc(String mEtc) {
		this.mEtc = mEtc;
	}

	public String getmUseReliefMessage() {
		return mUseReliefMessage;
	}

	public void setmUseReliefMessage(String mUseReliefMessage) {
		this.mUseReliefMessage = mUseReliefMessage;
	}

	public String getmEvaluatedYN() {
		return mEvaluatedYN;
	}

	public void setmEvaluatedYN(String mEvaluatedYN) {
		this.mEvaluatedYN = mEvaluatedYN;
	}

	public String getmReportedUnfairnessYN() {
		return mReportedUnfairnessYN;
	}

	public void setmReportedUnfairnessYN(String mReportedUnfairnessYN) {
		this.mReportedUnfairnessYN = mReportedUnfairnessYN;
	}

	public long getmCreatedTime() {
		return mCreatedTime;
	}

	public void setmCreatedTime(long mCreatedTime) {
		this.mCreatedTime = mCreatedTime;
	}

	public long getmDepartureTime() {
		return mDepartureTime;
	}

	public void setmDepartureTime(long mDepartureTime) {
		this.mDepartureTime = mDepartureTime;
	}

	public long getmEstimateTime() {
		return mEstimateTime;
	}

	public void setmEstimateTime(long mEstimateTime) {
		this.mEstimateTime = mEstimateTime;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public class CallInfoDriverViewModel {
		@JsonProperty("driverId")
		private String mDriverId;
		@JsonProperty("phone")
		private String mPhone;

		public CallInfoDriverViewModel() {}

		public CallInfoDriverViewModel(String mDriverId, String mPhone) {
			this.mDriverId = mDriverId;
			this.mPhone = mPhone;
		}

		public String getmDriverId() {
			return mDriverId;
		}

		public void setmDriverId(String mDriverId) {
			this.mDriverId = mDriverId;
		}

		public String getmPhone() {
			return mPhone;
		}

		public void setmPhone(String mPhone) {
			this.mPhone = mPhone;
		}
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public class CallInfoPositionViewModel {
		@JsonProperty("address")
		private String mAddress;
		@JsonProperty("location")
		private CoordinatesData mLocation;

		public CallInfoPositionViewModel() {}

		public CallInfoPositionViewModel(String mAddress, CoordinatesData mLocation) {
			this.mAddress = mAddress;
			this.mLocation = mLocation;
		}

		public String getmAddress() {
			return mAddress;
		}

		public void setmAddress(String mAddress) {
			this.mAddress = mAddress;
		}

		public CoordinatesData getmLocation() {
			return mLocation;
		}

		public void setmLocation(CoordinatesData mLocation) {
			this.mLocation = mLocation;
		}
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public class CallInfoUserViewModel {
		@JsonProperty("userId")
		private String mUserId;
		@JsonProperty("phone")
		private String mPhone;
		@JsonProperty("address")
		private String mAddress;
		@JsonProperty("city_do")
		private String mCityDo;
		@JsonProperty("gu_gun")
		private String mGuGun;
		@JsonProperty("location")
		private CoordinatesData mLocation;

		public CallInfoUserViewModel() {}

		public CallInfoUserViewModel(String mUserId, String mPhone, String mAddress, String mCityDo, String mGuGun, CoordinatesData mLocation) {
			this.mUserId = mUserId;
			this.mPhone = mPhone;
			this.mAddress = mAddress;
			this.mCityDo = mCityDo;
			this.mGuGun = mGuGun;
			this.mLocation = mLocation;
		}

		public String getmUserId() {
			return mUserId;
		}

		public void setmUserId(String mUserId) {
			this.mUserId = mUserId;
		}

		public String getmPhone() {
			return mPhone;
		}

		public void setmPhone(String mPhone) {
			this.mPhone = mPhone;
		}

		public String getmAddress() {
			return mAddress;
		}

		public void setmAddress(String mAddress) {
			this.mAddress = mAddress;
		}

		public String getmCityDo() {
			return mCityDo;
		}

		public void setmCityDo(String mCityDo) {
			this.mCityDo = mCityDo;
		}

		public String getmGuGun() {
			return mGuGun;
		}

		public void setmGuGun(String mGuGun) {
			this.mGuGun = mGuGun;
		}

		public CoordinatesData getmLocation() {
			return mLocation;
		}

		public void setmLocation(CoordinatesData mLocation) {
			this.mLocation = mLocation;
		}
	}

}

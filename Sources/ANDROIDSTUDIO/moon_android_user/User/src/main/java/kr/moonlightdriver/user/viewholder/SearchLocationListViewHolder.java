package kr.moonlightdriver.user.viewholder;

import android.widget.TextView;

/**
 * Created by youngmin on 2016-07-01.
 */
public class SearchLocationListViewHolder {
	public TextView mTextViewLocationName;
	public TextView mTextViewAddress;
}

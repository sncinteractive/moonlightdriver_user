package kr.moonlightdriver.user.view.settings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.RequestParams;

import kr.moonlightdriver.user.Adapter.FavoriteListAdapter;
import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.FavoriteAddAck;
import kr.moonlightdriver.user.common.network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.view.callDriver.MapSearchLocationFragment;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewmodel.common.LocationData;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.FavoriteListViewModel;

/**
 * Created by youngmin on 2016-06-21.
 */
public class SettingFavoriteAddFragment extends Fragment {
    private static final String TAG = "SettingFavoriteAddFragment";

    private static final int REQ_SEARCH_LOCATION = 1;
    private static final int REQ_SEARCH_MAP = 2;

    private MainActivity mMainActivity;

    private ListView mListView;
    private FavoriteListAdapter mListAdapter;
    private LocationData mLocationData;

    private EditText mNameEditText;
    private TextView mAddressTextView;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;

        try {
            mMainActivity = (MainActivity) getActivity();

            view = inflater.inflate(R.layout.fragment_setting_favorite_add, container, false);

            TextView headerTitleTextView = (TextView)view.findViewById(R.id.tv_header_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanB(), headerTitleTextView);

            LinearLayout goBackButton = (LinearLayout) view.findViewById(R.id.ib_setting_goback);
            goBackButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getActivity().getSupportFragmentManager().beginTransaction().remove(SettingFavoriteAddFragment.this).commit();
                }
            });

            ImageButton completeButton = (ImageButton) view.findViewById(R.id.ib_complete);
            completeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String name = mNameEditText.getText().toString();
                    if (mLocationData == null) {
                        CommonDialog.showSimpleDialog(mMainActivity, getString(R.string.favorite_add_invalid_location_error_msg));
                    } else if (isDuplicatedFavoriteName(name)) {
                        CommonDialog.showSimpleDialog(mMainActivity, getString(R.string.favorite_add_duplicated_name_error_msg));
                    } else {
                        // 서버 연동시 주석 제거
//                        testFavoriteListAddResponse();
                        requestFavoriteListAdd();
                    }
                }
            });

            mListAdapter = new FavoriteListAdapter(mMainActivity, R.layout.row_setting_favorite_list, mMainActivity.mUserAdditionalData.getFavoriteList());

            mListView = (ListView) view.findViewById(R.id.lv_favorite);
            mListView.setAdapter(mListAdapter);

            LinearLayout searchKeywordLayout = (LinearLayout) view.findViewById(R.id.ll_search_keyword);
            searchKeywordLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Fragment fragment = new SearchPOIFragment();
                    fragment.setTargetFragment(SettingFavoriteAddFragment.this, REQ_SEARCH_LOCATION);
                    mMainActivity.addFragment(fragment);
                }
            });

            mNameEditText = (EditText) view.findViewById(R.id.et_name);
            mAddressTextView = (TextView) view.findViewById(R.id.tv_address);

            ImageButton searchMapButton = (ImageButton) view.findViewById(R.id.ib_search_map);
            searchMapButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Utils.log("error", TAG, "onClick search map button");
                    Fragment fragment = new MapSearchLocationFragment();
                    fragment.setTargetFragment(SettingFavoriteAddFragment.this, REQ_SEARCH_MAP);

                    Bundle bundle = new Bundle();
                    bundle.putString("locationType", "target");
                    fragment.setArguments(bundle);
                    mMainActivity.addFragment(fragment);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private boolean isDuplicatedFavoriteName(String name) {
        for (FavoriteListViewModel model : mMainActivity.mUserAdditionalData.getFavoriteList()) {
            if (name.equalsIgnoreCase(model.getName())) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQ_SEARCH_LOCATION:
                    // fall through
                case REQ_SEARCH_MAP: {
                    if (data != null) {
                        mLocationData = data.getParcelableExtra("locationData");
                        mAddressTextView.setText(mLocationData.getmAddress());

                        boolean hasValidLocationName = !Utils.isStringNullOrEmpty(mLocationData.getmLocationName());
                        if (hasValidLocationName) {
                            boolean isDefaultFavoriteName = mNameEditText.getText().toString().equals(getString(R.string.favorite_add_default_name));
                            if (isDefaultFavoriteName) {
                                mNameEditText.setText(mLocationData.getmLocationName());
                            }
                        } else {
                            mLocationData.setmLocationName(getString(R.string.favorite_add_default_name));
                        }
                    }
                }
                break;
            }
        }
    }

    private void requestFavoriteListAdd() {
        mMainActivity.showLoadingAnim();

        RequestParams requestParams = new RequestParams();
        requestParams.add("userId", mMainActivity.mUserDetail.getUserId());
        requestParams.put("favoriteName", mNameEditText.getText().toString());
        requestParams.put("locationName", mLocationData.getmLocationName());
        requestParams.put("locationAddress", mLocationData.getmAddress());
        requestParams.put("lat", Double.toString(mLocationData.getmLocation().latitude));
        requestParams.put("lng", Double.toString(mLocationData.getmLocation().longitude));

        // 서버 연동시 주석 제거
        NetClient.send(mMainActivity, Configuration.URL_FAVORITE_LIST_ADD, "POST", requestParams, new FavoriteAddAck(), new NetResponseCallback(new FavoriteListAddResponse()));
    }

    private class FavoriteListAddResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            mMainActivity.hideLoadingAnim();

            if (_netAPI != null) {
                ResultData resultData = _netAPI.getmResultData();
                if (resultData.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                    LocationData locationData = new LocationData(new LatLng(mLocationData.getmLocation().latitude, mLocationData.getmLocation().longitude),
                            mLocationData.getmLocationName(),
                            mLocationData.getmAddress());

                    FavoriteListViewModel item = ((FavoriteAddAck)_netAPI).getFavorite();
                    item.setLocationData(locationData);

                    mMainActivity.mUserAdditionalData.addFavoriteListItem(item);

                    mListAdapter.notifyDataSetChanged();

                    mListView.smoothScrollToPosition(mListAdapter.getCount() - 1);

                    mNameEditText.setText(getString(R.string.favorite_add_default_name));
                    mAddressTextView.setText(getString(R.string.favorite_add_address_title));
                    mLocationData = null;

                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                    getActivity().getSupportFragmentManager().beginTransaction().remove(SettingFavoriteAddFragment.this).commit();
                } else { // rollback
                    CommonDialog.showSimpleDialog(mMainActivity, resultData.getmDetail());
                }
            }
        }
    }

    // TestCode => 즐겨찾기 리스트 추가 처리 테스트 함수
    private void testFavoriteListAddResponse() {
        NoResponseDataAck successAck = new NoResponseDataAck(new ResultData(Enums.NetResultCode.SUCCESS.getValue(), null, null));
        NoResponseDataAck failAck = new NoResponseDataAck(new ResultData(Enums.NetResultCode.UNKNOWN_ERROR.getValue(), null, "오류 발생으로 즐겨찾기 추가에 실패했습니다"));
        new SettingFavoriteAddFragment.FavoriteListAddResponse().onResponse(successAck);
    }
}

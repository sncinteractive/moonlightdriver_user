package kr.moonlightdriver.user.viewmodel.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2016-06-21.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserViewModel {
    @JsonProperty("userId")
    public String mUserId;

    @JsonProperty("device_uuid")
    public String mDeviceUUID;

    @JsonProperty("gcmId")
    public String mGcmId;

    @JsonProperty("emal")
    public String mEmail;

    @JsonProperty("phone")
    private String mPhoneNumber;

    @JsonProperty("name")
    private String mUserName;

    @JsonProperty("profileImage")
    private String mProfileImage;

    @JsonProperty("locations")
    private CoordinatesData mLocation;

    public UserViewModel() {
    }

    public UserViewModel(String userId, String deviceUUID, String gcmId, String email, String phoneNumber, String userName, String profileImage, CoordinatesData location) {
        mUserId = userId;
        mDeviceUUID = deviceUUID;
        mGcmId = gcmId;
        mEmail = email;
        mPhoneNumber = phoneNumber;
        mUserName = userName;
        mProfileImage = profileImage;
        mLocation = location;
    }

    public String getDeviceUUID() {
        return mDeviceUUID;
    }

    public void setDeviceUUID(String mDeviceUUID) {
        this.mDeviceUUID = mDeviceUUID;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getGcmId() {
        return mGcmId;
    }

    public void setGcmId(String mGcmId) {
        this.mGcmId = mGcmId;
    }

    public CoordinatesData getLocation() {
        return mLocation;
    }

    public void setLocation(CoordinatesData mLocation) {
        this.mLocation = mLocation;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String mPhoneNumber) {
        this.mPhoneNumber = mPhoneNumber;
    }

    public String getProfileImage() {
        return mProfileImage;
    }

    public void setProfileImage(String mProfileImage) {
        this.mProfileImage = mProfileImage;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String mUserName) {
        this.mUserName = mUserName;
    }
}

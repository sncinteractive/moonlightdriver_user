package kr.moonlightdriver.user.view.settings;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

import kr.moonlightdriver.user.Adapter.FavoriteModifyListAdapter;
import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.FavoriteListViewModel;

/**
 * Created by youngmin on 2016-06-21.
 */
public class SettingFavoriteModifyFragment extends Fragment implements View.OnClickListener {
    private MainActivity mMainActivity;

    private FavoriteModifyListAdapter mListAdapter;
    private List<FavoriteListViewModel> mModelListClone;
    private List<FavoriteListViewModel> mRemovedModelList;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            mMainActivity = (MainActivity) getActivity();

            view = inflater.inflate(R.layout.fragment_setting_favorite_modify, container, false);

            TextView headerTitleTextView = (TextView)view.findViewById(R.id.tv_header_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanB(), headerTitleTextView);

            LinearLayout goBackButton = (LinearLayout) view.findViewById(R.id.ib_setting_goback);
            goBackButton.setOnClickListener(this);

            ImageButton completeButton = (ImageButton) view.findViewById(R.id.ib_complete);
            completeButton.setOnClickListener(this);

            mModelListClone = new ArrayList<>(mMainActivity.mUserAdditionalData.getFavoriteList());
            mRemovedModelList = new ArrayList<>();

            mListAdapter = new FavoriteModifyListAdapter(mMainActivity, R.layout.row_setting_favorite_modify_list, mModelListClone);
            mListAdapter.setDeleteButtonClickListener(new FavoriteModifyListAdapter.OnDeleteButtonClickListener() {
                @Override
                public void onDeleteButtonClick(int position) {
                    FavoriteListViewModel removedData = mModelListClone.remove(position);
                    mRemovedModelList.add(removedData);
                    mListAdapter.notifyDataSetChanged();
                }
            });

            ListView listView = (ListView) view.findViewById(R.id.lv_favorite);
            listView.setAdapter(mListAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onClick(View _view) {
        switch (_view.getId()) {
            case R.id.ib_setting_goback: {
                closeFragment();
            }
            break;
            case R.id.ib_complete: {
                if (mRemovedModelList.size() > 0) {
                    // 서버 연동시 주석 제거
//                    testFavoriteListResponse();
                    requestFavoriteListModify();
                } else {
                    closeFragment();
                }
            }
            break;
        }
    }

    private void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    private void requestFavoriteListModify() {
        mMainActivity.showLoadingAnim();

        String removedIds = "";
        for (FavoriteListViewModel model : mRemovedModelList) {
            removedIds += model.getmFavoriteId() + ",";
        }

        if(removedIds.length() > 0) {
            removedIds = removedIds.substring(0, removedIds.length() - 1);
        }

        RequestParams requestParams = new RequestParams();
        requestParams.add("userId", mMainActivity.mUserDetail.getUserId());
        requestParams.add("removedList", removedIds);

        // 서버 연동시 주석 제거
        NetClient.send(mMainActivity, Configuration.URL_FAVORITE_LIST_MODIFY, "POST", requestParams, new NoResponseDataAck(), new NetResponseCallback(new FavoriteListModifyResponse()));
    }

    private class FavoriteListModifyResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            mMainActivity.hideLoadingAnim();

            if (_netAPI != null) {
                ResultData resultData = _netAPI.getmResultData();
                if (resultData.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                    for (FavoriteListViewModel removed : mRemovedModelList) {
                        mMainActivity.mUserAdditionalData.removeFavoriteListItem(removed);
                    }
                    mRemovedModelList.clear();

                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);

                    closeFragment();
                } else { // rollback
                    mRemovedModelList.clear();
                    mModelListClone.clear();
                    mModelListClone.addAll(mMainActivity.mUserAdditionalData.getFavoriteList());
                    mListAdapter.notifyDataSetChanged();

                    CommonDialog.showSimpleDialog(mMainActivity, resultData.getmDetail());
                }
            }
        }
    }

    // TestCode => 즐겨찾기 리스트 조회 처리 테스트 함수
    private void testFavoriteListResponse() {
        NoResponseDataAck successAck = new NoResponseDataAck(new ResultData(Enums.NetResultCode.SUCCESS.getValue(), null, null));
        NoResponseDataAck failAck = new NoResponseDataAck(new ResultData(Enums.NetResultCode.UNKNOWN_ERROR.getValue(), null, "오류 발생으로 즐겨찾기 목록이 이전으로 복구되었습니다"));
        new SettingFavoriteModifyFragment.FavoriteListModifyResponse().onResponse(successAck);
    }
}

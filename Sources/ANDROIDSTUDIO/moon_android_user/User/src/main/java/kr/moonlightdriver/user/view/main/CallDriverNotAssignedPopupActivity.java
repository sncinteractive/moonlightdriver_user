package kr.moonlightdriver.user.view.main;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;

/**
 * Created by eklee on 2017. 6. 1..
 */

public class CallDriverNotAssignedPopupActivity extends Activity {
    private static String TAG = "CallDriverNotAssignedPopupActivity";

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.popup_call_driver_not_assigned);

        TextView titleTextView = (TextView) findViewById(R.id.tv_popup_call_driver_not_assigned_title);
        Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanVert(), titleTextView);

        TextView messageTextView = (TextView) findViewById(R.id.tv_popup_call_driver_not_assigned_message);
        Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), messageTextView);

        ImageButton closeButton = (ImageButton) findViewById(R.id.ib_popup_call_driver_not_assigned_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(false);
            }
        });

        ImageButton receiptButton = (ImageButton) findViewById(R.id.ib_popup_call_driver_not_assigned_receipt);
        receiptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(true);
            }
        });

        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

    private void sendResult(boolean doReceipt) {
//        SharedPreferences prefs = Utils.getGCMPreferences(this);
//        prefs.edit().putString(Configuration.PROPERTY_CALL_INFO_STATUS, Enums.CallStatus.Cancel).apply();

        Intent intent = new Intent();
        intent.putExtra("doReceipt", doReceipt);
        setResult(Activity.RESULT_OK, intent);
        overridePendingTransition(0, 0);
        finish();
    }
}

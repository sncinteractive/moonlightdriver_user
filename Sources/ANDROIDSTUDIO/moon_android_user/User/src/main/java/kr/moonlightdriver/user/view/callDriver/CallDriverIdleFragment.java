package kr.moonlightdriver.user.view.callDriver;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.loopj.android.http.RequestParams;
import com.skt.Tmap.TMapAddressInfo;
import com.skt.Tmap.TMapData;
import com.skt.Tmap.TMapPoint;
import com.skt.Tmap.TMapPolyLine;
import com.squareup.otto.Subscribe;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import kr.moonlightdriver.user.Adapter.StopByListAdapter;
import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.BusProvider;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.ResultCallback;
import kr.moonlightdriver.user.common.TmapManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.CalculatePriceAck;
import kr.moonlightdriver.user.common.network.NetApi.CallDetailAck;
import kr.moonlightdriver.user.common.network.NetApi.CallDriverAck;
import kr.moonlightdriver.user.common.network.NetApi.NearDriverListAck;
import kr.moonlightdriver.user.common.network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.user.common.network.NetApi.TMapRouteModel;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.common.network.TMapRouteResultCallback;
import kr.moonlightdriver.user.view.main.CallDriverArrivedTargetPopupActivity;
import kr.moonlightdriver.user.view.main.CallDriverNotAssignedPopupActivity;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewmodel.callDriver.CallInfoViewModel;
import kr.moonlightdriver.user.viewmodel.common.ActivityResultEvent;
import kr.moonlightdriver.user.viewmodel.common.CoordinatesData;
import kr.moonlightdriver.user.viewmodel.common.LocationData;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.crackdown.DriverPointViewModel;

public class CallDriverIdleFragment extends Fragment implements OnMapReadyCallback, StopByListAdapter.OnChildViewClickListener {
    public interface CancelCallDriverCallback {
        void OnCallback();
    }

    //region constants
    private static final String TAG = "CallDriverIdleFragment";

    private static final int REQ_SEARCH_LOCATION = 1;
    private static final int REQ_SEARCH_MAP = 2;

    private static final int STOP_BY_COUNT_MAX = 3;
    //endregion constants

    private MainActivity mMainActivity;

    private View mViewMyself;

    // 출발지
    private TextView mSourceTextView;
    private LocationData mSourceLocationData;
    private Marker mSourceMarker;

    // 도착지
    private TextView mTargetTextView;
    private LocationData mTargetLocationData;
    private Marker mTargetMarker;

    // 경유지
    private ImageButton mAddStopByButton;
    private StopByListAdapter mStopByListAdapter;
    private ListView mStopByListView;
    private List<Marker> mStopByMarkerList;

    // 맵
    private GoogleMap mGoogleMap;
    private LatLng mCurrentLocation;
    private Marker mCurrentPositionMarker;
    private Marker mCurrentPositionMarkerInner;
    private List<Marker> mNearDriverMarkerList;
    private boolean mIsVisibleNearDriver;
    private float mPrevZoom;
    private Polyline mPolyline;

    // 대리 호출
    private LinearLayout mCallDriverLayout;
    private TextView mPathInfoTextView;
    private TextView mPayAmountTextView;
    private Spinner mPayAmountSpinner;
    private TextView mPeakTimeTextView;
    private String mPayMethod;
    private CallInfoViewModel mCallInfo;

    // 대리 호출 응답 대기 팝업
    private RelativeLayout mPopupCallDriverWaitAssign;
    private TextView mWaitAssignMessageTextView;
    private List<ImageView> mWaitAssignProgressImageViewList;
    private Handler mWaitAssignTimeHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            mMainActivity = (MainActivity) getActivity();

            initializeView(inflater, container, savedInstanceState);

            mCurrentLocation = MainActivity.mCurrentLocation;

            if (mGoogleMap == null) {
                mPrevZoom = Utils.GOOGLE_MAP_DEFAULT_ZOOM;
            }

            setCurrentLocationToSource();

            setUpMapIfNeeded();

            processIfCallStatusIsWait();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return mViewMyself;
    }

    private void processIfCallStatusIsWait() {
        try {
            SharedPreferences prefs = Utils.getGCMPreferences(getContext());
            @Enums.CallStatus String callStatus = prefs.getString(Configuration.PROPERTY_CALL_INFO_STATUS, null);
            if (callStatus != null && callStatus.equals(Enums.CallStatus.Wait)) {
                String callId = prefs.getString(Configuration.PROPERTY_CALL_INFO_CALL_ID, null);
                if (!Utils.isStringNullOrEmpty(callId)) {
                    RequestParams requestParams = new RequestParams();
                    requestParams.put("callId", callId);

                    mMainActivity.showLoadingAnim();
                    NetClient.send(getContext(), Configuration.URL_CALL_DRIVER_CALL_DETAIL, "POST", requestParams, new CallDetailAck(), new NetResponseCallback(new NetResponse() {
                        @Override
                        public void onResponse(NetAPI _netAPI) {
                            mMainActivity.hideLoadingAnim();

                            if (_netAPI != null) {
                                ResultData resultData = _netAPI.getmResultData();
                                if (resultData.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                                    CommonDialog.showSimpleDialog(getContext(), resultData.getmDetail());
                                    return;
                                }

                                CallDetailAck ack = (CallDetailAck) _netAPI;
                                mCallInfo = ack.getmCallInfo();

                                SharedPreferences prefs = Utils.getGCMPreferences(getContext());
                                showCallDriverWaitAssignPopup(prefs.getInt(Configuration.PROPERTY_CALL_INFO_CALL_RANGE, 0),
                                    prefs.getInt(Configuration.PROPERTY_CALL_INFO_CALL_WAIT_TIME, 0));

                                // set location data
                                mSourceLocationData = new LocationData(mCallInfo.getmStart());
                                mTargetLocationData = new LocationData(mCallInfo.getmEnd());

                                mStopByListAdapter = new StopByListAdapter(mMainActivity, R.layout.row_stopby_list, new ArrayList<LocationData>(STOP_BY_COUNT_MAX));
                                if (mCallInfo.getmThrough() != null && !Utils.isStringNullOrEmpty(mCallInfo.getmThrough().getmAddress())) {
                                    mStopByListAdapter.add(new LocationData(mCallInfo.getmThrough()));
                                }

                                if (mCallInfo.getmThrough1() != null && !Utils.isStringNullOrEmpty(mCallInfo.getmThrough1().getmAddress())) {
                                    mStopByListAdapter.add(new LocationData(mCallInfo.getmThrough1()));
                                }

                                if (mCallInfo.getmThrough2() != null && !Utils.isStringNullOrEmpty(mCallInfo.getmThrough1().getmAddress())) {
                                    mStopByListAdapter.add(new LocationData(mCallInfo.getmThrough2()));
                                }

                                if (mStopByListAdapter.getCount() > 0) {
                                    mStopByListView.setVisibility(View.VISIBLE);
                                }

                                // set location view
                                mSourceTextView.setText(mSourceLocationData.getmLocationName());
                                mTargetTextView.setText(mTargetLocationData.getmLocationName());
                                updateAddStopByButtonEnabled();

                                // update route, marker, call layout, polyline
                                onLocationChanged();
                            }
                        }
                    }));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
//            if (mViewMyself != null) {
//                return;
//            }

            mViewMyself = inflater.inflate(R.layout.fragment_call_driver_idle, container, false);

            // 출발지
            View sourceViewRoot = mViewMyself.findViewById(R.id.layout_source);
            TextView sourceLocationTypeTextView = (TextView) sourceViewRoot.findViewById(R.id.tv_location_type);
            sourceLocationTypeTextView.setText(getString(R.string.common_search_source_title));
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangB(), sourceLocationTypeTextView);

            mSourceTextView = (TextView) sourceViewRoot.findViewById(R.id.tv_location);
            mSourceTextView.setText(getString(R.string.common_search_source));
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), mSourceTextView);

            View sourceSearchLocationLayout = sourceViewRoot.findViewById(R.id.rl_search_keyword);
            sourceSearchLocationLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showSearchLocationFragment("source");
                }
            });

            ImageButton sourceSearchMapButton = (ImageButton) sourceViewRoot.findViewById(R.id.ib_search_map);
            sourceSearchMapButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showSearchMapFragment("source");
                }
            });

            // 도착지
            View targetViewRoot = mViewMyself.findViewById(R.id.layout_target);
            TextView targetLocationTypeTextView = (TextView) targetViewRoot.findViewById(R.id.tv_location_type);
            targetLocationTypeTextView.setText(getString(R.string.common_search_target_title));
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangB(), targetLocationTypeTextView);

            mTargetTextView = (TextView) targetViewRoot.findViewById(R.id.tv_location);
            mTargetTextView.setText(getString(R.string.common_search_target));
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), mTargetTextView);

            View targetSearchLocationLayout = targetViewRoot.findViewById(R.id.rl_search_keyword);
            targetSearchLocationLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showSearchLocationFragment("target");
                }
            });

            ImageButton targetSearchMapButton = (ImageButton) targetViewRoot.findViewById(R.id.ib_search_map);
            targetSearchMapButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showSearchMapFragment("target");
                }
            });

            // 경유지
            mStopByListAdapter = new StopByListAdapter(mMainActivity, R.layout.row_stopby_list, new ArrayList<LocationData>(STOP_BY_COUNT_MAX));
            mStopByListAdapter.setOnChildViewClickListener(this);
            mStopByListView = (ListView) mViewMyself.findViewById(R.id.lv_stopby);
            mStopByListView.setAdapter(mStopByListAdapter);

            // 경유지 추가
            mAddStopByButton = (ImageButton) mViewMyself.findViewById(R.id.ib_add_stopby);
            mAddStopByButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (mStopByListView.getVisibility() != View.VISIBLE) {
                            mStopByListView.setVisibility(View.VISIBLE);
                        }

                        if (mStopByListAdapter.getCount() < STOP_BY_COUNT_MAX) {
                            mStopByListAdapter.add(new LocationData());
                            updateAddStopByButtonEnabled();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            mStopByMarkerList = new ArrayList<>(STOP_BY_COUNT_MAX);

            //region 대리 호출 레이아웃
            mCallDriverLayout = (LinearLayout) mViewMyself.findViewById(R.id.ll_call_driver);
            setCallDriverLayoutVisibility(View.GONE);

            mPathInfoTextView = (TextView) mViewMyself.findViewById(R.id.tv_path_info);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanM(), mPathInfoTextView);

            TextView payAmountTitleTextView = (TextView) mViewMyself.findViewById(R.id.tv_pay_amount_title);
            mPayAmountTextView = (TextView) mViewMyself.findViewById(R.id.tv_pay_amount);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanM(), mPayAmountTextView, payAmountTitleTextView);

            /*
            mPayAmountSpinner = (Spinner) mViewMyself.findViewById(R.id.spinner_pay_amount);
            List<String> payAmountList = new ArrayList<>();
            payAmountList.add("10,000원");
            payAmountList.add("20,000원");
            payAmountList.add("30,000원");
            ArrayAdapter<String> payAmountSpinnerAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item_call_driver_pay_amount, payAmountList);
            payAmountSpinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item_call_driver_pay_amount);
            mPayAmountSpinner.setAdapter(payAmountSpinnerAdapter);
            */

            mPeakTimeTextView = (TextView) mViewMyself.findViewById(R.id.tv_peak_time);
            mPeakTimeTextView.setVisibility(View.GONE);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), mPeakTimeTextView);

            TextView cardPaymentTitleTextView = (TextView) mViewMyself.findViewById(R.id.tv_card_payment_title);
            TextView cashPaymentTitleTextView = (TextView) mViewMyself.findViewById(R.id.tv_cash_payment_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), cardPaymentTitleTextView, cashPaymentTitleTextView);

            final ImageButton cardPaymentButton = (ImageButton) mViewMyself.findViewById(R.id.ib_card_payment);
            final ImageButton cashPaymentButton = (ImageButton) mViewMyself.findViewById(R.id.ib_cash_payment);
            cardPaymentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!cardPaymentButton.isSelected()) {
                        cardPaymentButton.setSelected(true);
                        cashPaymentButton.setSelected(false);
                        mPayMethod = Configuration.PAY_METHOD_CARD;
                    }
                }
            });

            cashPaymentButton.setSelected(false);
            cashPaymentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!cashPaymentButton.isSelected()) {
                        cashPaymentButton.setSelected(true);
                        cardPaymentButton.setSelected(false);
                        mPayMethod = Configuration.PAY_METHOD_CASH;
                    }
                }
            });
            cardPaymentButton.setSelected(true);
            mPayMethod = Configuration.PAY_METHOD_CARD;

            ImageButton callDriverButton = (ImageButton) mViewMyself.findViewById(R.id.ib_call_driver);
            callDriverButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 서버 연동시 수정
//                    testCallDriverResponse();
                    requestCallDriver();
                }
            });
            //endregion 대리 호출 레이아웃

            //region 지도 컨트롤 버튼
            // 현위치로
            ImageButton currentLocationButton = (ImageButton) mViewMyself.findViewById(R.id.ib_current_location);
            currentLocationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mGoogleMap != null) {
                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(MainActivity.mCurrentLocation, Utils.GOOGLE_MAP_DEFAULT_ZOOM));
                    }
                }
            });

            // 주변기사 갱신
            final ImageButton updateDriversButton = (ImageButton) mViewMyself.findViewById(R.id.ib_update_drivers);
            updateDriversButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mGoogleMap != null && mIsVisibleNearDriver) {
                        float distance = Utils.getGoogleMapScreenSize(mGoogleMap.getProjection().getVisibleRegion().latLngBounds);
                        requestNearDriverList(mCurrentLocation, distance);
                    }
                }
            });

            // 줌인
            ImageButton zoomInButton = (ImageButton) mViewMyself.findViewById(R.id.ib_zoom_in);
            zoomInButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mGoogleMap != null) {
                        mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
                    }
                }
            });

            // 줌아웃
            ImageButton zoomOutButton = (ImageButton) mViewMyself.findViewById(R.id.ib_zoom_out);
            zoomOutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mGoogleMap != null) {
                        mGoogleMap.animateCamera(CameraUpdateFactory.zoomOut());
                    }
                }
            });

            // 주변기사 Visibility 토글
            mNearDriverMarkerList = new ArrayList<>();
            mIsVisibleNearDriver = false;
            ImageButton nearDriverVisibilityButton = (ImageButton) mViewMyself.findViewById(R.id.ib_toggle_driver_visibility);
            nearDriverVisibilityButton.setSelected(mIsVisibleNearDriver);
            nearDriverVisibilityButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mIsVisibleNearDriver = !mIsVisibleNearDriver;
                    v.setSelected(mIsVisibleNearDriver);

                    if (mNearDriverMarkerList.isEmpty()) {
                        if (mGoogleMap != null && mIsVisibleNearDriver) {
                            float distance = Utils.getGoogleMapScreenSize(mGoogleMap.getProjection().getVisibleRegion().latLngBounds);
                            requestNearDriverList(mCurrentLocation, distance);
                        }
                    } else {
                        for (Marker marker : mNearDriverMarkerList) {
                            marker.setVisible(mIsVisibleNearDriver);
                        }
                    }
                }
            });
            //endregion 지도 컨트롤

            //region popup call driver wait assign
            mPopupCallDriverWaitAssign = (RelativeLayout) mViewMyself.findViewById(R.id.popup_call_driver_wait_assign);
            mPopupCallDriverWaitAssign.setVisibility(View.GONE);

            mWaitAssignMessageTextView = (TextView) mViewMyself.findViewById(R.id.tv_popup_call_driver_wait_assign_message);

            LinearLayout waitRequestProgressLayout = (LinearLayout) mViewMyself.findViewById(R.id.ll_popup_call_driver_wait_assign_progress);
            int childCount = waitRequestProgressLayout.getChildCount();
            mWaitAssignProgressImageViewList = new ArrayList<>(childCount);
            for (int i = 0; i < childCount; i++) {
                ImageView childImageView = (ImageView) waitRequestProgressLayout.getChildAt(i);
                mWaitAssignProgressImageViewList.add(childImageView);
            }

            ImageButton cancelCallDriverButton = (ImageButton) mViewMyself.findViewById(R.id.ib_popup_call_driver_wait_assign_cancel);
            cancelCallDriverButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestCancelCallDriver(null);
                }
            });

            ImageButton closeCallDriverPopupButton = (ImageButton) mViewMyself.findViewById(R.id.ib_popup_call_driver_wait_assign_close);
            closeCallDriverPopupButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestCancelCallDriver(null);
                }
            });
            //endregion popup call driver wait assign
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateAddStopByButtonEnabled() {
        try {
            if (mStopByListAdapter.getCount() >= STOP_BY_COUNT_MAX) {
                mAddStopByButton.setEnabled(false);
            } else {
                mAddStopByButton.setEnabled(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showSearchLocationFragment(String locationType) {
        try {
            Bundle bundle = new Bundle();
            bundle.putString("locationType", locationType);

            SearchLocationFragment fragment = new SearchLocationFragment();
            fragment.setArguments(bundle);
            fragment.setTargetFragment(this, REQ_SEARCH_LOCATION);

            mMainActivity.addFragment(fragment);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showSearchMapFragment(String locationType) {
        try {
            Bundle bundle = new Bundle();
            bundle.putString("locationType", locationType);

            MapSearchLocationFragment fragment = new MapSearchLocationFragment();
            fragment.setArguments(bundle);
            fragment.setTargetFragment(this, REQ_SEARCH_MAP);

            mMainActivity.addFragment(fragment);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setCurrentLocationToSource() {
        try {
            TmapManager tmapManager = new TmapManager();
            tmapManager.convertGpsToAddress(mCurrentLocation, new ResultCallback() {
                @Override
                public void resultCallback(HashMap<String, Object> params) {
                    try {
                        TMapAddressInfo addressInfo = (TMapAddressInfo) params.get("addressInfo");
                        if (addressInfo != null) {
                            if (mSourceLocationData == null) {
                                mSourceLocationData = new LocationData();
                            }
                            mSourceLocationData.setmLocationName(addressInfo.strBuildingName);
                            mSourceLocationData.setmAddress(addressInfo.strFullAddress);
                            mSourceLocationData.setmLocation(mCurrentLocation);

                            updateSourceTextViewText();

                            onLocationChanged();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateSourceTextViewText() {
        try {
            if (!Utils.isStringNullOrEmpty(mSourceLocationData.getmLocationName())) {
                mSourceTextView.setText(mSourceLocationData.getmLocationName());
            } else {
                mSourceTextView.setText(mSourceLocationData.getmAddress());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateTargetTextViewText() {
        try {
            if (!Utils.isStringNullOrEmpty(mTargetLocationData.getmLocationName())) {
                mTargetTextView.setText(mTargetLocationData.getmLocationName());
            } else {
                mTargetTextView.setText(mTargetLocationData.getmAddress());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpMapIfNeeded() {
        try {
            if (mGoogleMap == null) {
                SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_google_map);
                mapFragment.getMapAsync(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeMyLocation() {
        try {
            Utils.log("error", TAG, "===== changeMyLocation() =====");
            if (mGoogleMap != null) {
                mCurrentLocation = MainActivity.mCurrentLocation;

                if (mCurrentPositionMarker != null) {
                    mCurrentPositionMarker.setPosition(mCurrentLocation);
                }

                if (mCurrentPositionMarkerInner != null) {
                    mCurrentPositionMarkerInner.setPosition(mCurrentLocation);
                }

                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(mCurrentLocation));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            mGoogleMap = googleMap;

            mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
            mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
            mGoogleMap.getUiSettings().setCompassEnabled(false);

            mCurrentPositionMarker = mGoogleMap.addMarker(new MarkerOptions().position(mCurrentLocation)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_current_location_outer))
                .zIndex(1f)
                .anchor(0.5f, 0f));
            mCurrentPositionMarkerInner = mGoogleMap.addMarker(new MarkerOptions().position(mCurrentLocation)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_current_location_inner))
                .zIndex(1f)
                .anchor(0.5f, -0.3739f));
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCurrentLocation, Utils.GOOGLE_MAP_DEFAULT_ZOOM));

            mGoogleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {
                    if (mPrevZoom != mGoogleMap.getCameraPosition().zoom) {
                        resetDriverMarkersIcon();
                        mPrevZoom = mGoogleMap.getCameraPosition().zoom;
                    }
                }
            });

            PolylineOptions polylineOptions = new PolylineOptions();
            polylineOptions.width(15f); // default width 10
            mPolyline = mGoogleMap.addPolyline(polylineOptions);
            mPolyline.setVisible(false);

            mMainActivity.removeSplash();

            SharedPreferences prefs = Utils.getGCMPreferences(getContext());
            @Enums.CallStatus String callStatus = prefs.getString(Configuration.PROPERTY_CALL_INFO_STATUS, null);
            if (!Utils.isStringNullOrEmpty(callStatus) && callStatus.equals(Enums.CallStatus.End)) {
                Intent intent = new Intent(getContext(), CallDriverArrivedTargetPopupActivity.class);
                startActivityForResult(intent, Configuration.CALL_DRIVER_ARRIVED_TARGET_POPUP_REQ_CODE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == Activity.RESULT_OK) {
                switch (requestCode) {
                    case REQ_SEARCH_LOCATION:
                        // fall through
                    case REQ_SEARCH_MAP: {
                        String locationType = data.getStringExtra("locationType");
                        LocationData locationData = data.getParcelableExtra("locationData");

                        if (locationType.equals("source")) {
                            mSourceLocationData = locationData;
                            updateSourceTextViewText();
                        } else if (locationType.equals("target")) {
                            mTargetLocationData = locationData;
                            updateTargetTextViewText();
                        } else if (locationType.startsWith("stopby")) {
                            int index = Integer.parseInt(locationType.replaceAll("\\D", ""));
                            LocationData stopbyLocationData = mStopByListAdapter.getItem(index);
                            if (stopbyLocationData != null) {
                                stopbyLocationData.set(new LatLng(locationData.getmLocation().latitude, locationData.getmLocation().longitude), locationData.getmLocationName(), locationData.getmAddress());
                                mStopByListAdapter.notifyDataSetChanged();
                            }
                        }

                        onLocationChanged();
                    }
                    break;
                    case Configuration.POSITION_CHANGED: {
                        changeMyLocation();
                    }
                    break;
                    case Configuration.CALL_DRIVER_ARRIVED_TARGET_POPUP_REQ_CODE: {
                        Utils.log("error", TAG, "===== OnActivityResult - CALL_DRIVER_ARRIVED_TARGET_POPUP_REQ_CODE =====");
                    }
                    break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onActivityResult(ActivityResultEvent _activityResultEvent) {
        try {
            onActivityResult(_activityResultEvent.getRequestCode(), _activityResultEvent.getResultCode(), _activityResultEvent.getData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        BusProvider.getInstance().unregister(this);

        if (mWaitAssignTimeHandler != null) {
            mWaitAssignTimeHandler.removeCallbacksAndMessages(null);
        }

        super.onDestroyView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BusProvider.getInstance().register(this);
    }

    //region stopby list item click event
    @Override
    public void onSearchKeywordClick(int position) {
        showSearchLocationFragment("stopby" + position);
    }

    @Override
    public void onSearchMapClick(int position) {
        showSearchMapFragment("stopby" + position);
    }

    @Override
    public void onRemoveClick(int position) {
        try {
            if (mStopByListAdapter.getCount() == 0) {
                mStopByListView.setVisibility(View.GONE);
            }

            updateAddStopByButtonEnabled();

            onLocationChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //endregion stopby list item click event

    private void onLocationChanged() {
        try {
            if (hasValidPath()) {
                setCallDriverLayoutVisibility(View.VISIBLE);
                updateTMapRoute();
            } else {
                setCallDriverLayoutVisibility(View.GONE);
                setPolylineVisibility(false);
            }

            updatePathMarkers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //region unused tmap api..

    /**
     * 실시간 자동차 경로 요청 및 처리하는 함수.
     * TMap SDK API. 총 거리 및 출발지, 도착지, 경유지에 대한 경로를 제공하나 소요시간을 제공하지 않음.
     * 소요시간을 포함한 처리는 updateTMapRoute를 사용.
     */
    private void requestPathData() {
        try {
            mMainActivity.showLoadingAnim();

            TMapPoint source = Utils.convertLatLngToTMapPoint(mSourceLocationData.getmLocation());
            TMapPoint target = Utils.convertLatLngToTMapPoint(mTargetLocationData.getmLocation());
            ArrayList<TMapPoint> stopbyList = null;
            if (mStopByListAdapter.getCount() > 0) {
                stopbyList = new ArrayList<>(mStopByListAdapter.getCount());
                for (int i = 0; i < mStopByListAdapter.getCount(); i++) {
                    stopbyList.add(Utils.convertLatLngToTMapPoint(mStopByListAdapter.getItem(i).getmLocation()));
                }
            }

            TMapData tMapData = new TMapData();
            tMapData.findPathDataWithType(TMapData.TMapPathType.CAR_PATH, source, target, stopbyList, 0, new TMapData.FindPathDataListenerCallback() {
                @Override
                public void onFindPathData(final TMapPolyLine tMapPolyLine) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mMainActivity.hideLoadingAnim();

                            updateCallDriverLayout(tMapPolyLine.getDistance(), 0);

                            setPolylinePointsWithTMapPointList(tMapPolyLine.getLinePoint());
                            setPolylineVisibility(true);

                            updateCameraBounds();
                        }
                    });

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            mMainActivity.hideLoadingAnim();
        }
    }

    /**
     * 타임머신 자동차 경로 요청 및 처리하는 함수. 실시간이 아닌 통계치임.
     * TMap SDK API. 총 거리 및 소요시간, 출발지, 도착지, 경유지에 대한 경로 제공.
     * 소요시간을 포함한 실시간 처리는 updateTMapRoute를 사용.
     */
    private void requestTimeMachinePathData() {
        try {
            mMainActivity.showLoadingAnim();

            ArrayList<TMapPoint> stopbyList = null;
            if (mStopByListAdapter.getCount() > 0) {
                stopbyList = new ArrayList<>(mStopByListAdapter.getCount());
                for (int i = 0; i < mStopByListAdapter.getCount(); i++) {
                    stopbyList.add(Utils.convertLatLngToTMapPoint(mStopByListAdapter.getItem(i).getmLocation()));
                }
            }

            TMapData tmapdata = new TMapData();

            HashMap<String, String> pathInfo = new HashMap<String, String>();
            pathInfo.put("rStName", mSourceLocationData.getmLocationName());
            pathInfo.put("rStlat", Double.toString(mSourceLocationData.getmLocation().latitude));
            pathInfo.put("rStlon", Double.toString(mSourceLocationData.getmLocation().longitude));
            pathInfo.put("rGoName", mTargetLocationData.getmLocationName());
            pathInfo.put("rGolat", Double.toString(mTargetLocationData.getmLocation().latitude));
            pathInfo.put("rGolon", Double.toString(mTargetLocationData.getmLocation().longitude));
            pathInfo.put("type", "arrival");

            tmapdata.findTimeMachineCarPath(pathInfo, new Date(), stopbyList, new TMapData.FindTimeMachineCarPathListenerCallback() {
                @Override
                public void onFindTimeMachineCarPath(final Document document) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mMainActivity.hideLoadingAnim();

                            XPath xPath = XPathFactory.newInstance().newXPath();

                            // total distance
                            double totalDistance = 0D;
                            try {
                                Node node = (Node) xPath.evaluate("/kml/Document/totalDistance", document, XPathConstants.NODE);
                                if (node != null) {
                                    totalDistance = Double.parseDouble(node.getTextContent());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            // total time
                            double totalTime = 0D;
                            try {
                                Node node = (Node) xPath.evaluate("/kml/Document/totalTime", document, XPathConstants.NODE);
                                if (node != null) {
                                    totalTime = Double.parseDouble(node.getTextContent());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            // coordinates
                            NodeList coordinateList = null;
                            try {
                                coordinateList = (NodeList) xPath.evaluate("/kml/Document/Placemark[styleUrl='#lineStyle']/LineString/coordinates", document, XPathConstants.NODESET);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            updateCallDriverLayout(totalDistance, totalTime);

                            setPolylinePointsWithNodeList(coordinateList);
                            setPolylineVisibility(true);

                            updateCameraBounds();
                        }
                    });

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            mMainActivity.hideLoadingAnim();
        }
    }
    //endregion unused tmap api..

    private void updateTMapRoute() {
        try {
            mMainActivity.showLoadingAnim();

            List<LatLng> stopbyList = null;
            int stopbyCount = mStopByListAdapter.getCount();
            if (stopbyCount > 0) {
                stopbyList = new ArrayList<>(stopbyCount);
                for (int i = 0; i < stopbyCount; i++) {
                    LocationData locationData = mStopByListAdapter.getItem(i);
                    if (locationData == null) {
                        continue;
                    }

                    LatLng location = locationData.getmLocation();
                    if (location == null) {
                        continue;
                    }
                    stopbyList.add(location);
                }
            }

            NetClient.requestTMapRoute(getContext(), mSourceLocationData.getmLocation(), mTargetLocationData.getmLocation(), stopbyList, new TMapRouteResultCallback() {
                @Override
                public void onResult(boolean success, TMapRouteModel model) {
                    if (success) {
                        updateCallDriverLayout(model.getTotalDistance(), model.getTotalTime());
                        setPolylinePoints(model.getPointList());
                        setPolylineVisibility(true);
                        updateCameraBounds();
                    } else {
                        setPolylineVisibility(false);
                        setCallDriverLayoutVisibility(View.GONE);
                    }

                    mMainActivity.hideLoadingAnim();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            mMainActivity.hideLoadingAnim();
        }
    }

    private ArrayList<String> getStopByAddressList() {
        ArrayList<String> stopbyList = new ArrayList<>();

        try {

        } catch (Exception e) {
            e.printStackTrace();
        }

        return stopbyList;
    }

    private void setPolylinePointsWithTMapPointList(List<TMapPoint> tMapPointList) {
        try {
            mPolyline.setPoints(Utils.convertTMapPointListToLatLngList(tMapPointList));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setPolylinePointsWithNodeList(NodeList coordinateList) {
        try {
            mPolyline.setPoints(Utils.parseCoordinateListToLatLngList(coordinateList));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setPolylinePoints(List<LatLng> latLngList) {
        try {
            mPolyline.setPoints(latLngList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setPolylineVisibility(boolean visible) {
        mPolyline.setVisible(visible);
    }

    private boolean hasValidPath() {
        try {
            if (mSourceLocationData == null || mTargetLocationData == null) {
                return false;
            }

            // check duplicated
            Set<String> hashSet = new HashSet<>();
            hashSet.add(mSourceLocationData.getmAddress());
            if (!hashSet.add(mTargetLocationData.getmAddress())) {
                return false;
            }

            int stopbyCount = mStopByListAdapter.getCount();
            for (int i = 0; i < stopbyCount; i++) {
                LocationData stopby = mStopByListAdapter.getItem(i);
                if (stopby == null) {
                    continue;
                }

                String address = stopby.getmAddress();
                if (Utils.isStringNullOrEmpty(address)) {
                    continue;
                }

                if (!hashSet.add(stopby.getmAddress())) {
                    return false;
                }
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    //region call driver layout
    private void updateCallDriverLayout(double totalDistance, double totalTime) {
        mPathInfoTextView.setText(getString(R.string.call_driver_path_info_format, Utils.convertDistanceString(totalDistance), Utils.convertTimeString(totalTime)));

        requestPriceCalculate();
    }

    private void requestPriceCalculate() {
        try {
            RequestParams params = new RequestParams();
            params.add("startAddress", mSourceLocationData.getmAddress());
            params.add("endAddress", mTargetLocationData.getmAddress());
            params.add("stopByAddress", "test");

            NetClient.send(mMainActivity, Configuration.URL_CALL_DRIVER_CALCULATE_PRICE, "POST", params, new CalculatePriceAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    try {
                        if (_netAPI != null) {
                            CalculatePriceAck retNetApi = (CalculatePriceAck) _netAPI;
                            ResultData result = retNetApi.getmResultData();

                            boolean isPeakTime = false;
                            int payAmount = Configuration.DEFAULT_PRICE;
                            int extraCharge = Configuration.DEFAULT_EXTRA_PRICE;

                            if (result.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                                payAmount = retNetApi.getmPrice();

                                isPeakTime = retNetApi.getmPeakYn().equals("Y");
                                if (isPeakTime) {
                                    extraCharge = retNetApi.getmExtraPrice();
                                } else {
                                    extraCharge = 0;
                                }
                            }

                            mPayAmountTextView.setText(Utils.convertToPriceString(payAmount));
                            mPeakTimeTextView.setVisibility(isPeakTime ? View.VISIBLE : View.GONE);
                            mPeakTimeTextView.setText(getString(R.string.call_driver_peak_time_message_format, Utils.convertToPriceString(extraCharge)));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setCallDriverLayoutVisibility(int visibility) {
        mCallDriverLayout.setVisibility(visibility);
    }

    private void requestCallDriver() {
        try {
            mMainActivity.showLoadingAnim();

            TmapManager tmapManager = new TmapManager();
            tmapManager.convertGpsToAddress(MainActivity.mCurrentLocation, new ResultCallback() {
                @Override
                public void resultCallback(HashMap<String, Object> params) {
                    try {
                        TMapAddressInfo addressInfo = (TMapAddressInfo) params.get("addressInfo");
                        if (addressInfo != null) {
                            RequestParams requestParams = new RequestParams();
                            requestParams.put("uniqueId", UUID.randomUUID().toString());
                            requestParams.put("userId", mMainActivity.mUserDetail.getUserId());
                            requestParams.put("user_address", addressInfo.strFullAddress);
                            requestParams.put("user_city_do", addressInfo.strCity_do);
                            requestParams.put("user_gu_gun", addressInfo.strGu_gun);
                            requestParams.put("user_lat", MainActivity.mCurrentLocation.latitude);
                            requestParams.put("user_lng", MainActivity.mCurrentLocation.longitude);
                            requestParams.put("start_address", mSourceLocationData.getmAddress());
                            requestParams.put("start_lat", mSourceLocationData.getmLocation().latitude);
                            requestParams.put("start_lng", mSourceLocationData.getmLocation().longitude);
                            requestParams.put("end_address", mTargetLocationData.getmAddress());
                            requestParams.put("end_lat", mTargetLocationData.getmLocation().latitude);
                            requestParams.put("end_lng", mTargetLocationData.getmLocation().longitude);

                            for (int i = 0; i < mStopByListAdapter.getCount(); i++) {
                                if (i == 0) {
                                    requestParams.put("through_address", mStopByListAdapter.getItem(i).getmAddress());
                                    requestParams.put("through_lat", mStopByListAdapter.getItem(i).getmLocation().latitude);
                                    requestParams.put("through_lng", mStopByListAdapter.getItem(i).getmLocation().longitude);
                                } else {
                                    requestParams.put("through" + i + "_address", mStopByListAdapter.getItem(i).getmAddress());
                                    requestParams.put("through" + i + "_lat", mStopByListAdapter.getItem(i).getmLocation().latitude);
                                    requestParams.put("through" + i + "_lng", mStopByListAdapter.getItem(i).getmLocation().longitude);
                                }
                            }

                            requestParams.put("money", Utils.convertToPriceValue(mPayAmountTextView.getText().toString()));
                            requestParams.put("callType", "normal");
                            requestParams.put("etc", "");
                            requestParams.put("payment_option", mPayMethod);
                            requestParams.put("status", "wait");

                            // url 변경 필요
                            NetClient.send(getContext(), Configuration.URL_CALL_DRIVER_CALL, "POST", requestParams, new CallDriverAck(), new NetResponseCallback(new CallDriverResponse()));
                        } else {
                            mMainActivity.hideLoadingAnim();
                            CommonDialog.showSimpleDialog(mMainActivity, "현위치 조회에 실패했습니다. 다시 시도해 주세요.");
                            return;
                        }
                    } catch (Exception e) {
                        mMainActivity.hideLoadingAnim();
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            mMainActivity.hideLoadingAnim();
            e.printStackTrace();
        }
    }

    private class CallDriverResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            try {
                mMainActivity.hideLoadingAnim();

                if (_netAPI != null) {
                    ResultData resultData = _netAPI.getmResultData();
                    if (resultData.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                        CommonDialog.showSimpleDialog(mMainActivity, resultData.getmDetail());
                        return;
                    }

                    CallDriverAck ack = (CallDriverAck) _netAPI;
                    showCallDriverWaitAssignPopup(ack.getmRange(), ack.getmTime());

                    mCallInfo = ack.getmCallInfo();

                    SharedPreferences prefs = Utils.getGCMPreferences(getActivity());
                    SharedPreferences.Editor editor = prefs.edit();

                    editor.putString(Configuration.PROPERTY_CALL_INFO_STATUS, Enums.CallStatus.Wait);

                    editor.putString(Configuration.PROPERTY_CALL_INFO_SOURCE_LOCATION, mSourceLocationData.toString());
                    editor.putString(Configuration.PROPERTY_CALL_INFO_TARGET_LOCATION, mTargetLocationData.toString());
                    List<LocationData> stopbyLocationDataList = new ArrayList<>();
                    for (int i = 0; i < mStopByListAdapter.getCount(); i++) {
                        stopbyLocationDataList.add(mStopByListAdapter.getItem(i));
                    }
                    editor.putString(Configuration.PROPERTY_CALL_INFO_STOPBY_LOCATION_LIST, Utils.parseStopByLocationDataListToJSONString(stopbyLocationDataList));
                    editor.putString(Configuration.PROPERTY_CALL_INFO_PAYMENT_OPTION, mPayMethod);

                    editor.putInt(Configuration.PROPERTY_CALL_INFO_CALL_RANGE, ack.getmRange());
                    editor.putInt(Configuration.PROPERTY_CALL_INFO_CALL_WAIT_TIME, ack.getmTime());

                    editor.apply();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void testCallDriverResponse() {
        mMainActivity.showLoadingAnim();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                CallDriverAck successAck = new CallDriverAck(new ResultData(Enums.NetResultCode.SUCCESS.getValue(), null, null), 1, 10, 5);
                CallDriverAck failAck = new CallDriverAck(new ResultData(Enums.NetResultCode.SUCCESS.getValue(), null, null), 1, 10, 5);
                new CallDriverResponse().onResponse(successAck);
            }
        }, 500);
    }
    //endregion call driver layout

    //region call driver wait assign popup
    private void showCallDriverWaitAssignPopup(int range, int time) {
        try {
            mMainActivity.setTabMenuVisibility(View.GONE);

            mPopupCallDriverWaitAssign.setVisibility(View.VISIBLE);

            mWaitAssignMessageTextView.setText(getString(R.string.popup_call_driver_wait_assign_message_format, range, time));

            for (ImageView child : mWaitAssignProgressImageViewList) {
                AnimationDrawable animationDrawable = (AnimationDrawable) child.getDrawable();
                animationDrawable.start();
            }

            if (mWaitAssignTimeHandler == null) {
                mWaitAssignTimeHandler = new Handler();
            }

            mWaitAssignTimeHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    requestCancelCallDriver(new CancelCallDriverCallback() {
                        @Override
                        public void OnCallback() {
                            setCallDriverLayoutVisibility(View.GONE);

                            Intent intent = new Intent(CallDriverIdleFragment.this.getContext(), CallDriverNotAssignedPopupActivity.class);
                            getActivity().startActivityForResult(intent, Configuration.CALL_DRIVER_NOT_ASSIGNED_POPUP_REQ_CODE);
                        }
                    });
                }
            }, 1000 * 60 * 5);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideCallDriverWaitAssignPopup() {
        try {
            for (ImageView child : mWaitAssignProgressImageViewList) {
                AnimationDrawable animationDrawable = (AnimationDrawable) child.getDrawable();
                animationDrawable.stop();
            }

            mMainActivity.setTabMenuVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mPopupCallDriverWaitAssign.setVisibility(View.GONE);
    }

    private void requestCancelCallDriver(final CancelCallDriverCallback callback) {
        try {
            if (mCallInfo == null) {
                CommonDialog.showSimpleDialog(getContext(), "호출 취소할 콜이 없습니다.");
                return;
            }

            mMainActivity.showLoadingAnim();

            RequestParams requestParams = new RequestParams();
            requestParams.put("userId", mMainActivity.mUserDetail.getUserId());
            requestParams.put("callId", mCallInfo.getmCallId());

            NetClient.send(getContext(), Configuration.URL_CALL_DRIVER_CANCEL_CALL, "POST", requestParams, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    mMainActivity.hideLoadingAnim();

                    if (_netAPI != null) {
                        ResultData resultData = _netAPI.getmResultData();
                        if (resultData.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                            CommonDialog.showSimpleDialog(getContext(), resultData.getmDetail());
                            return;
                        }

                        hideCallDriverWaitAssignPopup();

                        setCallDriverLayoutVisibility(View.VISIBLE);

                        mCallInfo = null;

                        if (callback != null) {
                            callback.OnCallback();
                        }
                    }
                }
            }));
        } catch (Exception e) {
            mMainActivity.hideLoadingAnim();
            e.printStackTrace();
        }
    }
    //endregion call driver wait assign popup

    //region near drivers
    private void requestNearDriverList(LatLng location, float range) {
        try {
            // 서버 연동시 수정 필요
//            testFindDriverResponse();
            RequestParams params = new RequestParams();
            params.put("lat", Double.toString(location.latitude));
            params.put("lng", Double.toString(location.longitude));
            params.put("range", Float.toString(range));

            NetClient.send(getContext(), Configuration.URL_DRIVER_FIND, "POST", params, new NearDriverListAck(), new NetResponseCallback(new FindDriverResponse()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class FindDriverResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            try {
                removeDriverMarkers();

                if (_netAPI != null) {
                    ResultData resultData = _netAPI.getmResultData();
                    if (resultData.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                        NearDriverListAck retNetApi = (NearDriverListAck) _netAPI;
                        ArrayList<DriverPointViewModel> retDriverList = retNetApi.getmDriverPointViewModel();

                        for (DriverPointViewModel driverPoint : retDriverList) {
                            String markerText = driverPoint.getmPhoneNumber().substring(driverPoint.getmPhoneNumber().length() - 4, driverPoint.getmPhoneNumber().length()) + " 기사님";
                            LatLng markerPosition = new LatLng(driverPoint.getmLocation().getmCoordinates().get(1), driverPoint.getmLocation().getmCoordinates().get(0));
                            Marker marker = mGoogleMap.addMarker(new MarkerOptions().position(markerPosition).title(markerText).visible(mIsVisibleNearDriver));
                            marker.setIcon(Utils.getDriverIconByZoom(mGoogleMap.getCameraPosition().zoom));
                            mNearDriverMarkerList.add(marker);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void testFindDriverResponse() {
        Random random = new Random(new Date().getTime());

        ArrayList<DriverPointViewModel> list = new ArrayList<>();
        int loopCount = random.nextInt(20 - 10 + 1) + 10;

        for (int i = 0; i < loopCount; ++i) {
            DriverPointViewModel model = new DriverPointViewModel();

            model.setmDriverId("driver" + i);

            int phoneNumber = random.nextInt(9999 - 1111 + 1) + 1111;
            model.setmPhoneNumber(Integer.toString(phoneNumber));

            Double randomLat = mCurrentLocation.latitude + random.nextDouble() * 0.001D + 0.0005D;
            Double randomLng = mCurrentLocation.longitude + random.nextDouble() * 0.001D + 0.0005D;
            model.setmLocation(new CoordinatesData(new ArrayList<>(Arrays.asList(randomLng, randomLat)), null));

            list.add(model);
        }

        NearDriverListAck successAck = new NearDriverListAck(new ResultData(Enums.NetResultCode.SUCCESS.getValue(), null, null), list);
        new FindDriverResponse().onResponse(successAck);
    }

    private void removeDriverMarkers() {
        try {
            if (mNearDriverMarkerList != null && mNearDriverMarkerList.size() > 0) {
                for (Marker marker : mNearDriverMarkerList) {
                    marker.remove();
                }

                mNearDriverMarkerList.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetDriverMarkersIcon() {
        try {
            if (mNearDriverMarkerList != null && mNearDriverMarkerList.size() > 0) {
                for (Marker marker : mNearDriverMarkerList) {
                    marker.setIcon(Utils.getDriverIconByZoom(mGoogleMap.getCameraPosition().zoom));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //endregion near drivers

    private void updatePathMarkers() {
        updateSourceMarker();
        updateTargetMarker();
        updateStopbyMarkerList();

        updateCameraBounds();
    }

    private void updateSourceMarker() {
        try {
            if (mSourceLocationData == null) {
                if (mSourceMarker != null) {
                    mSourceMarker.setVisible(false);
                }
                return;
            }
            if (mSourceMarker == null) {
                mSourceMarker = mGoogleMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_source))
                    .position(mSourceLocationData.getmLocation()));
            } else {
                mSourceMarker.setPosition(mSourceLocationData.getmLocation());
                mSourceMarker.setVisible(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateTargetMarker() {
        try {
            if (mTargetLocationData == null) {
                if (mTargetMarker != null) {
                    mTargetMarker.setVisible(false);
                }
                return;
            }

            if (mTargetMarker == null) {
                mTargetMarker = mGoogleMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_target))
                    .position(mTargetLocationData.getmLocation()));
            } else {
                mTargetMarker.setPosition(mTargetLocationData.getmLocation());
                mTargetMarker.setVisible(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateStopbyMarkerList() {
        try {
            for (Marker marker : mStopByMarkerList) {
                marker.setVisible(false);
            }

            for (int i = 0; i < mStopByListAdapter.getCount(); i++) {
                LatLng location = mStopByListAdapter.getItem(i).getmLocation();
                if (location == null) {
                    continue;
                }

                Marker marker;
                if (mStopByMarkerList.size() < mStopByListAdapter.getCount()) {
                    marker = mGoogleMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_stopby))
                        .position(location));
                    mStopByMarkerList.add(0, marker);
                } else {
                    marker = mStopByMarkerList.get(i);
                    marker.setPosition(location);
                }
                marker.setVisible(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateCameraBounds() {
        List<LatLng> latLngList = new ArrayList<>();

        if (mSourceLocationData != null) {
            latLngList.add(mSourceLocationData.getmLocation());
        }

        if (mTargetLocationData != null) {
            latLngList.add(mTargetLocationData.getmLocation());
        }

        for (int i = 0; i < mStopByListAdapter.getCount(); i++) {
            LocationData locationData = mStopByListAdapter.getItem(i);
            if (locationData.getmLocation() != null) {
                latLngList.add(locationData.getmLocation());
            }
        }

        if (latLngList.size() >= 2) {
            LatLngBounds.Builder builder = LatLngBounds.builder();
            for (LatLng latLng : latLngList) {
                builder = builder.include(latLng);
            }

            int padding = 210;
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), padding));
        }
    }

    public void initLocationDataAndView() {
        try {
            mSourceLocationData = null;
            mTargetLocationData = null;
            mStopByListAdapter.clear();
            mStopByListAdapter = null;

            mSourceTextView.setText(getString(R.string.common_search_source));
            mTargetTextView.setText(getString(R.string.common_search_target));
            mStopByListView.setAdapter(null);
            mStopByListView.setVisibility(View.GONE);
            updateAddStopByButtonEnabled();

            mSourceMarker.remove();
            mTargetMarker.remove();
            for (Marker marker : mStopByMarkerList) {
                marker.remove();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

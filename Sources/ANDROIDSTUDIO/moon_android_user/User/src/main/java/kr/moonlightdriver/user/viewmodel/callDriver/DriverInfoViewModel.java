package kr.moonlightdriver.user.viewmodel.callDriver;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.android.gms.maps.model.LatLng;

import kr.moonlightdriver.user.common.Utils;

/**
 * Created by youngmin on 2016-06-21.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DriverInfoViewModel implements Parcelable {
    @JsonProperty("driverId")
    private String mDriverId;

    @JsonProperty("driverName")
    private String mDriverName;

    @JsonProperty("phone")
    private String mDriverPhoneNumber;

    @JsonProperty("photo")
    private String mDriverPhoto;

    @JsonProperty("starnum")
    private float mDriverGradeAverage;

    @JsonProperty("lat")
    private double mLat;

    @JsonProperty("lng")
    private double mLng;

    private LatLng mLocation;

    public DriverInfoViewModel() {
    }

    public DriverInfoViewModel(String value) {
        if (!Utils.isStringNullOrEmpty(value)) {
            String[] values = value.split(",");
            if (values.length == 6) {
                mDriverName = values[0];
                mDriverPhoneNumber = values[1];
                mDriverPhoto = values[2];
                mDriverGradeAverage = Float.parseFloat(values[3]);
                mLat = Double.parseDouble(values[4]);
                mLng = Double.parseDouble(values[5]);
            }
        }
    }

    public DriverInfoViewModel(String driverName, String driverPhoneNumber, String driverPhoto, float driverGradeAverage, double lat, double lng) {
        mDriverName = driverName;
        mDriverPhoneNumber = driverPhoneNumber;
        mDriverPhoto = driverPhoto;
        mDriverGradeAverage = driverGradeAverage;
        mLat = lat;
        mLng = lng;
    }

    public String getmDriverId() {
        return mDriverId;
    }

    public void setmDriverId(String mDriverId) {
        this.mDriverId = mDriverId;
    }

    public void setmLocation(LatLng mLocation) {
        this.mLocation = mLocation;
    }

    public String getmDriverName() {
        return mDriverName;
    }

    public void setmDriverName(String mDriverName) {
        this.mDriverName = mDriverName;
    }

    public String getmDriverPhoneNumber() {
        return mDriverPhoneNumber;
    }

    public void setmDriverPhoneNumber(String mDriverPhoneNumber) {
        this.mDriverPhoneNumber = mDriverPhoneNumber;
    }

    public String getmDriverPhoto() {
        return mDriverPhoto;
    }

    public void setmDriverPhoto(String mDriverPhoto) {
        this.mDriverPhoto = mDriverPhoto;
    }

    public float getmDriverGradeAverage() {
        return mDriverGradeAverage;
    }

    public void setmDriverGradeAverage(float mDriverGradeAverage) {
        this.mDriverGradeAverage = mDriverGradeAverage;
    }

    public double getmLat() {
        return mLat;
    }

    public void setmLat(double mLat) {
        this.mLat = mLat;
    }

    public double getmLng() {
        return mLng;
    }

    public void setmLng(double mLng) {
        this.mLng = mLng;
    }

    public LatLng getmLocation() {
        if (this.mLocation == null) {
            this.mLocation = new LatLng(this.getmLat(), this.getmLng());
        }

        return this.mLocation;
    }

    public String getMaskedDriverName() {
        if (Utils.isStringNullOrEmpty(mDriverName)) {
            return "";
        } else if (mDriverName.length() >= 3) {
            return mDriverName.substring(0, mDriverName.length() - 2) + "*" + mDriverName.substring(mDriverName.length() - 1, mDriverName.length());
        } else if (mDriverName.length() >= 2) {
            return mDriverName.substring(0, 1) + "*";
        } else {
            return mDriverName;
        }
    }

    public String getDriverLastPhoneNumber() {
        String result = "";
        try {
            if (!Utils.isStringNullOrEmpty(mDriverPhoneNumber)) {
                result = mDriverPhoneNumber.substring(mDriverPhoneNumber.length() - 4);
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = mDriverPhoneNumber;
        }

        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(mDriverName);
        sb.append(',');
        sb.append(mDriverPhoneNumber);
        sb.append(',');
        sb.append(mDriverPhoto);
        sb.append(',');
        sb.append(mDriverGradeAverage);
        sb.append(',');
        sb.append(mLat);
        sb.append(',');
        sb.append(mLng);

        return sb.toString();
    }

    //region implements parcelable
    protected DriverInfoViewModel(Parcel in) {
        mDriverName = in.readString();
        mDriverPhoneNumber = in.readString();
        mDriverPhoto = in.readString();
        mDriverGradeAverage = in.readFloat();
        mLat = in.readDouble();
        mLng = in.readDouble();
    }

    public static final Creator<DriverInfoViewModel> CREATOR = new Creator<DriverInfoViewModel>() {
        @Override
        public DriverInfoViewModel createFromParcel(Parcel in) {
            return new DriverInfoViewModel(in);
        }

        @Override
        public DriverInfoViewModel[] newArray(int size) {
            return new DriverInfoViewModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mDriverName);
        dest.writeString(mDriverPhoneNumber);
        dest.writeString(mDriverPhoto);
        dest.writeFloat(mDriverGradeAverage);
        dest.writeDouble(mLat);
        dest.writeDouble(mLng);
    }
    //endregion implements parcelable
}

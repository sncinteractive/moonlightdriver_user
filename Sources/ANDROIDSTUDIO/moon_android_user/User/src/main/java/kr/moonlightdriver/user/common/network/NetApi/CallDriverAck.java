package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.callDriver.CallInfoViewModel;
import kr.moonlightdriver.user.viewmodel.common.ResultData;

/**
 * Created by eklee on 2017. 5. 11..
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CallDriverAck extends NetAPI {
    @JsonProperty("callInfo")
    private CallInfoViewModel mCallInfo;

    @JsonProperty("range")
    private int mRange;

    @JsonProperty("requestCount")
    private int mRequestCount;

    @JsonProperty("time")
    private int mTime;

    public CallDriverAck() {}

    public CallDriverAck(ResultData mResultData, CallInfoViewModel mCallInfo, int mRange, int mRequestCount, int mTime) {
        super(mResultData);
        this.mCallInfo = mCallInfo;
        this.mRange = mRange;
        this.mRequestCount = mRequestCount;
        this.mTime = mTime;
    }

    public CallDriverAck(ResultData mResultData, int range, int requestCount, int time) {
        super(mResultData);
        mRange = range;
        mRequestCount = requestCount;
        mTime = time;
    }

    public CallInfoViewModel getmCallInfo() {
        return mCallInfo;
    }

    public void setmCallInfo(CallInfoViewModel mCallInfo) {
        this.mCallInfo = mCallInfo;
    }

    public int getmRange() {
        return mRange;
    }

    public void setmRange(int mRange) {
        this.mRange = mRange;
    }

    public int getmRequestCount() {
        return mRequestCount;
    }

    public void setmRequestCount(int mRequestCount) {
        this.mRequestCount = mRequestCount;
    }

    public int getmTime() {
        return mTime;
    }

    public void setmTime(int mTime) {
        this.mTime = mTime;
    }
}

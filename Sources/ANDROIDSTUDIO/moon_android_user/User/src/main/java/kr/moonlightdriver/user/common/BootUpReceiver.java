package kr.moonlightdriver.user.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by youngmin on 2016-11-09.
 */

public class BootUpReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		Intent serviceIntent = new Intent();
		serviceIntent.putExtra("start_from", "main");
		serviceIntent.setClass(context, SendLocationService.class);

		context.startService(serviceIntent);
	}
}

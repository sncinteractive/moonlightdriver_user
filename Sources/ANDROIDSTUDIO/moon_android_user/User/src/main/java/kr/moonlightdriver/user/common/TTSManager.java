package kr.moonlightdriver.user.common;

/**
 * Created by eklee on 2017. 4. 6..
 */

import android.content.Context;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class TTSManager {

    //region interface
    public interface OnUtteranceDoneListener {
        void onUtteranceDone(String utteranceId);
    }
    //endregion

    //region public methods
    public void speak(@NonNull Context _context, @NonNull String _text) {
        speak(_context, _text, null, null);
    }

    public void speak(@NonNull final Context _context,
                      @NonNull final String _text,
                      @Nullable final String _utteranceId,
                      @Nullable final OnUtteranceDoneListener _onUtteranceDoneListener) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            if (mTextToSpeech == null) {
                mTextToSpeech = new TextToSpeech(_context.getApplicationContext(), new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        mIsAvailable = (status == TextToSpeech.SUCCESS);
                        if (mIsAvailable)
                            speak(_context, _text, _utteranceId, _onUtteranceDoneListener);
                    }
                });
            } else if (mIsAvailable) {
                mLatestUtteranceId = _utteranceId;
                mOnUtteranceDoneListener = _onUtteranceDoneListener;

                mTextToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                    @Override
                    public void onStart(String utteranceId) {

                    }

                    @Override
                    public void onDone(String utteranceId) {
                        if (mOnUtteranceDoneListener != null && mLatestUtteranceId.equals(utteranceId)) {
                            mOnUtteranceDoneListener.onUtteranceDone(utteranceId);
                        }
                    }

                    @Override
                    public void onError(String utteranceId) {

                    }
                });

                mTextToSpeech.speak(_text, TextToSpeech.QUEUE_FLUSH, null, _utteranceId);

            } else {
                System.out.println("waiting tts..");
            }
        }
    }

    public void shutdown() {
        if (mTextToSpeech != null) {
            mTextToSpeech.stop();
            mTextToSpeech.shutdown();
            mTextToSpeech = null;
        }
    }

    public static TTSManager getInstance() {
        return sInstance;
    }
    //endregion

    //region private methods
    private TTSManager() {
        mTextToSpeech = null;
        mIsAvailable = false;
        mLatestUtteranceId = null;
        mOnUtteranceDoneListener = null;
    }
    //endregion

    //region private variables
    private static final TTSManager sInstance = new TTSManager();

    private TextToSpeech mTextToSpeech;
    private boolean mIsAvailable;
    private String mLatestUtteranceId;
    private OnUtteranceDoneListener mOnUtteranceDoneListener;
    //endregion
}
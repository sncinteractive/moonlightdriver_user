package kr.moonlightdriver.user.view.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Utils;

/**
 * Created by youngmin on 2016-12-29.
 */

public class NoticePushActivity extends Activity {
    private static final String TAG = "NoticePushActivity";
    private TextView mNoticePushTitle;
    private TextView mNoticePushMessage;
    private ImageView mNoticePushImage;
    private ImageButton mBtnNoticePushClose;
	private ImageButton mBtnPushPopupOk;

	private Context mContext;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        try {
//			SVUtil.log("error", TAG, "onNewIntent()");

            Bundle extras = intent.getExtras();
            if(extras != null) {
	            recvMessage(extras);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.activity_notice_push);

	        mContext = this;

            mBtnNoticePushClose = (ImageButton) findViewById(R.id.btn_notice_push_close);
            mNoticePushTitle = (TextView) findViewById(R.id.notice_push_title);
            mNoticePushMessage = (TextView) findViewById(R.id.notice_push_message);
            mNoticePushImage = (ImageView) findViewById(R.id.notice_push_image);
	        mBtnPushPopupOk = (ImageButton) findViewById(R.id.btn_push_popup_ok);

            Intent intent = getIntent();
            Bundle extras = intent.getExtras();
            if(extras != null) {
	            recvMessage(extras);
            }

	        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	private void recvMessage(Bundle _extra) {
		try {
			String pushMsgType = _extra.getString("pushMsgType", "");
			if(pushMsgType.equals("crackdown_warning")) {
				float distance = _extra.getFloat("distance", -1);
				int crackdownCount = _extra.getInt("crackdownCount", 0);

				showCrackdownWarningPopup(distance, crackdownCount);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    private void setNoticePopupContent(String _title, String _message, String _imgUrl) {
        try {
            mNoticePushTitle.setText(_title);
            mNoticePushMessage.setText(_message);
	        mNoticePushMessage.setTextSize(12.5f);

	        if (!Utils.isStringNullOrEmpty(_imgUrl)) {
	            new Utils.DownloadImageTask(mNoticePushImage).execute(Configuration.IMAGE_SERVER_HOST + _imgUrl);
	            mNoticePushImage.setVisibility(View.VISIBLE);
            }

	        mBtnNoticePushClose.setOnClickListener(new View.OnClickListener() {
		        @Override
		        public void onClick(View view) {
			        Intent intent = new Intent(NoticePushActivity.this, MainActivity.class);
			        startActivity(intent);
			        finish();
		        }
	        });

	        mBtnPushPopupOk.setOnClickListener(new View.OnClickListener() {
		        @Override
		        public void onClick(View view) {
			        Intent intent = new Intent(NoticePushActivity.this, MainActivity.class);
			        startActivity(intent);
			        finish();
		        }
	        });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showCrackdownWarningPopup(float _distance, int _crackdownCount) {
        try {
            if(_distance < 0 || _distance > 1000 || _crackdownCount <= 0) {
                return;
            }

	        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
	        final MediaPlayer player = MediaPlayer.create(this, notification);
	        player.setLooping(true);

	        final Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            long[] pattern = new long[] { 200, 1000, 200, 1000, 200, 1000 };
            String message = "";

            if(_distance == 300) {
                message = "현재 300m 이내에 " + _crackdownCount + "곳에서 음주단속 중입니다";
                vibrator.vibrate(pattern, 0);

	            new android.os.Handler().postDelayed(
			            new Runnable() {
				            public void run() {
					            if(player.isPlaying()) {
						            player.stop();
					            }

					            vibrator.cancel();
				            }
			            }, (3600 * 5));
	        } else if(_distance == 500) {
	            message = "현재 500m 이내에 " + _crackdownCount + "곳에서 음주단속 중입니다";
                vibrator.vibrate(pattern, -1);

	            new android.os.Handler().postDelayed(
			            new Runnable() {
				            public void run() {
					            if(player.isPlaying()) {
						            player.stop();
					            }
				            }
			            }, 4000);
            } else if(_distance == 1000) {
	            message = "현재 1km 이내에 " + _crackdownCount + "곳에서 음주단속 중입니다";
                vibrator.vibrate(2000);

	            new android.os.Handler().postDelayed(
			            new Runnable() {
				            public void run() {
					            if(player.isPlaying()) {
						            player.stop();
					            }
				            }
			            }, 2000);
            }

	        player.start();

	        mNoticePushTitle.setText("카대리 알림");
	        mNoticePushMessage.setText(message);
	        mNoticePushMessage.setTextSize(23f);
	        mNoticePushImage.setVisibility(View.GONE);

	        mBtnNoticePushClose.setOnClickListener(new View.OnClickListener() {
		        @Override
		        public void onClick(View view) {
			        vibrator.cancel();

			        if(player.isPlaying()) {
				        player.stop();
			        }

			        Intent intent = new Intent(NoticePushActivity.this, MainActivity.class);
			        startActivity(intent);

			        finish();
		        }
	        });

	        mBtnPushPopupOk.setOnClickListener(new View.OnClickListener() {
		        @Override
		        public void onClick(View view) {
			        vibrator.cancel();

			        if(player.isPlaying()) {
				        player.stop();
			        }

			        Intent intent = new Intent(NoticePushActivity.this, MainActivity.class);
			        startActivity(intent);

			        finish();
		        }
	        });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

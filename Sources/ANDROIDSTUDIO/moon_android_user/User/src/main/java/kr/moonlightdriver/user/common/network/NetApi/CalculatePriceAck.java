package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;

/**
 * Created by kyo on 2017-04-10.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class CalculatePriceAck extends NetAPI {
    @JsonProperty("price")
    private int mPrice;

    @JsonProperty("peakYn")
    private String mPeakYn;

    @JsonProperty("extraPrice")
    private int mExtraPrice;

    public CalculatePriceAck() {}

    public CalculatePriceAck(int mPrice, String mPeakYn, int mExtraPrice) {
        this.mPrice = mPrice;
        this.mPeakYn = mPeakYn;
        this.mExtraPrice = mExtraPrice;
    }

    public CalculatePriceAck(ResultData mResultData, int mPrice, String mPeakYn, int mExtraPrice) {
        super(mResultData);
        this.mPrice = mPrice;
        this.mPeakYn = mPeakYn;
        this.mExtraPrice = mExtraPrice;
    }

    public int getmPrice() {
        return mPrice;
    }

    public void setmPrice(int mPrice) {
        this.mPrice = mPrice;
    }

    public String getmPeakYn() {
        return mPeakYn;
    }

    public void setmPeakYn(String mPeakYn) {
        this.mPeakYn = mPeakYn;
    }

    public int getmExtraPrice() {
        return mExtraPrice;
    }

    public void setmExtraPrice(int mExtraPrice) {
        this.mExtraPrice = mExtraPrice;
    }
}

package kr.moonlightdriver.user.viewmodel.setting;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmincho on 2017. 7. 18..
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class SettingsMyCarInfoViewModel {
	@JsonProperty("carType")
	private String mCarType;

	@JsonProperty("carMadeType")
	private String mMadeType;

	@JsonProperty("transmissionType")
	private String mTransmissionType;

	public SettingsMyCarInfoViewModel() {
	}

	public SettingsMyCarInfoViewModel(String mCarType, String mMadeType, String mTransmissionType) {
		this.mCarType = mCarType;
		this.mMadeType = mMadeType;
		this.mTransmissionType = mTransmissionType;
	}

	public String getmCarType() {
		return mCarType;
	}

	public void setmCarType(String mCarType) {
		this.mCarType = mCarType;
	}

	public String getmMadeType() {
		return mMadeType;
	}

	public void setmMadeType(String mMadeType) {
		this.mMadeType = mMadeType;
	}

	public String getmTransmissionType() {
		return mTransmissionType;
	}

	public void setmTransmissionType(String mTransmissionType) {
		this.mTransmissionType = mTransmissionType;
	}
}

package kr.moonlightdriver.user.common;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import kr.moonlightdriver.user.R;

/**
 * Created by eklee on 2017. 6. 9..
 */

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private final View mView;

    public CustomInfoWindowAdapter(Context context) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        mView = layoutInflater.inflate(R.layout.google_map_custom_info_window, null);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        render(marker, mView);
        return mView;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    private void render(Marker marker, View view) {
        String snippet = marker.getSnippet();
        TextView snippetTextView = ((TextView) view.findViewById(R.id.snippet));
        snippetTextView.setText(snippet);
    }
}
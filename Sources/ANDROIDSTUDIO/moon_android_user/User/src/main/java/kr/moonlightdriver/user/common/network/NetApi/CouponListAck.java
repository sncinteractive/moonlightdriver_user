package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.CouponListViewModel;

/**
 * Created by kyo on 2017-04-13.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class CouponListAck extends NetAPI {
    @JsonProperty("couponList")
    private List<CouponListViewModel> mCouponList;

    public CouponListAck() {
    }

    public CouponListAck(ResultData mResultData, List<CouponListViewModel> couponModelList) {
        super(mResultData);
        mCouponList = couponModelList;
    }

    public List<CouponListViewModel> getCouponList() {
        return mCouponList;
    }

    public void setCouponList(List<CouponListViewModel> couponList) {
        mCouponList = couponList;
    }
}

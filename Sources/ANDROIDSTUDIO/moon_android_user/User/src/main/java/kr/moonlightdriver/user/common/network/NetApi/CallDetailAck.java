package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.callDriver.CallInfoViewModel;
import kr.moonlightdriver.user.viewmodel.common.ResultData;

/**
 * Created by eklee on 2017. 5. 11..
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CallDetailAck extends NetAPI {
    @JsonProperty("callDetail")
    private CallInfoViewModel mCallInfo;

    public CallDetailAck() {

    }

    public CallDetailAck(CallInfoViewModel mCallInfo) {
        this.mCallInfo = mCallInfo;
    }

    public CallDetailAck(ResultData mResultData, CallInfoViewModel mCallInfo) {
        super(mResultData);
        this.mCallInfo = mCallInfo;
    }

    public CallInfoViewModel getmCallInfo() {
        return mCallInfo;
    }

    public void setmCallInfo(CallInfoViewModel mCallInfo) {
        this.mCallInfo = mCallInfo;
    }
}

package kr.moonlightdriver.user.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.TextView;

public class CommonDialog {
	private static final String TAG = "CommonDialog";

	public static ProgressDialog mProgressDialog;

	public static void showProgressDialog(Context cx, String msg) {
		hideProgressDialog();

		mProgressDialog = ProgressDialog.show(cx, "", msg, true, false);
	}

	public static void hideProgressDialog() {
		if(mProgressDialog != null) {
			mProgressDialog.dismiss();
		}
	}

	public static void showSimpleDialog(Context _context, String _message) {
		showDialogWithListener(_context, "알림", _message, null, "확인", null, null, null);
	}

	public static void showDialogWithListener(Context _context, String _message, String _buttonText, DialogInterface.OnClickListener _buttonListener) {
		showDialogWithListener(_context, "알림", _message, null, _buttonText, _buttonListener, null, null);
	}

	public static void showDialogWithListener(Context _context, String _title, String _message, String _positiveButtonText, DialogInterface.OnClickListener _positiveListener,
	                                          String _negativeButtonText, DialogInterface.OnClickListener _negativeListener) {
		showDialogWithListener(_context, _title, _message, null, _positiveButtonText, _positiveListener, _negativeButtonText, _negativeListener);
	}

	public static void showDialogWithListener(Context _context, String _title, View _view, String _buttonText, DialogInterface.OnClickListener _buttonListener) {
		showDialogWithListener(_context, _title, null, _view, _buttonText, _buttonListener, null, null);
	}

	public static AlertDialog showDialogWithListener(Context _context, String _title, View _view, String _positiveButtonText, DialogInterface.OnClickListener _positiveListener,
	                                                 String _negativeButtonText, DialogInterface.OnClickListener _negativeListener) {
		return showDialogWithListener(_context, _title, null, _view, _positiveButtonText, _positiveListener, _negativeButtonText, _negativeListener);
	}

	public static AlertDialog showDialogWithListener(Context _context, String _title, String _message, View _view, String _positiveButtonText, DialogInterface.OnClickListener _positiveListener,
	                                                 String _negativeButtonText, DialogInterface.OnClickListener _negativeListener) {
		try {
			if (_context == null) {
				return null;
			}

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(_context);
			alertDialogBuilder.setTitle(_title);

			if(_message != null) {
				alertDialogBuilder.setMessage(_message);
			}

			if(_view != null) {
				alertDialogBuilder.setView(_view);
			}

			if(!Utils.isStringNullOrEmpty(_positiveButtonText)) {
				alertDialogBuilder.setPositiveButton(_positiveButtonText, _positiveListener);
			}

			if(!Utils.isStringNullOrEmpty(_negativeButtonText)) {
				alertDialogBuilder.setNegativeButton(_negativeButtonText, _negativeListener);
			}

			AlertDialog dialog = alertDialogBuilder.show();

//		setDialogTitleColor(dialog, _context.getResources().getColor(R.color.main200));

			return dialog;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static void showItemDialog(Context _context, String _title, CharSequence[] _items, DialogInterface.OnClickListener _itemClickListener) {
		try {
			if (_context == null) {
				return;
			}

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(_context);
			alertDialogBuilder.setTitle(_title);

			alertDialogBuilder.setItems(_items, _itemClickListener);

			AlertDialog dialog = alertDialogBuilder.show();

//		setDialogTitleColor(dialog, _context.getResources().getColor(R.color.main200));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void showAdapterDialog(Context _context, String _title, ListAdapter _adapter, DialogInterface.OnClickListener _itemClickListener, String _buttonText, DialogInterface.OnClickListener _buttonListener) {
		try {
			if (_context == null) {
				return;
			}

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(_context);
			alertDialogBuilder.setTitle(_title);

			alertDialogBuilder.setAdapter(_adapter, _itemClickListener);

			if(!Utils.isStringNullOrEmpty(_buttonText)) {
				alertDialogBuilder.setNegativeButton(_buttonText, _buttonListener);
			}

			AlertDialog dialog = alertDialogBuilder.show();

//		setDialogTitleColor(dialog, _context.getResources().getColor(R.color.main200));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void showSingleChoiceItemDialog(Context _context, String _title, CharSequence[] _items, int _checkedItem, DialogInterface.OnClickListener _itemClickListener) {
		showSingleChoiceItemDialog(_context, _title, _items, _checkedItem, _itemClickListener, null, null, null, null);
	}

	public static void showSingleChoiceItemDialog(Context _context, String _title, CharSequence[] _items, int _checkedItem, DialogInterface.OnClickListener _itemClickListener,
	                                              String _positiveButtonText, DialogInterface.OnClickListener _positiveListener, String _negativeButtonText, DialogInterface.OnClickListener _negativeListener) {
		try {
			if (_context == null) {
				return;
			}

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(_context);
			alertDialogBuilder.setTitle(_title);

			alertDialogBuilder.setSingleChoiceItems(_items, _checkedItem, _itemClickListener);

			if(!Utils.isStringNullOrEmpty(_positiveButtonText)) {
				alertDialogBuilder.setPositiveButton(_positiveButtonText, _positiveListener);
			}

			if(!Utils.isStringNullOrEmpty(_negativeButtonText)) {
				alertDialogBuilder.setNegativeButton(_negativeButtonText, _negativeListener);
			}

			AlertDialog dialog = alertDialogBuilder.show();

//		    setDialogTitleColor(dialog, _context.getResources().getColor(R.color.main200));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void showTimeoutDialog(final Context _context) {
		try {
			showDialogWithListener(_context, "네트워크 연결이 원할하지 않습니다.\n다시 시도해 주십시요.", "확인", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Utils.finishActivity((Activity) _context);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void setDialogTitleColor(Dialog _dialog, int _color) {
		try {
			int textViewId = _dialog.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
			TextView tv = (TextView) _dialog.findViewById(textViewId);
			tv.setTextColor(_color);
			tv.setGravity(Gravity.CENTER);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

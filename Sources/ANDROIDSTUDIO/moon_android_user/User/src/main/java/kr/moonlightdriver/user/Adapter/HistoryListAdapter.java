package kr.moonlightdriver.user.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.viewholder.HistoryListViewHolder;
import kr.moonlightdriver.user.viewmodel.callDriver.DriverInfoViewModel;
import kr.moonlightdriver.user.viewmodel.setting.HistoryListViewModel;

/**
 * Created by eklee on 2017. 4. 24..
 */

public class HistoryListAdapter extends ArrayAdapter<HistoryListViewModel> {
    public interface InteractionListener {
        void OnReportUnfairnessClick(int position);

        void OnEvaluateDriverClick(int position);

        void OnReliefMessageClick(int position);
    }

    private int mResourceId;
    private HistoryListViewHolder mViewHolder;
    private InteractionListener mInteractionListener;

    public HistoryListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<HistoryListViewModel> objects, InteractionListener interactionListener) {
        super(context, resource, objects);

        mResourceId = resource;
        mInteractionListener = interactionListener;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(mResourceId, null);

            mViewHolder = new HistoryListViewHolder();
            mViewHolder.mDateTextView = (TextView) convertView.findViewById(R.id.tv_date);
            mViewHolder.mReliefMessageTextView = (TextView) convertView.findViewById(R.id.tv_relief_message_title);
            mViewHolder.mPathTextView = (TextView) convertView.findViewById(R.id.tv_path);
            mViewHolder.mPayAmountTextView = (TextView) convertView.findViewById(R.id.tv_pay_amount);
            mViewHolder.mDriverInfoTextView = (TextView) convertView.findViewById(R.id.tv_driver_info);
            mViewHolder.mDriverCompanyTextView = (TextView) convertView.findViewById(R.id.tv_driver_company);
            mViewHolder.mEvaluateDriverButton = (ImageButton) convertView.findViewById(R.id.ib_evaluate_driver);
            mViewHolder.mReportUnfairnessButton = (ImageButton) convertView.findViewById(R.id.ib_report_unfairness);

            Utils.setFontToTextViewRecursively(FontManager.getInstance().getFontSeoulNamsanJangM(), convertView);

            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (HistoryListViewHolder) convertView.getTag();
        }

        Context context = getContext();

        HistoryListViewModel historyInfo = getItem(position);

        mViewHolder.mDateTextView.setText(context.getString(R.string.string_format_history_list_date,
            position + 1,
            Utils.convertSimpleDateFormat("yyyy-MM-dd KK-mm a", historyInfo.getDate())));

        boolean hasReliefMessage = !Utils.isStringNullOrEmpty(historyInfo.getReliefMessage());
        if (hasReliefMessage) {
            String title = context.getString(R.string.history_relief_message, historyInfo.getReliefMessage());
            SpannableString spannableString = new SpannableString(title);
            spannableString.setSpan(new UnderlineSpan(), title.indexOf('안'), title.length(), 0);
            mViewHolder.mReliefMessageTextView.setText(spannableString);
            mViewHolder.mReliefMessageTextView.setTextColor(context.getResources().getColor(R.color.main_orange));
            mViewHolder.mReliefMessageTextView.setClickable(true);

            mViewHolder.mReliefMessageTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mInteractionListener != null) {
                        mInteractionListener.OnReliefMessageClick(position);
                    }
                }
            });
        } else {
            mViewHolder.mReliefMessageTextView.setText(context.getString(R.string.history_no_relief_message));
            mViewHolder.mReliefMessageTextView.setTextColor(context.getResources().getColor(R.color.main_light_gray));
            mViewHolder.mReliefMessageTextView.setClickable(false);
        }

        mViewHolder.mPathTextView.setText(context.getString(R.string.string_format_history_list_path, historyInfo.getFrom(), historyInfo.getTo()));

        mViewHolder.mPayAmountTextView.setText(context.getString(R.string.string_format_history_list_pay_amount, String.format(Locale.getDefault(), "%,d", historyInfo.getPrice())));

        DriverInfoViewModel driverInfo = historyInfo.getDriverInfo();
        mViewHolder.mDriverInfoTextView.setText(context.getString(R.string.string_format_history_list_driver_info,
            driverInfo.getMaskedDriverName(),
            Utils.convertDriverGradeAverageToString(driverInfo.getmDriverGradeAverage()),
            driverInfo.getmDriverPhoneNumber()));

        mViewHolder.mDriverCompanyTextView.setText(historyInfo.getCompanyName());

        mViewHolder.mEvaluateDriverButton.setEnabled(!historyInfo.isEvaluated());
        mViewHolder.mEvaluateDriverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mInteractionListener != null) {
                    mInteractionListener.OnEvaluateDriverClick(position);
                }
            }
        });

        mViewHolder.mReportUnfairnessButton.setEnabled(!historyInfo.isReportedUnfairness());
        mViewHolder.mReportUnfairnessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mInteractionListener != null) {
                    mInteractionListener.OnReportUnfairnessClick(position);
                }
            }
        });

        return convertView;
    }
}

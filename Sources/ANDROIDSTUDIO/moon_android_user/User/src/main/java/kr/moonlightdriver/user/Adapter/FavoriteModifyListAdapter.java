package kr.moonlightdriver.user.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.viewholder.FavoriteModifyListViewHolder;
import kr.moonlightdriver.user.viewmodel.setting.FavoriteListViewModel;

/**
 * Created by kyo on 2017-04-12.
 */

public class FavoriteModifyListAdapter extends ArrayAdapter<FavoriteListViewModel> {

    public interface OnDeleteButtonClickListener {
        void onDeleteButtonClick(int position);
    }

    private int mResourceId;
    private List<FavoriteListViewModel> mDataList;
    private FavoriteModifyListViewHolder mViewHolder;
    private OnDeleteButtonClickListener mOnDeleteButtonClickListener;

    public FavoriteModifyListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<FavoriteListViewModel> objects) {
        super(context, resource, objects);

        mResourceId = resource;
        mDataList = objects;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(mResourceId, null);

            mViewHolder = new FavoriteModifyListViewHolder();
            mViewHolder.nameTextView = (TextView) convertView.findViewById(R.id.tv_favorite_name);
            mViewHolder.addressTextView = (TextView) convertView.findViewById(R.id.tv_favorite_address);
            mViewHolder.deleteButton = (ImageButton) convertView.findViewById(R.id.ib_delete);

            mViewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnDeleteButtonClickListener != null) {
                        mOnDeleteButtonClickListener.onDeleteButtonClick(position);
                    }
                }
            });

            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), mViewHolder.nameTextView, mViewHolder.addressTextView);

            convertView.setTag(mViewHolder);

        } else {
            mViewHolder = (FavoriteModifyListViewHolder) convertView.getTag();
        }

        FavoriteListViewModel data = mDataList.get(position);
        mViewHolder.nameTextView.setText(data.getName());
        mViewHolder.addressTextView.setText(data.getLocationData().getmAddress());

        return convertView;
    }

    public void setDeleteButtonClickListener(OnDeleteButtonClickListener listener) {
        mOnDeleteButtonClickListener = listener;
    }
}

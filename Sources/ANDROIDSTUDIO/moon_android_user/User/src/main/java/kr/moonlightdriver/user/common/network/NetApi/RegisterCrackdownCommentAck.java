package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.crackdown.CrackdownCommentListViewModel;

/**
 * Created by youngmin on 2016-08-18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterCrackdownCommentAck extends NetAPI {
	@JsonProperty("comment_list")
	private ArrayList<CrackdownCommentListViewModel> mCrackdownCommentList;

	public RegisterCrackdownCommentAck() {}

	public RegisterCrackdownCommentAck(ResultData mResultData, ArrayList<CrackdownCommentListViewModel> mCrackdownCommentList) {
		super(mResultData);
		this.mCrackdownCommentList = mCrackdownCommentList;
	}

	public ArrayList<CrackdownCommentListViewModel> getmCrackdownCommentList() {
		return mCrackdownCommentList;
	}

	public void setmCrackdownCommentList(ArrayList<CrackdownCommentListViewModel> mCrackdownCommentList) {
		this.mCrackdownCommentList = mCrackdownCommentList;
	}
}

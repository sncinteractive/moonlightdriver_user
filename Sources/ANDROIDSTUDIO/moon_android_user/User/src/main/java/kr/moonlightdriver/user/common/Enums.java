package kr.moonlightdriver.user.common;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by kyo on 2017-05-08.
 */

public class Enums {
    public enum SearchLocationListItemType {
        TITLE(0),
        FAVORITE_ITEM(1),
        LATEST_ITEM(2);

        SearchLocationListItemType(int value) {
            mValue = value;
        }

        public int getValue() {
            return mValue;
        }

        private int mValue;
    }

    @StringDef({CallStatus.Wait, CallStatus.Bidding, CallStatus.Cancel, CallStatus.Catch, CallStatus.Start, CallStatus.End, CallStatus.Done})
    @Retention(RetentionPolicy.SOURCE)
    public @interface CallStatus {
        String Wait = "wait";
        String Bidding = "bidding";
        String Cancel = "cancel";
        String Catch = "catch";
        String Start = "start";
        String End = "end";
        String Done = "done";
    }

//    public enum CallDriverState {
//        IDLE(0),
//        WAIT_ASSIGN(1),
//        ASSIGNED(2),
//        NOT_ASSIGNED(3),
//        PRICE_OFFER(4),
//        CARD_PAYMENT(5),
//        PAY_COMPLETED(6),
//        DRIVER_ARRIVED(7),
//        DRIVING(8),
//        ARRIVED_TARGET(9);
//
//        CallDriverState(int value) {
//            mValue = value;
//        }
//
//        public int getValue() {
//            return mValue;
//        }
//
//        private int mValue;
//    }

    public enum NetResultCode {
        UNKNOWN_ERROR(0),
        SUCCESS(100),
        VERSION_UPDATE(236);

        NetResultCode(int value) {
            mValue = value;
        }

        public int getValue() {
            return mValue;
        }

        private int mValue;
    }
}

package kr.moonlightdriver.user.view.settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.CreditCardListAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewholder.CardListViewHolder;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.CardListViewModel;

/**
 * Created by youngmin on 2016-06-21.
 */
public class SettingCardFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "SettingCardFragment";

    public static final int REQ_CODE_CARD_MODIFY = 1;
    public static final int REQ_CODE_CARD_ADD = 2;

    private ImageButton ib_card_add, ib_setting_card_modify;
    private LinearLayout ic_setting_goback;
    private MainActivity mMainActivity;
    private SettingsFragment mFragment;
    private TextView mEmptyListText;

    private ListView mCardListView;
    private CardListAdapter mCardListAdapter;
    private ArrayList<CardListViewModel> mCardModelList;
    private int mLastSelectedItemIndex;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            mMainActivity = (MainActivity) getActivity();

            view = inflater.inflate(R.layout.fragment_setting_card_list, container, false);

            TextView headerTitleTextView = (TextView)view.findViewById(R.id.tv_header_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanB(), headerTitleTextView);

            mEmptyListText = (TextView) view.findViewById(R.id.tv_card_list_empty);

            ic_setting_goback = (LinearLayout) view.findViewById(R.id.ib_setting_goback);
            ib_card_add = (ImageButton) view.findViewById(R.id.ib_card_add);
            ib_setting_card_modify = (ImageButton) view.findViewById(R.id.ib_setting_card_modify);

            ic_setting_goback.setOnClickListener(this);
            ib_card_add.setOnClickListener(this);
            ib_setting_card_modify.setOnClickListener(this);

            mCardModelList = new ArrayList<>();
            mCardListAdapter = new CardListAdapter(mMainActivity, R.layout.row_setting_card_list, mCardModelList);
            mCardListView = (ListView) view.findViewById(R.id.card_list);
            mCardListView.setAdapter(mCardListAdapter);
            mCardListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    if( mLastSelectedItemIndex >= 0 && mLastSelectedItemIndex < mCardListAdapter.getCount() ) {
                        View oldItem = adapterView.getChildAt(mLastSelectedItemIndex);
                        if(oldItem != null) {
                            oldItem.setSelected(false);
                        }
                    }
                    view.setSelected(true);
                    mLastSelectedItemIndex = i;
                }
            });
            mLastSelectedItemIndex = -1;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        Utils.log("error", TAG, "onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            Utils.log("error", TAG, "onResume()");
            requestCreditCardList();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQ_CODE_CARD_MODIFY: {
                    requestCreditCardList();
                }
                break;
                case REQ_CODE_CARD_ADD: {
                    requestCreditCardList();
                }
                break;
            }
        }
    }

    @Override
    public void onClick(View _view) {
        try {
            switch (_view.getId()) {
                case R.id.ib_setting_goback: {
                    getActivity().getSupportFragmentManager().beginTransaction().remove(SettingCardFragment.this).commit();
                }
                break;
                case R.id.ib_card_add: {
                    SettingCardAddFragment fragment = new SettingCardAddFragment();
                    fragment.setTargetFragment(this, REQ_CODE_CARD_ADD);
                    mMainActivity.addFragment(fragment);
                }
                break;
                case R.id.ib_setting_card_modify: {
                    SettingCardModifyFragment fragment = new SettingCardModifyFragment();
                    fragment.setTargetFragment(this, REQ_CODE_CARD_MODIFY);
                    mMainActivity.addFragment(fragment);
                }
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class CardListAdapter extends ArrayAdapter<CardListViewModel> {
        private ArrayList<CardListViewModel> mCardListViewModelList;
        private int mResourceId;
        private CardListViewHolder mViewHolder;

        public CardListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull ArrayList<CardListViewModel> objects) {
            super(context, resource, objects);

            mCardListViewModelList = objects;
            mResourceId = resource;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(mResourceId, null);

                mViewHolder = new CardListViewHolder();
                mViewHolder.nameAndCardNumberTextView = (TextView) convertView.findViewById(R.id.tv_name_and_card_number);
                mViewHolder.userEmailTextView = (TextView) convertView.findViewById(R.id.tv_user_email);

                convertView.setTag(mViewHolder);
            } else {
                mViewHolder = (CardListViewHolder) convertView.getTag();
            }

            CardListViewModel model = mCardListViewModelList.get(position);
            String displayCardNumber = model.getmUserName() + " " + model.getmCardNumber();
            mViewHolder.nameAndCardNumberTextView.setText(displayCardNumber);
            mViewHolder.userEmailTextView.setText(model.getmUserEmail());

            return convertView;
        }
    }

    private void requestCreditCardList() {
        mMainActivity.showLoadingAnim();

        RequestParams requestParams = new RequestParams();
        requestParams.add("userId", mMainActivity.mUserDetail.getUserId());

        // 서버 연동시 주석 제거
        NetClient.send(mMainActivity, Configuration.URL_CREDIT_CARD_LIST, "POST", requestParams, new CreditCardListAck(), new NetResponseCallback(new CreditCardListResponse()));
    }

    private class CreditCardListResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            mMainActivity.hideLoadingAnim();

            mCardModelList.clear();

            if (_netAPI != null) {
                ResultData resultData = _netAPI.getmResultData();
                if (resultData.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                    mCardModelList.addAll(((CreditCardListAck)_netAPI).getmCreditCardList());
                } else { // rollback
                    CommonDialog.showSimpleDialog(mMainActivity, resultData.getmDetail());
                }
            }

            mCardListAdapter.notifyDataSetChanged();

            if(mCardModelList.size() > 0) {
                mEmptyListText.setVisibility(View.GONE);
            } else {
                mEmptyListText.setVisibility(View.VISIBLE);
            }
        }
    }
}

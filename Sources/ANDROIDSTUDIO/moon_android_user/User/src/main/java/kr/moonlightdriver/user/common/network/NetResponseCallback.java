package kr.moonlightdriver.user.common.network;

/**
 * Created by youngmin on 2016-08-11.
 */
public class NetResponseCallback {
	private NetResponse mNetResponse;

	public NetResponseCallback(NetResponse mNetResponse) {
		this.mNetResponse = mNetResponse;
	}

	public void onResponse(NetAPI _netAPI) {
		this.mNetResponse.onResponse(_netAPI);
	}
}

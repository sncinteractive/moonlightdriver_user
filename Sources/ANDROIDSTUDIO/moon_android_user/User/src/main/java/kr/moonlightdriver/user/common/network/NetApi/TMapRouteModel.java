package kr.moonlightdriver.user.common.network.NetApi;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by eklee on 2017. 5. 29..
 */

public class TMapRouteModel {
    private long mTotalDistance; // meter
    private long mTotalTime; // second
    private List<LatLng> mPointList;
    private List<Integer> mRemainDistanceList;
    private List<Integer> mRemainTimeList;

    public TMapRouteModel() {
    }

    public TMapRouteModel(long totalDistance, long totalTime, List<LatLng> pointList, List<Integer> remainDistanceList, List<Integer> remainTimeList) {
        mTotalDistance = totalDistance;
        mTotalTime = totalTime;
        mPointList = pointList;
        mRemainDistanceList = remainDistanceList;
        mRemainTimeList = remainTimeList;
    }

    public double getTotalDistance() {
        return mTotalDistance;
    }

    public void setTotalDistance(long totalDistance) {
        mTotalDistance = totalDistance;
    }

    public double getTotalTime() {
        return mTotalTime;
    }

    public void setTotalTime(long totalTime) {
        mTotalTime = totalTime;
    }

    public List<LatLng> getPointList() {
        return mPointList;
    }

    public void setPointList(List<LatLng> pointList) {
        mPointList = pointList;
    }

    public List<Integer> getRemainDistanceList() {
        return mRemainDistanceList;
    }

    public void setRemainDistanceList(List<Integer> remainDistanceList) {
        mRemainDistanceList = remainDistanceList;
    }

    public List<Integer> getRemainTimeList() {
        return mRemainTimeList;
    }

    public void setRemainTimeList(List<Integer> remainTimeList) {
        mRemainTimeList = remainTimeList;
    }

    public int getRemainDistance(int index) {
        if(mRemainDistanceList == null) {
            return 0;
        }

        if(index < 0 || index >= mRemainDistanceList.size()) {
            return 0;
        }

        return mRemainDistanceList.get(index);
    }

    public int getRemainTime(int index) {
        if(mRemainTimeList == null) {
            return 0;
        }

        if(index < 0 || index >= mRemainTimeList.size()) {
            return 0;
        }

        return mRemainTimeList.get(index);
    }
}

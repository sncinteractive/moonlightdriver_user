package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.PointListViewModel;

/**
 * Created by kyo on 2017-04-10.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class PointListAck extends NetAPI {
    @JsonProperty("pointHistList")
    private List<PointListViewModel> mPointList;

    public PointListAck() {}

    public PointListAck(ResultData mResultData, List<PointListViewModel> pointList) {
        super(mResultData);
        mPointList = pointList;
    }

    public List<PointListViewModel> getPointList() {
        return mPointList;
    }

    public void setPointList(List<PointListViewModel> pointList) {
        mPointList = pointList;
    }
}

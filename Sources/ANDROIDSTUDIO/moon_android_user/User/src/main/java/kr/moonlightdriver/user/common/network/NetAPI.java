package kr.moonlightdriver.user.common.network;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.viewmodel.common.ResultData;

/**
 * Created by youngmin on 2016-08-11.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NetAPI {
    @JsonProperty("result")
    protected ResultData mResultData;

    public NetAPI() {
    }

    public NetAPI(ResultData mResultData) {
        this.mResultData = mResultData;
    }

    public ResultData getmResultData() {
        return mResultData;
    }

    public void setmResultData(ResultData mResultData) {
        this.mResultData = mResultData;
    }
}

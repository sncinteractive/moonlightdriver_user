package kr.moonlightdriver.user.viewmodel.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

import kr.moonlightdriver.user.viewmodel.setting.FavoriteListViewModel;

/**
 * Created by youngmin on 2016-06-21.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserAdditionalViewModel {
    @JsonProperty("userId")
    public String mUserId;

    @JsonProperty("pushYN")
    public String mPushYN;

    @JsonProperty("point")
    public int mPoint;

    @JsonProperty("favoriteList")
    public List<FavoriteListViewModel> mFavoriteList;

    public UserAdditionalViewModel() {
        mFavoriteList = new ArrayList<>();
    }

    public UserAdditionalViewModel(String mUserId, String mPushYN, int mPoint, List<FavoriteListViewModel> mFavoriteList) {
        this.mUserId = mUserId;
        this.mPushYN = mPushYN;
        this.mPoint = mPoint;
        this.mFavoriteList = mFavoriteList;
    }

    public String getmUserId() {
        return mUserId;
    }

    public void setmUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public String getmPushYN() {
        return mPushYN;
    }

    public void setmPushYN(String mPushYN) {
        this.mPushYN = mPushYN;
    }

    public int getmPoint() {
        return mPoint;
    }

    public void setmPoint(int mPoint) {
        this.mPoint = mPoint;
    }

    public List<FavoriteListViewModel> getFavoriteList() {
        return mFavoriteList;
    }

    public void setFavoriteList(List<FavoriteListViewModel> favoriteList) {
        mFavoriteList = favoriteList;
    }

    public boolean removeFavoriteListItem(FavoriteListViewModel item) {
        try {
            return mFavoriteList.remove(item);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public void addFavoriteListItem(FavoriteListViewModel item) {
        try {
            mFavoriteList.add(item);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package kr.moonlightdriver.user.view.settings;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.CreditCardAddAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewmodel.common.ResultData;

/**
 * Created by youngmin on 2016-06-21.
 */
public class SettingCardAddFragment extends Fragment implements View.OnClickListener {

    private ImageButton mBtnAddCard, mBtnCompanyYN;
    private LinearLayout mBtnSettingGoback;
    private MainActivity mMainActivity;
    private SettingsFragment mFragment;

    private EditText mCardNumberText;
    private EditText mUserNameText;
    private EditText mUserEmailText;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            mMainActivity = (MainActivity) getActivity();

            view = inflater.inflate(R.layout.fragment_setting_card_add, container, false);

            TextView headerTitleTextView = (TextView) view.findViewById(R.id.tv_header_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanB(), headerTitleTextView);

            mCardNumberText = (EditText) view.findViewById(R.id.card_number_edit_text);
            mUserNameText = (EditText) view.findViewById(R.id.name_edit_text);
            mUserEmailText = (EditText) view.findViewById(R.id.email_edit_text);

            mBtnSettingGoback = (LinearLayout) view.findViewById(R.id.ib_setting_goback);
            mBtnAddCard = (ImageButton) view.findViewById(R.id.ib_card_add);
            mBtnCompanyYN = (ImageButton) view.findViewById(R.id.company_card_button);

            mBtnSettingGoback.setOnClickListener(this);
            mBtnAddCard.setOnClickListener(this);
            mBtnCompanyYN.setOnClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onClick(View _view) {
        try {
            if (_view == mBtnSettingGoback) {
                closeFragment();
            } else if (_view == mBtnAddCard) {
                CommonDialog.showDialogWithListener(mMainActivity, "카드 등록", "카드를 등록 하시겠습니까?", "등록", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestCreditCardAdd();
                    }
                }, "취소", null);
            } else if(_view == mBtnCompanyYN) {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void toggleCompanyYN() {
        try {
            mBtnCompanyYN.setSelected(!mBtnCompanyYN.isSelected());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkCardInformation() {
        try {
            String cardNumber = mCardNumberText.getText().toString();
            String userName = mUserNameText.getText().toString();
            String userEmail = mUserEmailText.getText().toString();

            if (cardNumber.length() == 0) {
                CommonDialog.showSimpleDialog(mMainActivity, getString(R.string.card_register_cardnumber_error));
                return false;
            }

            if (userName.length() == 0) {
                CommonDialog.showSimpleDialog(mMainActivity, getString(R.string.card_register_username_error));
                return false;
            }

            if (userEmail.length() == 0) {
                CommonDialog.showSimpleDialog(mMainActivity, getString(R.string.card_register_useremail_error));
                return false;
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void requestCreditCardAdd() {
        if (!checkCardInformation()) {
            return;
        }

        Utils.hideSoftInput(mMainActivity);

        mMainActivity.showLoadingAnim();

        RequestParams requestParams = new RequestParams();
        requestParams.add("userId", mMainActivity.mUserDetail.getUserId());
        requestParams.put("cardNumber", mCardNumberText.getText().toString());
        requestParams.put("userName", mUserNameText.getText().toString());
        requestParams.put("userEmail", mUserEmailText.getText().toString());
        requestParams.put("companyCardYN", mBtnCompanyYN.isSelected() ? "Y" : "N");

        // 서버 연동시 주석 제거
        NetClient.send(mMainActivity, Configuration.URL_CREDIT_CARD_ADD, "POST", requestParams, new CreditCardAddAck(), new NetResponseCallback(new CreditCardAddResponse()));
    }

    private class CreditCardAddResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            mMainActivity.hideLoadingAnim();

            if (_netAPI != null) {
                ResultData resultData = _netAPI.getmResultData();
                if (resultData.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                    closeFragment();
                } else { // rollback
                    CommonDialog.showSimpleDialog(mMainActivity, resultData.getmDetail());
                }
            }
        }
    }

    private void closeFragment() {
        try {
            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
            getActivity().getSupportFragmentManager().beginTransaction().remove(SettingCardAddFragment.this).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Utils.hideSoftInput(getActivity());
    }
}

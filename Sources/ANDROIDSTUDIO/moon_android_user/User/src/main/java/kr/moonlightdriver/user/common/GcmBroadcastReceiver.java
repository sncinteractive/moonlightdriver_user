package kr.moonlightdriver.user.common;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.WakefulBroadcastReceiver;

import kr.moonlightdriver.user.viewmodel.callDriver.DriverInfoViewModel;
import kr.moonlightdriver.user.viewmodel.common.ActivityResultEvent;

/**
 * Created by youngmin on 2016-10-31.
 */

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {
    private static final String TAG = "GcmBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            ComponentName comp = new ComponentName(context.getPackageName(), GcmIntentService.class.getName());
            startWakefulService(context, (intent.setComponent(comp)));

            Utils.log("error", TAG, "receive : " + intent.getExtras().toString()); // 서버에서 어떻게 넘어올지 모르니 일단.. pass
            String pushMsgType = intent.getExtras().getString("type", null);
            if (Utils.isStringNullOrEmpty(pushMsgType)) {
                return;
            }

            updateCallDriverInfoToSharedPreferences(context, intent);

            sendBroadcast(context, intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateCallDriverInfoToSharedPreferences(Context context, Intent intent) {
        try {
            String pushMsgType = intent.getStringExtra("type");
            String callStatus = intent.getStringExtra("callStatus");

            switch (pushMsgType) {
                case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_ASSIGNED: {
                    SharedPreferences prefs = Utils.getGCMPreferences(context);
                    SharedPreferences.Editor editor = prefs.edit();

                    editor.putString(Configuration.PROPERTY_LATEST_RECEIVED_CALL_DRIVER_PUSH_MSG_TYPE, pushMsgType);
	                editor.putString(Configuration.PROPERTY_CALL_INFO_STATUS, callStatus);
                    editor.putString(Configuration.PROPERTY_CALL_INFO_CALL_ID, intent.getStringExtra("callId"));

                    DriverInfoViewModel driverInfo = new DriverInfoViewModel(intent.getStringExtra("driverInfo"));
                    editor.putString(Configuration.PROPERTY_CALL_INFO_DRIVER_INFO, driverInfo.toString());

                    editor.apply();
                }
                break;
                case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_PRICE_OFFER: {
                    SharedPreferences prefs = Utils.getGCMPreferences(context);
                    SharedPreferences.Editor editor = prefs.edit();

                    editor.putString(Configuration.PROPERTY_LATEST_RECEIVED_CALL_DRIVER_PUSH_MSG_TYPE, pushMsgType);
	                //editor.putString(Configuration.PROPERTY_CALL_INFO_STATUS, callStatus);
                    editor.putString(Configuration.PROPERTY_CALL_INFO_CALL_ID, intent.getStringExtra("callId"));
	                editor.putInt(Configuration.PROPERTY_CALL_INFO_BIDDING_PRICE, Integer.parseInt(intent.getExtras().getString("offeredPrice", "0")));

                    DriverInfoViewModel driverInfo = intent.getParcelableExtra("driverInfo");
                    editor.putString(Configuration.PROPERTY_CALL_INFO_DRIVER_INFO, driverInfo.toString());

                    editor.apply();
                }
                break;
                case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_DRIVING: {
                    SharedPreferences prefs = Utils.getGCMPreferences(context);
                    SharedPreferences.Editor editor = prefs.edit();

                    editor.putString(Configuration.PROPERTY_LATEST_RECEIVED_CALL_DRIVER_PUSH_MSG_TYPE, pushMsgType);

	                editor.putString(Configuration.PROPERTY_CALL_INFO_STATUS, callStatus);
                    editor.putString(Configuration.PROPERTY_CALL_INFO_CALL_ID, intent.getStringExtra("callId"));

                    editor.putLong(Configuration.PROPERTY_CALL_INFO_DEPARTURE_TIME, intent.getLongExtra("departureTime", 0));
                    editor.putLong(Configuration.PROPERTY_CALL_INFO_ESTIMATED_ARRIVAL_TIME, intent.getLongExtra("estimateTime", 0));

                    editor.apply();
                }
                break;
                case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_CANCELED: {
                    SharedPreferences prefs = Utils.getGCMPreferences(context);
                    SharedPreferences.Editor editor = prefs.edit();

                    editor.putString(Configuration.PROPERTY_LATEST_RECEIVED_CALL_DRIVER_PUSH_MSG_TYPE, pushMsgType);
                    editor.putString(Configuration.PROPERTY_CALL_INFO_STATUS, callStatus);
                    editor.putString(Configuration.PROPERTY_CALL_INFO_CALL_ID, intent.getStringExtra("callId"));

                    editor.apply();
                }
                break;
                case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_ARRIVED_TARGET: {
                    SharedPreferences prefs = Utils.getGCMPreferences(context);
                    SharedPreferences.Editor editor = prefs.edit();

                    editor.putString(Configuration.PROPERTY_LATEST_RECEIVED_CALL_DRIVER_PUSH_MSG_TYPE, pushMsgType);
                    editor.putString(Configuration.PROPERTY_CALL_INFO_STATUS, callStatus);
                    editor.putString(Configuration.PROPERTY_CALL_INFO_CALL_ID, intent.getStringExtra("callId"));

                    editor.apply();
                }
                break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendBroadcast(Context context, Intent intent) {
        String pushMsgType = intent.getExtras().getString("type", "");
        switch (pushMsgType) {
            case Configuration.PUSH_MSG_TYPE_REGISTER_VIRTUAL_NUMBER: {
                String virtualNumber = intent.getExtras().getString("virtual_number", "");

                Intent pushMsgIntent = new Intent();
                pushMsgIntent.setAction(Configuration.PUSH_INTENT_ACTION_VIRTUAL_NUMBER_RECEIVED);
                pushMsgIntent.putExtra("virtualNumber", virtualNumber);
                context.sendBroadcast(pushMsgIntent);
            }
            break;
//            case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_NOT_ASSIGNED: {
//                Intent pushMsgIntent = new Intent();
//                pushMsgIntent.setAction(Configuration.PUSH_INTENT_ACTION_CALL_DRIVER_NOT_ASSIGNED);
//                context.sendBroadcast(pushMsgIntent);
//            }
//            break;
            case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_ASSIGNED: {
                Intent pushMsgIntent = new Intent();
                pushMsgIntent.setAction(Configuration.PUSH_INTENT_ACTION_CALL_DRIVER_ASSIGNED);
                context.sendBroadcast(pushMsgIntent);
            }
            break;
            case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_PRICE_OFFER: {
                Intent pushMsgIntent = new Intent();
                pushMsgIntent.setAction(Configuration.PUSH_INTENT_ACTION_CALL_DRIVER_PRICE_OFFER);
                context.sendBroadcast(pushMsgIntent);
            }
            break;
            case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_DRIVING: {
                Intent pushMsgIntent = new Intent();
                pushMsgIntent.setAction(Configuration.PUSH_INTENT_ACTION_CALL_DRIVER_DRIVING);
                context.sendBroadcast(pushMsgIntent);
            }
            break;
            case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_CANCELED: {
                Intent pushMsgIntent = new Intent();
                pushMsgIntent.setAction(Configuration.PUSH_INTENT_ACTION_CALL_DRIVER_CANCEL);
                context.sendBroadcast(pushMsgIntent);
            }
            break;
            case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_ARRIVED_TARGET: {
                Intent pushMsgIntent = new Intent();
                pushMsgIntent.setAction(Configuration.PUSH_INTENT_ACTION_CALL_ARRIVED_TARGET);
                context.sendBroadcast(pushMsgIntent);
            }
            break;
            case Configuration.PUSH_MSG_TYPE_CALL_DRIVER_POSITION_CHANGED: {
                Intent newIntent = new Intent();
                newIntent.putExtra("lat", intent.getDoubleExtra("lat", 0D));
                newIntent.putExtra("lng", intent.getDoubleExtra("lng", 0D));
                BusProvider.getInstance().post(new ActivityResultEvent(Configuration.CALL_DRIVER_POSITION_CHANGED, Activity.RESULT_OK, intent));
            }
            break;
            default:
                break;
        }
    }
}

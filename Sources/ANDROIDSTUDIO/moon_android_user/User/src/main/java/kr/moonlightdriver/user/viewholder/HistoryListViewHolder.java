package kr.moonlightdriver.user.viewholder;

import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by eklee on 2017. 4. 24..
 */

public class HistoryListViewHolder {
    public TextView mDateTextView;
    public TextView mReliefMessageTextView;
    public TextView mPathTextView;
    public TextView mPayAmountTextView;
    public TextView mDriverInfoTextView;
    public TextView mDriverCompanyTextView;
    public ImageButton mEvaluateDriverButton;
    public ImageButton mReportUnfairnessButton;
}

package kr.moonlightdriver.user.common.network;

import kr.moonlightdriver.user.common.network.NetApi.TMapRouteModel;

/**
 * Created by eklee on 2017. 6. 8..
 */

public interface TMapRouteResultCallback {
    void onResult(boolean success, TMapRouteModel model);
}

package kr.moonlightdriver.user.viewmodel.common;

import android.content.Context;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.viewmodel.callDriver.DriverInfoViewModel;

/**
 * Created by eklee on 2017. 6. 9..
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReliefMessageViewModel {
    @JsonProperty("sourceName")
    private String mSourceName;

    @JsonProperty("targetName")
    private String mTargetName;

    @JsonProperty("stopbyNameList")
    private List<String> mStopbyNameList;

    @JsonProperty("deparutreTime")
    private long mDepartureTime;

    @JsonProperty("arrivalTime")
    private long mArrivalTime;

    @JsonProperty("driverInfo")
    private DriverInfoViewModel mDriverInfo;

    public ReliefMessageViewModel() {}

    public ReliefMessageViewModel(String sourceName, String targetName, List<String> stopbyNameList, long departureTime, long arrivalTime, DriverInfoViewModel driverInfo) {
        mSourceName = sourceName;
        mTargetName = targetName;
        mStopbyNameList = stopbyNameList;
        mDepartureTime = departureTime;
        mArrivalTime = arrivalTime;
        mDriverInfo = driverInfo;
    }

    public String getSourceName() {
        return mSourceName;
    }

    public void setSourceName(String sourceName) {
        mSourceName = sourceName;
    }

    public String getTargetName() {
        return mTargetName;
    }

    public void setTargetName(String targetName) {
        mTargetName = targetName;
    }

    public List<String> getStopbyNameList() {
        return mStopbyNameList;
    }

    public void setStopbyNameList(List<String> stopbyNameList) {
        mStopbyNameList = stopbyNameList;
    }

    public long getDepartureTime() {
        return mDepartureTime;
    }

    public void setDepartureTime(long departureTime) {
        mDepartureTime = departureTime;
    }

    public long getArrivalTime() {
        return mArrivalTime;
    }

    public void setArrivalTime(long arrivalTime) {
        mArrivalTime = arrivalTime;
    }

    public DriverInfoViewModel getDriverInfo() {
        return mDriverInfo;
    }

    public void setDriverInfo(DriverInfoViewModel driverInfo) {
        mDriverInfo = driverInfo;
    }
}

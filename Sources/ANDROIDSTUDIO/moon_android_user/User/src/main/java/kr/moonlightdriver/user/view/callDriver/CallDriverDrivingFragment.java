package kr.moonlightdriver.user.view.callDriver;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.BusProvider;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.CustomInfoWindowAdapter;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetApi.TMapRouteModel;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.TMapRouteResultCallback;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewmodel.common.LocationData;

public class CallDriverDrivingFragment extends Fragment implements OnMapReadyCallback {
    public interface InteractionListener {
        void onShowReliefMessagePopup();
    }

    private static final String TAG = "CallDriverDrivingFragment";

    private MainActivity mMainActivity;

    private View mViewMyself;

    // 맵
    private GoogleMap mGoogleMap;
    private LatLng mCurrentLocation;
    private Marker mCurrentPositionMarker;
    private Marker mCurrentPositionMarkerInner;
    private Polyline mPolyline;

    private LatLng mSourceLocation;
    private LatLng mTargetLocation;
    private List<LatLng> mStopbyLocationList;

    private LinearLayout mReliefMessageLayout;

    private InteractionListener mInteractionListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            mViewMyself = inflater.inflate(R.layout.fragment_call_driver_driving, container, false);

            mMainActivity = (MainActivity) getActivity();

            mCurrentLocation = MainActivity.mCurrentLocation;

            //region init location data
            SharedPreferences prefs = Utils.getGCMPreferences(mMainActivity);

            LocationData sourceLocationData = new LocationData(prefs.getString(Configuration.PROPERTY_CALL_INFO_SOURCE_LOCATION, null));
            LatLng sourceLocation = sourceLocationData.getmLocation();
            mSourceLocation = new LatLng(sourceLocation.latitude, sourceLocation.longitude);

            LocationData targetLocationData = new LocationData(prefs.getString(Configuration.PROPERTY_CALL_INFO_TARGET_LOCATION, null));
            LatLng targetLocation = targetLocationData.getmLocation();
            mTargetLocation = new LatLng(targetLocation.latitude, targetLocation.longitude);

            List<LocationData> stopbyLocationDataList = Utils.parseJSONStringToStopByLocationDataList(prefs.getString(Configuration.PROPERTY_CALL_INFO_STOPBY_LOCATION_LIST, null));
            if (stopbyLocationDataList != null) {
                if (mStopbyLocationList == null) {
                    mStopbyLocationList = new ArrayList<>();
                }

                for (LocationData locationData : stopbyLocationDataList) {
                    LatLng location = locationData.getmLocation();
                    mStopbyLocationList.add(new LatLng(location.latitude, location.longitude));
                }
            }
            //endregion init location data

            TextView message1TextView = (TextView) mViewMyself.findViewById(R.id.tv_message1);
            TextView message2TextView = (TextView) mViewMyself.findViewById(R.id.tv_message2);
            TextView message3TextView = (TextView) mViewMyself.findViewById(R.id.tv_message3);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), message1TextView, message2TextView, message3TextView);

            // 현위치로
            ImageButton currentLocationButton = (ImageButton) mViewMyself.findViewById(R.id.ib_current_location);
            currentLocationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mGoogleMap != null) {
                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mCurrentLocation, Utils.GOOGLE_MAP_DEFAULT_ZOOM));
                    }
                }
            });

            // 줌인
            ImageButton zoomInButton = (ImageButton) mViewMyself.findViewById(R.id.ib_zoom_in);
            zoomInButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mGoogleMap != null) {
                        mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
                    }
                }
            });

            // 줌아웃
            ImageButton zoomOutButton = (ImageButton) mViewMyself.findViewById(R.id.ib_zoom_out);
            zoomOutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mGoogleMap != null) {
                        mGoogleMap.animateCamera(CameraUpdateFactory.zoomOut());
                    }
                }
            });

            // 안심 메시지 레이아웃
            mReliefMessageLayout = (LinearLayout) mViewMyself.findViewById(R.id.ll_relief_message);
            setReliefMessageLayoutVisibility();

            // 안심 메시지 취소 버튼 클릭
            ImageButton sendReliefMessageCancelButton = (ImageButton) mReliefMessageLayout.findViewById(R.id.ib_send_relief_message_cancel);
            sendReliefMessageCancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mReliefMessageLayout.setVisibility(View.GONE);
                }
            });

            // 안심 메시지 전송 버튼 클릭
            ImageButton sendReliefMessageOKButton = (ImageButton) mReliefMessageLayout.findViewById(R.id.ib_send_relief_message_ok);
            sendReliefMessageOKButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mInteractionListener != null) {
                        mInteractionListener.onShowReliefMessagePopup();
                    }
                }
            });

            setUpMapIfNeeded();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return mViewMyself;
    }

    private void setUpMapIfNeeded() {
        try {
            if (mGoogleMap == null) {
                SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_google_map);
                mapFragment.getMapAsync(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setInteractionListener(InteractionListener interactionListener) {
        mInteractionListener = interactionListener;
    }

    public void onMapReady(GoogleMap googleMap) {
        try {
            mGoogleMap = googleMap;

            mGoogleMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(getContext()));

            mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
            mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
            mGoogleMap.getUiSettings().setCompassEnabled(false);

            mCurrentPositionMarker = mGoogleMap.addMarker(new MarkerOptions().position(mCurrentLocation)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_current_location_outer))
                .anchor(0.5f, 0f));
            mCurrentPositionMarker.showInfoWindow();

            mCurrentPositionMarkerInner = mGoogleMap.addMarker(new MarkerOptions().position(mCurrentLocation)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_current_location_inner))
                .anchor(0.5f, -0.3739f));

            mGoogleMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_source))
                .position(mSourceLocation));

            mGoogleMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_target))
                .position(mTargetLocation));

            int stopbySize = mStopbyLocationList.size();
            for (int i = 0; i < stopbySize; i++) {
                mGoogleMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_stopby))
                    .position(mStopbyLocationList.get(i)));
            }

            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCurrentLocation, Utils.GOOGLE_MAP_DEFAULT_ZOOM));

            PolylineOptions polylineOptions = new PolylineOptions();
            polylineOptions.width(15f); // default width 10
            mPolyline = mGoogleMap.addPolyline(polylineOptions);
            setPolylineVisibility(false);

            mMainActivity.removeSplash();

            updatePath();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        BusProvider.getInstance().unregister(this);
        super.onDestroyView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == Activity.RESULT_OK) {
                switch (requestCode) {
                    case Configuration.POSITION_CHANGED: {
                        onChangedMyLocation();
                    }
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        setReliefMessageLayoutVisibility();
    }

    private void setReliefMessageLayoutVisibility() {
        try {
            SharedPreferences prefs = Utils.getGCMPreferences(getActivity());
            String reliefMessageState = prefs.getString(Configuration.PROPERTY_RELIEF_MESSAGE_STATE, "");
            if (!Utils.isStringNullOrEmpty(reliefMessageState)) {
                mReliefMessageLayout.setVisibility(View.GONE);
            } else {
                mReliefMessageLayout.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setPolylinePoints(List<LatLng> pointList) {
        try {
            if (mPolyline != null) {
                mPolyline.setPoints(pointList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setPolylineVisibility(boolean visible) {
        try {
            if (mPolyline != null) {
                mPolyline.setVisible(visible);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onChangedMyLocation() {
        try {
            if (mGoogleMap != null) {
                mCurrentLocation = MainActivity.mCurrentLocation;

                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(mCurrentLocation));

                if (mCurrentPositionMarker != null) {
                    mCurrentPositionMarker.setPosition(mCurrentLocation);
                }

                if (mCurrentPositionMarkerInner != null) {
                    mCurrentPositionMarkerInner.setPosition(mCurrentLocation);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updatePath() {
        try {
            if (mGoogleMap == null) {
                return;
            }

            mMainActivity.showLoadingAnim();

            NetClient.requestTMapRoute(getContext(), mSourceLocation, mTargetLocation, mStopbyLocationList, new TMapRouteResultCallback() {
                @Override
                public void onResult(boolean success, TMapRouteModel tMapRouteModel) {
                    if (success) {
                        List<LatLng> pointList = tMapRouteModel.getPointList();
                        if (pointList != null && !pointList.isEmpty()) {
                            setPolylinePoints(pointList);
                            setPolylineVisibility(true);

                            String snippet = getString(R.string.remaining_info_format,
                                Utils.convertTimeString(tMapRouteModel.getTotalTime()),
                                Utils.convertDistanceString(tMapRouteModel.getTotalDistance()));
                            mCurrentPositionMarker.setSnippet(snippet);
                            mCurrentPositionMarker.showInfoWindow();
                        }
                    } else {
                        setPolylineVisibility(false);
                    }

                    updateRemainingInfo();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            mMainActivity.hideLoadingAnim();
        }
    }

    private void updateRemainingInfo() {
        try {
            if (mGoogleMap == null) {
                return;
            }

            mMainActivity.showLoadingAnim();

            NetClient.requestTMapRoute(getContext(), mCurrentLocation, mTargetLocation, null, new TMapRouteResultCallback() {
                @Override
                public void onResult(boolean success, TMapRouteModel tMapRouteModel) {
                    if (success) {
                        String snippet = getString(R.string.remaining_info_format,
                            Utils.convertTimeString(tMapRouteModel.getTotalTime()),
                            Utils.convertDistanceString(tMapRouteModel.getTotalDistance()));
                        mCurrentPositionMarker.setSnippet(snippet);
                        mCurrentPositionMarker.showInfoWindow();
                    }

                    mMainActivity.hideLoadingAnim();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            mMainActivity.hideLoadingAnim();
        }
    }
}

package kr.moonlightdriver.user.view.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

import kr.moonlightdriver.user.Adapter.CouponListAdapter;
import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.CouponListAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.CouponListViewModel;

/**
 * Created by youngmin on 2016-06-21.
 */
public class SettingCouponFragment extends Fragment implements View.OnClickListener, CouponListAdapter.InteractionListener {
    public static final int REQ_CODE_MODIFY = 1;
    public static final int REQ_CODE_ADD = 2;

    private ListView mListView;
    private CouponListAdapter mListAdapter;
    private List<CouponListViewModel> mCouponModelList;

    private TextView mTvEmptyList;

    private MainActivity mMainActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            mMainActivity = (MainActivity) getActivity();

            view = inflater.inflate(R.layout.fragment_setting_coupon, container, false);

            TextView headerTitleTextView = (TextView)view.findViewById(R.id.tv_header_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanB(), headerTitleTextView);

            LinearLayout goBackButton = (LinearLayout) view.findViewById(R.id.ib_setting_goback);
            goBackButton.setOnClickListener(this);

            mTvEmptyList = (TextView) view.findViewById(R.id.tv_empty_list);
            mTvEmptyList.setVisibility(View.GONE);

            mCouponModelList = new ArrayList<>();

            mListAdapter = new CouponListAdapter(mMainActivity, R.layout.row_setting_coupon_list, mCouponModelList, this);
            mListView = (ListView) view.findViewById(R.id.lv_coupon);
            mListView.setAdapter(mListAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return view;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            requestCouponListGet();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View _view) {
        switch (_view.getId()) {
            case R.id.ib_setting_goback: {
                getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
            }
            break;
        }
    }

    private void requestCouponListGet() {
        try {
            mMainActivity.showLoadingAnim();

            RequestParams requestParams = new RequestParams();
            requestParams.add("userId", mMainActivity.mUserDetail.getUserId());

            NetClient.send(mMainActivity, Configuration.URL_COUPON_LIST_GET, "POST", requestParams, new CouponListAck(), new NetResponseCallback(new CouponListGetResponse()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class CouponListGetResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            mMainActivity.hideLoadingAnim();

            mCouponModelList.clear();

            if (_netAPI != null) {
                ResultData resultData = _netAPI.getmResultData();
                if (resultData.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                    List<CouponListViewModel> list = ((CouponListAck)_netAPI).getCouponList();

                    for (CouponListViewModel l : list) {
                        mCouponModelList.add(l);
                    }

                    mListAdapter.notifyDataSetChanged();

                    if(mCouponModelList.size() <= 0) {
                        mTvEmptyList.setVisibility(View.VISIBLE);
                    } else {
                        mTvEmptyList.setVisibility(View.GONE);
                    }
                } else { // rollback
                    CommonDialog.showSimpleDialog(mMainActivity, resultData.getmDetail());
                }
            }
        }
    }

    @Override
    public void OnCouponItemClick(int position) {
        try {
            Fragment fragment = new SettingCouponDetailFragment();
            Bundle bundle = new Bundle(2);
            bundle.putString(SettingCouponDetailFragment.PARAMS_1, mCouponModelList.get(position).getmUserCouponId());
            bundle.putString(SettingCouponDetailFragment.PARAMS_2, mCouponModelList.get(position).getmCouponId());
            fragment.setArguments(bundle);

            mMainActivity.addFragment(fragment);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
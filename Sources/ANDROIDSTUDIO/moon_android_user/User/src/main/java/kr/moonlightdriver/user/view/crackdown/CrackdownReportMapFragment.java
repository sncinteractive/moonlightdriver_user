package kr.moonlightdriver.user.view.crackdown;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.RequestParams;
import com.skt.Tmap.TMapAddressInfo;

import java.util.HashMap;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.ResultCallback;
import kr.moonlightdriver.user.common.TmapManager;
import kr.moonlightdriver.user.common.UploadFile;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.user.common.network.NetApi.UploadImageAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewmodel.common.ResultData;

/**
 * Created by youngmin on 2016-06-21.
 */
public class CrackdownReportMapFragment extends Fragment implements OnMapReadyCallback, View.OnClickListener {
	private static final String TAG = "CrackdownReportMapFragment";
	private final float DEFAULT_ZOOM = 16f;
	private MainActivity mMainActivity;

	private ImageButton mBtnCurrentPosition;
	private ImageButton mBtnZoomin;
	private ImageButton mBtnZoomout;
	private ImageButton mBtnCrackdownReportCancel;
	private ImageButton mBtnCrackdownReportRegister;

	private GoogleMap mGoogleMap;
	private Marker mCurrentPositionMarker;
//	private Marker mCurrentPositionInnerMarker;
//	private Marker mCurrentPositionOuterMarker;

	private String mSelectedImagePath;
	private View mView;

	private LatLng mCrackdownPosition;
	private TMapAddressInfo mCrackdownAddress;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.fragment_crackdown_report_map, container, false);
			mMainActivity = (MainActivity) getActivity();
//			mMainActivity.setTabMenuVisibility(View.GONE);

			TextView crackdownReportMapDesc = (TextView) mView.findViewById(R.id.crackdown_report_map_desc);
			mBtnCurrentPosition = (ImageButton) mView.findViewById(R.id.btn_current_position);
			mBtnZoomin = (ImageButton) mView.findViewById(R.id.btn_zoomin);
			mBtnZoomout = (ImageButton) mView.findViewById(R.id.btn_zoomout);
			mBtnCrackdownReportCancel = (ImageButton) mView.findViewById(R.id.btn_crackdown_report_cancel);
			mBtnCrackdownReportRegister = (ImageButton) mView.findViewById(R.id.btn_crackdown_report_register);

			mBtnCurrentPosition.setOnClickListener(this);
			mBtnZoomin.setOnClickListener(this);
			mBtnZoomout.setOnClickListener(this);
			mBtnCrackdownReportCancel.setOnClickListener(this);
			mBtnCrackdownReportRegister.setOnClickListener(this);

			Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), crackdownReportMapDesc);

			Bundle bundle = CrackdownReportMapFragment.this.getArguments();
			mSelectedImagePath = bundle.getString("selectedImagePath", "");

			setUpMapIfNeeded();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mView;
	}

	@Override
	public void onClick(View view) {
		try {
			if(view == mBtnCurrentPosition) {
				mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(MainActivity.mCurrentLocation, DEFAULT_ZOOM));
			} else if(view == mBtnZoomin) {
				mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
			} else if(view == mBtnZoomout) {
				mGoogleMap.animateCamera(CameraUpdateFactory.zoomOut());
			} else if(view == mBtnCrackdownReportCancel) {
				closeFragment();
			} else if(view == mBtnCrackdownReportRegister) {
				registerCrackdownReport();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void registerCrackdownReport() {
		try {
			mMainActivity.showConfirmPopup("음주 단속 제보", "선택한 위치로 음주 단속을 제보 하시겠습니까?", new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					try {
						mMainActivity.showLoadingAnim();

						getAddress();

						mMainActivity.closeConfirmPopup();
					} catch (Exception e) {
						mMainActivity.hideLoadingAnim();
						e.printStackTrace();
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendCrackdownReport(LatLng _location, String _sido, String _sigugun, String _address, String _picture) {
		try {
			RequestParams params = new RequestParams();
			params.add("userId", mMainActivity.mUserDetail.getUserId());
			params.add("lat", "" + _location.latitude);
			params.add("lng", "" + _location.longitude);
			params.add("sido", _sido);
			params.add("sigugun", _sigugun);
			params.add("address", _address);
			params.add("registrant", Utils.getCrackdownRegistrantEncrypt(mMainActivity.mUserDetail.getPhoneNumber().substring(mMainActivity.mUserDetail.getPhoneNumber().length() - 8, mMainActivity.mUserDetail.getPhoneNumber().length())));
			params.add("picture", _picture);

			if(!Utils.isStringNullOrEmpty(_picture)) {
				params.add("reportType", "photo");
			} else {
				params.add("reportType", "normal");
			}

			NetClient.send(mMainActivity, Configuration.URL_REGISTER_CRACKDOWN, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					try {
						CommonDialog.hideProgressDialog();
						mMainActivity.hideLoadingAnim();

						if (_netAPI != null) {
							NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
							ResultData result = retNetApi.getmResultData();
							if (result.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
								CommonDialog.showSimpleDialog(mMainActivity, "등록 되었습니다.");

								getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);

								closeFragment();
							} else {
								CommonDialog.showSimpleDialog(mMainActivity, result.getmDetail());
							}
						}
					} catch(Exception e) {
						CommonDialog.hideProgressDialog();
						mMainActivity.hideLoadingAnim();
						e.printStackTrace();
					}
				}
			}));
		} catch (Exception e) {
			CommonDialog.hideProgressDialog();
			mMainActivity.hideLoadingAnim();
			e.printStackTrace();
		}
	}

	private void uploadImage(String _selectedImagePath) {
		try {
			UploadImage uploadimage = new UploadImage(_selectedImagePath);
			uploadimage.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class UploadImage extends AsyncTask<Void, Void, String> {
		private String mSelectedImagePath;

		public UploadImage(String _selectedImagePath) {
			this.mSelectedImagePath = _selectedImagePath;
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			CommonDialog.showProgressDialog(mMainActivity, "사진첨부 중..");
		}

		@Override
		protected String doInBackground(Void... params) {
			String retMsg;

			try {
				UploadFile u = new UploadFile(Configuration.URL_UPLOAD_FILE, this.mSelectedImagePath);

				retMsg = u.uploadImage();
			} catch (Exception e) {
				e.printStackTrace();
				retMsg = "ERROR";
			}

			return retMsg;
		}

		@Override
		protected void onPostExecute(String responseBody) {
			super.onPostExecute(responseBody);

			try {
				Utils.log("error", TAG, "ret_msg : " + responseBody);

				ObjectMapper mapper = new ObjectMapper();
				JsonParser jp = mapper.getFactory().createParser(responseBody);
				UploadImageAck retNetApi = mapper.readValue(jp, UploadImageAck.class);
				jp.close();

				ResultData result = retNetApi.getmResultData();

				if (result.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
					CommonDialog.hideProgressDialog();
					mMainActivity.hideLoadingAnim();
					CommonDialog.showSimpleDialog(mMainActivity, result.getmDetail());
				} else {
					sendCrackdownReport(mCrackdownPosition, mCrackdownAddress.strCity_do, mCrackdownAddress.strGu_gun, mCrackdownAddress.strFullAddress, retNetApi.getmFilePath());
				}
			} catch (Exception e) {
				CommonDialog.hideProgressDialog();
				mMainActivity.hideLoadingAnim();
				e.printStackTrace();
			}
		}
	}

	private void closeFragment() {
		try {
			getChildFragmentManager().beginTransaction().remove(this).commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getAddress() {
		try {
			mCrackdownPosition = mGoogleMap.getCameraPosition().target;
			Utils.log("error", TAG, "crackdownPosition : " + mCrackdownPosition.toString());

			TmapManager tmapManager = new TmapManager();
			tmapManager.convertGpsToAddress(mCrackdownPosition, new ResultCallback() {
				@Override
				public void resultCallback(HashMap<String, Object> params) {
					try {
						mCrackdownAddress = (TMapAddressInfo) params.get("addressInfo");
						if(mCrackdownAddress != null) {
							if(!Utils.isStringNullOrEmpty(mSelectedImagePath)) {
								uploadImage(mSelectedImagePath);
							} else {
								sendCrackdownReport(mCrackdownPosition, mCrackdownAddress.strCity_do, mCrackdownAddress.strGu_gun, mCrackdownAddress.strFullAddress, "");
							}
						}
					} catch(Exception e) {
						mMainActivity.hideLoadingAnim();
						e.printStackTrace();
					}
				}
			});
		} catch (Exception e) {
			mMainActivity.hideLoadingAnim();
			e.printStackTrace();
		}
	}

	private void setUpMapIfNeeded() {
		try {
			if (mGoogleMap == null) {
				SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.crackdown_report_map);
				mapFragment.getMapAsync(this);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		try {
			mGoogleMap = googleMap;

			mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
			mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
			mGoogleMap.getUiSettings().setCompassEnabled(false);

			mCurrentPositionMarker = mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_carpool_current_position)).position(MainActivity.mCurrentLocation));
//			mCurrentPositionInnerMarker = mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(mMainActivity.mCurrentPositionInnerIcon)).position(MainActivity.mCurrentLocation));
//			mCurrentPositionOuterMarker = mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(mMainActivity.mCurrentPositionOuterIcon)).position(MainActivity.mCurrentLocation));

			mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MainActivity.mCurrentLocation, DEFAULT_ZOOM));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

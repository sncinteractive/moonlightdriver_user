package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.callDriver.CallInfoViewModel;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.common.UserAdditionalViewModel;
import kr.moonlightdriver.user.viewmodel.common.UserViewModel;

/**
 * Created by youngmin on 2016-08-18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserLoginAck extends NetAPI {
	@JsonProperty("userDetail")
	private UserViewModel mUserDetailData;

	@JsonProperty("userAdditional")
	private UserAdditionalViewModel mUserAdditionalData;

//	@JsonProperty("callDriver")
//	private CallDriverViewModel mCallDriverData;
	@JsonProperty("callInfo")
	private CallInfoViewModel mCallInfoData;
	public UserLoginAck() {}

    public UserLoginAck(ResultData mResultData, UserViewModel mUserDetailData, UserAdditionalViewModel mUserAdditionalData) {
        super(mResultData);
        this.mUserDetailData = mUserDetailData;
        this.mUserAdditionalData = mUserAdditionalData;
    }

    public UserViewModel getmUserDetailData() {
		return mUserDetailData;
	}

	public void setmUserDetailData(UserViewModel mUserDetailData) {
		this.mUserDetailData = mUserDetailData;
	}

	public UserAdditionalViewModel getmUserAdditionalData() {
		return mUserAdditionalData;
	}

	public void setmUserAdditionalData(UserAdditionalViewModel mUserAdditionalData) {
		this.mUserAdditionalData = mUserAdditionalData;
	}

	public CallInfoViewModel getmCallInfoData() {
		return mCallInfoData;
	}

	public void setmCallInfoData(CallInfoViewModel mCallInfoData) {
		this.mCallInfoData = mCallInfoData;
	}
//	public CallDriverViewModel getCallDriverData() {
//		return mCallDriverData;
//	}
//
//	public void setCallDriverData(CallDriverViewModel callDriverData) {
//		mCallDriverData = callDriverData;
//	}
}

package kr.moonlightdriver.user.view.main;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.CallDetailAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.viewmodel.callDriver.CallInfoViewModel;
import kr.moonlightdriver.user.viewmodel.common.ResultData;

/**
 * Created by eklee on 2017. 5. 31..
 */

public class CallDriverAssignedPopupActivity extends Activity {
    private static String TAG = "CallDriverAssignedPopupActivity";

    public RelativeLayout mLoadingProgress;

    private CallInfoViewModel mCallInfo;

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.popup_call_driver_assigned);

            mLoadingProgress = (RelativeLayout) findViewById(R.id.loading_progress);
            hideLoadingAnim();

            SharedPreferences prefs = Utils.getGCMPreferences(this);
            String callId = prefs.getString(Configuration.PROPERTY_CALL_INFO_CALL_ID, "");
            if(Utils.isStringNullOrEmpty(callId)) {
                CommonDialog.showSimpleDialog(this, "콜 상세조회에 실패했습니다. 다시 시도해 주세요.");
                return;
            } else {
                requestOrderDetail(callId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        try {
            TextView titleTextView = (TextView) findViewById(R.id.tv_popup_call_driver_assigned_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanVert(), titleTextView);

            TextView messageTextView = (TextView) findViewById(R.id.tv_popup_call_driver_assigned_message);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), messageTextView);

            String driverName = "";
            String driverPhoneNumber = mCallInfo.getmDriver().getmDriverPhoneNumber();
            if(!Utils.isStringNullOrEmpty(driverPhoneNumber)){
                driverName = driverPhoneNumber.substring(driverPhoneNumber.length() - 4, driverPhoneNumber.length());
            }

            String message = getString(R.string.popup_call_driver_assigned_message_format, Utils.convertToPriceString(mCallInfo.getmMoney()), driverName);
            if (mCallInfo.getmPaymentOption().equals(Configuration.PAY_METHOD_CASH)) {
                message += getString(R.string.popup_call_driver_assigned_pay_cash_message);
            }
            messageTextView.setText(message);

            ImageButton closeButton = (ImageButton) findViewById(R.id.ib_popup_call_driver_assigned_close);
            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCallInfo.getmPaymentOption().equals(Configuration.PAY_METHOD_CARD)) {
                        sendResult("cancel");
                    } else {
                        sendResult("confirm");
                    }
                }
            });

            ImageButton payCardButton = (ImageButton) findViewById(R.id.ib_popup_call_driver_assigned_pay_card);
            payCardButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO: 카드 결제 팝업 연동
                    sendResult("cardPayment");
                }
            });
            payCardButton.setVisibility(mCallInfo.getmPaymentOption().equals(Configuration.PAY_METHOD_CARD) ? View.VISIBLE : View.GONE);

            ImageButton payCashButton = findViewById(R.id.ib_popup_call_driver_assigned_pay_cash);
            payCashButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendResult("confirm");
                }
            });
            payCashButton.setVisibility(mCallInfo.getmPaymentOption().equals(Configuration.PAY_METHOD_CARD) ? View.GONE : View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendResult(String result) {
        Intent intent = new Intent();
        intent.putExtra("result", result);
        setResult(Activity.RESULT_OK, intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void requestOrderDetail(String callId) {
        try {
            RequestParams requestParams = new RequestParams();
            requestParams.put("callId", callId);

            //  오더 상세 조회 서버 연동
            NetClient.send(this, Configuration.URL_CALL_DRIVER_CALL_DETAIL, "POST", requestParams, new CallDetailAck(), new NetResponseCallback(new CallDetailResponse()));
        } catch (Exception e) {
            CommonDialog.showSimpleDialog(this, "콜 상세조회에 실패했습니다. 다시 시도해 주세요.");
        }
    }

    private class CallDetailResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            try {
                hideLoadingAnim();

                if (_netAPI != null) {
                    ResultData resultData = _netAPI.getmResultData();
                    if (resultData.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                        CommonDialog.showSimpleDialog(CallDriverAssignedPopupActivity.this, resultData.getmDetail());
                        return;
                    }

                    CallDetailAck ack = (CallDetailAck) _netAPI;
                    mCallInfo = ack.getmCallInfo();

                    initView();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void showLoadingAnim() {
        try {
            mLoadingProgress.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideLoadingAnim() {
        try {
            mLoadingProgress.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

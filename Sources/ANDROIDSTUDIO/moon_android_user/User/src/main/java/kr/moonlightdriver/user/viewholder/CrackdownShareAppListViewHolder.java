package kr.moonlightdriver.user.viewholder;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by youngmin on 2016-08-30.
 */
public class CrackdownShareAppListViewHolder {
	public ImageView mCrackdownShareRowAppIcon;
	public TextView mCrackdownShareRowAppName;
}

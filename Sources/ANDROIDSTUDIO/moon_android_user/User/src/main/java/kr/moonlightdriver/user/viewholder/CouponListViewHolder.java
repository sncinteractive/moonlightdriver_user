package kr.moonlightdriver.user.viewholder;

import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by kyo on 2017-04-12.
 */

public class CouponListViewHolder {
    public LinearLayout couponListItemLayout;
    public TextView couponNameTextView;
    public TextView couponDescTextView;
}

package kr.moonlightdriver.user.viewmodel.crackdown;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2016-08-30.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChauffeurCompanyViewModel {
	@JsonProperty("chauffeurCompanyId")
	private String mChauffeurCompanyId;

	@JsonProperty("companyName")
	private String mCompanyName;

	@JsonProperty("description")
	private String mDescription;

	@JsonProperty("useCount")
	private int mUseCount;

	@JsonProperty("region")
	private String mRegion;

	@JsonProperty("stopByFee")
	private int mStopByFee;

	@JsonProperty("price")
	private int mPrice;

	@JsonProperty("isBookmark")
	private boolean mIsBookmark;

	@JsonProperty("telNumber")
	private String mTelNumber;

	public ChauffeurCompanyViewModel() {}

	public ChauffeurCompanyViewModel(String mChauffeurCompanyId, String mCompanyName, String mDescription, int mUseCount, String mRegion, int mStopByFee, int mPrice, boolean mIsBookmark, String mTelNumber) {
		this.mChauffeurCompanyId = mChauffeurCompanyId;
		this.mCompanyName = mCompanyName;
		this.mDescription = mDescription;
		this.mUseCount = mUseCount;
		this.mRegion = mRegion;
		this.mStopByFee = mStopByFee;
		this.mPrice = mPrice;
		this.mIsBookmark = mIsBookmark;
		this.mTelNumber = mTelNumber;
	}

	public String getmChauffeurCompanyId() {
		return mChauffeurCompanyId;
	}

	public void setmChauffeurCompanyId(String mChauffeurCompanyId) {
		this.mChauffeurCompanyId = mChauffeurCompanyId;
	}

	public String getmCompanyName() {
		return mCompanyName;
	}

	public void setmCompanyName(String mCompanyName) {
		this.mCompanyName = mCompanyName;
	}

	public String getmDescription() {
		return mDescription;
	}

	public void setmDescription(String mDescription) {
		this.mDescription = mDescription;
	}

	public int getmUseCount() {
		return mUseCount;
	}

	public void setmUseCount(int mUseCount) {
		this.mUseCount = mUseCount;
	}

	public String getmRegion() {
		return mRegion;
	}

	public void setmRegion(String mRegion) {
		this.mRegion = mRegion;
	}

	public int getmStopByFee() {
		return mStopByFee;
	}

	public void setmStopByFee(int mStopByFee) {
		this.mStopByFee = mStopByFee;
	}

	public int getmPrice() {
		return mPrice;
	}

	public void setmPrice(int mPrice) {
		this.mPrice = mPrice;
	}

	public boolean ismIsBookmark() {
		return mIsBookmark;
	}

	public void setmIsBookmark(boolean mIsBookmark) {
		this.mIsBookmark = mIsBookmark;
	}

	public String getmTelNumber() {
		return mTelNumber;
	}

	public void setmTelNumber(String mTelNumber) {
		this.mTelNumber = mTelNumber;
	}
}

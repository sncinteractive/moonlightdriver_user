package kr.moonlightdriver.user.view.settings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.RequestParams;

import java.util.List;

import kr.moonlightdriver.user.Adapter.FavoriteListAdapter;
import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.FavoriteListAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewmodel.common.LocationData;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.FavoriteListViewModel;

/**
 * Created by youngmin on 2016-06-21.
 */
public class SettingFavoriteFragment extends Fragment implements View.OnClickListener {
    public static final int REQ_CODE_MODIFY = 1;
    public static final int REQ_CODE_ADD = 2;

    private ListView mListView;
    private FavoriteListAdapter mListAdapter;

    private MainActivity mMainActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;

        try {
            mMainActivity = (MainActivity) getActivity();

            view = inflater.inflate(R.layout.fragment_setting_favorite_manage, container, false);

            TextView headerTitleTextView = (TextView)view.findViewById(R.id.tv_header_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanB(), headerTitleTextView);

            LinearLayout goBackButton = (LinearLayout) view.findViewById(R.id.ib_setting_goback);
            goBackButton.setOnClickListener(this);

            ImageButton modifyButton = (ImageButton) view.findViewById(R.id.ib_setting_favorite_modify);
            modifyButton.setOnClickListener(this);

            ImageButton addButton = (ImageButton) view.findViewById(R.id.ib_favorite_add);
            addButton.setOnClickListener(this);

            mListAdapter = new FavoriteListAdapter(mMainActivity, R.layout.row_setting_favorite_list, mMainActivity.mUserAdditionalData.getFavoriteList());
            mListView = (ListView) view.findViewById(R.id.lv_favorite);
            mListView.setAdapter(mListAdapter);

            requestFavoriteListGet();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onClick(View _view) {
        switch (_view.getId()) {
            case R.id.ib_setting_goback: {
                getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
            }
            break;
            case R.id.ib_setting_favorite_modify: {
                SettingFavoriteModifyFragment fragment = new SettingFavoriteModifyFragment();
                fragment.setTargetFragment(this, REQ_CODE_MODIFY);
                mMainActivity.addFragment(fragment);
            }
            break;
            case R.id.ib_favorite_add: {
                SettingFavoriteAddFragment fragment = new SettingFavoriteAddFragment();
                fragment.setTargetFragment(this, REQ_CODE_ADD);
                mMainActivity.addFragment(fragment);
            }
            break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQ_CODE_ADD :
                case REQ_CODE_MODIFY :
                    requestFavoriteListGet();
                    CommonDialog.showSimpleDialog(mMainActivity, getString(R.string.favorite_add_complete));

                    break;
//                case REQ_CODE_MODIFY: {
//                    mListAdapter.notifyDataSetChanged();
//                    mListView.smoothScrollToPosition(0);
//                }
//                break;
//                case REQ_CODE_ADD: {
//                    mListAdapter.notifyDataSetChanged();
//                    mListView.smoothScrollToPosition(mListAdapter.getCount() - 1);
//                }
//                break;
            }
        }
    }

    private void requestFavoriteListGet() {
        try {
            mMainActivity.showLoadingAnim();

            RequestParams requestParams = new RequestParams();
            requestParams.add("userId", mMainActivity.mUserDetail.getUserId());

            // 서버 연동시 주석 제거
            NetClient.send(mMainActivity, Configuration.URL_FAVORITE_LIST_GET, "POST", requestParams, new FavoriteListAck(), new NetResponseCallback(new FavoriteListGetResponse()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class FavoriteListGetResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            mMainActivity.hideLoadingAnim();

            mMainActivity.mUserAdditionalData.getFavoriteList().clear();

            if (_netAPI != null) {
                ResultData resultData = _netAPI.getmResultData();
                if (resultData.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                    List<FavoriteListViewModel> list = ((FavoriteListAck)_netAPI).getFavoriteList();

                    for (FavoriteListViewModel l : list) {
                        LocationData locationData = new LocationData(new LatLng(l.getmLat(), l.getmLng()), l.getmLocationName(), l.getmLocationAddress());
                        FavoriteListViewModel model = new FavoriteListViewModel(l.getmFavoriteId(), l.getmName(), l.getmLocationName(), l.getmLocationAddress(), l.getmLat(), l.getmLng(), locationData);
                        mMainActivity.mUserAdditionalData.addFavoriteListItem(model);
                    }

                    mListAdapter.notifyDataSetChanged();
                } else { // rollback
                    CommonDialog.showSimpleDialog(mMainActivity, resultData.getmDetail());
                }
            }
        }
    }
}
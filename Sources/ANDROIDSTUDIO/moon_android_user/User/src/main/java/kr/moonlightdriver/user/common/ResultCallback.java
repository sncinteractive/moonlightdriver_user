package kr.moonlightdriver.user.common;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by youngmin on 2016-07-08.
 */
public interface ResultCallback extends Serializable{
	void resultCallback(HashMap<String, Object> params);
}

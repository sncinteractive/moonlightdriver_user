package kr.moonlightdriver.user.viewmodel.setting;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.viewmodel.common.LocationData;

/**
 * Created by kyo on 2017-04-12.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class FavoriteListViewModel{
    @JsonProperty("favoriteId")
    private String mFavoriteId;

    @JsonProperty("favoriteName")
    private String mName;

    @JsonProperty("locationName")
    private String mLocationName;

    @JsonProperty("locationAddress")
    private String mLocationAddress;

    @JsonProperty("lat")
    private Double mLat;

    @JsonProperty("lng")
    private Double mLng;

    @JsonProperty("location")
    private LocationData mLocationData;

    public FavoriteListViewModel() {
    }

    public FavoriteListViewModel(String name, LocationData locationData) {
        mName = name;
        mLocationData = locationData;
    }

    public FavoriteListViewModel(String mFavoriteId, String mName, String mLocationName, String mLocationAddress, Double mLat, Double mLng, LocationData mLocationData) {
        this.mFavoriteId = mFavoriteId;
        this.mName = mName;
        this.mLocationName = mLocationName;
        this.mLocationAddress = mLocationAddress;
        this.mLat = mLat;
        this.mLng = mLng;
        this.mLocationData = mLocationData;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public LocationData getLocationData() {
        return mLocationData;
    }

    public void setLocationData(LocationData locationData) {
        mLocationData = locationData;
    }

    public String getmFavoriteId() {
        return mFavoriteId;
    }

    public void setmFavoriteId(String mFavoriteId) {
        this.mFavoriteId = mFavoriteId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmLocationName() {
        return mLocationName;
    }

    public void setmLocationName(String mLocationName) {
        this.mLocationName = mLocationName;
    }

    public String getmLocationAddress() {
        return mLocationAddress;
    }

    public void setmLocationAddress(String mLocationAddress) {
        this.mLocationAddress = mLocationAddress;
    }

    public Double getmLat() {
        return mLat;
    }

    public void setmLat(Double mLat) {
        this.mLat = mLat;
    }

    public Double getmLng() {
        return mLng;
    }

    public void setmLng(Double mLng) {
        this.mLng = mLng;
    }

    public LocationData getmLocationData() {
        return mLocationData;
    }

    public void setmLocationData(LocationData mLocationData) {
        this.mLocationData = mLocationData;
    }
}

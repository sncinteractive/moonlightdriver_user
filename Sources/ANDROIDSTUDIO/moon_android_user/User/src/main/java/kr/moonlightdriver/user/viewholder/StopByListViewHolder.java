package kr.moonlightdriver.user.viewholder;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by youngmin on 2016-07-01.
 */
public class StopByListViewHolder {
	public TextView mLocationTypeTextView;
	public TextView mLocationTextView;
	public ImageButton mSearchMapButton;
}

package kr.moonlightdriver.user.viewmodel.common;

import android.content.Intent;

/**
 * Created by youngmin on 2016-11-09.
 */

public class ActivityResultEvent {
	private int requestCode;
	private int resultCode;
	private Intent data;

	public ActivityResultEvent() {	}

	public ActivityResultEvent(int requestCode, int resultCode, Intent data) {
		this.data = data;
		this.requestCode = requestCode;
		this.resultCode = resultCode;
	}

	public Intent getData() {
		return data;
	}

	public void setData(Intent data) {
		this.data = data;
	}

	public int getRequestCode() {
		return requestCode;
	}

	public void setRequestCode(int requestCode) {
		this.requestCode = requestCode;
	}

	public int getResultCode() {
		return resultCode;
	}

	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}
}

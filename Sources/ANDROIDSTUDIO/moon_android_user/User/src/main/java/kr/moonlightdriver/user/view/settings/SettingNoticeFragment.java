package kr.moonlightdriver.user.view.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.moonlightdriver.user.Adapter.NoticeListAdapter;
import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.view.main.MainActivity;

/**
 * Created by youngmin on 2016-06-21.
 */
public class SettingNoticeFragment extends Fragment {
    private static final String TAG = "SettingNoticeFragment";

    private MainActivity mMainActivity;
    private RecyclerView mRecyclerView;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            mMainActivity = (MainActivity) getActivity();

            view = inflater.inflate(R.layout.fragment_setting_notice, container, false);

            LinearLayout goBackButton = (LinearLayout) view.findViewById(R.id.ib_setting_goback);
            goBackButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getActivity().getSupportFragmentManager().beginTransaction().remove(SettingNoticeFragment.this).commit();
                }
            });

            TextView headerTitleTextView = (TextView) view.findViewById(R.id.tv_header_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanB(), headerTitleTextView);

            mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
            mRecyclerView.setAdapter(new NoticeListAdapter(mMainActivity.getNoticeDataList()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }
}
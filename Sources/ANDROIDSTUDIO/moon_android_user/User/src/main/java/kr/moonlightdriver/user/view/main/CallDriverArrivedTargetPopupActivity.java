package kr.moonlightdriver.user.view.main;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.CallDetailAck;
import kr.moonlightdriver.user.common.network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.viewmodel.callDriver.CallInfoViewModel;
import kr.moonlightdriver.user.viewmodel.callDriver.DriverInfoViewModel;
import kr.moonlightdriver.user.viewmodel.common.ResultData;

/**
 * Created by eklee on 2017. 5. 31..
 */

public class CallDriverArrivedTargetPopupActivity extends Activity implements View.OnClickListener {
    private static String TAG = "CallDriverAssignedPopupActivity";

    public RelativeLayout mLoadingProgress;

    private List<ImageButton> mEvaluateDriverStarButtonList = new ArrayList<>();

    private CallInfoViewModel mCallInfo;

    private RelativeLayout mPopupReportUnfairness;
    private EditText mPopupReportUnfairnessEditText;
    private TextView mPopupReportUnfairnessDriverInfoTextView;
    private TextView mPopupReportUnfairnessCompanyTextView;

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.popup_call_driver_arrived_at_target);

            mLoadingProgress = (RelativeLayout) findViewById(R.id.loading_progress);
            hideLoadingAnim();

            SharedPreferences prefs = Utils.getGCMPreferences(this);
            String callId = prefs.getString(Configuration.PROPERTY_CALL_INFO_CALL_ID, "");
            if (Utils.isStringNullOrEmpty(callId)) {
                CommonDialog.showSimpleDialog(this, "콜 상세조회에 실패했습니다. 다시 시도해 주세요.");
                return;
            } else {
                requestOrderDetail(callId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        String sourceMessage = "";
        String targetMessage = "";
        String driverName = "";
        try {
            sourceMessage = getString(R.string.popup_call_driver_arrived_at_target_source_info_format, mCallInfo.getmStart().getmAddress());
            targetMessage = getString(R.string.popup_call_driver_arrived_at_target_target_info_format, mCallInfo.getmEnd().getmAddress());
            driverName = mCallInfo.getmDriver().getmDriverPhoneNumber().substring(mCallInfo.getmDriver().getmDriverPhoneNumber().length() - 4, mCallInfo.getmDriver().getmDriverPhoneNumber().length());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            TextView titleTextView = (TextView) findViewById(R.id.tv_popup_call_driver_arrived_at_target_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanVert(), titleTextView);

            TextView sourceTextView = (TextView) findViewById(R.id.tv_popup_call_driver_arrived_at_target_source_info);
            TextView targetTextView = (TextView) findViewById(R.id.tv_popup_call_driver_arrived_at_target_target_info);
            TextView driverTextView = (TextView) findViewById(R.id.tv_popup_call_driver_arrived_at_target_driver_info);

            sourceTextView.setText(sourceMessage);
            targetTextView.setText(targetMessage);
            driverTextView.setText(driverName);

            ImageButton closeButton = (ImageButton) findViewById(R.id.ib_popup_call_driver_arrived_at_target_close);
            closeButton.setOnClickListener(this);

            LinearLayout starLayout = (LinearLayout) findViewById(R.id.ll_popup_call_driver_arrived_at_target_star_root);
            int childCount = starLayout.getChildCount();
            for (int i = 0; i < childCount; i++) {
                ImageButton starButton = (ImageButton) starLayout.getChildAt(i);
                if (starButton == null) {
                    continue;
                }
                starButton.setTag(i);
                starButton.setOnClickListener(this);
                mEvaluateDriverStarButtonList.add(starButton);
            }

            ImageButton submitButton = (ImageButton) findViewById(R.id.ib_popup_call_driver_arrived_at_target_evaluate_driver_complete);
            submitButton.setOnClickListener(this);

            TextView reportDriver = (TextView) findViewById(R.id.tv_popup_call_driver_arrived_at_target_report_driver_title);
            reportDriver.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendResult(String result) {
        Intent intent = new Intent();
        intent.putExtra("result", result);
        setResult(Activity.RESULT_OK, intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void requestOrderDetail(String callId) {
        try {
            RequestParams requestParams = new RequestParams();
            requestParams.put("callId", callId);

            //  오더 상세 조회 서버 연동
            NetClient.send(this, Configuration.URL_CALL_DRIVER_CALL_DETAIL, "POST", requestParams, new CallDetailAck(), new NetResponseCallback(new CallDetailResponse()));
        } catch (Exception e) {
            CommonDialog.showSimpleDialog(this, "콜 상세조회에 실패했습니다. 다시 시도해 주세요.");
        }
    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                // 닫기 클릭
                case R.id.ib_popup_call_driver_arrived_at_target_close: {
                    sendResult("cancel");
                }
                break;
                // 완료 클릭
                case R.id.ib_popup_call_driver_arrived_at_target_evaluate_driver_complete: {
                    requestEvaluateDriver();
                }
                break;
                // 기사 평가 별 클릭
                case R.id.ib_popup_call_driver_arrived_at_target_star1: // fall-through
                case R.id.ib_popup_call_driver_arrived_at_target_star2: // fall-through
                case R.id.ib_popup_call_driver_arrived_at_target_star3: // fall-through
                case R.id.ib_popup_call_driver_arrived_at_target_star4: // fall-through
                case R.id.ib_popup_call_driver_arrived_at_target_star5: {
                    int clickedPosition = (int) v.getTag();
                    int size = mEvaluateDriverStarButtonList.size();
                    for (int i = 0; i < size; i++) {
                        ImageButton starButton = mEvaluateDriverStarButtonList.get(i);
                        starButton.setSelected(i <= clickedPosition);
                    }
                }
                break;
                // 신고하기 클릭
                case R.id.tv_popup_call_driver_arrived_at_target_report_driver_title: {
                    showReportUnfairnessPopup();
                }
                break;
                // 부조리 신고 팝업 닫기 클릭
                case R.id.ib_popup_report_unfairness_close: {
                    mPopupReportUnfairness.setVisibility(View.GONE);
                }
                break;
                // 부조리 신고 팝업 신고하기 클릭
                case R.id.ib_popup_report_unfairness_submit: {
                    String content = mPopupReportUnfairnessEditText.getText().toString();
                    if (!Utils.isStringNullOrEmpty(content)) {
                        requestReportUnfairness(mCallInfo.getmCallId(), mCallInfo.getmDriver().getmDriverId(), content);
                    } else {
                        CommonDialog.showSimpleDialog(this, "신고할 내용을 적어주세요.");
                    }
                }
                break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class CallDetailResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            try {
                hideLoadingAnim();

                if (_netAPI != null) {
                    ResultData resultData = _netAPI.getmResultData();
                    if (resultData.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                        CommonDialog.showSimpleDialog(CallDriverArrivedTargetPopupActivity.this, resultData.getmDetail());
                        return;
                    }

                    CallDetailAck ack = (CallDetailAck) _netAPI;
                    mCallInfo = ack.getmCallInfo();

                    initView();
                    initReportUnfairnessPopup();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void showLoadingAnim() {
        try {
            mLoadingProgress.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideLoadingAnim() {
        try {
            mLoadingProgress.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initReportUnfairnessPopup() {
        try {
            mPopupReportUnfairness = (RelativeLayout) findViewById(R.id.popup_report_unfairness);

            TextView titleTextView = (TextView) mPopupReportUnfairness.findViewById(R.id.tv_popup_report_unfairness_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanVert(), titleTextView);

            TextView driverInfoTitleTextView = (TextView) mPopupReportUnfairness.findViewById(R.id.tv_popup_report_unfairness_driver_info_title);
            TextView companyTitleTextView = (TextView) mPopupReportUnfairness.findViewById(R.id.tv_popup_report_unfairness_driver_company_title);

            mPopupReportUnfairnessDriverInfoTextView = (TextView) mPopupReportUnfairness.findViewById(R.id.tv_popup_report_unfairness_driver_info);
            mPopupReportUnfairnessCompanyTextView = (TextView) mPopupReportUnfairness.findViewById(R.id.tv_popup_report_unfairness_driver_company);
            mPopupReportUnfairnessEditText = (EditText) mPopupReportUnfairness.findViewById(R.id.et_popup_report_unfairness_report_message);

            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(),
                driverInfoTitleTextView, companyTitleTextView,
                mPopupReportUnfairnessDriverInfoTextView, mPopupReportUnfairnessCompanyTextView, mPopupReportUnfairnessEditText);

            ImageButton reportUnfairnessCloseButton = (ImageButton) findViewById(R.id.ib_popup_report_unfairness_close);
            reportUnfairnessCloseButton.setOnClickListener(this);

            ImageButton reportUnfairnessSubmitButton = (ImageButton) findViewById(R.id.ib_popup_report_unfairness_submit);
            reportUnfairnessSubmitButton.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showReportUnfairnessPopup() {
        try {
            mPopupReportUnfairness.setVisibility(View.VISIBLE);

            mPopupReportUnfairnessEditText.setText("");

            String driverMaskedName = "";
            String driverGradeAverage = "";
            String driverLastPhoneNumber = "";

            if (mCallInfo != null) {
                DriverInfoViewModel driverInfoViewModel = mCallInfo.getmDriver();
                if (driverInfoViewModel != null) {
                    driverMaskedName = driverInfoViewModel.getMaskedDriverName();
                    driverGradeAverage = Utils.convertDriverGradeAverageToString(mCallInfo.getmDriver().getmDriverGradeAverage());
                    driverLastPhoneNumber = driverInfoViewModel.getDriverLastPhoneNumber();
                }
            }
            String driverInfo = getString(R.string.string_format_history_list_driver_info,
                driverMaskedName,
                driverGradeAverage,
                driverLastPhoneNumber);

            mPopupReportUnfairnessDriverInfoTextView.setText(driverInfo);
            mPopupReportUnfairnessCompanyTextView.setText("");
        } catch (Exception e) {
            e.printStackTrace();
            mPopupReportUnfairness.setVisibility(View.GONE);
        }
    }

    private void requestReportUnfairness(String callId, String driverId, String content) {
        showLoadingAnim();

        SharedPreferences prefs = Utils.getGCMPreferences(this);
        String userId = prefs.getString(Configuration.PROPERTY_USER_ID, null);

        RequestParams requestParams = new RequestParams();
        requestParams.put("userId", userId);
        requestParams.put("callId", callId);
        requestParams.put("driverId", driverId);
        requestParams.put("unfairness", content);

        // 서버 연동시 url 변경
        NetClient.send(this, Configuration.URL_CALL_DRIVER_REPORT_UNFAIRNESS, "POST", requestParams, new NoResponseDataAck(), new NetResponseCallback(new ReportUnfairnessResponse()));
    }

    private class ReportUnfairnessResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            try {
                hideLoadingAnim();

                if (_netAPI != null) {
                    ResultData resultData = _netAPI.getmResultData();
                    if (resultData.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                        CommonDialog.showSimpleDialog(CallDriverArrivedTargetPopupActivity.this, resultData.getmDetail());
                        return;
                    }

                    mPopupReportUnfairness.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void requestEvaluateDriver() {
        showLoadingAnim();

        try {
            int starNum = 0;
            for (ImageButton starButton : mEvaluateDriverStarButtonList) {
                if (starButton.isSelected()) {
                    starNum++;
                }
            }

            RequestParams requestParams = new RequestParams();
            requestParams.put("userId", mCallInfo.getmUser().getmUserId());
            requestParams.put("callId", mCallInfo.getmCallId());
            requestParams.put("driverId", mCallInfo.getmDriver().getmDriverId());
            requestParams.put("starNum", starNum);

            // 서버 연동시 url 변경
            NetClient.send(this, Configuration.URL_CALL_DRIVER_EVALUATE, "POST", requestParams, new NoResponseDataAck(), new NetResponseCallback(new EvaluateDriverResponse()));
        } catch (Exception e) {
            e.printStackTrace();
            hideLoadingAnim();
        }
    }

    private class EvaluateDriverResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            try {
                hideLoadingAnim();

                if (_netAPI != null) {
                    ResultData resultData = _netAPI.getmResultData();
                    if (resultData.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                        CommonDialog.showSimpleDialog(CallDriverArrivedTargetPopupActivity.this, resultData.getmDetail());
                        return;
                    }

                    SharedPreferences prefs = Utils.getGCMPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(Configuration.PROPERTY_CALL_INFO_STATUS, Enums.CallStatus.Done);
                    editor.apply();

                    sendResult("true");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

package kr.moonlightdriver.user.common.network;

import kr.moonlightdriver.user.common.network.NetApi.TMapRouteModel;

/**
 * Created by eklee on 2017.05.26..
 */
public interface TMapResponse {
    void onResponse(TMapRouteModel tMapRouteModel);
}

package kr.moonlightdriver.user.view.crackdown;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.loopj.android.http.RequestParams;
import com.skt.Tmap.TMapAddressInfo;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.BusProvider;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.ResultCallback;
import kr.moonlightdriver.user.common.TmapManager;
import kr.moonlightdriver.user.common.UploadFile;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.CrackdownDetailAck;
import kr.moonlightdriver.user.common.network.NetApi.CrackdownListAck;
import kr.moonlightdriver.user.common.network.NetApi.CrackdownRankListAck;
import kr.moonlightdriver.user.common.network.NetApi.NearDriverListAck;
import kr.moonlightdriver.user.common.network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.user.common.network.NetApi.RegisterCrackdownCommentAck;
import kr.moonlightdriver.user.common.network.NetApi.UploadImageAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.view.chauffeurCompany.ChauffeurCompanyListFragment;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.view.settings.SettingCouponFragment;
import kr.moonlightdriver.user.viewholder.CrackdownCommentListViewHolder;
import kr.moonlightdriver.user.viewholder.CrackdownRankListViewHolder;
import kr.moonlightdriver.user.viewholder.CrackdownShareAppListViewHolder;
import kr.moonlightdriver.user.viewmodel.common.ActivityResultEvent;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.crackdown.CrackdownCommentListViewModel;
import kr.moonlightdriver.user.viewmodel.crackdown.CrackdownListViewModel;
import kr.moonlightdriver.user.viewmodel.crackdown.CrackdownRankListViewModel;
import kr.moonlightdriver.user.viewmodel.crackdown.DriverPointViewModel;

/**
 * Created by youngmin on 2016-06-21.
 */
public class CrackdownFragment extends Fragment implements OnMapReadyCallback, View.OnClickListener {
    private static final String TAG = "CrackdownFragment";
    private static final int REQ_REPORT_MAP_CODE = 1;
    private MainActivity mMainActivity;

    private LinearLayout mBtnCurrentCrackdown;
    private LinearLayout mBtnPastCrackdown;
    private LinearLayout mCrackdownCommentInputLayout;
    private ImageButton mBtnCurrentPosition;
    private ImageButton mBtnRefresh;
    private ImageButton mBtnZoomin;
    private ImageButton mBtnZoomout;
    private ImageButton mBtnReportCrackdown_1;
    private ImageButton mBtnReportCrackdown_2;
    private ImageButton mBtnCallDriver;
    private Button mBtnCoupon;
    private ImageButton mBtnCrackdownShareClose;
    private LinearLayout mPopupCrackdownCommentLayout;
    private LinearLayout mPopupCrackdownPhotoLayout;
    private ImageButton mBtnCrackdownPhotoClose;
    private ImageButton mBtnCrackdownDelete;
    private TextView mTVCrackdownAddress;
    private TextView mTVCrackdownDistance;
    private TextView mTVCrackdownUpdateTime;
    private LinearLayout mBtnCrackdownApprove;
    private TextView mCrackdownApproveText;
    private ImageView mCrackdownApproveBg;
    private LinearLayout mBtnCrackdownDisapprove;
    private TextView mCrackdownDisapproveText;
    private ImageView mCrackdownDisapproveBg;
    private LinearLayout mBtnCrackdownShare;
    private TextView mCrackdownShareText;
    private LinearLayout mPopupCrackdownReportLayout;
    private ImageButton mBtnCrackdownReportVoice;
    private ImageButton mBtnCrackdownReportPoint;
    private ImageButton mBtnCrackdownReportPhoto;
    private ImageButton mBtnCrackdownReportClose;
    private ImageButton mBtnCrackdownRankListClose;
    private TextView mCrackdownCommentCount;
    private EditText mETCrackdownComment;
    private TextView mPastCrackdownText;
    private ListView mCrackdownCommentListView;
    private LinearLayout mPhotoLayout;
    private ImageView mPhotoPreview;
    private ImageButton mBtnCrackdownComment;
    private LinearLayout mPopupCrackdownShareLayout;
    private GridView mCrackdownShareAppListView;
    private CrackdownSharedAppListAdapter mCrackdownSharedAppListAdapter;
    private LinearLayout mCrackdownRankLayout;
    private LinearLayout mPopupCrackdownRankListLayout;
    private ListView mCrackdownWeeklyRankListView;
    private ListView mCrackdownMonthlyRankListView;
    private TextView mCrackdownRankMonthly;
    private TextView mCrackdownRankWeekly;
    private ImageView mImgCrackdownPhoto;
    private ImageButton mBtnPushOnOff;
    private TextView mCrackdownRankListTimeDesc;

    private GoogleMap mGoogleMap;
    private Marker mCurrentPositionMarker;
    private ArrayList<Marker> mCrackdownMarkerList;
    private ArrayList<Marker> mDriverMarkerList;

    private ArrayList<CrackdownRankListViewModel> mCrackdownWeeklyRankList;
    private ArrayList<CrackdownRankListViewModel> mCrackdownMonthlyRankList;
    private CrackdownRankListAdapter mCrackdownWeeklyRankListAdapter;
    private CrackdownRankListAdapter mCrackdownMonthlyRankListAdapter;

//	private Bitmap mDriverPinIcon;
//	private Bitmap mPastCrackdownPinIcon;
//	private Bitmap mPastCrackdownWithCommentPinIcon;
//	private Bitmap mPastCrackdownWidthPhotoNCommentPinIcon;
//	private Bitmap mCurrentCrackdownPinIcon;
//	private Bitmap mCurrentCrackdownWithCommentPinIcon;
//	private Bitmap mCurrentCrackdownWidthPhotoNCommentPinIcon;

    private ArrayList<CrackdownListViewModel> mCrackdownList;
    private ArrayList<CrackdownCommentListViewModel> mCrackdownCommentList;
    private CrackdownCommentListAdapter mCrackdownCommentListAdapter;

    private boolean mIsShowPastCrackdown;

    private String mSelectedCrackdownId;

    private View mView;

    private boolean mIsFragmentRunning;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
//            if (mView == null) {
                mView = inflater.inflate(R.layout.fragment_crackdown, container, false);
//            }

            mMainActivity = (MainActivity) getActivity();

            mSelectedCrackdownId = "";
            mIsFragmentRunning = false;

            initView();

            setUpMapIfNeeded();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mView;
    }

    private void initView() {
        try {
            TextView crackdownReportTitle = (TextView) mView.findViewById(R.id.crackdown_report_title);
            TextView crackdownRankTitle = (TextView) mView.findViewById(R.id.crackdown_rank_title);
            TextView crackdownDescription = (TextView) mView.findViewById(R.id.crackdown_description);
            TextView crackdownDescription2 = (TextView) mView.findViewById(R.id.crackdown_description_2);
            TextView crackdownReportPhotoDesc = (TextView) mView.findViewById(R.id.crackdown_report_photo_desc);
            TextView crackdownReportVoiceDesc = (TextView) mView.findViewById(R.id.crackdown_report_voice_desc);
            TextView crackdownReportPointDesc = (TextView) mView.findViewById(R.id.crackdown_report_point_desc);
            TextView currentCrackdownText = (TextView) mView.findViewById(R.id.current_crackdown_text);
            TextView crackdownPrevPhotoText = (TextView) mView.findViewById(R.id.crackdown_prev_photo_text);
            TextView crackdownAddressTitle = (TextView) mView.findViewById(R.id.crackdown_address_title);
            TextView crackdownDistanceTitle = (TextView) mView.findViewById(R.id.crackdown_distance_title);
            TextView crackdownRankListTitle = (TextView) mView.findViewById(R.id.crackdown_rank_list_title);
            TextView crackdownRankListWeeklyTitle = (TextView) mView.findViewById(R.id.crackdown_rank_list_weekly_title);
            TextView crackdownRankListMonthlyTitle = (TextView) mView.findViewById(R.id.crackdown_rank_list_monthly_title);

            mCrackdownRankWeekly = (TextView) mView.findViewById(R.id.crackdown_rank_weekly);
            mCrackdownRankMonthly = (TextView) mView.findViewById(R.id.crackdown_rank_monthly);
            mCrackdownWeeklyRankListView = (ListView) mView.findViewById(R.id.crackdown_rank_weekly_list);
            mCrackdownMonthlyRankListView = (ListView) mView.findViewById(R.id.crackdown_rank_monthly_list);
            mPopupCrackdownRankListLayout = (LinearLayout) mView.findViewById(R.id.popup_crackdown_rank_list_layout);
            mPopupCrackdownRankListLayout.setVisibility(View.GONE);
            mCrackdownRankLayout = (LinearLayout) mView.findViewById(R.id.crackdown_rank_layout);
            mCrackdownCommentInputLayout = (LinearLayout) mView.findViewById(R.id.crackdown_comment_input_layout);
            mPopupCrackdownShareLayout = (LinearLayout) mView.findViewById(R.id.popup_crackdown_share_layout);
            mPopupCrackdownShareLayout.setVisibility(View.GONE);
            mBtnCrackdownComment = (ImageButton) mView.findViewById(R.id.btn_crackdown_comment);
            mCrackdownDisapproveBg = (ImageView) mView.findViewById(R.id.crackdown_disapprove_bg);
            mCrackdownApproveBg = (ImageView) mView.findViewById(R.id.crackdown_approve_bg);
            mPhotoPreview = (ImageView) mView.findViewById(R.id.crackdown_prev_photo);
            mPhotoLayout = (LinearLayout) mView.findViewById(R.id.photo_layout);
            mPhotoLayout.setVisibility(View.GONE);
            mBtnCallDriver = (ImageButton) mView.findViewById(R.id.btn_call_driver);
            mCrackdownCommentListView = (ListView) mView.findViewById(R.id.crackdown_comment_list);
            mPastCrackdownText = (TextView) mView.findViewById(R.id.past_crackdown_text);
            mETCrackdownComment = (EditText) mView.findViewById(R.id.crackdown_comment_edit_text);
            mCrackdownCommentCount = (TextView) mView.findViewById(R.id.crackdown_comment_count);
            mBtnCrackdownReportClose = (ImageButton) mView.findViewById(R.id.btn_crackdown_report_close);
            mBtnCurrentCrackdown = (LinearLayout) mView.findViewById(R.id.btn_current_crackdown);
            mBtnPastCrackdown = (LinearLayout) mView.findViewById(R.id.btn_past_crackdown);
            mBtnCurrentPosition = (ImageButton) mView.findViewById(R.id.btn_current_position);
            mBtnRefresh = (ImageButton) mView.findViewById(R.id.btn_refresh);
            mBtnZoomin = (ImageButton) mView.findViewById(R.id.btn_zoomin);
            mBtnZoomout = (ImageButton) mView.findViewById(R.id.btn_zoomout);
            mBtnCrackdownShareClose = (ImageButton) mView.findViewById(R.id.btn_crackdown_share_close);
            mBtnReportCrackdown_1 = (ImageButton) mView.findViewById(R.id.btn_report_crackdown_1);
            mBtnReportCrackdown_2 = (ImageButton) mView.findViewById(R.id.btn_report_crackdown_2);
            mPopupCrackdownCommentLayout = (LinearLayout) mView.findViewById(R.id.popup_crackdown_comment_layout);
            mPopupCrackdownCommentLayout.setVisibility(View.GONE);
            mTVCrackdownAddress = (TextView) mView.findViewById(R.id.crackdown_address);
            mTVCrackdownDistance = (TextView) mView.findViewById(R.id.crackdown_distance);
            mTVCrackdownUpdateTime = (TextView) mView.findViewById(R.id.crackdown_update_time);
            mBtnCrackdownApprove = (LinearLayout) mView.findViewById(R.id.btn_crackdown_approve);
            mCrackdownApproveText = (TextView) mView.findViewById(R.id.crackdown_approve_text);
            mBtnCrackdownDisapprove = (LinearLayout) mView.findViewById(R.id.btn_crackdown_disapprove);
            mCrackdownDisapproveText = (TextView) mView.findViewById(R.id.crackdown_disapprove_text);
            mBtnCrackdownShare = (LinearLayout) mView.findViewById(R.id.btn_crackdown_share);
            mCrackdownShareText = (TextView) mView.findViewById(R.id.crackdown_share_text);
            mPopupCrackdownReportLayout = (LinearLayout) mView.findViewById(R.id.popup_crackdown_report_layout);
            mPopupCrackdownReportLayout.setVisibility(View.GONE);
            mBtnCrackdownReportVoice = (ImageButton) mView.findViewById(R.id.btn_crackdown_report_voice);
            mBtnCrackdownReportPoint = (ImageButton) mView.findViewById(R.id.btn_crackdown_report_point);
            mBtnCrackdownReportPhoto = (ImageButton) mView.findViewById(R.id.btn_crackdown_report_photo);
            mCrackdownShareAppListView = (GridView) mView.findViewById(R.id.crackdown_share_app_list);
            mBtnCrackdownDelete = (ImageButton) mView.findViewById(R.id.btn_crackdown_delete);
            mBtnCrackdownDelete.setVisibility(View.GONE);
            mBtnCrackdownRankListClose = (ImageButton) mView.findViewById(R.id.btn_crackdown_rank_list_close);
            mPopupCrackdownPhotoLayout = (LinearLayout) mView.findViewById(R.id.popup_crackdown_photo_layout);
            mPopupCrackdownPhotoLayout.setVisibility(View.GONE);
            mBtnCrackdownPhotoClose = (ImageButton) mView.findViewById(R.id.btn_crackdown_photo_close);
            mImgCrackdownPhoto = (ImageView) mView.findViewById(R.id.img_crackdown_photo);
            mBtnPushOnOff = (ImageButton) mView.findViewById(R.id.btn_push_onoff);
            mCrackdownRankListTimeDesc = (TextView) mView.findViewById(R.id.crackdown_rank_list_desc);
            mBtnCoupon = (Button) mView.findViewById(R.id.btn_coupon);

            // TODO START : 하단 탭 보여짐 여부
            if(true) {
                // 보여짐
                mBtnReportCrackdown_1.setVisibility(View.VISIBLE);
                mBtnReportCrackdown_2.setVisibility(View.GONE);
                mBtnCallDriver.setVisibility(View.GONE);
                mBtnCoupon.setVisibility(View.GONE);
            } else {
            // 가려짐
//                mBtnReportCrackdown_1.setVisibility(View.GONE);
//                mBtnReportCrackdown_2.setVisibility(View.VISIBLE);
//                mBtnCallDriver.setVisibility(View.VISIBLE);
//                mBtnCoupon.setVisibility(View.VISIBLE);
            }
            // TODO END : 하단 탭 보여짐 여부

            mCrackdownSharedAppListAdapter = new CrackdownSharedAppListAdapter(mMainActivity, R.layout.row_crackdown_share_list, mMainActivity.mCrackdownShareAppInfoList);
            mCrackdownShareAppListView.setAdapter(mCrackdownSharedAppListAdapter);

            mCrackdownShareAppListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int _position, long l) {
                    try {
                        Utils.log("error", TAG, "click position : " + _position + ", packageName : " + mMainActivity.mCrackdownShareAppInfoList.get(_position).activityInfo.packageName.toLowerCase());

                        mPopupCrackdownShareLayout.setVisibility(View.GONE);

                        CommonDialog.showProgressDialog(mMainActivity, "공유중...");

                        sendShare(mMainActivity.mCrackdownShareAppInfoList.get(_position));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            mCrackdownSharedAppListAdapter.notifyDataSetChanged();

            mBtnCallDriver.setOnClickListener(this);
            mBtnCurrentCrackdown.setOnClickListener(this);
            mBtnPastCrackdown.setOnClickListener(this);
            mBtnRefresh.setOnClickListener(this);
            mBtnZoomin.setOnClickListener(this);
            mBtnZoomout.setOnClickListener(this);
            mBtnReportCrackdown_1.setOnClickListener(this);
            mBtnReportCrackdown_2.setOnClickListener(this);
            mBtnCrackdownApprove.setOnClickListener(this);
            mBtnCrackdownDisapprove.setOnClickListener(this);
            mBtnCrackdownShare.setOnClickListener(this);
            mBtnCrackdownReportVoice.setOnClickListener(this);
            mBtnCrackdownReportPoint.setOnClickListener(this);
            mBtnCrackdownReportPhoto.setOnClickListener(this);
            mBtnCrackdownReportClose.setOnClickListener(this);
            mBtnCurrentPosition.setOnClickListener(this);
            mBtnCrackdownComment.setOnClickListener(this);
            mBtnCrackdownShareClose.setOnClickListener(this);
            mBtnCrackdownDelete.setOnClickListener(this);
            mCrackdownRankLayout.setOnClickListener(this);
            mBtnCrackdownRankListClose.setOnClickListener(this);
            mBtnCrackdownPhotoClose.setOnClickListener(this);
            mPhotoPreview.setOnClickListener(this);
            mBtnPushOnOff.setOnClickListener(this);
            mBtnCoupon.setOnClickListener(this);

            mCrackdownMarkerList = new ArrayList<>();
            mDriverMarkerList = new ArrayList<>();
            mCrackdownList = new ArrayList<>();
            mCrackdownCommentList = new ArrayList<>();
            mCrackdownWeeklyRankList = new ArrayList<>();
            mCrackdownMonthlyRankList = new ArrayList<>();
            mIsShowPastCrackdown = false;

            mBtnPastCrackdown.setSelected(mIsShowPastCrackdown);

//			getCrackdownPinIcon();

            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanM(), crackdownRankTitle, mCrackdownRankMonthly, mCrackdownRankWeekly, currentCrackdownText, mPastCrackdownText, mCrackdownCommentCount, crackdownPrevPhotoText,
                    mCrackdownApproveText, mCrackdownDisapproveText, mCrackdownShareText, crackdownAddressTitle, mTVCrackdownAddress, crackdownDistanceTitle, mTVCrackdownDistance, mTVCrackdownUpdateTime,
                    crackdownRankListWeeklyTitle, crackdownRankListMonthlyTitle);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanVert(), crackdownReportTitle, crackdownReportPhotoDesc, crackdownReportVoiceDesc, crackdownReportPointDesc, crackdownRankListTitle);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), crackdownDescription, crackdownDescription2, mETCrackdownComment);

            mCrackdownCommentListAdapter = new CrackdownCommentListAdapter(mMainActivity, R.layout.row_crackdown_comment_list, mCrackdownCommentList);
            mCrackdownCommentListView.setAdapter(mCrackdownCommentListAdapter);

            mCrackdownWeeklyRankListAdapter = new CrackdownRankListAdapter(mMainActivity, R.layout.row_crackdown_rank_list, mCrackdownWeeklyRankList);
            mCrackdownMonthlyRankListAdapter = new CrackdownRankListAdapter(mMainActivity, R.layout.row_crackdown_rank_list, mCrackdownMonthlyRankList);
            mCrackdownWeeklyRankListView.setAdapter(mCrackdownWeeklyRankListAdapter);
            mCrackdownMonthlyRankListView.setAdapter(mCrackdownMonthlyRankListAdapter);

            changePushButton(mMainActivity.mUserAdditionalData.getmPushYN().equals("Y"));

            getCrackdownRankList();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpMapIfNeeded() {
        try {
            if (mGoogleMap == null) {
                SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.crackdown_map);
                mapFragment.getMapAsync(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            mGoogleMap = googleMap;

            mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
            mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
            mGoogleMap.getUiSettings().setCompassEnabled(false);

            mCurrentPositionMarker = mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_carpool_current_position)).position(MainActivity.mCurrentLocation).zIndex(5f));
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MainActivity.mCurrentLocation, Utils.GOOGLE_MAP_DEFAULT_ZOOM));

            setMapEventListener();

            mMainActivity.removeSplash();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setMapEventListener() {
        try {
//			mGoogleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
//				@Override
//				public void onCameraChange(CameraPosition cameraPosition) {
//					getNearData();
//				}
//			});

            mGoogleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {
                    getNearData();
                }
            });

            mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    try {
                        closeCrackdownComments();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    try {
                        closeCrackdownComments();

                        if (marker.getSnippet() != null && !marker.getSnippet().isEmpty()) {
                            String marker_snippet = marker.getSnippet();
                            String[] marker_snippet_array = marker_snippet.split("_");

                            switch (marker_snippet_array[0]) {
                                case "crackdown":
                                    getCrackdownDetail(marker_snippet_array[1]);
                                    break;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getCrackdownRankList() {
        try {
            NetClient.send(mMainActivity, Configuration.URL_CRACKDOWN_RANKING, "GET", null, new CrackdownRankListAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    try {
                        mMainActivity.hideLoadingAnim();

                        if (_netAPI != null) {
                            CrackdownRankListAck retNetApi = (CrackdownRankListAck) _netAPI;
                            ResultData result = retNetApi.getmResultData();
                            if (result.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                                CommonDialog.showSimpleDialog(mMainActivity, result.getmDetail());
                                return;
                            }

                            mCrackdownWeeklyRankList.clear();
                            mCrackdownMonthlyRankList.clear();

                            String weeklyRankTop = "주간 1위 없음";
                            if (retNetApi.getmCrackdownWeeklyRankList().size() > 0) {
                                for (CrackdownRankListViewModel crackdownRankListItem : retNetApi.getmCrackdownWeeklyRankList()) {
                                    crackdownRankListItem.setmReportType("weekly");
                                    mCrackdownWeeklyRankList.add(crackdownRankListItem);
                                }

                                weeklyRankTop = "주간 1위 【고객 " + mCrackdownWeeklyRankList.get(0).getmRegistrant() + "님】 " + mCrackdownWeeklyRankList.get(0).getmReportPoint() + "점";
                            }

                            String monthlyRankTop = "월간 1위 없음";
                            if (retNetApi.getmCrackdownMonthlyRankList().size() > 0) {
                                for (CrackdownRankListViewModel crackdownRankListItem : retNetApi.getmCrackdownMonthlyRankList()) {
                                    crackdownRankListItem.setmReportType("monthly");
                                    mCrackdownMonthlyRankList.add(crackdownRankListItem);
                                }

                                monthlyRankTop = "월간 1위 【고객 " + mCrackdownMonthlyRankList.get(0).getmRegistrant() + "님】 " + mCrackdownMonthlyRankList.get(0).getmReportPoint() + "점";
                            }

                            mCrackdownWeeklyRankListAdapter.notifyDataSetChanged();
                            mCrackdownMonthlyRankListAdapter.notifyDataSetChanged();

                            mCrackdownRankWeekly.setText(weeklyRankTop);
                            mCrackdownRankMonthly.setText(monthlyRankTop);

                            Calendar calendar = Calendar.getInstance();
                            String updateDate = calendar.get(Calendar.YEAR) + "년 " + (calendar.get(Calendar.MONTH) + 1) + "월 " + calendar.get(Calendar.DAY_OF_MONTH) + "일 00시 00분 기준";
                            mCrackdownRankListTimeDesc.setText(updateDate);
                        }
                    } catch (Exception e) {
                        mMainActivity.hideLoadingAnim();
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getCrackdownDetail(String _crackdownId) {
        try {
            mMainActivity.showLoadingAnim();

            RequestParams params = new RequestParams();
            params.add("crackdownId", _crackdownId);
            params.add("userId", mMainActivity.mUserDetail.getUserId());

            NetClient.send(mMainActivity, Configuration.URL_CRACKDOWN_DETAIL, "POST", params, new CrackdownDetailAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    try {
                        mMainActivity.hideLoadingAnim();

                        if (_netAPI != null) {
                            CrackdownDetailAck retNetApi = (CrackdownDetailAck) _netAPI;
                            ResultData result = retNetApi.getmResultData();
                            if (result.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                                CommonDialog.showSimpleDialog(mMainActivity, result.getmDetail());
                                return;
                            }

                            CrackdownListViewModel retCrackdownDetail = retNetApi.getmCrackdownListViewModel();
                            showCrackdownDetail(retCrackdownDetail);
                        }
                    } catch (Exception e) {
                        mMainActivity.hideLoadingAnim();
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            mMainActivity.hideLoadingAnim();
            e.printStackTrace();
        }
    }

    private void showCrackdownDetail(CrackdownListViewModel _crackdownDetail) {
        try {
            String elapsedTime = "【고객 " + _crackdownDetail.getmRegistrant() + "님】 " + Utils.getElapsedTime(_crackdownDetail.getmCreateTime()) + " 등록";
            String approveButtonText = "추천 " + _crackdownDetail.getmLikeCount();
            String disapproveButtonText = "비추천 " + _crackdownDetail.getmDislikeCount();
            LatLng crackdownPosition = new LatLng(_crackdownDetail.getmLocation().getmCoordinates().get(1), _crackdownDetail.getmLocation().getmCoordinates().get(0));

            if (_crackdownDetail.getmReportType() != null && _crackdownDetail.getmReportType().equals("photo")) {
                UrlImageViewHelper.setUrlDrawable(mPhotoPreview, Configuration.SERVER_HOST + _crackdownDetail.getmPicture());
                mPhotoLayout.setVisibility(View.VISIBLE);
            } else {
                mPhotoLayout.setVisibility(View.GONE);
            }

            if (_crackdownDetail.getmUserId() != null && _crackdownDetail.getmUserId().equals(mMainActivity.mUserDetail.getUserId())) {
                mBtnCrackdownDelete.setVisibility(View.VISIBLE);
            } else {
                mBtnCrackdownDelete.setVisibility(View.GONE);
            }

            mSelectedCrackdownId = _crackdownDetail.getmCrackdownId();

            mCrackdownCommentCount.setText("댓글" + _crackdownDetail.getmCommentList().size());
            mTVCrackdownAddress.setText(_crackdownDetail.getmAddress());
            mTVCrackdownDistance.setText(getDistance(crackdownPosition));
            mTVCrackdownUpdateTime.setText(elapsedTime);
            mCrackdownApproveText.setText(approveButtonText);
            mCrackdownDisapproveText.setText(disapproveButtonText);

            if (_crackdownDetail.ismIsUserLike()) {
                mCrackdownApproveBg.setSelected(true);
                mCrackdownApproveText.setSelected(true);
            } else {
                mCrackdownApproveBg.setSelected(false);
                mCrackdownApproveText.setSelected(false);
            }

            if (_crackdownDetail.ismIsUserDislike()) {
                mCrackdownDisapproveBg.setSelected(true);
                mCrackdownDisapproveText.setSelected(true);
            } else {
                mCrackdownDisapproveBg.setSelected(false);
                mCrackdownDisapproveText.setSelected(false);
            }

            showCrackdownComments(_crackdownDetail.getmCommentList());

            mPopupCrackdownCommentLayout.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showCrackdownComments(ArrayList<CrackdownCommentListViewModel> _commentList) {
        try {
            if (_commentList.size() > 0) {
                mCrackdownCommentList.clear();

                int parentIndex = 0;
                for (CrackdownCommentListViewModel commentItem : _commentList) {
                    commentItem.setmDepth(1);
                    commentItem.setmParentIndex(parentIndex);
                    mCrackdownCommentList.add(commentItem);

                    if (commentItem.getmSubCommentsList().size() > 0) {
                        for (CrackdownCommentListViewModel subCommentItem : commentItem.getmSubCommentsList()) {
                            subCommentItem.setmDepth(2);
                            subCommentItem.setmParentIndex(parentIndex);
                            mCrackdownCommentList.add(subCommentItem);
                        }
                    }

                    parentIndex++;
                }

                mCrackdownCommentListAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getDistance(LatLng _crackdownPosition) {
        String crackdownDistance = "";

        try {
            float[] dist = new float[3];
            Location.distanceBetween(MainActivity.mCurrentLocation.latitude, MainActivity.mCurrentLocation.longitude, _crackdownPosition.latitude, _crackdownPosition.longitude, dist);

            crackdownDistance = String.format("%.2fkm", dist[0] / 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return crackdownDistance;
    }

    public void changeMyLocation() {
        try {
            Utils.log("error", TAG, "===== changeMyLocation() =====");
            if (mGoogleMap != null && mCurrentPositionMarker != null && mIsFragmentRunning) {
                mCurrentPositionMarker.setPosition(MainActivity.mCurrentLocation);
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(MainActivity.mCurrentLocation));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void closeCrackdownComments() {
        try {
            mPopupCrackdownCommentLayout.setVisibility(View.GONE);
            mSelectedCrackdownId = "";
            mCrackdownCommentList.clear();
            mCrackdownCommentListAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class CrackdownCommentListAdapter extends ArrayAdapter<CrackdownCommentListViewModel> {
        private ArrayList<CrackdownCommentListViewModel> mCrackdownCommentListData;
        private int mResourceId;
        private CrackdownCommentListViewHolder mViewHolder;

        public CrackdownCommentListAdapter(Context _context, int _resourceId, ArrayList<CrackdownCommentListViewModel> _listData) {
            super(_context, _resourceId, _listData);

            this.mCrackdownCommentListData = _listData;
            this.mResourceId = _resourceId;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;

            try {
                if (v == null) {
                    LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    v = li.inflate(mResourceId, null);

                    mViewHolder = new CrackdownCommentListViewHolder();
                    mViewHolder.mCrackdownSubCommentReplyLayout = (LinearLayout) v.findViewById(R.id.crackdown_sub_comment_reply_layout);
                    mViewHolder.mCrackdownSubCommentReplyLayout.setVisibility(View.GONE);
                    mViewHolder.mCommentReplyIcon = (ImageView) v.findViewById(R.id.crackdown_comment_reply_icon);
                    mViewHolder.mCommentReplyTitle = (TextView) v.findViewById(R.id.crackdown_comment_reply_title);
                    mViewHolder.mCommentReplyContent = (TextView) v.findViewById(R.id.crackdown_comment_reply_content);
                    mViewHolder.mBtnCommentReply = (ImageButton) v.findViewById(R.id.btn_crackdown_comment_reply);
                    mViewHolder.mCrackdownSubCommentEditText = (EditText) v.findViewById(R.id.crackdown_sub_comment_edit_text);
                    mViewHolder.mBtnCrackdownSubComment = (ImageButton) v.findViewById(R.id.btn_crackdown_sub_comment);

                    Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), mViewHolder.mCommentReplyTitle, mViewHolder.mCommentReplyContent);

                    v.setTag(mViewHolder);
                } else {
                    mViewHolder = (CrackdownCommentListViewHolder) v.getTag();
                }

                CrackdownCommentListViewModel crackdownCommentData = mCrackdownCommentListData.get(position);

                mViewHolder.mBtnCommentReply.setTag(null);
                mViewHolder.mBtnCommentReply.setOnClickListener(null);
                mViewHolder.mBtnCrackdownSubComment.setTag(null);
                mViewHolder.mBtnCrackdownSubComment.setOnClickListener(null);
                mViewHolder.mCrackdownSubCommentEditText.setTag(null);
                mViewHolder.mCrackdownSubCommentEditText.setText("");
                mViewHolder.mCrackdownSubCommentReplyLayout.setVisibility(View.GONE);

                if (crackdownCommentData.getmDepth() == 2) {
                    mViewHolder.mCommentReplyIcon.setVisibility(View.VISIBLE);
                    mViewHolder.mBtnCommentReply.setVisibility(View.GONE);
                } else {
                    mViewHolder.mCommentReplyIcon.setVisibility(View.GONE);
                    mViewHolder.mBtnCommentReply.setVisibility(View.VISIBLE);

                    mViewHolder.mBtnCommentReply.setTag(mViewHolder.mCrackdownSubCommentReplyLayout);
                    mViewHolder.mBtnCommentReply.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                LinearLayout layout = (LinearLayout) view.getTag();
                                layout.setVisibility(View.VISIBLE);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    mViewHolder.mCrackdownSubCommentEditText.setTag(crackdownCommentData.getmParentIndex());
                    mViewHolder.mCrackdownSubCommentEditText.setText("");
                    mViewHolder.mBtnCrackdownSubComment.setTag(mViewHolder.mCrackdownSubCommentEditText);
                    mViewHolder.mBtnCrackdownSubComment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                EditText subContent = (EditText) view.getTag();
                                int parentIndex = (int) subContent.getTag();
                                String content = subContent.getText().toString();

                                sendComment(content, parentIndex, 2);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }

                String title = "【고객 " + crackdownCommentData.getmRegistrant() + "님 댓글】 " + Utils.getCommentRegisteredDate(crackdownCommentData.getmCreateTime());
                String content = " ▷ " + crackdownCommentData.getmContents();

                mViewHolder.mCommentReplyTitle.setText(title);
                mViewHolder.mCommentReplyContent.setText(content);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return v;
        }
    }

    private class CrackdownRankListAdapter extends ArrayAdapter<CrackdownRankListViewModel> {
        private ArrayList<CrackdownRankListViewModel> mChauffeurRankListData;
        private int mResourceId;
        private CrackdownRankListViewHolder mViewHolder;

        public CrackdownRankListAdapter(Context _context, int _resourceId, ArrayList<CrackdownRankListViewModel> _listData) {
            super(_context, _resourceId, _listData);

            this.mChauffeurRankListData = _listData;
            this.mResourceId = _resourceId;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;

            try {
                if (v == null) {
                    LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    v = li.inflate(mResourceId, null);

                    mViewHolder = new CrackdownRankListViewHolder();
                    mViewHolder.mCrackdownRankRanking = (TextView) v.findViewById(R.id.row_crackdown_rank_ranking);
                    mViewHolder.mCrackdownRankUser = (TextView) v.findViewById(R.id.row_crackdown_rank_user);
                    mViewHolder.mCrackdownRankCount = (TextView) v.findViewById(R.id.row_crackdown_rank_count);

                    Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanB(), mViewHolder.mCrackdownRankRanking, mViewHolder.mCrackdownRankUser, mViewHolder.mCrackdownRankCount);

                    v.setTag(mViewHolder);
                } else {
                    mViewHolder = (CrackdownRankListViewHolder) v.getTag();
                }

                CrackdownRankListViewModel crackdownRankData = mChauffeurRankListData.get(position);

                String ranking = crackdownRankData.getmRank() + "위";
                String rankUser = crackdownRankData.getmRegistrant() + "님";
                String reportCount = crackdownRankData.getmReportPoint() + "점";

                mViewHolder.mCrackdownRankRanking.setText(ranking);
                mViewHolder.mCrackdownRankUser.setText(rankUser);
                mViewHolder.mCrackdownRankCount.setText(reportCount);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return v;
        }
    }

    @Override
    public void onClick(View view) {
        try {
            if (view == mBtnCurrentCrackdown) {
//				mIsShowCurrentCrackdown = !mIsShowCurrentCrackdown;

//				showCrackdownList();
            } else if (view == mBtnPastCrackdown) {
                mIsShowPastCrackdown = !mIsShowPastCrackdown;
                mBtnPastCrackdown.setSelected(mIsShowPastCrackdown);

                showCrackdownList();
            } else if (view == mBtnCurrentPosition) {
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(MainActivity.mCurrentLocation, Utils.GOOGLE_MAP_DEFAULT_ZOOM));
            } else if (view == mBtnRefresh) {
                removeCrackdownMarkers();
                removeDriverMarkers();

                getNearData();
            } else if (view == mBtnZoomin) {
                mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
            } else if (view == mBtnZoomout) {
                mGoogleMap.animateCamera(CameraUpdateFactory.zoomOut());
            } else if (view == mBtnReportCrackdown_1 || view == mBtnReportCrackdown_2) {
                showCrackdownReport();
            } else if (view == mBtnCrackdownApprove) {
                mMainActivity.showConfirmPopup("알림", "이 게시물을 ‘추천’ 하시겠습니까?", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        updateCrackdownAction(mSelectedCrackdownId, "like");
                    }
                });
            } else if (view == mBtnCrackdownDisapprove) {
                mMainActivity.showConfirmPopup("알림", "이 게시물을 ‘비추천’ 하시겠습니까?", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        updateCrackdownAction(mSelectedCrackdownId, "dislike");
                    }
                });
            } else if (view == mBtnCrackdownShare) {
                if (checkShareImagePermission()) {
                    //sendShare();
                    showShareLayer();
                }
            } else if (view == mBtnCrackdownShareClose) {
                mPopupCrackdownShareLayout.setVisibility(View.GONE);
            } else if (view == mBtnCrackdownReportClose) {
                hideCrackdownReport();
            } else if (view == mBtnCrackdownReportVoice) {
                hideCrackdownReport();
                mMainActivity.makePhoneCall();
            } else if (view == mBtnCrackdownReportPoint) {
                moveToReportMapFragment(null);
            } else if (view == mBtnCrackdownReportPhoto) {
                if (checkCameraPermission()) {
                    reportPhoto();
                }
            } else if (view == mBtnCallDriver) {
                mMainActivity.addFragment(new ChauffeurCompanyListFragment());
            } else if (view == mBtnCrackdownComment) {
                String content = mETCrackdownComment.getText().toString();
                sendComment(content, 0, 1);
            } else if (view == mBtnCrackdownDelete) {
                mMainActivity.showConfirmPopup("삭제", "삭제 하겠습니까?", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mMainActivity.closeConfirmPopup();
                        deleteCrackdown();
                    }
                });
            } else if (view == mCrackdownRankLayout) {
                showRankList();
            } else if (view == mBtnCrackdownRankListClose) {
                hideRankList();
            } else if (view == mBtnCrackdownPhotoClose) {
                hidePhotoViewer();
            } else if (view == mPhotoPreview) {
                showPhotoViewer(mPhotoPreview.getDrawable());
            } else if (view == mBtnPushOnOff) {
                updatePushYN(!mMainActivity.mUserAdditionalData.getmPushYN().equals("Y"));
            } else if (view == mBtnCoupon) {
                mMainActivity.addFragment(new SettingCouponFragment());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changePushButton(boolean _isSelected) {
        try {
            mBtnPushOnOff.setSelected(_isSelected);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updatePushYN(final boolean _isSelected) {
        try {
            mMainActivity.showLoadingAnim();

            RequestParams params = new RequestParams();
            params.add("pushYN", _isSelected ? "Y" : "N");
            params.add("userId", mMainActivity.mUserDetail.getUserId());

            NetClient.send(mMainActivity, Configuration.URL_UPDATE_PUSH_YN, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    try {
                        mMainActivity.hideLoadingAnim();

                        if (_netAPI != null) {
                            NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
                            ResultData result = retNetApi.getmResultData();
                            if (result.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                                CommonDialog.showSimpleDialog(mMainActivity, result.getmDetail());
                                return;
                            }

                            mMainActivity.mUserAdditionalData.setmPushYN(_isSelected ? "Y" : "N");

                            changePushButton(_isSelected);
                        }
                    } catch (Exception e) {
                        mMainActivity.hideLoadingAnim();
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showPhotoViewer(Drawable _image) {
        try {
            mImgCrackdownPhoto.setImageDrawable(_image);
//			UrlImageViewHelper.setUrlDrawable(mImgCrackdownPhoto, Configuration.SERVER_HOST + _imagePath);
            mPopupCrackdownPhotoLayout.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hidePhotoViewer() {
        try {
            mPopupCrackdownPhotoLayout.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showRankList() {
        try {
            mPopupCrackdownRankListLayout.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideRankList() {
        try {
            mPopupCrackdownRankListLayout.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteCrackdown() {
        try {
            if (Utils.isStringNullOrEmpty(mSelectedCrackdownId)) {
                return;
            }

            mMainActivity.showLoadingAnim();

            RequestParams params = new RequestParams();
            params.add("userId", mMainActivity.mUserDetail.getUserId());
            params.add("crackdownId", mSelectedCrackdownId);

            NetClient.send(mMainActivity, Configuration.URL_CRACKDOWN_DELETE, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    try {
                        mMainActivity.hideLoadingAnim();

                        if (_netAPI != null) {
                            NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
                            ResultData result = retNetApi.getmResultData();
                            if (result.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                                removeDeletedCrackdownMarker(mSelectedCrackdownId);
                                closeCrackdownComments();
                                CommonDialog.showSimpleDialog(mMainActivity, "삭제 되었습니다.");
                            } else {
                                CommonDialog.showSimpleDialog(mMainActivity, result.getmDetail());
                            }
                        }
                    } catch (Exception e) {
                        mMainActivity.hideLoadingAnim();
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            mMainActivity.hideLoadingAnim();
            e.printStackTrace();
        }
    }

    private void reportPhoto() {
        try {
            // Camera.
            final List<Intent> cameraIntents = new ArrayList<>();
            final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            final PackageManager packageManager = getActivity().getPackageManager();
            final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
            for (ResolveInfo res : listCam) {
                final String packageName = res.activityInfo.packageName;
                final Intent intent = new Intent(captureIntent);
                intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                intent.setPackage(packageName);
                cameraIntents.add(intent);
            }

            // Gallery.
            final Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            galleryIntent.setType("image/*");

            // Chooser of filesystem options.
            final Intent chooserIntent = Intent.createChooser(galleryIntent, "사진 제보");

            // Add the camera options.
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

            getActivity().startActivityForResult(chooserIntent, Configuration.SELECT_PHOTO_REQ_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendComment(String _content, int _parentIndex, int _commentDepth) {
        try {
            if (Utils.isStringNullOrEmpty(_content)) {
                CommonDialog.showSimpleDialog(mMainActivity, "댓글을 입력해 주세요.");
                return;
            }

            mMainActivity.showLoadingAnim();

            RequestParams params = new RequestParams();
            params.add("userId", mMainActivity.mUserDetail.getUserId());
            params.add("crackdownId", mSelectedCrackdownId);
            params.add("parentIndex", _parentIndex + "");
            params.add("depth", _commentDepth + "");
            params.add("registrant", Utils.getCrackdownRegistrantEncrypt(mMainActivity.mUserDetail.getPhoneNumber().substring(mMainActivity.mUserDetail.getPhoneNumber().length() - 8, mMainActivity.mUserDetail.getPhoneNumber().length())));
            params.add("comment", _content);

            NetClient.send(mMainActivity, Configuration.URL_REGISTER_CRACKDOWN_COMMENT, "POST", params, new RegisterCrackdownCommentAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    try {
                        mMainActivity.hideLoadingAnim();

                        if (_netAPI != null) {
                            RegisterCrackdownCommentAck retNetApi = (RegisterCrackdownCommentAck) _netAPI;
                            ResultData result = retNetApi.getmResultData();
                            if (result.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                                showCrackdownComments(retNetApi.getmCrackdownCommentList());

                                mETCrackdownComment.setText("");
                                Utils.hideSoftInput(mMainActivity);
                            } else {
                                CommonDialog.showSimpleDialog(mMainActivity, result.getmDetail());
                            }
                        }
                    } catch (Exception e) {
                        mMainActivity.hideLoadingAnim();
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            mMainActivity.hideLoadingAnim();
            e.printStackTrace();
        }
    }

    private void showShareLayer() {
        try {
            mPopupCrackdownShareLayout.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendShare(final ResolveInfo _appInfo) {
        try {
            TmapManager tmapManager = new TmapManager();
            tmapManager.convertGpsToAddress(mGoogleMap.getCameraPosition().target, new ResultCallback() {
                @Override
                public void resultCallback(HashMap<String, Object> params) {
                    try {
                        TMapAddressInfo addressInfo = (TMapAddressInfo) params.get("addressInfo");
                        if (addressInfo != null) {
                            final String targetAddress = addressInfo.strFullAddress;
                            Utils.log("error", TAG, "targetAddress : " + targetAddress);

                            mGoogleMap.snapshot(new GoogleMap.SnapshotReadyCallback() {
                                @Override
                                public void onSnapshotReady(Bitmap _screenshotImg) {
                                    try {
                                        String saveImagePath = Utils.saveScreenshot(_screenshotImg);

                                        uploadImage(saveImagePath, _screenshotImg.getWidth(), _screenshotImg.getHeight(), targetAddress, _appInfo);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    CommonDialog.hideProgressDialog();
                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadImage(String _selectedImagePath, int _imgWidth, int _imgHeight, String _targetAddress, ResolveInfo _resolveInfo) {
        try {
            UploadImage uploadimage = new UploadImage(_selectedImagePath, _imgWidth, _imgHeight, _targetAddress, _resolveInfo);
            uploadimage.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class UploadImage extends AsyncTask<Void, Void, String> {
        private String mSelectedImagePath;
        private ResolveInfo mResolveInfo;
        private int mImgWidth;
        private int mImgHeight;
        private String mTargetAddress;

        public UploadImage(String _selectedImagePath, int _imgWidth, int _imgHeight, String _targetAddress, ResolveInfo _resolveInfo) {
            this.mSelectedImagePath = _selectedImagePath;
            this.mImgWidth = _imgWidth;
            this.mImgHeight = _imgHeight;
            this.mTargetAddress = _targetAddress;
            this.mResolveInfo = _resolveInfo;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            CommonDialog.showProgressDialog(mMainActivity, "사진첨부 중..");
        }

        @Override
        protected String doInBackground(Void... params) {
            String retMsg;

            try {
                UploadFile u = new UploadFile(Configuration.URL_SHARE_FILE, this.mSelectedImagePath);

                retMsg = u.uploadImage();
            } catch (Exception e) {
                e.printStackTrace();
                retMsg = "ERROR";
            }

            return retMsg;
        }

        @Override
        protected void onPostExecute(String responseBody) {
            super.onPostExecute(responseBody);

            try {
                Utils.log("error", TAG, "ret_msg : " + responseBody);

                ObjectMapper mapper = new ObjectMapper();
                JsonParser jp = mapper.getFactory().createParser(responseBody);
                UploadImageAck retNetApi = mapper.readValue(jp, UploadImageAck.class);
                jp.close();

                ResultData result = retNetApi.getmResultData();

                if (result.getmCode() != Enums.NetResultCode.SUCCESS.getValue()) {
                    CommonDialog.hideProgressDialog();
                    mMainActivity.hideLoadingAnim();
                    CommonDialog.showSimpleDialog(mMainActivity, result.getmDetail());
                } else {
                    if (this.mResolveInfo.activityInfo.packageName.toLowerCase().contains("com.kakao.talk")) {
                        mMainActivity.mKakaoTalkLinkMessageBuilder
                                .addText(Configuration.SHARED_TEXT)
                                .addImage(Configuration.SERVER_HOST + retNetApi.getmFilePath(), this.mImgWidth, this.mImgHeight)
                                .addInWebLink("단속위치공유 : " + this.mTargetAddress, Configuration.SERVER_HOST + retNetApi.getmFilePath())
                                .addAppButton("앱으로 이동")
                                .build();
                        mMainActivity.mKakaoLink.sendMessage(mMainActivity.mKakaoTalkLinkMessageBuilder, mMainActivity);
                    } else if (this.mResolveInfo.activityInfo.packageName.toLowerCase().contains("com.android.mms")) {
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("image/*");
                        intent.putExtra("subject", Configuration.SHARED_TEXT);
                        intent.putExtra("sms_body", "단속위치공유 : " + this.mTargetAddress + "\n" + Configuration.SERVER_HOST + retNetApi.getmFilePath());
                        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(Configuration.SERVER_HOST + retNetApi.getmFilePath()));
                        intent.setPackage(this.mResolveInfo.activityInfo.packageName);
                        startActivity(intent);
                    }
                }
            } catch (Exception e) {
                CommonDialog.hideProgressDialog();
                mMainActivity.hideLoadingAnim();
                e.printStackTrace();
            }
        }
    }

    private void getNearData() {
        try {
            if (mGoogleMap == null || !mIsFragmentRunning || mGoogleMap.getCameraPosition().zoom < 10) {
                removeCrackdownMarkers();
                removeDriverMarkers();

                return;
            }

            float distance = Utils.getGoogleMapScreenSize(mGoogleMap.getProjection().getVisibleRegion().latLngBounds);
            getCrackdownData(mGoogleMap.getCameraPosition().target, distance);
            getNearDriverList(mGoogleMap.getCameraPosition().target, distance, mGoogleMap.getCameraPosition().zoom);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getCrackdownData(LatLng _center, float _range) {
        try {
            RequestParams params = new RequestParams();
            params.add("lat", _center.latitude + "");
            params.add("lng", _center.longitude + "");
            params.add("range", _range + "");

            NetClient.send(mMainActivity, Configuration.URL_CRACKDOWN_LIST, "POST", params, new CrackdownListAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    try {
                        if (_netAPI != null) {
                            CrackdownListAck retNetApi = (CrackdownListAck) _netAPI;
                            ResultData result = retNetApi.getmResultData();
                            if (result.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                                mCrackdownList = retNetApi.getmCrackdownListViewModel();
                                showCrackdownList();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getNearDriverList(LatLng _center, float _range, final float _zoom) {
        try {
            RequestParams params = new RequestParams();
            params.add("lat", _center.latitude + "");
            params.add("lng", _center.longitude + "");
            params.add("range", _range + "");

            NetClient.send(mMainActivity, Configuration.URL_DRIVER_FIND, "POST", params, new NearDriverListAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    try {
                        removeDriverMarkers();

                        if (_netAPI != null) {
                            NearDriverListAck retNetApi = (NearDriverListAck) _netAPI;
                            ResultData result = retNetApi.getmResultData();
                            if (result.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                                ArrayList<DriverPointViewModel> retDriverList = retNetApi.getmDriverPointViewModel();

                                for (DriverPointViewModel driverPoint : retDriverList) {
                                    if (!mIsFragmentRunning) {
                                        return;
                                    }

                                    String markerText = driverPoint.getmPhoneNumber().substring(driverPoint.getmPhoneNumber().length() - 4, driverPoint.getmPhoneNumber().length()) + " 기사님";
                                    LatLng markerPosition = new LatLng(driverPoint.getmLocation().getmCoordinates().get(1), driverPoint.getmLocation().getmCoordinates().get(0));
                                    Marker m = mGoogleMap.addMarker(new MarkerOptions().position(markerPosition).title(markerText));

                                    if (_zoom >= Utils.GOOGLE_MAP_DEFAULT_ZOOM) {
                                        m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin_driver_blue_120));
                                    } else if (_zoom >= 15) {
                                        m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin_driver_blue_75));
                                    } else if (_zoom >= 13) {
                                        m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin_driver_blue_50));
                                    } else if (_zoom >= 11) {
                                        m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin_driver_blue_40));
                                    } else {
                                        m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin_driver_blue_22));
                                    }

                                    mDriverMarkerList.add(m);
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateCrackdownAction(String _crackdownId, final String _action_type) {
        try {
            mMainActivity.closeConfirmPopup();

            if (Utils.isStringNullOrEmpty(_crackdownId)) {
                return;
            }

            mMainActivity.showLoadingAnim();

            RequestParams params = new RequestParams();
            params.add("crackdownId", _crackdownId);
            params.add("actionType", _action_type);
            params.add("userId", mMainActivity.mUserDetail.getUserId());

            NetClient.send(mMainActivity, Configuration.URL_UPDATE_ACTION, "POST", params, new CrackdownDetailAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    try {
                        mMainActivity.hideLoadingAnim();

                        if (_netAPI != null) {
                            CrackdownDetailAck retNetApi = (CrackdownDetailAck) _netAPI;
                            ResultData result = retNetApi.getmResultData();

                            String dialogMsg = "";
                            if (result.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                                if (_action_type.equals("like")) {
                                    dialogMsg = "추천 되었습니다.";
                                } else if (_action_type.equals("dislike")) {
                                    dialogMsg = "비추천 되었습니다.";
                                }

                                showCrackdownDetail(retNetApi.getmCrackdownListViewModel());
                            } else {
                                dialogMsg = result.getmDetail();
                            }

                            CommonDialog.showSimpleDialog(mMainActivity, dialogMsg);
                        }
                    } catch (Exception e) {
                        mMainActivity.hideLoadingAnim();
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            mMainActivity.hideLoadingAnim();
            e.printStackTrace();
        }
    }

//	private void getCrackdownPinIcon() {
//		try {
//			int width;
//			int height;
//			BitmapDrawable bitmapDrawable;
//
//			if(mDriverPinIcon == null) {
//				width = Utils.dpFromPx(mMainActivity, 76);
//				height = Utils.dpFromPx(mMainActivity, 101);
//				bitmapDrawable = ((BitmapDrawable) getResources().getDrawable(R.drawable.pin_driver));
//				if(bitmapDrawable != null) {
//					mDriverPinIcon = Bitmap.createScaledBitmap(bitmapDrawable.getBitmap(), width, height, false);
//				}
//			}
//
//			if(mPastCrackdownPinIcon == null) {
//				width = Utils.dpFromPx(mMainActivity, 161);
//				height = Utils.dpFromPx(mMainActivity, 201);
//				bitmapDrawable = ((BitmapDrawable) getResources().getDrawable(R.drawable.pin_past_crackdown));
//				if(bitmapDrawable != null) {
//					mPastCrackdownPinIcon = Bitmap.createScaledBitmap(bitmapDrawable.getBitmap(), width, height, false);
//				}
//			}
//
//			if(mPastCrackdownWithCommentPinIcon == null) {
//				width = Utils.dpFromPx(mMainActivity, 161);
//				height = Utils.dpFromPx(mMainActivity, 259);
//				bitmapDrawable = ((BitmapDrawable) getResources().getDrawable(R.drawable.pin_past_crackdown_with_comment));
//				if(bitmapDrawable != null) {
//					mPastCrackdownWithCommentPinIcon = Bitmap.createScaledBitmap(bitmapDrawable.getBitmap(), width, height, false);
//				}
//			}
//
//			if(mPastCrackdownWidthPhotoNCommentPinIcon == null) {
//				width = Utils.dpFromPx(mMainActivity, 203);
//				height = Utils.dpFromPx(mMainActivity, 259);
//				bitmapDrawable = ((BitmapDrawable) getResources().getDrawable(R.drawable.pin_past_crackdown_with_photo_n_comment));
//				if(bitmapDrawable != null) {
//					mPastCrackdownWidthPhotoNCommentPinIcon = Bitmap.createScaledBitmap(bitmapDrawable.getBitmap(), width, height, false);
//				}
//			}
//
//			if(mCurrentCrackdownPinIcon == null) {
//				width = Utils.dpFromPx(mMainActivity, 161);
//				height = Utils.dpFromPx(mMainActivity, 201);
//				bitmapDrawable = ((BitmapDrawable) getResources().getDrawable(R.drawable.pin_current_crackdown));
//				if(bitmapDrawable != null) {
//					mCurrentCrackdownPinIcon = Bitmap.createScaledBitmap(bitmapDrawable.getBitmap(), width, height, false);
//				}
//			}
//
//			if(mCurrentCrackdownWithCommentPinIcon == null) {
//				width = Utils.dpFromPx(mMainActivity, 161);
//				height = Utils.dpFromPx(mMainActivity, 259);
//				bitmapDrawable = ((BitmapDrawable) getResources().getDrawable(R.drawable.pin_current_crackdown_with_comment));
//				if(bitmapDrawable != null) {
//					mCurrentCrackdownWithCommentPinIcon = Bitmap.createScaledBitmap(bitmapDrawable.getBitmap(), width, height, false);
//				}
//			}
//
//			if(mCurrentCrackdownWidthPhotoNCommentPinIcon == null) {
//				width = Utils.dpFromPx(mMainActivity, 203);
//				height = Utils.dpFromPx(mMainActivity, 259);
//				bitmapDrawable = ((BitmapDrawable) getResources().getDrawable(R.drawable.pin_current_crackdown_with_photo_n_comment));
//				if(bitmapDrawable != null) {
//					mCurrentCrackdownWidthPhotoNCommentPinIcon = Bitmap.createScaledBitmap(bitmapDrawable.getBitmap(), width, height, false);
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

    private void showCrackdownList() {
        try {
            removeCrackdownMarkers();

            Marker m;
            for (CrackdownListViewModel crackdownData : mCrackdownList) {
                long now = new Date().getTime();
                long diff = 60 * 60 * 1000;   // 현재 단속 지점 표시는 지점 등록 후 1시간까지만..

                String snippet = "crackdown_" + crackdownData.getmCrackdownId();
                LatLng markerPosition = new LatLng(crackdownData.getmLocation().getmCoordinates().get(1), crackdownData.getmLocation().getmCoordinates().get(0));

                if (!mIsFragmentRunning) {
                    return;
                }

                if (now - crackdownData.getmCreateTime() <= diff) {
                    if (crackdownData.getmReportType().equals("photo")) {
                        if (crackdownData.getmCommentCount() > 0) {
                            m = mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_current_crackdown_with_photo_n_comment)).position(markerPosition).snippet(snippet));
                        } else {
                            m = mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_current_crackdown_with_photo)).position(markerPosition).snippet(snippet));
                        }
                    } else {
                        if (crackdownData.getmCommentCount() > 0) {
                            m = mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_current_crackdown_with_comment)).position(markerPosition).snippet(snippet));
                        } else {
                            m = mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_current_crackdown)).position(markerPosition).snippet(snippet));
                        }
                    }

                    mCrackdownMarkerList.add(m);
                }

                if (mIsShowPastCrackdown) {
                    if (now - crackdownData.getmCreateTime() > diff) {
                        if (crackdownData.getmReportType().equals("photo")) {
                            if (crackdownData.getmCommentCount() > 0) {
                                m = mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_past_crackdown_with_photo_n_comment)).position(markerPosition).snippet(snippet));
                            } else {
                                m = mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_past_crackdown_with_photo)).position(markerPosition).snippet(snippet));
                            }
                        } else {
                            if (crackdownData.getmCommentCount() > 0) {
                                m = mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_past_crackdown_with_comment)).position(markerPosition).snippet(snippet));
                            } else {
                                m = mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_past_crackdown)).position(markerPosition).snippet(snippet));
                            }
                        }

                        mCrackdownMarkerList.add(m);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void removeDriverMarkers() {
        try {
            if (mDriverMarkerList != null && mDriverMarkerList.size() > 0) {
                for (Marker m : mDriverMarkerList) {
                    m.remove();
                }

                mDriverMarkerList.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void removeDeletedCrackdownMarker(String _crackdownId) {
        try {
            if (mCrackdownMarkerList != null && mCrackdownMarkerList.size() > 0) {
                for (Marker m : mCrackdownMarkerList) {
                    if (m.getSnippet().contains(_crackdownId)) {
                        m.remove();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void removeCrackdownMarkers() {
        try {
            if (mCrackdownMarkerList != null && mCrackdownMarkerList.size() > 0) {
                for (Marker m : mCrackdownMarkerList) {
                    m.remove();
                }

                mCrackdownMarkerList.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showCrackdownReport() {
        try {
            if (isCrackdownReportAvailableTime()) {
                mPopupCrackdownReportLayout.setVisibility(View.VISIBLE);
            } else {
                CommonDialog.showSimpleDialog(mMainActivity, "현재는 음주단속제보 가능시간이 아니오니 저녁 7시 이후에 제보하여주시기 바랍니다.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideCrackdownReport() {
        try {
            mPopupCrackdownReportLayout.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isCrackdownReportAvailableTime() {
        try {
            Calendar calendar = Calendar.getInstance();
            int hours = calendar.get(Calendar.HOUR_OF_DAY);
            if (hours < Configuration.CRACKDOWN_REPORT_END_TIME || hours >= Configuration.CRACKDOWN_REPORT_START_TIME) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    private void moveToReportMapFragment(final String _selectedImagePath) {
        try {
            hideCrackdownReport();

            Handler handler_ = new Handler(Looper.getMainLooper());
            handler_.postDelayed(new Runnable() {
                @Override
                public void run() {
                    CrackdownReportMapFragment frag = new CrackdownReportMapFragment();
                    Bundle args = new Bundle();
                    args.putString("selectedImagePath", "");

                    if (!Utils.isStringNullOrEmpty(_selectedImagePath)) {
                        args.putString("selectedImagePath", _selectedImagePath);
                    }

                    frag.setArguments(args);

                    frag.setTargetFragment(CrackdownFragment.this, REQ_REPORT_MAP_CODE);

                    mMainActivity.addFragment(frag);
                }
            }, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getPath(Uri _uri) {
        try {
            Cursor cursor = getActivity().getContentResolver().query(Uri.parse(_uri.toString()), null, null, null, null);
            cursor.moveToNext();
            String absolutePath = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.DATA));

            cursor.close();

            return absolutePath;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == REQ_REPORT_MAP_CODE) {
                    getNearData();
                } else if (requestCode == Configuration.SELECT_PHOTO_RES_CODE) {
                    if (data != null) {
                        Uri selectedImage = data.getData();
                        Utils.log("error", TAG, "selectedImage : " + selectedImage.toString());

                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);

                        String selectedImagePath = getRotatePhotoPath(bitmap, getPath(selectedImage));

                        if (Utils.isStringNullOrEmpty(selectedImagePath)) {
                            Toast.makeText(mMainActivity, "사진 업로드 중 오류가 발생했습니다.", Toast.LENGTH_SHORT).show();
                        }

                        moveToReportMapFragment(selectedImagePath);
                    }
                } else if (requestCode == Configuration.BACK_PRESSED) {
                    closeCrackdownComments();
                } else if (requestCode == Configuration.POSITION_CHANGED) {
                    changeMyLocation();
                } else if (requestCode == Configuration.CAMERA_REQ_CODE) {
                    reportPhoto();
                } else if (requestCode == Configuration.SHARE_IMAGE_REQ_CODE) {
                    showShareLayer();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onActivityResult(ActivityResultEvent _activityResultEvent) {
        try {
            onActivityResult(_activityResultEvent.getRequestCode(), _activityResultEvent.getResultCode(), _activityResultEvent.getData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getRotatePhotoPath(Bitmap _realImage, String _absoluteImagePath) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String output_file_name = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator + timeStamp + ".jpeg";

        File pictureFile = new File(output_file_name);
        if (pictureFile.exists()) {
            pictureFile.delete();
        }

        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);

            ExifInterface exif = new ExifInterface(_absoluteImagePath);

            Log.d("EXIF value", exif.getAttribute(ExifInterface.TAG_ORIENTATION));
            if (exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("6")) {
                _realImage = rotate(_realImage, 90);
            } else if (exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("8")) {
                _realImage = rotate(_realImage, 270);
            } else if (exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("3")) {
                _realImage = rotate(_realImage, 180);
            } else if (exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("0")) {
                _realImage = rotate(_realImage, 90);
            }

            boolean bo = _realImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);

            fos.close();

            Utils.log("error", TAG, "output_file_name : " + output_file_name);
            return output_file_name;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        //       mtx.postRotate(degree);
        mtx.setRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    private boolean checkShareImagePermission() {
        boolean isPermissionGranted = true;

        try {
            if (Build.VERSION.SDK_INT >= 23) {
                if (ContextCompat.checkSelfPermission(mMainActivity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(mMainActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(mMainActivity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Configuration.SHARE_IMAGE_REQ_CODE);
                    isPermissionGranted = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            isPermissionGranted = false;
        }

        return isPermissionGranted;
    }

    private boolean checkCameraPermission() {
        boolean isPermissionGranted = true;

        try {
            if (Build.VERSION.SDK_INT >= 23) {
                if (ContextCompat.checkSelfPermission(mMainActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(mMainActivity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(mMainActivity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, Configuration.CAMERA_REQ_CODE);
                    isPermissionGranted = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            isPermissionGranted = false;
        }

        return isPermissionGranted;
    }

    private class CrackdownSharedAppListAdapter extends ArrayAdapter<ResolveInfo> {
        private ArrayList<ResolveInfo> mCrackdownShareAppListData;
        private int mResourceId;
        private CrackdownShareAppListViewHolder mViewHolder;

        public CrackdownSharedAppListAdapter(Context _context, int _resourceId, ArrayList<ResolveInfo> _listData) {
            super(_context, _resourceId, _listData);

            this.mCrackdownShareAppListData = _listData;
            this.mResourceId = _resourceId;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;

            try {
                if (v == null) {
                    LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    v = li.inflate(mResourceId, null);

                    mViewHolder = new CrackdownShareAppListViewHolder();
                    mViewHolder.mCrackdownShareRowAppIcon = (ImageView) v.findViewById(R.id.crackdown_share_row_app_icon);
                    mViewHolder.mCrackdownShareRowAppName = (TextView) v.findViewById(R.id.crackdown_share_row_app_name);

                    v.setTag(mViewHolder);
                } else {
                    mViewHolder = (CrackdownShareAppListViewHolder) v.getTag();
                }

                ResolveInfo crackdownShareAppData = mCrackdownShareAppListData.get(position);

                mViewHolder.mCrackdownShareRowAppName.setText(crackdownShareAppData.activityInfo.applicationInfo.loadLabel(getActivity().getPackageManager()).toString());
                mViewHolder.mCrackdownShareRowAppIcon.setImageDrawable(crackdownShareAppData.activityInfo.applicationInfo.loadIcon(getActivity().getPackageManager()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            return v;
        }
    }

    @Override
    public void onDestroyView() {
        BusProvider.getInstance().unregister(this);
        super.onDestroyView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onStart() {
        super.onStart();

        mIsFragmentRunning = true;
    }

    @Override
    public void onStop() {
        super.onStop();

        mIsFragmentRunning = false;
    }
}

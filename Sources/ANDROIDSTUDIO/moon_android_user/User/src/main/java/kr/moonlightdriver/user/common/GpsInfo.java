package kr.moonlightdriver.user.common;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class GpsInfo implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
	private static final String TAG = "GpsInfo";
	private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10 * 1000;
	private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
	private static final float SMALLEST_DISPLACEMENT_IN_METERS = 10f;

	private GoogleApiClient mGoogleApiClient;
	private LocationRequest mLocationRequest;

	private Location mCurrentLocation;

	private LocationListener mLocationListener;

	private final Context mContext;

	public GpsInfo(Context context, LocationListener locationListener) {
		this.mContext = context;
		this.mLocationListener = locationListener;

		buildGoogleApiClient();
	}

	private synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(mContext)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API)
				.build();

		createLocationRequest();
	}

	public boolean isConnected() {
		return mGoogleApiClient.isConnected();
	}

	public void connectGoogleApi() {
		try {
			mGoogleApiClient.connect();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void disconnectGoogleApi() {
		try {
			mGoogleApiClient.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createLocationRequest() {
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
		mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
		mLocationRequest.setSmallestDisplacement(SMALLEST_DISPLACEMENT_IN_METERS);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	}

	public void startLocationUpdates() {
		try {
			if(checkPermission()) {
				LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, mLocationListener);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void stopLocationUpdates() {
		try {
			LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, mLocationListener);
			mContext.sendBroadcast(new Intent("com.skt.intent.action.GPS_TURN_OFF"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean checkPermission() {
		boolean isPermissionGranted = true;

		try {
			if(Build.VERSION.SDK_INT >= 23) {
				if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
					ActivityCompat.requestPermissions((Activity) mContext, new String[] { Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION  }, Configuration.ACCESS_LOCATION_REQ_CODE);
					isPermissionGranted = false;
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
			isPermissionGranted = false;
		}

		return isPermissionGranted;
	}

	public Location getLastLocation() {
		if(checkPermission()) {
			return LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
		}

		return null;
	}

	@Override
	public void onConnected(@Nullable Bundle bundle) {
		try {
			if (mCurrentLocation == null && checkPermission()) {
				mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
			}

			startLocationUpdates();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onConnectionSuspended(int i) {
		mGoogleApiClient.connect();
	}

	@Override
	public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
		Utils.log("error", TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
	}
}

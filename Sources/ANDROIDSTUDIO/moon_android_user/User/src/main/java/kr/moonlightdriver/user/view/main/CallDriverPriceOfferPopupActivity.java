package kr.moonlightdriver.user.view.main;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.Enums;
import kr.moonlightdriver.user.common.FontManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.viewmodel.callDriver.DriverInfoViewModel;
import kr.moonlightdriver.user.viewmodel.common.ResultData;

/**
 * Created by eklee on 2017. 6. 1..
 */

public class CallDriverPriceOfferPopupActivity extends Activity {
    private static String TAG = "CallDriverPriceOfferPopupActivity";

    private RelativeLayout mLoadingProgress;

    private boolean mAccept;

    private String mCallId;

    private int mOfferedPrice;

    //private CallDriverReceiver mCallDriverReceiver;

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.popup_call_driver_price_offer);

            SharedPreferences prefs = Utils.getGCMPreferences(this);
            int price = prefs.getInt(Configuration.PROPERTY_CALL_INFO_PRICE, Integer.MAX_VALUE);
            mOfferedPrice = prefs.getInt(Configuration.PROPERTY_CALL_INFO_BIDDING_PRICE, Integer.MAX_VALUE);
            mCallId = prefs.getString(Configuration.PROPERTY_CALL_INFO_CALL_ID, "");
            DriverInfoViewModel driverInfoData = new DriverInfoViewModel(prefs.getString(Configuration.PROPERTY_CALL_INFO_DRIVER_INFO, null));

            TextView titleTextView = (TextView) findViewById(R.id.tv_popup_call_driver_price_offer_title);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanVert(), titleTextView);

            TextView messageTextView = (TextView) findViewById(R.id.tv_popup_call_driver_price_offer_message);
            Utils.setFontToView(FontManager.getInstance().getFontSeoulNamsanJangM(), messageTextView);

            String message = getString(R.string.popup_call_driver_price_offer_message_format,
                Utils.convertToPriceString(price),
                driverInfoData.getmDriverName(),
                Utils.convertDriverGradeAverageToString(driverInfoData.getmDriverGradeAverage()),
                Utils.convertToPriceString(mOfferedPrice));
            messageTextView.setText(message);

            ImageButton closeButton = (ImageButton) findViewById(R.id.ib_popup_call_driver_price_offer_close);
            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                     requestAcceptPriceOffer(false);
                }
            });

            ImageButton refuseButton = (ImageButton) findViewById(R.id.ib_popup_call_driver_price_offer_refuse);
            refuseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestAcceptPriceOffer(false);
                }
            });

            ImageButton acceptButton = (ImageButton) findViewById(R.id.ib_popup_call_driver_price_offer_accept);
            acceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestAcceptPriceOffer(true);
                }
            });

            mLoadingProgress = (RelativeLayout) findViewById(R.id.loading_progress);
            hideLoadingAnim();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestAcceptPriceOffer(boolean accept) {
        try {
            mAccept = accept;

            showLoadingAnim();

            SharedPreferences prefs = Utils.getGCMPreferences(this);
            String userId = prefs.getString(Configuration.PROPERTY_USER_ID, null);

            RequestParams requestParams = new RequestParams();
            requestParams.put("userId", userId);
            requestParams.put("acceptYn", accept ? "Y" : "N");
            requestParams.put("callId", mCallId);
            requestParams.put("offeredPrice", mOfferedPrice);

            NetClient.send(this, Configuration.URL_CALL_DRIVER_PRICE_OFFER, "POST", requestParams, new NoResponseDataAck(), new NetResponseCallback(new PriceOfferResponse()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class PriceOfferResponse implements NetResponse {
        @Override
        public void onResponse(NetAPI _netAPI) {
            hideLoadingAnim();

            if (_netAPI != null) {
                ResultData resultData = _netAPI.getmResultData();
                if (resultData.getmCode() == Enums.NetResultCode.SUCCESS.getValue()) {
                    sendResult(mAccept);
                } else {
                    CommonDialog.showSimpleDialog(CallDriverPriceOfferPopupActivity.this, resultData.getmDetail());
                }
            }
        }
    }

    private void showLoadingAnim() {
        mLoadingProgress.setVisibility(View.VISIBLE);
    }

    private void hideLoadingAnim() {
        mLoadingProgress.setVisibility(View.GONE);
    }

    private void sendResult(boolean accept) {
        try {
            if (accept) {
                SharedPreferences prefs = Utils.getGCMPreferences(CallDriverPriceOfferPopupActivity.this);
                prefs.edit().putString(Configuration.PROPERTY_CALL_INFO_STATUS, Enums.CallStatus.Catch).apply();
            }

            Intent intent = new Intent();
            intent.putExtra("accept", accept);
            setResult(Activity.RESULT_OK, intent);
            overridePendingTransition(0, 0);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

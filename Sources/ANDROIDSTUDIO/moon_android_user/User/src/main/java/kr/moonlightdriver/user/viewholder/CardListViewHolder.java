package kr.moonlightdriver.user.viewholder;

import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by eklee on 2017. 4. 8..
 */

public class CardListViewHolder {
    public TextView nameAndCardNumberTextView;
    public TextView userEmailTextView;
    public ImageButton btnDelete;
}

package kr.moonlightdriver.user.view.settings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.skt.Tmap.TMapAddressInfo;
import com.skt.Tmap.TMapData;
import com.skt.Tmap.TMapPOIItem;

import java.util.ArrayList;
import java.util.List;

import kr.moonlightdriver.user.Adapter.SearchPOIListAdapter;
import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewmodel.common.LocationData;

/**
 * Created by eklee on 2017. 4. 18..
 */

public class SearchPOIFragment extends Fragment {

    private static final String TAG = "SearchPOIFragment";

    ListView mAutoCompleteListView;
    List<String> mAutoCompleteList;
    ArrayAdapter<String> mAutoCompleteListAdapter;

    ListView mSearchResultListView;
    List<TMapPOIItem> mSearchResultList;
    SearchPOIListAdapter mSearchResultListAdapter;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            view = inflater.inflate(R.layout.fragment_setting_favorite_search_location, container, false);

            LinearLayout goBackButton = (LinearLayout) view.findViewById(R.id.ib_setting_goback);
            goBackButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getActivity().getSupportFragmentManager().beginTransaction().remove(SearchPOIFragment.this).commit();
                }
            });

            final EditText searchKeyword = (EditText) view.findViewById(R.id.et_search_keyword);
            searchKeyword.requestFocus();
            Utils.showSoftInput(getActivity());
            searchKeyword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if (b) {
                        mAutoCompleteListView.setVisibility(View.VISIBLE);
                        mSearchResultListView.setVisibility(View.GONE);
                    }
                }
            });
            searchKeyword.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    TMapData tMapData = new TMapData();
                    tMapData.autoComplete(charSequence.toString(), new TMapData.AutoCompleteListenerCallback() {
                        @Override
                        public void onAutoComplete(final ArrayList<String> arrayList) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAutoCompleteList.clear();
                                    mAutoCompleteListAdapter.notifyDataSetChanged();
                                    mAutoCompleteList.addAll(arrayList);
                                    mAutoCompleteListAdapter.notifyDataSetChanged();
                                }
                            });
                        }
                    });
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            searchKeyword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    switch (actionId) {
                        case EditorInfo.IME_ACTION_DONE:
                            Utils.hideSoftInput(getActivity());
                            break;
                        default:
                            return false;
                    }
                    return true;
                }
            });

            mAutoCompleteList = new ArrayList<>();
            mAutoCompleteListAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, mAutoCompleteList);
            mAutoCompleteListView = (ListView) view.findViewById(R.id.lv_auto_complete);
            mAutoCompleteListView.setAdapter(mAutoCompleteListAdapter);
            mAutoCompleteListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(final AdapterView<?> adapterView, View view, int i, long l) {
                    Utils.hideSoftInput(getActivity());

                    searchKeyword.clearFocus();

                    TMapData tMapData = new TMapData();
                    tMapData.findAllPOI(mAutoCompleteList.get(i), 10, new TMapData.FindAllPOIListenerCallback() {
                        @Override
                        public void onFindAllPOI(final ArrayList<TMapPOIItem> arrayList) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mSearchResultList.clear();
                                    mSearchResultListAdapter.notifyDataSetChanged();
                                    mSearchResultList.addAll(arrayList);
                                    mSearchResultListAdapter.notifyDataSetChanged();

                                    mAutoCompleteListView.setVisibility(View.GONE);
                                    mSearchResultListView.setVisibility(View.VISIBLE);
                                }
                            });
                        }
                    });
                }
            });

            mSearchResultList = new ArrayList<>();
            mSearchResultListAdapter = new SearchPOIListAdapter(getActivity(), R.layout.row_search_location_list, mSearchResultList);
            mSearchResultListView = (ListView) view.findViewById(R.id.lv_search_result);
            mSearchResultListView.setAdapter(mSearchResultListAdapter);
            mSearchResultListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    try {

                        ((MainActivity) getActivity()).showLoadingAnim();

                        final TMapPOIItem tMapPOIItem = mSearchResultList.get(i);

                        TMapData tMapData = new TMapData();
                        tMapData.reverseGeocoding(Double.parseDouble(tMapPOIItem.noorLat), Double.parseDouble(tMapPOIItem.noorLon), "A04", new TMapData.reverseGeocodingListenerCallback() {
                            @Override
                            public void onReverseGeocoding(final TMapAddressInfo tMapAddressInfo) {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ((MainActivity) getActivity()).hideLoadingAnim();

                                        String address;
                                        if (tMapAddressInfo != null) {
                                            address = tMapAddressInfo.strFullAddress;
                                        } else {
                                            address = Utils.getPOIAddressExceptNullOrEmpty(tMapPOIItem);
                                        }

                                        LocationData locationData = new LocationData(
                                            new LatLng(Double.parseDouble(tMapPOIItem.noorLat), Double.parseDouble(tMapPOIItem.noorLon)),
                                            tMapPOIItem.getPOIName(),
                                            address);

                                        Intent intent = new Intent();
                                        intent.putExtra("locationData", locationData);

                                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);

                                        getActivity().getSupportFragmentManager().beginTransaction().remove(SearchPOIFragment.this).commit();
                                    }
                                });
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        Utils.hideSoftInput(getActivity());
    }
}

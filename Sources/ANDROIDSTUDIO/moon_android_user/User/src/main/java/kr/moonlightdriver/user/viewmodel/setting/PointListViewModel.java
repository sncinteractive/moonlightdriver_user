package kr.moonlightdriver.user.viewmodel.setting;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.viewmodel.callDriver.DriverInfoViewModel;

/**
 * Created by kyo on 2017-04-10.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class PointListViewModel {
    @JsonProperty("createdDate")
    private long mCreatedDate;

    @JsonProperty("userId")
    private String mUserId;

    @JsonProperty("point")
    private int mPoint;

    @JsonProperty("beforePoint")
    private int mBeforePoint;

    @JsonProperty("afterPoint")
    private int mAfterPoint;

    @JsonProperty("pointMessage")
    private String mPointMessage;

    @JsonProperty("pointType")
    private String mPointType;

    @JsonProperty("from")
    private String mFrom;

    @JsonProperty("to")
    private String mTo;

    @JsonProperty("price")
    private int mPrice;

    @JsonProperty("driverInfo")
    private DriverInfoViewModel mDriverInfo;

    public PointListViewModel() {
    }

    public PointListViewModel(long mCreatedDate, String mUserId, int mPoint, int mBeforePoint, int mAfterPoint, String mPointMessage, String mPointType, String mFrom, String mTo, int mPrice, DriverInfoViewModel mDriverInfo) {
        this.mCreatedDate = mCreatedDate;
        this.mUserId = mUserId;
        this.mPoint = mPoint;
        this.mBeforePoint = mBeforePoint;
        this.mAfterPoint = mAfterPoint;
        this.mPointMessage = mPointMessage;
        this.mPointType = mPointType;
        this.mFrom = mFrom;
        this.mTo = mTo;
        this.mPrice = mPrice;
        this.mDriverInfo = mDriverInfo;
    }

    public PointListViewModel(long mCreatedDate, int mPoint, String mPointType, String mFrom, String mTo, int mPrice, DriverInfoViewModel mDriverInfo) {
        this.mCreatedDate = mCreatedDate;
        this.mUserId = mUserId;
        this.mPoint = mPoint;
        this.mBeforePoint = mBeforePoint;
        this.mAfterPoint = mAfterPoint;
        this.mPointMessage = mPointMessage;
        this.mPointType = mPointType;
        this.mFrom = mFrom;
        this.mTo = mTo;
        this.mPrice = mPrice;
        this.mDriverInfo = mDriverInfo;
    }

    public long getmCreatedDate() {
        return mCreatedDate;
    }

    public void setmCreatedDate(long mCreatedDate) {
        this.mCreatedDate = mCreatedDate;
    }

    public String getmUserId() {
        return mUserId;
    }

    public void setmUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public int getmPoint() {
        return mPoint;
    }

    public void setmPoint(int mPoint) {
        this.mPoint = mPoint;
    }

    public int getmBeforePoint() {
        return mBeforePoint;
    }

    public void setmBeforePoint(int mBeforePoint) {
        this.mBeforePoint = mBeforePoint;
    }

    public int getmAfterPoint() {
        return mAfterPoint;
    }

    public void setmAfterPoint(int mAfterPoint) {
        this.mAfterPoint = mAfterPoint;
    }

    public String getmPointMessage() {
        return mPointMessage;
    }

    public void setmPointMessage(String mPointMessage) {
        this.mPointMessage = mPointMessage;
    }

    public String getmPointType() {
        return mPointType;
    }

    public void setmPointType(String mPointType) {
        this.mPointType = mPointType;
    }

    public String getmFrom() {
        return mFrom;
    }

    public void setmFrom(String mFrom) {
        this.mFrom = mFrom;
    }

    public String getmTo() {
        return mTo;
    }

    public void setmTo(String mTo) {
        this.mTo = mTo;
    }

    public int getmPrice() {
        return mPrice;
    }

    public void setmPrice(int mPrice) {
        this.mPrice = mPrice;
    }

    public DriverInfoViewModel getmDriverInfo() {
        return mDriverInfo;
    }

    public void setmDriverInfo(DriverInfoViewModel mDriverInfo) {
        this.mDriverInfo = mDriverInfo;
    }
}

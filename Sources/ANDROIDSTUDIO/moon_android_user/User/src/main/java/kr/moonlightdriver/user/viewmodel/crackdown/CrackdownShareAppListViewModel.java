package kr.moonlightdriver.user.viewmodel.crackdown;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by youngmin on 2016-08-11.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CrackdownShareAppListViewModel {
	private String mAppName;
	private String mAppIcon;

	public CrackdownShareAppListViewModel() {}

	public CrackdownShareAppListViewModel(String mAppIcon, String mAppName) {
		this.mAppIcon = mAppIcon;
		this.mAppName = mAppName;
	}

	public String getmAppIcon() {
		return mAppIcon;
	}

	public void setmAppIcon(String mAppIcon) {
		this.mAppIcon = mAppIcon;
	}

	public String getmAppName() {
		return mAppName;
	}

	public void setmAppName(String mAppName) {
		this.mAppName = mAppName;
	}
}

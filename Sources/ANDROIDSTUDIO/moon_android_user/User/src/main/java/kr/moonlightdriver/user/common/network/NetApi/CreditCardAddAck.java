package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.setting.CardListViewModel;

/**
 * Created by kyo on 2017-04-13.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreditCardAddAck extends NetAPI {
    @JsonProperty("creditCard")
    private CardListViewModel mCreditCard;

    public CreditCardAddAck() {
    }

    public CreditCardAddAck(CardListViewModel mCreditCard) {
        this.mCreditCard = mCreditCard;
    }

    public CreditCardAddAck(ResultData mResultData, CardListViewModel mCreditCard) {
        super(mResultData);
        this.mCreditCard = mCreditCard;
    }

    public CardListViewModel getmCreditCard() {
        return mCreditCard;
    }

    public void setmCreditCard(CardListViewModel mCreditCard) {
        this.mCreditCard = mCreditCard;
    }
}

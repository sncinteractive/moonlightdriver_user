package kr.moonlightdriver.user.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skt.Tmap.TMapPOIItem;

import java.util.List;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.viewholder.SearchLocationListViewHolder;

/**
 * Created by eklee on 2017. 4. 19..
 */

public class SearchPOIListAdapter extends ArrayAdapter<TMapPOIItem> {
    private int mResourceId;
    private SearchLocationListViewHolder mViewHolder;

    public SearchPOIListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<TMapPOIItem> objects) {
        super(context, resource, objects);
        mResourceId = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(mResourceId, null);

            mViewHolder = new SearchLocationListViewHolder();
            mViewHolder.mTextViewLocationName = (TextView) convertView.findViewById(R.id.row_search_list_location_name);
            mViewHolder.mTextViewAddress = (TextView) convertView.findViewById(R.id.row_search_list_address);

            convertView.setTag(mViewHolder);

        } else {
            mViewHolder = (SearchLocationListViewHolder) convertView.getTag();
        }

        TMapPOIItem tMapPOIItem = getItem(position);
        mViewHolder.mTextViewLocationName.setText(tMapPOIItem.getPOIName());

        mViewHolder.mTextViewAddress.setText(Utils.getPOIAddressExceptNullOrEmpty(tMapPOIItem));

        return convertView;
    }
}

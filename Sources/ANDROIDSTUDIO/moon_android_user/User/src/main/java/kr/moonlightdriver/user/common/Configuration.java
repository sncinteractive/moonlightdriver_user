package kr.moonlightdriver.user.common;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

public class Configuration {
    public static final LatLng SEOUL_CITY_HALL = new LatLng(37.566221, 126.977921);

    public static final String APP_DOWNLOAD_URL = "https://play.google.com/store/apps/details?id=kr.moonlightdriver.user";
    public static final String DATABASE_NAME = "moonlightdriver.db";
    public static final int DATABASE_VERSION = 1;

    public static final long DOUBLE_PRESS_INTERVAL = 200;

    public static final String HEADQUARTER_NUMBER = "070-7585-3100";

    // preference key
    public static final String PREFERENCES_NAME = "moonlightdriver_user";
    public static final String PROPERTY_DEVICE_UUID = "DEVICE_UUID";
    public static final String PROPERTY_FIRST_LAUNCH = "FIRST_LAUNCH";
    public static final String PROPERTY_REG_ID = "REG_ID";
    public static final String PROPERTY_APP_VERSION = "APP_VERSION";
    public static final String PROPERTY_USER_ID = "USER_ID";
    public static final String PROPERTY_LATEST_LOCATION_LIST = "LATEST_LOCATION_LIST";
    public static final String PROPERTY_LATEST_RECEIVED_CALL_DRIVER_PUSH_MSG_TYPE = "LATEST_RECEIVED_CALL_DRIVER_PUSH_MSG_TYPE";

    public static final String PROPERTY_CALL_INFO_PRICE = "PROPERTY_CALL_INFO_PRICE";
    public static final String PROPERTY_CALL_INFO_BIDDING_PRICE = "PROPERTY_CALL_INFO_BIDDING_PRICE";
    public static final String PROPERTY_CALL_INFO_PAYMENT_OPTION = "PROPERTY_CALL_INFO_PAYMENT_OPTION";
    public static final String PROPERTY_CALL_INFO_DRIVER_INFO = "PROPERTY_CALL_INFO_DRIVER_INFO";
    public static final String PROPERTY_CALL_INFO_SOURCE_LOCATION = "PROPERTY_CALL_INFO_SOURCE_LOCATION";
    public static final String PROPERTY_CALL_INFO_TARGET_LOCATION = "PROPERTY_CALL_INFO_TARGET_LOCATION";
    public static final String PROPERTY_CALL_INFO_STOPBY_LOCATION_LIST = "PROPERTY_CALL_INFO_STOPBY_LOCATION_LIST";
    public static final String PROPERTY_CALL_INFO_STATUS = "PROPERTY_CALL_INFO_STATUS";
    public static final String PROPERTY_CALL_INFO_DEPARTURE_TIME = "PROPERTY_CALL_INFO_DEPARTURE_TIME";
    public static final String PROPERTY_CALL_INFO_ESTIMATED_ARRIVAL_TIME = "PROPERTY_CALL_INFO_ESTIMATED_ARRIVAL_TIME";
    public static final String PROPERTY_CALL_INFO_CALL_ID = "PROPERTY_CALL_INFO_CALL_ID";
    public static final String PROPERTY_CALL_INFO_CALL_RANGE = "PROPERTY_CALL_INFO_CALL_RANGE";
    public static final String PROPERTY_CALL_INFO_CALL_WAIT_TIME = "PROPERTY_CALL_INFO_CALL_WAIT_TIME";
    //public static final String PROPERTY_CALL_INFO_EVALUTED_YN = "PROPERTY_CALL_INFO_EVALUTED_YN";

    public static final String PROPERTY_RELIEF_MESSAGE_STATE = "PROPERTY_RELIEF_MESSAGE_STATE";

    public static final String PATH_SHARED_IMAGE = "sharedImages";
    public static final String SHARED_TEXT = "달빛기사 - 교통정보 SNS 앱";

    public static final String SENDER_ID = "563199847799";

    // 사용자 위치 서버로 전송하는 업데이트 거리 1000미터
    public static int UPDATE_DISTANCE = 1000;

    public static final int RESET_UPDATE_DISTANCE_INTERVAL = 60 * 60 * 1000;

    public static final int ACCESS_LOCATION_REQ_CODE = 1;
    public static final int PHONE_NUMBER_REQ_CODE = 2;
    public static final int CALL_PHONE_REQ_CODE = 3;
    public static final int GPS_SETTING_REQ_CODE = 4;
    public static final int WIFI_SETTING_REQ_CODE = 5;
    public static final int SHARE_IMAGE_REQ_CODE = 6;
    public static final int SELECT_PHOTO_REQ_CODE = 7;
    public static final int BACK_PRESSED = 8;
    public static final int POSITION_CHANGED = 9;
    public static final int REQ_SEARCH_LOCATION = 10;
    public static final int INCOMING_CALL = 11;
    public static final int CAMERA_REQ_CODE = 12;
    public static final int SECRET_CALL_PHONE_REQ_CODE = 13;
    public static final int SELECT_PHOTO_RES_CODE = 14;
    public static final int CALL_DRIVER_NOT_ASSIGNED_POPUP_REQ_CODE = 15;
    public static final int CALL_DRIVER_ASSIGNED_POPUP_REQ_CODE = 16;
    public static final int CALL_DRIVER_CANCEL_PAYMENT_POPUP_REQ_CODE = 17;
    public static final int CALL_DRIVER_CARD_PAYMENT_POPUP_REQ_CODE = 18;
    public static final int CALL_DRIVER_PRICE_OFFER_POPUP_REQ_CODE = 19;
    public static final int CALL_DRIVER_POSITION_CHANGED = 20;
    public static final int CALL_DRIVER_RELIEF_MESSAGE_POPUP_REQ_CODE = 21;
    public static final int CALL_DRIVER_ARRIVED_TARGET_POPUP_REQ_CODE = 22;

    public static final int REQUEST_GOOGLE_PLAY_SERVICES = 9000;

    public static String[] REGION_LIST = new String[]{"전국콜", "서울경기", "강원도", "충청도", "경상도", "전라도", "제주도", "대구"};
    public static JSONObject REGION;

    public static final int CRACKDOWN_REPORT_START_TIME = 19;
    public static final int CRACKDOWN_REPORT_END_TIME = 5;

    //  T-map api key
    public static final String TMAP_API_KEY = "ed9e65ac-df82-4c17-af08-1bd9c7054586";

    // REAL SVR
    public static final String SERVER_HOST = "http://115.68.104.30:8888";
    public static final String IMAGE_SERVER_HOST = "http://115.68.122.108:8080";
    public static final String COUPON_IMAGE_SERVER_HOST = "http://115.68.122.108:8080";

    // DEV SVR
//    public static final String SERVER_HOST = "http://dev.qwerts.co.kr:8888";
//    public static final String IMAGE_SERVER_HOST = "http://dev.qwerts.co.kr:8889";
//    public static final String COUPON_IMAGE_SERVER_HOST = "http://dev.qwerts.co.kr:8890";

    public static final String URL_CHECK_VERSION = Configuration.SERVER_HOST + "/user/version";
    public static final String URL_AUTO_REGISTER_USER = Configuration.SERVER_HOST + "/user/register/auto";
    public static final String URL_UPDATE_USER_LOCATION = Configuration.SERVER_HOST + "/user/update/location";
    public static final String URL_UPDATE_PUSH_YN = Configuration.SERVER_HOST + "/user/update/push_yn";
    public static final String URL_DRIVER_FIND = Configuration.SERVER_HOST + "/driver/find";
    public static final String URL_CRACKDOWN_LIST = Configuration.SERVER_HOST + "/crackdown/list";
    public static final String URL_CRACKDOWN_DETAIL = Configuration.SERVER_HOST + "/crackdown/detail";
    public static final String URL_CRACKDOWN_DELETE = Configuration.SERVER_HOST + "/crackdown/delete";
    public static final String URL_CRACKDOWN_RANKING = Configuration.SERVER_HOST + "/crackdown/list/rank";
    public static final String URL_REGISTER_CRACKDOWN = Configuration.SERVER_HOST + "/crackdown/register";
    public static final String URL_REGISTER_CRACKDOWN_COMMENT = Configuration.SERVER_HOST + "/crackdown/comment/register";
    public static final String URL_UPDATE_ACTION = Configuration.SERVER_HOST + "/crackdown/action";
    public static final String URL_CHAUFFEUR_COMPANY_LIST = Configuration.SERVER_HOST + "/chauffeur_company/list";
    public static final String URL_CHAUFFEUR_COMPANY_DETAIL = Configuration.SERVER_HOST + "/chauffeur_company/detail";
    public static final String URL_CHAUFFEUR_COMPANY_UPDATE_BOOKMARK = Configuration.SERVER_HOST + "/chauffeur_company/update/bookmark";
    public static final String URL_VIRTUAL_NUMBER_REGISTER = Configuration.SERVER_HOST + "/virtual_number/register";
    public static final String URL_VIRTUAL_NUMBER_UNREGISTER = Configuration.SERVER_HOST + "/virtual_number/unregister";
    public static final String URL_UPLOAD_FILE = Configuration.SERVER_HOST + "/crackdown/upload/file";
    public static final String URL_SHARE_FILE = Configuration.SERVER_HOST + "/crackdown/share/file";
    public static final String URL_NOTICE_LIST = Configuration.SERVER_HOST + "/notice/user/list";
    public static final String URL_CALL_DRIVER_CALCULATE_PRICE = Configuration.SERVER_HOST + "/calldriver/calculate/price";
    public static final String URL_CALL_DRIVER_CALL = Configuration.SERVER_HOST + "/calldriver/save/call";
    public static final String URL_CALL_DRIVER_CALL_DETAIL = Configuration.SERVER_HOST + "/calldriver/detail";
    public static final String URL_CALL_DRIVER_CANCEL_CALL = Configuration.SERVER_HOST + "/calldriver/cancel/call";
    public static final String URL_CALL_DRIVER_HISTORY_CALL = Configuration.SERVER_HOST + "/calldriver/history/call";
    public static final String URL_CALL_DRIVER_CANCEL_PAYMENT = Configuration.SERVER_HOST + "/calldriver/cancelPayment";
    public static final String URL_CALL_DRIVER_PRICE_OFFER = Configuration.SERVER_HOST + "/calldriver/priceoffer";
    public static final String URL_CALL_DRIVER_UPDATE_RELIEF_MESSAGE_STATE = Configuration.SERVER_HOST + "/calldriver/update/relief_message_state";
    public static final String URL_CALL_DRIVER_EVALUATE = Configuration.SERVER_HOST + "/calldriver/evaluate/driver";
    public static final String URL_CALL_DRIVER_REPORT_UNFAIRNESS = Configuration.SERVER_HOST + "/calldriver/unfairness/driver";
    public static final String URL_CALL_DRIVER_UPDATE_PAYMENT = Configuration.SERVER_HOST + "/calldriver/update/payment";
    public static final String URL_SETTINGS_GET_MY_CAR_INFO = Configuration.SERVER_HOST + "/settings/get/my_car_info";
    public static final String URL_SETTINGS_SAVE_MY_CAR_INFO = Configuration.SERVER_HOST + "/settings/save/my_car_info";
    public static final String URL_FAVORITE_LIST_ADD = Configuration.SERVER_HOST + "/settings/save/favorite";
    public static final String URL_FAVORITE_LIST_GET = Configuration.SERVER_HOST + "/settings/get/favorite";
    public static final String URL_FAVORITE_LIST_MODIFY = Configuration.SERVER_HOST + "/settings/delete/favorite";
    public static final String URL_CREDIT_CARD_ADD = Configuration.SERVER_HOST + "/settings/add/card";
    public static final String URL_CREDIT_CARD_LIST = Configuration.SERVER_HOST + "/settings/list/card";
    public static final String URL_CREDIT_CARD_DELETE = Configuration.SERVER_HOST + "/settings/delete/card";
    public static final String URL_COUPON_LIST_GET = Configuration.SERVER_HOST + "/settings/get/coupons";
    public static final String URL_COUPON_DETAIL_GET = Configuration.SERVER_HOST + "/settings/get/coupon/detail";
    public static final String URL_COUPON_USE_UPDATE = Configuration.SERVER_HOST + "/settings/update/coupon/status";
    public static final String URL_POINT_LIST = Configuration.SERVER_HOST + "/settings/list/point";
    public static final String URL_POINT_CHARGE = Configuration.SERVER_HOST + "/settings/charge/point";


    public static final String IMAGE_URL_FOR_KAKAO_LINK = "";
    public static final int IMAGE_WIDTH_FOR_KAKAO_LINK = 70; // 최소 70 이상
    public static final int IMAGE_HEIGHT_FOR_KAKAO_LINK = 70; // 최소 70 이상

    // paymethod
    public static final String PAY_METHOD_CARD = "card";
    public static final String PAY_METHOD_CASH = "cash";

    // push intent action type
    public static final String PUSH_INTENT_ACTION_VIRTUAL_NUMBER_RECEIVED = "PUSH_INTENT_ACTION_VIRTUAL_NUMBER_RECEIVED";
    public static final String PUSH_INTENT_ACTION_CALL_DRIVER_ASSIGNED = "PUSH_INTENT_ACTION_CALL_DRIVER_ASSIGNED";
    public static final String PUSH_INTENT_ACTION_CALL_DRIVER_NOT_ASSIGNED = "PUSH_INTENT_ACTION_CALL_DRIVER_NOT_ASSIGNED";
    public static final String PUSH_INTENT_ACTION_CALL_DRIVER_PRICE_OFFER = "PUSH_INTENT_ACTION_CALL_DRIVER_PRICE_OFFER";
    public static final String PUSH_INTENT_ACTION_CALL_DRIVER_DRIVING = "PUSH_INTENT_ACTION_CALL_DRIVER_DRIVING";
    public static final String PUSH_INTENT_ACTION_CALL_ARRIVED_TARGET = "PUSH_INTENT_ACTION_CALL_ARRIVED_TARGET";
    public static final String PUSH_INTENT_ACTION_CALL_DRIVER_CANCEL = "PUSH_INTENT_ACTION_CALL_DRIVER_CANCEL";

    // push message type
    public static final String PUSH_MSG_TYPE_REGISTER_VIRTUAL_NUMBER = "register_virtual_number";
    public static final String PUSH_MSG_TYPE_CRACK_DOWN_WARNING = "crackdown_warning";
    public static final String PUSH_MSG_TYPE_CALL_DRIVER_ASSIGNED = "call_driver_assigned";
    //public static final String PUSH_MSG_TYPE_CALL_DRIVER_NOT_ASSIGNED = "call_driver_not_assigned";
    public static final String PUSH_MSG_TYPE_CALL_DRIVER_PRICE_OFFER = "call_driver_price_offer";
    public static final String PUSH_MSG_TYPE_CALL_DRIVER_POSITION_CHANGED = "call_driver_position_changed";
    public static final String PUSH_MSG_TYPE_CALL_DRIVER_DRIVING = "call_driver_driving";
    public static final String PUSH_MSG_TYPE_CALL_DRIVER_ARRIVED_TARGET = "call_driver_arrived_target";
    public static final String PUSH_MSG_TYPE_CALL_DRIVER_CANCELED = "call_driver_canceled";
    public static final String PUSH_MSG_TYPE_NOTICE = "notice";
    public static final String PUSH_MSG_TYPE_COUPON = "coupon";
    public static final String PUSH_MSG_TYPE_POINT = "point";

    // push notification id
    public static final int NOTIFICATION_ID_CRACKDOWN_WARNING = 1;
    public static final int NOTIFICATION_ID_CALL_DRIVER_ASSIGNED = 2;
    public static final int NOTIFICATION_ID_CALL_DRIVER_NOT_ASSIGNED = 3;
    public static final int NOTIFICATION_ID_CALL_DRIVER_PRICE_OFFER = 4;

    // call driver default
    public static final int DEFAULT_PRICE = 20000;
    public static final int DEFAULT_EXTRA_PRICE = 2000;
}

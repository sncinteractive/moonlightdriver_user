package kr.moonlightdriver.user.viewmodel.crackdown;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.viewmodel.common.CoordinatesData;

/**
 * Created by youngmin on 2016-08-12.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DriverPointViewModel {
	@JsonProperty("driverId")
	private String mDriverId;

	@JsonProperty("phone")
	private String mPhoneNumber;

	@JsonProperty("date")
	private long mDate;

	@JsonProperty("gcmId")
	private String mGcmId;

	@JsonProperty("location")
	private CoordinatesData mLocation;

	public DriverPointViewModel() {}

	public DriverPointViewModel(long mDate, String mDriverId, String mGcmId, CoordinatesData mLocation, String mPhoneNumber) {
		this.mDate = mDate;
		this.mDriverId = mDriverId;
		this.mGcmId = mGcmId;
		this.mLocation = mLocation;
		this.mPhoneNumber = mPhoneNumber;
	}

	public long getmDate() {
		return mDate;
	}

	public void setmDate(long mDate) {
		this.mDate = mDate;
	}

	public String getmDriverId() {
		return mDriverId;
	}

	public void setmDriverId(String mDriverId) {
		this.mDriverId = mDriverId;
	}

	public String getmGcmId() {
		return mGcmId;
	}

	public void setmGcmId(String mGcmId) {
		this.mGcmId = mGcmId;
	}

	public CoordinatesData getmLocation() {
		return mLocation;
	}

	public void setmLocation(CoordinatesData mLocation) {
		this.mLocation = mLocation;
	}

	public String getmPhoneNumber() {
		return mPhoneNumber;
	}

	public void setmPhoneNumber(String mPhoneNumber) {
		this.mPhoneNumber = mPhoneNumber;
	}
}

package kr.moonlightdriver.user.viewmodel.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by youngmin on 2016-08-12.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CoordinatesData {
	@JsonProperty("type")
	public String mType;

	@JsonProperty("coordinates")
	public ArrayList<Double> mCoordinates;

	public CoordinatesData() { }

	public CoordinatesData(ArrayList<Double> mCoordinates, String mType) {
		this.mCoordinates = mCoordinates;
		this.mType = mType;
	}

	public ArrayList<Double> getmCoordinates() {
		return mCoordinates;
	}

	public void setmCoordinates(ArrayList<Double> mCoordinates) {
		this.mCoordinates = mCoordinates;
	}

	public String getmType() {
		return mType;
	}

	public void setmType(String mType) {
		this.mType = mType;
	}
}
